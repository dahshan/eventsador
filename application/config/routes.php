<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Admin';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// For Admin Auth
$route['admin'] = "admin";
$route['check_admin_login'] = "admin/check_login";
$route['admin_aboutus'] = "admin/aboutus";
$route['check_admin_forgot_password'] = "admin/check_forgot_password";
// $route['admin_forgot_password'] = "admin/forgot_password";
$route['admin_logout'] = "admin/admin_logout";
$route['admin_dashboard'] = "admin/dashboard";
$route['admin_edit_profile'] = "admin/admin_edit_profile";
$route['admin_change_password'] = "admin/admin_change_password";
// $route['change_status/(:any)/(:any)'] = "admin/change_status/$1/$2";

// For Admin Users
$route['admin_user'] = "user/admin/index";
$route['admin_add_user'] = "user/admin/form";
$route['admin_update_user/(:any)'] = "user/admin/form/$1";
$route['admin_status_user/(:any)'] = "user/admin/status/$1";
$route['admin_resend_email_user/(:any)'] = "user/admin/resend_email/$1";
$route['admin_delete_user/(:any)'] = "user/admin/delete/$1";
$route['admin_event_user/(:any)'] = "user/admin/users_by_event/$1";

// For Role
$route['admin_role'] = "role/admin/index";
$route['admin_add_role'] = "role/admin/form";
$route['admin_update_role/(:any)'] = "role/admin/form/$1";
$route['admin_status_role/(:any)'] = "role/admin/status/$1";
$route['admin_delete_role/(:any)'] = "role/admin/delete/$1";

// For Admin Organizers
$route['admin_organizer'] = "organizer/admin/index";
$route['admin_add_organizer'] = "organizer/admin/form";
$route['admin_update_organizer/(:any)'] = "organizer/admin/form/$1";
$route['admin_status_organizer/(:any)'] = "organizer/admin/status/$1";
$route['admin_delete_organizer/(:any)'] = "organizer/admin/delete/$1";
$route['admin_event_organizer/(:any)'] = "organizer/admin/organizers_by_event/$1";

// For Admin Event
$route['admin_event'] = "event/admin/index";
$route['admin_event_rearrange'] = "event/admin/rearrange";
$route['admin_add_event'] = "event/admin/form";
$route['admin_update_event/(:any)'] = "event/admin/form/$1";
$route['admin_status_event/(:any)'] = "event/admin/status/$1";
$route['admin_delete_event/(:any)'] = "event/admin/delete/$1";

// For Admin Query
$route['admin_query'] = "query/admin/index";
$route['admin_view_query/(:any)'] = "query/admin/view/$1";

// For Admin Sponsore
$route['admin_sponsor'] = "sponsor/admin/index";
$route['admin_sponsor_rearrange'] = "sponsor/admin/rearrange";
$route['admin_sponsor_search'] = "sponsor/admin/search";
$route['admin_add_sponsor'] = "sponsor/admin/form";
$route['admin_update_sponsor/(:any)'] = "sponsor/admin/form/$1";
$route['admin_status_sponsor/(:any)'] = "sponsor/admin/status/$1";
$route['admin_delete_sponsor/(:any)'] = "sponsor/admin/delete/$1";
$route['admin_event_sponsor/(:any)'] = "sponsor/admin/sponsors_by_event/$1";

// For Admin Poll
$route['admin_poll'] 				  = "poll/admin/index";
$route['admin_poll_search'] = "poll/admin/search";
$route['admin_add_poll'] 		      = "poll/admin/form";
$route['admin_update_poll/(:any)'] 	  = "poll/admin/form/$1";
$route['admin_status_poll/(:any)'] 	  = "poll/admin/status/$1";
$route['admin_delete_poll/(:any)'] 	  = "poll/admin/delete/$1";


// For Admin Survey
$route['admin_survey'] 				  = "survey/admin/index";
$route['admin_survey_search'] = "survey/admin/search";
$route['admin_add_survey'] 		      = "survey/admin/form";
$route['admin_update_survey/(:any)']  = "survey/admin/form/$1";
$route['admin_status_survey/(:any)']  = "survey/admin/status/$1";
$route['admin_delete_survey/(:any)']  = "survey/admin/delete/$1";
$route['admin_survey_result/(:any)']  = "survey/admin/view/$1";


// For Admin Media
$route['admin_media'] = "media/admin/index";
$route['admin_add_media'] = "media/admin/form";
$route['admin_add_media_image/(:any)'] = "media/admin/formimage/$1";
$route['admin_add_media_video/(:any)'] = "media/admin/formvideo/$1";
$route['admin_add_media_document/(:any)'] = "media/admin/formdocument/$1";
$route['admin_view_media_image/(:any)'] = "media/admin/listviewimage/$1";
$route['admin_view_media_image_pending/(:any)'] = "media/admin/listviewimage_pending/$1";
$route['admin_view_media_video/(:any)'] = "media/admin/listviewvideo/$1";
$route['admin_view_media_video_pending/(:any)'] = "media/admin/listviewvideo_pending/$1";
$route['admin_view_media_document/(:any)'] = "media/admin/listviewdocument/$1";
$route['admin_update_media/(:any)'] = "media/admin/form/$1";
$route['admin_status_media/(:any)'] = "media/admin/status/$1";
$route['admin_delete_media/(:any)'] = "media/admin/delete/$1";
$route['admin_delete_media/(:any)/(:any)'] = "media/admin/delete/$1/$2"; // when pending
$route['admin_delete_media_file/(:any)'] = "media/admin/delete_file/$1";
$route['admin_update_media_document/(:any)/(:any)'] = "media/admin/formdocument/$1/$2";


$route['admin_media_approve_status/(:any)/(:any)'] = "media/admin/media_approve_status/$1/$2";
$route['admin_media_approve_status/(:any)/(:any)/(:any)'] = "media/admin/media_approve_status/$1/$2/$3";            // When on pending
$route['admin_media_video_approve_status/(:any)/(:any)'] = "media/admin/media_video_approve_status/$1/$2";
$route['admin_media_video_approve_status/(:any)/(:any)/(:any)'] = "media/admin/media_video_approve_status/$1/$2/$3";// When on pending


// For Admin Agenda
$route['admin_agenda'] = "agenda/admin/index";
$route['admin_add_agenda/(:any)'] = "agenda/admin/form/$1";
$route['admin_update_agenda/(:any)/(:any)'] = "agenda/admin/form/$1/$2";
$route['admin_status_agenda/(:any)'] = "agenda/admin/status/$1";
$route['admin_delete_agenda/(:any)'] = "agenda/admin/delete/$1";
$route['admin_view_agenda/(:any)'] = "agenda/admin/listagenda/$1";

// For Company
$route['admin_company'] = "company/admin/index";
$route['admin_add_company'] = "company/admin/form";
$route['admin_update_company/(:any)'] = "company/admin/form/$1";
$route['admin_status_company/(:any)'] = "company/admin/status/$1";
$route['admin_delete_company/(:any)'] = "company/admin/delete/$1";

// For Exhhibitor
$route['admin_exhibitor'] = "exhibitor/admin/index";
$route['admin_exhibitor_search'] = "exhibitor/admin/search";
$route['admin_add_exhibitor'] = "exhibitor/admin/form";
$route['admin_update_exhibitor/(:any)'] = "exhibitor/admin/form/$1";
$route['admin_status_exhibitor/(:any)'] = "exhibitor/admin/status/$1";
$route['admin_delete_exhibitor/(:any)'] = "exhibitor/admin/delete/$1";
$route['admin_event_exhibitor/(:any)'] = "exhibitor/admin/exihibitors_by_event/$1";

// For Logistics
// $route['admin_logistics'] = "logistics/admin/index";
$route['admin_add_logistics'] = "logistics/admin/form";
$route['admin_logistics'] = "logistics/admin/form";
$route['admin_update_logistics/(:any)'] = "logistics/admin/form/$1";

// For Host
$route['admin_host'] = "host/admin/index";
$route['admin_host_search'] = "host/admin/search";
$route['admin_host_rearrange'] = "host/admin/rearrange";
$route['admin_add_host'] = "host/admin/form";
$route['admin_update_host/(:any)'] = "host/admin/form/$1";
$route['admin_status_host/(:any)'] = "host/admin/status/$1";
$route['admin_delete_host/(:any)'] = "host/admin/delete/$1";
$route['admin_event_host/(:any)'] = "host/admin/hosts_by_event/$1";

// For Speaker
$route['admin_speaker'] = "speaker/admin/index";
$route['admin_speaker_search'] = "speaker/admin/search";
$route['admin_add_speaker'] = "speaker/admin/form";
$route['admin_update_speaker/(:any)'] = "speaker/admin/form/$1";
$route['admin_status_speaker/(:any)'] = "speaker/admin/status/$1";
$route['admin_delete_speaker/(:any)'] = "speaker/admin/delete/$1";
$route['admin_event_speaker/(:any)'] = "speaker/admin/speakers_by_event/$1";

// For Attendees
$route['admin_attendees'] = "attendees/admin/index";
$route['admin_view_request/(:any)'] = "attendees/admin/listrequest/$1";
$route['admin_view_attendees/(:any)'] = "attendees/admin/listattendees/$1";
$route['admin_view_attendees/(:any)/(:any)'] = "attendees/admin/listattendees/$1/$2";
$route['admin_email_attendees/(:any)'] = "attendees/admin/emailattendees/$1";
$route['admin_approve_request/(:any)/(:any)/(:any)'] = "attendees/admin/approve_request/$1/$2/$3";
$route['admin_print_badge/(:any)/(:any)'] = "attendees/admin/print_badge/$1/$2";
$route['admin_checkedin_status/(:any)/(:any)'] = "attendees/admin/checked_in_status/$1/$2";
$route['admin_import/(:any)'] = "attendees/admin/import_attendee/$1";
$route['admin_import_process'] = "attendees/admin/csvimport";
$route['admin_qrcode'] = "attendees/admin/qr_code";

$route['admin_delete_attee/(:any)/(:any)'] = "attendees/admin/deleteAttee/$1/$2";

// For Bulletin
$route['admin_bulletin'] = "bulletin/admin/index";
$route['admin_bulletin_topic_list/(:any)'] = "bulletin/admin/bulletin_topic_list/$1";
$route['admin_bulletin_list/(:any)/(:any)'] = "bulletin/admin/bulletin_list/$1/$2";
$route['admin_bulletin_reply/(:any)'] = "bulletin/admin/bulletin_reply/$1";
$route['admin_update-bulletin_reply/(:any)'] = "bulletin/admin/updatebulletin/$1";
$route['admin_view-bulletin_reply/(:any)'] = "bulletin/admin/viewbulletin/$1";
$route['admin_add_bulletin_topic/(:any)'] = "bulletin/admin/form/$1";
$route['admin_delete_bulletin_topic/(:any)'] = "bulletin/admin/delete_topic/$1";
$route['admin_update_bulletin_topic/(:any)/(:any)'] = "bulletin/admin/form/$1/$2";

$route['admin_notification'] = "notification/admin/index";
$route['admin_notification_search'] = "notification/admin/search";
$route['admin_add_notification'] = "notification/admin/form";
$route['admin_delete_notification/(:any)'] = "notification/admin/delete/$1";

// For Admin Accomodation
$route['admin_accomodation'] = "accomodation/admin/index";
$route['admin_add_accomodation/(:any)'] = "accomodation/admin/form/$1";
$route['admin_update_accomodation/(:any)/(:any)'] = "accomodation/admin/form/$1/$2";
$route['admin_status_accomodation/(:any)'] = "accomodation/admin/status/$1";
$route['admin_delete_accomodation/(:any)'] = "accomodation/admin/delete/$1";
$route['admin_view_accomodation/(:any)'] = "accomodation/admin/listaccomodation/$1";

$route['admin_save_currency'] = "accomodation/admin/save_currency";



// For Admin Transportation
$route['admin_transportation'] = "transportation/admin/index";
$route['admin_add_transportation/(:any)'] = "transportation/admin/form/$1";
$route['admin_update_transportation/(:any)/(:any)'] = "transportation/admin/form/$1/$2";
$route['admin_status_transportation/(:any)'] = "transportation/admin/status/$1";
$route['admin_delete_transportation/(:any)'] = "transportation/admin/delete/$1";
$route['admin_view_transportation/(:any)'] = "transportation/admin/listtransportation/$1";

// For Admin Local Attraction
$route['admin_local_attraction'] = "local_attraction/admin/index";
$route['admin_add_local_attraction/(:any)'] = "local_attraction/admin/form/$1";
$route['admin_update_local_attraction/(:any)/(:any)'] = "local_attraction/admin/form/$1/$2";
$route['admin_status_local_attraction/(:any)'] = "local_attraction/admin/status/$1";
$route['admin_delete_local_attraction/(:any)'] = "local_attraction/admin/delete/$1";
$route['admin_view_local_attraction/(:any)'] = "local_attraction/admin/listlocal_attraction/$1";
$route['admin_delete_attraction_image/(:any)/(:any)/(:any)'] = "local_attraction/admin/delete_file/$1/$2/$3";

// For Admin Wifi Access
$route['admin_wifi_access'] = "wifi_access/admin/index";
$route['admin_add_wifi_access/(:any)'] = "wifi_access/admin/form/$1";
$route['admin_update_wifi_access/(:any)/(:any)'] = "wifi_access/admin/form/$1/$2";
$route['admin_status_wifi_access/(:any)'] = "wifi_access/admin/status/$1";
$route['admin_delete_wifi_access/(:any)'] = "wifi_access/admin/delete/$1";
$route['admin_view_wifi_access/(:any)'] = "wifi_access/admin/listwifi_access/$1";

// For Admin Floor Map
$route['admin_floor_map'] = "floor_map/admin/index";
$route['admin_add_floor_map/(:any)'] = "floor_map/admin/form/$1";
$route['admin_update_floor_map/(:any)/(:any)'] = "floor_map/admin/form/$1/$2";
$route['admin_status_floor_map/(:any)'] = "floor_map/admin/status/$1";
$route['admin_delete_floor_map/(:any)'] = "floor_map/admin/delete/$1";
$route['admin_view_floor_map/(:any)'] = "floor_map/admin/listfloor_map/$1";

// For Admin Announcement
$route['admin_announcement'] = "announcement/admin/index";
$route['admin_add_announcement/(:any)'] = "announcement/admin/form/$1";
$route['admin_update_announcement/(:any)/(:any)'] = "announcement/admin/form/$1/$2";
$route['admin_status_announcement/(:any)'] = "announcement/admin/status/$1";
$route['admin_delete_announcement/(:any)'] = "announcement/admin/delete/$1";
$route['admin_view_announcement/(:any)'] = "announcement/admin/listannouncement/$1";

// For Admin Feedback
$route['admin_feedback'] = "feedback/admin/index";
$route['admin_reply_feedback/(:any)'] = "feedback/admin/reply/$1";
$route['admin_delete_feedback/(:any)'] = "feedback/admin/delete/$1";

// For Admin Lead
$route['admin_lead'] = "lead/admin/index";
$route['admin_add_lead/(:any)'] = "lead/admin/form/$1";
$route['admin_update_lead/(:any)/(:any)'] = "lead/admin/form/$1/$2";
$route['admin_status_lead/(:any)'] = "lead/admin/status/$1";
$route['admin_delete_lead/(:any)'] = "lead/admin/delete/$1";
$route['admin_view_lead/(:any)'] = "lead/admin/listlead/$1";
$route['export_leadlist_as_csv/(:any)'] = "lead/admin/export_as_csv/$1";


// For FAQ
$route['admin_faq'] = "faq/admin/index";
$route['admin_add_faq'] = "faq/admin/form";
$route['admin_update_faq/(:any)'] = "faq/admin/form/$1";
$route['admin_status_faq/(:any)'] = "faq/admin/status/$1";
$route['admin_delete_faq/(:any)'] = "faq/admin/delete/$1";
$route['admin_delete_faq_image/(:any)'] = "faq/admin/delete_file/$1";

// For Point
$route['admin_point'] = "point/admin/index";
$route['admin_point_search'] = "point/admin/search";
$route['admin_point_delete/(:any)'] = "point/admin/delete/$1";
// For Appuser
$route['admin_appuser'] = "appuser/admin/index";
$route['admin_delete_appuser/(:any)'] = "appuser/admin/delete/$1";


// For Role
$route['admin_report'] = "report/admin/index";
$route['admin_report_search'] = "report/admin/report_search";

// For Organizer Dashboard
$route['admin_organizer_dashboard'] = "organizer_dashboard/admin/index";
$route['admin_organizer_dashboard_search'] = "organizer_dashboard/admin/reportt_search";
$route['admin_organizer_dashboard_attendees'] = "organizer_dashboard/admin/event_attendees";
$route['admin_organizer_dashboard_exhibitors'] = "organizer_dashboard/admin/event_exhibitors";
$route['admin_organizer_dashboard_accommodations'] = "organizer_dashboard/admin/event_accommodations";
$route['admin_organizer_dashboard_transportations'] = "organizer_dashboard/admin/event_transportations";
$route['admin_organizer_dashboard_hosts'] = "organizer_dashboard/admin/event_hosts";
$route['admin_organizer_dashboard_sponsors'] = "organizer_dashboard/admin/event_sponsors";
$route['admin_organizer_dashboard_attractions'] = "organizer_dashboard/admin/event_attractions";

// For Admin Currency
$route['admin_currency'] = "currency/admin/index";
$route['admin_add_currency'] = "currency/admin/form";
$route['admin_update_currency/(:any)'] = "currency/admin/form/$1";
$route['admin_status_currency/(:any)'] = "currency/admin/status/$1";
$route['admin_delete_currency/(:any)'] = "currency/admin/delete/$1";



// For Admin parking_map
$route['admin_parking_map'] = "parking_and_map/admin/index";
$route['admin_add_parking_map/(:any)'] = "parking_and_map/admin/form/$1";
$route['admin_update_parking_map/(:any)/(:any)'] = "parking_and_map/admin/form/$1/$2";
$route['admin_status_parking_map/(:any)'] = "parking_and_map/admin/status/$1";
$route['admin_delete_parking_map/(:any)'] = "parking_and_map/admin/delete/$1";
$route['admin_view_parking_map/(:any)'] = "parking_and_map/admin/listparking_map/$1";


// For Admin all Users /* 18.07.18*/
$route['admin_allusers'] = "all_user/admin/index";


// For Exhhibitor /* 05.09.18 */
$route['admin_exhibitor_type'] = "exhibitor_type/admin/index";
$route['admin_add_exhibitor_type'] = "exhibitor_type/admin/form";
$route['admin_delete_exhibitor_type/(:any)'] = "exhibitor_type/admin/delete/$1";
$route['admin_update_exhibitor_type/(:any)'] = "exhibitor_type/admin/form/$1";
$route['admin_status_exhibitor_type/(:any)'] = "exhibitor_type/admin/status/$1";



/* End of file routes.php */
/* Location: ./application/config/routes.php */
