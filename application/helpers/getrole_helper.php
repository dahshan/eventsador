<?php
function getRole(){
  $CI = & get_instance();
  
  $admin_u_type = $CI->session->userdata('admin_role_type');
  $admin_uid = $CI->session->userdata('admin_uid');
  $CI_user_role = $CI->Adminauthmodel->get_row_data('role', ['id' => $admin_u_type]);
  if (isset($CI_user_role->permited_module) && $CI_user_role->permited_module != '') {
      $permited_module = explode(',', $CI_user_role->permited_module);
  } else {
      $permited_module = [];
  }
  return $permited_module;
}