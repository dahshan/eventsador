<?php
/**
 *
 * This helper is used for adding the table prefix after table name
 *
 * @author	pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since	Version 0.0.1
 *
 */
function tablename($tablename) {
    $CI = & get_instance();
    $CI->load->database();
    return $CI->db->dbprefix($tablename);
}

/* End of file tblprfx_helper.php */
/* Location: ./application/helpers/tblprfx_helper.php */