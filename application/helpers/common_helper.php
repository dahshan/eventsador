<?php

function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function assigned_events($id,$field)
{
    $CI = & get_instance();
    $CI->load->model('event/admin/eventmodel');
    return $CI->eventmodel->checked_assigned_events($id,$field);
}

function notification($fcm,$text,$title)
{
    $CI = & get_instance();
  $msg=$title;
  $msgg=$text;
  $CI->load->library('gcmpushmessage','AAAAQQY2SPA:APA91bHXm3KVLxckAVs2-hYNfbif6fxdN-eXTEHNUnBG3efje1vvv2aYNTJs2usgZNmDFeHy5EH0wwynQ4vtG6t8DYb-MuDfJC4KOd_Y1kgOowCJOh1pe9ZF8avPpI80oCM_ffWGl6Mu');
  // $gcmregtoken = "ckpnfYyp5jc:APA91bFd8ZVoSHFd9URKne44NqgOMOZZk6Ix1aBTmfY8Y2bUloY3Sgl4tHjJ5uJgoLmUUuMVCBSC6vggK4ZbhtoFPfjLFn1lOmuISGwIETV-hw2UT_rKw57em5-XLGTdsLbP9GvNBBly";
  $gcmregtoken = $fcm;
  $CI->gcmpushmessage->setDevices($gcmregtoken);
  $r = $CI->gcmpushmessage->send($text, array('title' => $title,'body' => "Eventsador"));
  //print_r($r);die;
  return $r;
  // $deviceid="4840472bdd7e1bb69ed2e53b2f485f4f2e78938aae69214a21764bd6b8e07d83";
  // $message="hello!This is Rima from YPO";
  // push_apns($deviceid,$message,"Testing"); 
}

function get_all_user_of_event($event_data, $my_id, $organizer = true){
    $ans = [];
    // category - All, Attendees, Speakers, Exhibitors, Organizers, Sponsors, Hosts
    $all = $attendees = $speakers = $exhibitors = $organizers = $sponsors = $hosts = [];
    $array_group = [];
    $ans['speakers']    = $speakers  = $event_data->speakers  ? array_filter(explode(',', $event_data->speakers))  : [];    $array_group[] = $speakers;
    $ans['exhibitors']  = $exhibitors= $event_data->exhibitors? array_filter(explode(',', $event_data->exhibitors)): [];    $array_group[] = $exhibitors;
    //if ($organizer) $ans['organizers']  = $organizers= $event_data->users? array_filter(explode(',', $event_data->organizers)): [];    $array_group[] = $organizers;
    $ans['organizers']  = $organizers= $event_data->users? array_filter(explode(',', $event_data->users)): [];    $array_group[] = $organizers;
    $ans['sponsors']    = $sponsors  = $event_data->sponsors  ? array_filter(explode(',', $event_data->sponsors))  : [];    $array_group[] = $sponsors;
    $ans['hosts']       = $hosts     = $event_data->hosts     ? array_filter(explode(',', $event_data->hosts))     : [];    $array_group[] = $hosts;
    //$ans['attendees']   = $attendees = $event_data->attendees ? array_filter(explode(',', $event_data->attendees)) : [];    $array_group[] = $attendees;
    $ans['attendees']   = $attendees = get_attendees_list($array_group, -1, $event_data->attendees);    $array_group[] = $attendees;
    $ans['all']         = $all = get_one_array_from_array_group($array_group, -1);
    $ans = ['all'=> $all, 'organizers'=> $organizers,   'speakers'=> $speakers, 'exhibitors'=> $exhibitors, 'sponsors'=> $sponsors, 'hosts'=> $hosts, 'attendees'=> $attendees];
    return $ans;
}

function get_one_array_from_array_group($group, $my_id){
    $all = [];
    if (count($group) > 0){
        foreach ($group as $arr){
            if (count($arr) > 0){
                foreach ($arr as $item){
                    if ((intval($item) > 0) && (!in_array($item, $all))) $all[] = $item;//($my_id != $item) && 
                }
            }
        }
    }
    return $all;
}

function get_attendees_list($group, $my_id, $att_str){
    $attendees = [];
    if (strlen($att_str) > 0 && is_array(explode(',', $att_str))){ 
        $att = explode(',', $att_str);//echo json_encode($group); exit;
        foreach ($att as $one){
            $dup = false;
            foreach ($group as $one_arr){
                if ((intval($one) > 0) && (!in_array($one, $attendees)) && (!in_array($one, $one_arr))){//($my_id != $one) && 
                    
                }else{
                    $dup = true; break;
                }
            }
            if (!$dup) $attendees[] = $one;
        }
    }
    return $attendees;
}

// function random_string($length) {
//     $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//     $charactersLength = strlen($characters);
//     $randomString = '';
//     for ($i = 0; $i < $length; $i++) {
//         $randomString .= $characters[rand(0, $charactersLength - 1)];
//     }
//     return $randomString;
// }

function filter_str($str){
    if (strlen($str) > 0){
        return trim(strtolower($str));
    }else{
        return "";
    }
}

function valid_data($data){
    return $data ? $data : "";
}
