<?php
/**
 *
 * This helper is used for Login Authentication
 *
 * @author : rE@cT <sketch.dev23@gmail.com>
 * @param : none
 * @return : Success or Failure of Admin Authentication
 * @since Version 1.0.0
 *
 */
function admin_authenticate() {
  $CI = & get_instance(); 
  $admin_uid = $CI->session->userdata('admin_uid');

  // echo "<pre>";
  // print_r($CI->session->userdata());
  // exit;

  if(empty($admin_uid)) {
    $CI->session->set_flashdata('errormessage', 'Invalid Access in Admin Panel');
    $redirect = base_url('admin');
    redirect($redirect);
  }
}

/**
 *
 * This helper is used for User Login Authentication
 *
 * @author : rE@cT <sketch.dev06@gmail.com>
 * @param : none
 * @return : Success or Failure of User Authentication
 * @since Version 1.0.0
 *
 */
function user_authenticate() {
  $CI = & get_instance(); 
  $admin_uid = $CI->session->userdata('userid');

  if(empty($admin_uid)) {
    $CI->session->set_flashdata('errormessage', 'Invalid Access in Front End');
    $redirect = base_url('front/login');
    redirect($redirect);
  }
}

function word_limit($string) 
{
  if(strlen($string) > 50)
  {
    return substr($string,0,50)."...";
  }
  else
  {
    return $string;
  }
}



/* End of file authentication_helper.php */
/* Location: ./application/helpers/authentication_helper.php */