<?php

/**
 * Auth Model Class. Handles all the datatypes and methodes required for Authentication Purpose
 *
 * @author  rE@cT <sketch.dev22@gmail.com>
 * @access   public
 * @link  http://example.com
 * @since Version 1.0.0
 */
class TemplateModel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * get content from about_us table
     * 
     * @access public
     * @param none
     */
    public function getAllCMS() {
        return $this->db->get('cms')->result();
        
    }

    /**
     * save the cms details to cms table
     * 
     * @access public
     * @param array containing page_title, page_content, banner(not mandatory)
     */
    public function saveCMS($data) {
        
        $response = $this->db->insert('cms', $data);
        
        if ($response) {
            $this->session->set_flashdata('template', '
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> CMS Successfully Added
                            </div>
                        ');
            return true;
        }else {
            $this->session->set_flashdata('template', '
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Error!</strong> Database Error
                            </div>
                        ');
            return false;
        }
    }
    
    
    /**
     * edit a cms details
     * 
     * @access public
     * @param cms id
     */
    public function getCMSById($id) {
        $this->db->where('id', $id);
        return $this->db->get('cms')->row();
    }
    
    /**
     * update a cms after edit
     * 
     * @access public
     * @param array of data containing cms details, cms id
     */
    public function editCMS($data, $id) {
        $cms = $this->getCMSById($id);
        
        if( $data['banner'] && $cms->banner != '' ){
            unlink(FCPATH.'cms/banner/'.$cms->banner);
        }
        
        $this->db->where('id', $id);
        $response = $this->db->update('cms', $data);        
        
        if ($response) {
            $this->session->set_flashdata('template', '
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> CMS Successfully Edited
                            </div>
                        ');
            return true;
        }else {
            $this->session->set_flashdata('template', '
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Error!</strong> Database Error
                            </div>
                        ');
            return false;
        }
    }
    
    public function changeStatus($id, $status) {
        $data = array();
        $data['is_active'] = ($status == 1) ? 0 : 1;
        $this->db->where('id', $id);
        $response = $this->db->update('cms', $data);
        
    }
    
    public function getAllUsers() {
        return $this->db->get('user')->result();
        
    }
    
    
    

}

/* End of file authmodel.php */
/* Location: ./application/models/front/authmodel.php */