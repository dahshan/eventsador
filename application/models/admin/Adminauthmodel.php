<?php

/**
 * Auth Model Class. Handles all the datatypes and methodes required for Authentication Purpose
 *
 * @author	pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since	Version 0.0.1
 */
class Adminauthmodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for the log-in functionality of an admin
     *
     * <p>Description</p>
     *
     * <p>This function uses the Global Array POST, fetches the variables containing the credential of an admin
     * and validates them against the same [Table: ets_user]. If validation is a success,
     * the admin gets redirected to the dashboard and the first hand details gets saved into session</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function admin_login() {
        $email_id = $this->input->post('username');
        $password = md5($this->input->post('passwd'));
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('emailid', addslashes(trim($email_id)));
        $this->db->where('password', $password);
        $auth_query = $this->db->get();

       // echo $this->db->last_query(); exit;
        $auth_result = $auth_query->row();

        // echo "<pre>";
        // print_r($auth_result);
        // exit;

        if (!empty($auth_result)) {
            
            
            // Check the User Role
            if($this->input->post('user_type')=="admin")
            {
                if($auth_result->role_id!=1)
                {
                    $this->session->set_flashdata('errormessage', 'Invalid User Type');
                    redirect(base_url('admin'));
                }
            }
            elseif($this->input->post('user_type')=="org")
            {
                if($auth_result->role_id!=2)
                {
                    $this->session->set_flashdata('errormessage', 'Invalid User Type');
                    redirect(base_url('admin'));
                }
            }
            else
            {
                if($auth_result->role_id==1 || $auth_result->role_id==2)
                {
                    $this->session->set_flashdata('errormessage', 'Invalid User Type');
                    redirect(base_url('admin'));
                }
            }
            $admin_id = $auth_result->id;

           
            if(!empty($this->input->post('remember'))) {
                setcookie ("member_login",$this->input->post('username'),time()+ (10 * 365 * 24 * 60 * 60));
                setcookie ("member_password",$this->input->post('passwd'),time()+ (10 * 365 * 24 * 60 * 60));
            } else {
                if(isset($_COOKIE["member_login"])) {
                    setcookie ("member_login","");
                }
                if(isset($_COOKIE["member_password"])) {
                    setcookie ("member_password","");
                }
            }


            $user_agent = $_SERVER['HTTP_USER_AGENT'];
            $ip_address = $_SERVER['REMOTE_ADDR'];
            $online = 1;
            $modified_date = date('Y-m-d H:i:s');

            $update_value = array('ip_address' => addslashes($ip_address),
                'user_agent' => addslashes($user_agent),
                'modified_date' => $modified_date
            );

            $this->db->where('id', $admin_id);

            if ($this->db->update('user', $update_value)) {
                $this->db->select('*');
                $this->db->from(tablename('user'));
                $this->db->where('emailid', addslashes(trim($email_id)));
                $this->db->where('password', $password);

                $admin_query = $this->db->get();
                $admin_result = $admin_query->row();

                $admin_id = $admin_result->id;
                $role_id = $admin_result->role_id;
                $login_date_time = date('Y-m-d H:i:s');

                $log_data = array('user_id' => $admin_id,
                    'ip_address' => $ip_address,
                    'user_agent' => $user_agent,
                    'login_datetime' => $login_date_time
                );

                $this->db->insert(tablename('logs'), $log_data);
                $log_id = $this->db->insert_id();

                $admin_arr = (array) $admin_result;
                $js_admin_array = json_encode($admin_arr);

                $this->session->set_userdata('admin_uid', $admin_id);
                $this->session->set_userdata('admin_role_type', $role_id);
                $this->session->set_userdata('admin_org_id', $admin_result->organizer_id);
                $this->session->set_userdata('admin_detail', $js_admin_array);
                $this->session->set_userdata('admin_logid', $log_id);
                

                $this->session->set_flashdata('successmessage', 'You have logged in successfully');
                if ($auth_result->role_id == 1) {
                    redirect(base_url('admin_dashboard'));
                } else {
                    redirect(base_url('admin_organizer_dashboard'));
                }
                
            }

            $this->session->set_flashdata('errormessage', 'Invalid Credentials');
            redirect(base_url('admin'));
        } else {
            $this->session->set_flashdata('errormessage', 'Invalid Credentials');
            redirect(base_url('admin'));
        }
    }

    /**
     * Used for the log-out functionality of an admin
     *
     * <p>Description</p>
     *
     * <p>This function logs out the admin. The admin details gets destructed from the session</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function admin_logout() {
        $admin_uid = $this->session->userdata('admin_uid');
        $admin_logid = $this->session->userdata('admin_logid');
        $modified_date = date('Y-m-d H:i:s');

        $update_logs = array('logout_datetime' => $modified_date);
        $this->db->where('id', $admin_logid);
        $this->db->update('logs', $update_logs);

        $update_admin = array('ip_address' => '',
            'user_agent' => '',
            'last_login' => $modified_date,
            'modified_date' => $modified_date
        );

        $this->db->where('id', $admin_uid);

        if ($this->db->update('user', $update_admin)) {
            $this->session->set_userdata('adminuid', '');
            $this->session->set_userdata('admin_detail', '');
            $this->session->set_userdata('admin_logid', '');

            $this->session->set_flashdata('successmessage', 'You have logged out successfully');
            redirect(base_url('admin'));
        } else {
            return "";
        }
    }

    /**
     * Used for the reset password functionality of Admin
     *
     * <p>Description</p>
     *
     * <p>This function uses the Global Array POST, fetches the variables containing the email of admin
     * and validates them against the same [Table: pb_admin]. If validation is a success,
     * the password gets reset and the user receives a mail containing the same.</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function admin_forgot_password() {
        $emailid = $this->input->post('emailid');

        if (!empty($emailid)) {
            $this->db->select('*');
            $this->db->from(tablename('user'));
            $this->db->where('emailid', $emailid);

            $query = $this->db->get();
            $admin_result = $query->row();


            if (!empty($admin_result)) {

                $new_password = "BI_" . time();
                $new_encrypted_password = md5($new_password);

                $update_data = array(
                    'password' => $new_encrypted_password
                );

                $this->db->where('id', $admin_result->id);
                $this->db->update(tablename('user'), $update_data);

                //echo $this->db->last_query(); exit;
                /**
                 * Sending Reset Password to User's Email Id
                 */
                
                $subject = "Password has been Reset";
                $message = 'Hello ' . $admin_result->name . ',<br/><br/><p>Your password has been reset. The new password is <b>' . $new_password . '</b>. Please use this next time you log in. Make sure to change yor password again to avoid any potential mishap. <b>For security reasons, do not share your password with anyone</b> </p>';
                $message .= '<p>Thank you,</p>';
                $message .= '<p>Eventsador Admin</p>';

                $mail_data = [
                     'name' => $admin_result->name,
                     'body' => $message,
                ];
                $this->load->helper('email');
                send_email($emailid, $subject, $mail_data);

                $this->session->set_flashdata('successmessage', '<div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success!</strong> Check your email for new password.
                        </div>'
                );

                return $admin_result->id;
            } else {
                $this->session->set_flashdata('errormessage', "<div class='alert alert-danger' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Error!</strong> Incorrect Email Id or Password.</div>");

                redirect(base_url('admin'));
            }
        } else {
            $this->session->set_flashdata('errormessage', "<div class='alert alert-danger' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Error!</strong> Incorrect Email Id or Password.</div>");

            redirect(base_url('admin'));
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

    /**
     * Used for updating one row in a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches update one row in table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function update_row_data($table, $data, $where) {
        $query = $this->db->update(tablename($table), $data, $where);
        return true;
    }
/**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1", $arrange=null) {
        if ($arrange) {
            $query = $this->db->order_by('arrange', 'DESC')->get_where(tablename($table), $where);
        } else {
            $query = $this->db->get_where(tablename($table), $where);
        }
        
        
        return $query->result();
    }
}

/* End of file authmodel.php */
/* Location: ./application/models/front/authmodel.php */
