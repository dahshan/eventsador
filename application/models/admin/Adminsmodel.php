<?php
class adminsmodel extends CI_Model
{
    public function __construct()
	{
		
        	parent::__construct();
          	$this->load->database();
          	$this->load->library("session");
            $this->load->helper('string');
	}

	public function admininfo() 
	{
        $sql = "select * from ".tablename('admins')." where id='1'";
        $query = $this->db->query($sql);
        $result = $query->row();
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    public function siteinfo() 
	{
        $sql = "select * from ".tablename('site_settings')." where id='1'";
        $query = $this->db->query($sql);
        $result = $query->row();
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }
    
    public function updateadmin($fname,$lname,$email,$phone,$profilepic,$username)
	{
		$admin_uid=$this->session->userdata('admin_uid');
		if(!empty($profilepic))
		{
			$profilepic=json_decode($profilepic);
			$this->load->library('upload');
			$filename=$profilepic->profile_pic->name;
			$imarr=explode(".",$filename);
			$ext=end($imarr);
			$_FILES['profile_pic']['name']=$profilepic->profile_pic->name;
			$_FILES['profile_pic']['type']=$profilepic->profile_pic->type;
			$_FILES['profile_pic']['tmp_name']=$profilepic->profile_pic->tmp_name;
			$_FILES['profile_pic']['error']=$profilepic->profile_pic->error;
			$_FILES['profile_pic']['size']=$profilepic->profile_pic->size;
			
			$config = array(
				'file_name' => str_replace(".","",microtime(true)).".".$ext,
				'allowed_types' => 'gif|png|jpg|jpeg',
				'upload_path' => APPPATH.'../assets/uploads',
				'max_size' => 2000
			);
			$this->upload->initialize($config);
			if($_FILES['profile_pic']['error']==0)
			{
				$profilepic=$_FILES['profile_pic']['name'];
				$profilepic=$_FILES['profile_pic']['name']	;
				$destination="./assets/uploads/".$profilepic;
				if (move_uploaded_file($_FILES['profile_pic']['tmp_name'] ,$destination))
				{
					$image=$profilepic;
					$sql1="update ".tablename('admins')." set image='".$image."' where id='1'";
					$query1=$this->db->query($sql1);
					$oldfile=$this->input->post('oldimage');
					$olddestination="./assets/uploads/".$oldfile;
					@unlink($olddestination);
				}
			}
			else
			{
				$image_data = $this->upload->data();
				$upName=$image_data['file_name'];
				$viewdet['status']=1;
		
				$config = array(
				'source_image' => $image_data['full_path'],
				'new_image' => APPPATH.'../assets/uploads/thumbs',
				'maintain_ration' => true,
				'width' => 200,
				'height' => 150
				);
		
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				
				$imagename=$image_data['file_name'];
				
				$sql="update ".tablename('admins')." set image='".$imagename."' where id='".$admin_uid."'";
				$query=$this->db->query($sql);
				
				$adminimage=get_from_session('image');
				
				@unlink(APPPATH.'../assets/uploads/'.$adminimage);
				@unlink(APPPATH.'../assets/uploads/thumbs/'.$adminimage);
			}
		}
		
		$sql="update ".tablename('admins')." set fname='".$fname."',lname='".$lname."',username='".$username."',email='".$email."',phone='".$phone."' where id='".$admin_uid."'";
		$qq=$this->db->query($sql);

		$selsql="select * from ".tablename('admins')." where id='".$admin_uid."'";
		$query=$this->db->query($selsql);
		$admindet=$query->row();
		$admindet=json_encode((array)$admindet);
		$arr=array('error'=>'','success'=>1,'errormsg'=>'');
		$this->session->set_userdata('admin_detail',$admindet);
		return json_encode($arr);
	}

	public function changecontent($arr,$id=NULL)
    {
        if(empty($id))
        {
            $this->db->insert(tablename('aboutus'),$arr);
            return $this->db->insert_id();
        }
        else
        {
            $this->db->where("id",$id);
            $this->db->update(tablename('aboutus'),$arr);
            return "1";
        }
    }

    public function content_get($where)
    {
        return $this->db->where($where)->get(tablename('aboutus'))->row();
    }
}
