<?php
class PushNotification{
    public $apiKey = 'AAAAHTNB1gk:APA91bFToDqDfCaNWZ0v_leZKGHdhJryuBpti-1p3Lnfde730exwxPb75go_xQ2MGX6VUjzuGPilMmHj-PItBEljMgnTjwBGqfoaIIZkQqZ8ygw3pkTdhSQq1YpYdH_1redD2L2u_9NM';
    public $url = 'https://fcm.googleapis.com/fcm/send';
    /**
     * @function: sendNotificationAndroid
     * @comments: send notification to android device
     * @params: @token, $title, $msg, $payload['title', 'message', 'callback', 'type']
     * @returns: boolean
    */
    public function sendNotificationAndroid($token, $title, $msg, $payload = false){
        $fields = array(
            // 'to'		=> $token,
            'registration_ids'  => [$token],
            'data'=> array('title'=> $title, 'body'=> $msg)
        );
        
        if ($payload){
            $fields['data']['payload'] = $payload;
        }
        
        $headers = array(
            'Authorization: key=' . $this->apiKey,
            'Content-Type: application/json'
        );
        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, $this->url );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        return $result;
    }

    /**
     * @function: sendNotificationIos
     * @comments: send notification to ios device
     * @params: $token, $title, $message, $payload['title', 'message', 'callback', 'type']
    */
    public function sendNotificationIos($tokens, $title, $message, $payload = false){
        // 	$message = array(
        //     "body" => $mess,
        //     "message" => $mess,
        //     "title" => $title,
        //     "sound" => 1,
        //     "vibrate" => 1,
        //     "badge" => 1
        // 	);
        
        $notification = array(
            'body'  =>$payload ? $payload['message']: '',
            'title' =>$payload ? $payload['title']: '',
            //'badge' => 1,
            'data'  => $payload
        );
        // $notification = array(
        //     "aps"=>array(
        //         'alert'=>array(
        //             'body'=>$message,
        //             'title'=>$title,
        //             'data'=> 'DATA'
        //         ),
        //         //'badge'=> 0,
        //         'content-available'=> "1",
        //         'voip'=> true
        //     ),
        //     //"notId"=> 1,
        //     'content-available'=> "1"
        // );
        if ($payload) $notification['payload'] = $payload;
        
        $fields = array(
            //'registration_ids' => [$tokens],
            'to' => $tokens,
            'notification' => $notification,
            'priority' => 'high',
        );
        $headers = array(
            'Authorization:key = '. $this->apiKey,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
    
    public function sendNotificationIosTEST($tokens, $title, $message, $payload = false){
        //$tokens = 'dOEry17Ji0E:APA91bH5saM21RzZqgXKCWvR63htU--nWdGhpq0SvOWPLR36Hq3jcqvAMI9at2pFIaumOmgFwqRpjroIatOq9iCjBlbRM16GWwwKf5Hy8xV0rNuB1PEypvPTbPwxc6Tc4-Is8GxREt-i';
        // 	$message = array(
        //     "body" => $mess,
        //     "message" => $mess,
        //     "title" => $title,
        //     "sound" => 1,
        //     "vibrate" => 1,
        //     "badge" => 1
        // 	);
        // $notification = array(
        //     "aps"=>array(
        //         'alert'=>array(
        //             'body'=>$message,
        //             'title'=>$title
        //         ),
        //         'badge'=> 1
        //     )
        // );
        
        $notification = array(
            'body'  =>$message,
            'title' =>$title,
            //'badge' => 0,
            'data'  => $payload
        );
        
        //{"aps":{"alert":"Enter your message","badge":1,"sound":"default"}}
        
        if ($payload) $notification['payload'] = $payload;
        
        $fields = array(
            'to' => $tokens,
            'notification' => $notification,
            'priority' => 'high'
            // 'content_available'=> "1",
            // 'content-available'=> "1",
            // "mutable_content" => true,
        );
        $headers = array(
            'Authorization:key = '. $this->apiKey,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);// return $payload;
        return $result;
    }
    
}