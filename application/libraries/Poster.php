<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Format class
 * Help convert between various formats such as XML, JSON, CSV, etc.
 *
 * @author    Phil Sturgeon, Chris Kacerguis, @softwarespot
 * @license   http://www.dbad-license.org/
 */
 
require 'vendor/autoload.php';
class Poster {
    
    public function get_poster($data){
        $url = !empty($data['url']) ? $data['url'] : 'media_15180778760.mp4';
        $url_arr = explode('.', $url);
		$sec = 1;
		$movie = base_url().'assets/upload/media_files/'.$url;
		$thumbnail = 'assets/upload/media_poster/origin/'.$url_arr[0].'.png';

		$ffmpeg = FFMpeg\FFMpeg::create();
		$video = $ffmpeg->open($movie);
		$frame = $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds($sec));
		$frame->save($thumbnail);
		return $thumbnail;
    }
}
