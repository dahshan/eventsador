<!doctype html>
<html class="no-js" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">

	<!-- Page Title Here -->
    <title>PicSnippet </title>

    <!-- Web fonts and Web Icons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/fonts/opensans/stylesheet.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/fonts/roboto/stylesheet.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/fonts/ionicons.min.css">

    <!-- Vendor CSS style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/pageloader.css">


</head>

<body id="menu" class="hh-body alt-bg left-light">

	<!-- Page Loader -->
	<div class="page-loader" id="page-loader">
		<div>
			<div class="icon ion-spin"></div>
			<p>Coming Soon</p>
		</div>
	</div>

</body>

</html>
