<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Eventsador Admin</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/matrix-login.css" />
        <link href="<?php echo base_url(); ?>assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
        <style type="text/css">
         .body-with-bg{
            background-image: url(<?php echo base_url('assets/admin/img/Eventshador-banner.jpg'); ?>);
            background-size:cover;
            background-position: center center;
            background-attachment: fixed;
         }
         .white_bg{ background:#fff;}
        </style>
    </head>

    <body class="white_bg">
        <div id="loginbox">
            <!--<form role="form" class="form-vertical" action="<?php echo base_url('check_admin_login'); ?>" method="post" id="loginform" enctype="multipart/form-data">-->
            <form role="form" class="form-vertical" action="<?php echo base_url('admin'); ?>" method="post" id="loginform" enctype="multipart/form-data">
                <div class="control-group normal_text"> 
                    <h3>
                        <img src="<?php echo base_url(); ?>assets/front/images/admin_logo.png" alt="Logo" />
                    </h3>
                                    <!--<h1><strong>PICTURE SNIPPET ADMIN</strong></h1>-->
                </div>
                <div style="box-shadow: 0 1px 4px 3px #ccc;">
                <div class="control-group normal_text" style="background: #fff;"> 
                    <span style="color:red;">
                        <?php if ($this->session->flashdata('errormessage')) : ?>
                            <?= $this->session->flashdata('errormessage'); ?>
                        <?php endif; ?>
                    </span>
                     <span style="color:green;">
                        <?php if ($this->session->flashdata('successmessage')) : ?>
                            <?= $this->session->flashdata('successmessage'); ?>
                        <?php endif; ?>
                    </span>
                </div>
                <div class="control-group">
                    <div class="controls">
                      <label for="user_type">Select User Type:</label>
                      <select class="form-control" id="user_type" name="user_type">
                        <option value="admin">Admin</option>
                        <option value="org">Organizer</option>
                        <option value="user">Organizer Representative</option>
                      </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
<!--                            <span class="add-on bg_lh"><i class="icon-user"> </i></span>-->
                            <label>Username or Email Address</label>
                            <input type="text" id="username" name="username" placeholder="User Email" value="<?php if(isset($_COOKIE["member_login"])) { echo $_COOKIE["member_login"]; } ?>" />
                            <?php echo form_error('username', '<div style="color:red;">', '</div>'); ?>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
<!--                            <span class="add-on bg_ly"><i class="icon-lock"></i></span>-->
                            <label>Password</label>
                            <input type="password" id="passwd" name="passwd" placeholder="Password" value="<?php if(isset($_COOKIE["member_password"])) { echo $_COOKIE["member_password"]; } ?>" />
                            <?php echo form_error('passwd', '<div style="color:red;">', '</div>'); ?>
                        </div>
                    </div>
                </div>
                    
                    <div class="form-actions" style="margin-top:0">
                    <!-- <span class="pull-left"><div class="checkbox"> <label> <input type="checkbox" name="remember" id="remember" <?php if(isset($_COOKIE["member_login"])) { ?> checked <?php } ?>> Remember Me </label> </div></span> -->
                    <div class="form-row text-center">
                        <div class="col-12">
                            <input name="submit" type="submit" value="Login" class="btn btn-success btn-purple btn-round" style="width: 40%;">
                        </div>
                     </div>
                </div>
                    <div class="control-group normal_text"></div>
                </div>
                <div class="control-group">
                    <!-- <a href="#">Lost your password</a> -->
                     <a href="javascript:void(0);" class="flip-link forget_password" id="to-recover">Lost your password?</a>
                </div>
                
            </form>


           
            <form role="form" class="form-vertical" action="<?php echo base_url('check_admin_forgot_password'); ?>" method="post" id="recoverform" enctype="multipart/form-data">

             <div style=" box-shadow: 0 1px 4px 3px #ccc;">
                <div  class="control-group normal_text" ></div>
                <div class="forgot-cls">
                    <p>Forgot Password</p>
                    Enter your e-mail address below and we will send a recovery password.
                </div>

                <div class="controls">
                    <div class="main_input_box">
                        <!-- <span class="add-on bg_lo"><i class="icon-envelope"></i></span> -->
                        <input type="text" placeholder="E-mail address" id="emailid" name="emailid" />
                    </div>
                </div>

                <div class="form-actions">
                    <span class="pull-left"><a href="javascript:void(0);" class="flip-link btn btn-success btn-purple btn-round" id="to-login">&laquo; Back to login</a></span>
                    <span class="pull-right"><a type="submit" class="btn btn-info btn-lava btn-round" onclick="forgot_password_submit_form();"/>Recover</a></span>
                </div>

                <div  class="control-group normal_text" ></div>
                </div>

            </form>
            <div class="login-footer">
                <a href="https://eventsador.net/terms-of-use/">Terms of Use</a>
                |
                <a href="https://eventsador.net/privacy-policy/">Privacy Policy</a>
                |
                <p>Powered by </p>
                <a href="https://eventsador.net/">Eventsador</a>

                
            </div>           
        </div>

        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/matrix.login.js"></script>

        <script type="text/javascript">
                        function login_submit_form() {
                            $("#loginform").submit();
                        }

                        function forgot_password_submit_form() {
                            $("#recoverform").submit();
                        }
        </script>
    </body>
</html>
