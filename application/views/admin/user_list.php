<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> 
            <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
            <a href="javascript:void" class="current">Bidder/Recruiter list</a> </div>
        <h1>Bidder/Recruiter list</h1>
        <!--<a href="<?= base_url('add_cms'); ?>" class="btn btn-info" style="margin-left: 20px;"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add CMS</a>-->
        <p><?php if ($this->session->flashdata('user')) : ?>
            <?= $this->session->flashdata('user'); ?>
        <?php endif; ?></p>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="fa fa-cog" aria-hidden="true"></i> </span>
                        <h5>Bidder/Recruiter list</h5>
                    </div>
                     <div class="container-fluid">
        <hr>
       
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> 
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table id="example20" class="table table-bordered data-table">
                            
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php
                            if (!empty($users)) {
                                ?>
                               
                                <?php
                                foreach ($users as $data) {
                                    ?>
                                    
                                        <tr class="gradeX">
                                            <td>
                                                <?php echo $data->fname . " " . $data->familyname; ?>
                                            </td>
                                            <td>
                                                <?php echo $data->emailid; ?>
                                            </td>
                                        </tr>
                                    
                                    <?php
                                }
                                ?>
                                </tbody>
                                <?php
                            }
                            ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
                </div>
            </div>
        </div>
    </div>
</div>