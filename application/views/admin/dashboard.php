<script src="<?php echo base_url(); ?>assets/admin/js/matrix.dashboard.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/admin/js/matrix.interface.js"></script> -->
<!--main-container-part-->
<div id="content">

    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
    </div>
    <!--End-breadcrumbs-->

    <!--Action boxes-->
    <div class="container-fluid">

        <!--Chart-box-->
        <div class="row-fluid">
            <div class="widget-box">
                <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
                    <h5>Site Analytics</h5>
                </div>
                <div class="widget-content" >
                    <div class="row-fluid">
                        <!-- <div class="span3">
                            <div class="chart"></div>
                        </div> --> 
                        <div class="span9">
                            <ul class="site-stats">
                                <a href="<?php echo base_url('admin_appuser');?>"><li class="bg_lh"><i class="icon-group"></i> <strong><?php echo $total_appuser; ?></strong> <small>Total App Users</small></li></a>
                                <?php if($this->session->userdata('admin_role_type')==1){ ?>
                                <a href="<?php echo base_url('admin_company');?>"><li class="bg_lh"><i class="icon-building"></i> <strong><?php echo $total_company; ?></strong> <small>Total Companies </small></li></a>
                                <?php } ?>
                                <?php if($this->session->userdata('admin_role_type')==1){ ?>
                                <a href="<?php echo base_url('admin_organizer');?>"><li class="bg_lh"><i class="icon-user"></i> <strong><?php echo $total_organizer; ?></strong> <small>Total Organizers</small></li></a>
                                <?php } ?>
                                <a href="<?php echo base_url('admin_event');?>"><li class="bg_lh"><i class="icon-calendar"></i> <strong><?php echo $total_event; ?></strong> <small>Total Event</small></li></a>
                                <a href="<?php echo base_url('admin_event');?>"><li class="bg_lh"><i class="icon-plus"></i> <strong><?php echo $upcoming_event; ?></strong> <small>Upcoming Events</small></li></a>
                                <a href="<?php echo base_url('admin_event');?>"><li class="bg_lh"><i class="icon-repeat"></i> <strong><?php echo $ongoing_event; ?></strong> <small>Ongoing Events</small></li></a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End-Chart-box-->
    </div>
</div>

<!--end-main-container-part-->
