<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> 
            <a href="<?= base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
            <a href="#" class="current"><?= ($task == 'add') ? 'Add' : 'Edit'; ?> CMS</a>
        </div>
        <h1><?= ($task == 'add') ? 'Add' : 'Edit'; ?> CMS</h1>
        <p><?php if ($this->session->flashdata('template')) : ?>
            <?= $this->session->flashdata('template'); ?>
        <?php endif; ?></p>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </span>
                        <h5><?= ($task == 'add') ? 'Add' : 'Edit'; ?> CMS</h5>
                        
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($task == 'edit') ? base_url('edit_cms/' . $cms_data->id) : base_url('add_cms'); ?>" name="basic_validate" id="basic_validate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Page Title</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Page Title" type="text" name="page_title" value="<?= ($task == 'edit') ? $cms_data->page_title : ''; ?>">
                                    <?php echo form_error('page_title', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Page Content</label>
                                <div class="controls">
                                    <?php echo $this->ckeditor->editor("page_content",(($task == 'edit') ? $cms_data->page_content : '')); ?>
                                    <?php echo form_error('page_content', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Banner</label>
                                <div class="controls">
                                    <input class="span11" type="file" name="banner">
                                    <?php //echo form_error('page_content', '<div style="color:red;">', '</div>'); ?>
                                    <?php if($task == 'edit' && $cms_data->banner != "") : ?>
                                        <br/><img class="" src="<?=base_url('cms/banner/') . $cms_data->banner; ?>" width="100" />
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input name="submit" type="submit" value="Submit" class="btn btn-success">
                                <a href="<?=base_url('cms_list'); ?>" class="btn btn-warning">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>