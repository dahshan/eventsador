<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> 
            <a href="<?= base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
            <a href="#" class="current"><?= ($task == 'add') ? 'Add' : 'Edit'; ?> Platform</a>
        </div>
        <h1><?= ($task == 'add') ? 'Add' : 'Edit'; ?> Platform</h1>
        <p><?php if ($this->session->flashdata('technology')) : ?>
            <?= $this->session->flashdata('technology'); ?>
        <?php endif; ?></p>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </span>
                        <h5><?= ($task == 'add') ? 'Add' : 'Edit'; ?> Platform</h5>
                        
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($task == 'edit') ? base_url('edit_technology/' . $tech->id) : base_url('add_technology'); ?>" name="basic_validate" id="basic_validate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Platform</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Platform Name" type="text" name="tech_name" value="<?= ($task == 'edit') ? $tech->tech_name : ''; ?>">
                                </div>
                            </div>
                            <div class="form-actions">
                                <input name="submit" type="submit" value="Submit" class="btn btn-success">
                                <a href="<?=base_url('technology_list'); ?>" class="btn btn-warning">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>