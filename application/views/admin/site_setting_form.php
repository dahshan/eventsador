<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> 
            <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
            <a href="#">CMS</a> <a href="#" class="current">Social Network</a> </div>
        <h1>Social Network</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="fa fa-cog" aria-hidden="true"></i> </span>
                        <h5>Social Network</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('site_settings'); ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Facebook Link</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Facebook Link" type="text" name="facebook_link" value="<?php echo (!empty(set_value('facebook_link'))) ? set_value('facebook_link') : ((!empty($data_single->facebook_link)) ? $data_single->facebook_link : ''); ?>">
                                    <?php echo form_error('facebook_link', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Twitter Link</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Twitter Link" type="text" name="twitter_link" value="<?php echo (!empty(set_value('twitter_link'))) ? set_value('twitter_link') : ((!empty($data_single->twitter_link)) ? $data_single->twitter_link : ''); ?>">
                                    <?php echo form_error('twitter_link', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Linkedin Link</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Linkedin Link" type="text" name="linkedin_link" value="<?php echo (!empty(set_value('linkedin_link'))) ? set_value('linkedin_link') : ((!empty($data_single->linkedin_link)) ? $data_single->linkedin_link : ''); ?>">
                                    <?php echo form_error('linkedin_link', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Instagram Link</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Instagram Link" type="text" name="instagram_link" value="<?php echo (!empty(set_value('instagram_link'))) ? set_value('instagram_link') : ((!empty($data_single->instagram_link)) ? $data_single->instagram_link : ''); ?>">
                                    <?php echo form_error('instagram_link', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">App Store</label>
                                <div class="controls">
                                    <input class="span11" placeholder="App Store Link" type="text" name="app_store_link" value="<?php echo (!empty(set_value('app_store_link'))) ? set_value('app_store_link') : ((!empty($data_single->app_store_link)) ? $data_single->app_store_link : ''); ?>">
                                    <?php echo form_error('app_store_link', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Google Play</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Google Play Link" type="text" name="google_play_link" value="<?php echo (!empty(set_value('google_play_link'))) ? set_value('google_play_link') : ((!empty($data_single->google_play_link)) ? $data_single->google_play_link : ''); ?>">
                                    <?php echo form_error('instagram_link', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>