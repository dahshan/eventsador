<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> 
            <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
            <a href="javascript:void" class="current">CMS</a> </div>
        <h1>CMS</h1>
        <a href="<?= base_url('add_cms'); ?>" class="btn btn-info" style="margin-left: 20px;"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add CMS</a>
        <p><?php if ($this->session->flashdata('template')) : ?>
            <?= $this->session->flashdata('template'); ?>
        <?php endif; ?></p>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="fa fa-cog" aria-hidden="true"></i> </span>
                        <h5>CMS</h5>
                    </div>
                     <div class="container-fluid">
        <hr>
       
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> 
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table id="example20" class="table table-bordered data-table">
                            
                            <thead>
                                <tr>
                                    <th>Page Title</th>
                                    <th>Page Content</th>
                                    <th>Banner</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php
                            if (!empty($cms_list)) {
                                ?>
                               
                                <?php
                                foreach ($cms_list as $data) {
                                    ?>
                                    
                                        <tr class="gradeX">
                                            <td>
                                                <?php echo $data->page_title; ?>
                                            </td>
                                            <td>
                                                <?php echo $data->page_content; ?>
                                            </td>
                                            <td>
                                                <?php if($data->banner != "") : ?>
                                                    <img class="" src="<?=base_url('cms/banner/') . $data->banner; ?>" width="50" />
                                                <?php endif; ?>
                                            </td>                                                
                                            <td>
                                                <a href="<?=base_url('edit_cms/') . $data->id; ?>" class="btn btn-lg btn-info" data-toggle="tooltip" title="Edit">
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>
                                                <?php if($data->is_active == 1): ?>
                                                    <a href="<?=base_url('change_status/') . $data->id . '/' . $data->is_active; ?>" class="btn btn-lg btn-success" data-toggle="tooltip" title="Deactivate">Active</a>
                                                <?php else: ?>
                                                    <a href="<?=base_url('change_status/') . $data->id . '/' . $data->is_active; ?>" class="btn btn-lg btn-danger" data-toggle="tooltip" title="Activate">Inactive</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    
                                    <?php
                                }
                                ?>
                                </tbody>
                                <?php
                            }
                            ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
                </div>
            </div>
        </div>
    </div>
</div>