<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current">Change Password</a>
        </div>
        <h1>Change Password</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </span>
                        <h5>Change Password</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('admin_change_password'); ?>" name="basic_validate" id="basic_validate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Old Password</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Old Password" type="password" name="o_password" value="">
                                    <?php echo form_error('o_password', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">New Password</label>
                                <div class="controls">
                                    <input class="span11" placeholder="New Password" type="password" name="n_password" value="">
                                    <?php echo form_error('n_password', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Confirm Password</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Confirm Password" type="password" name="c_password" value="">
                                    <?php echo form_error('c_password', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input name="submit" type="submit" value="Submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
