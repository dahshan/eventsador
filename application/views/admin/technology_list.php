<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> 
            <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
            <a href="javascript:void" class="current">Platform</a> </div>
        <h1>Platforms</h1>
        <a href="<?= base_url('add_technology'); ?>" class="btn btn-info" style="margin-left: 20px;"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Platform</a>
        
        <p><?php if ($this->session->flashdata('technology')) : ?>
            <?= $this->session->flashdata('technology'); ?>
        <?php endif; ?></p>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="fa fa-cog" aria-hidden="true"></i> </span>
                        <h5>Platforms</h5>
                    </div>
                     <div class="container-fluid">
        <hr>
       
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> 
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table id="example20" class="table table-bordered data-table">
                            
                            <thead>
                                <tr>                                    
                                    <th>Platform</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php
                            if (!empty($technology_list)) {
                                ?>
                               
                                <?php
                                foreach ($technology_list as $data) {
                                    ?>
                                    
                                        <tr class="gradeX">
                                            <td>
                                                <?php echo $data->tech_name; ?>
                                            </td>                                              
                                            <td>
                                                <a href="<?=base_url('edit_technology/') . $data->id; ?>" class="btn btn-lg btn-info" data-toggle="tooltip" title="Edit">
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>
                                                <?php if($data->is_active == 1): ?>
                                                    <a href="<?=base_url('change_tech_status/') . $data->id . '/' . $data->is_active; ?>" class="btn btn-lg btn-success" data-toggle="tooltip" title="Deactivate">Active</a>
                                                <?php else: ?>
                                                    <a href="<?=base_url('change_tech_status/') . $data->id . '/' . $data->is_active; ?>" class="btn btn-lg btn-danger" data-toggle="tooltip" title="Activate">Inactive</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    
                                    <?php
                                }
                                ?>
                                </tbody>
                                <?php
                            }
                            ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
                </div>
            </div>
        </div>
    </div>
</div>