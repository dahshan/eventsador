<?php
$type_id = $this->session->userdata('admin_user_type');
?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current">Edit Profile</a>
        </div>
        <h1>Edit Profile</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </span>
                        <h5>Edit Profile</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('admin_edit_profile'); ?>" name="basic_validate" id="basic_validate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Name" type="text" name="name" value="<?php echo (set_value('name')) ? set_value('name') : (($data_single->name) ? $data_single->name : ''); ?>">
                                    <?php echo form_error('name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Email</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Email" type="text" name="email" value="<?php echo (set_value('email')) ? set_value('email') : (($data_single->emailid) ? $data_single->emailid : ''); ?>">
                                    <?php echo form_error('email', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Phone</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Phone" type="text" name="phoneno" value="<?php echo (set_value('phoneno')) ? set_value('phoneno') : (($data_single->phoneno) ? $data_single->phoneno : ''); ?>">
                                    <?php echo form_error('phoneno', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <?php if (in_array($type_id, ['2'])) { ?>
                                <div class="control-group">
                                    <label class="control-label">Phone</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Phone" type="text" name="phoneno" value="<?php echo (set_value('phoneno')) ? set_value('phoneno') : (($data_single->phoneno) ? $data_single->phoneno : ''); ?>">
                                        <?php echo form_error('phoneno', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Designation</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Designation" type="text" name="designation" value="<?php echo (set_value('designation')) ? set_value('designation') : (($data_single->designation) ? $data_single->designation : ''); ?>" readonly>
                                        <?php echo form_error('designation', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            <?php }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Upload Image</label>
                                <div class="controls">
                                    <input type="file" name="img_file" id="img_file">
                                </div>
                                <div class="controls">
                                    <?php
                                    if (!empty($data_single->profile_image)) {
                                        ?>
                                        <img src="<?php echo base_url(); ?>assets/upload/profimg/<?php echo $data_single->profile_image; ?>" height="100" width="150">
                                        <?php } else {
                                        ?>
                                        <img src="<?php echo base_url(); ?>assets/front/images/default_profile_image.jpg" height="100" width="150">  
                                    <?php }
                                    ?>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input name="submit" type="submit" value="Submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
