<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> 
            <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
            <a href="#">CMS</a> <a href="#" class="current">Contact Us</a> </div>
        <h1>Contact Us</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="fa fa-cog" aria-hidden="true"></i> </span>
                        <h5>Contact Us</h5>
                    </div>
                     <div class="container-fluid">
        <hr>
       
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> 
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <!--<h5>Total Count: <?php echo $user_count; ?></h5>-->
                    </div>
                    <div class="widget-content nopadding">
                        <table id="example20" class="table table-bordered data-table">
                            
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Message</th>
                                    <th>Sent On</th>
                                    <!--<th>Action</th>-->
                                </tr>
                            </thead>

                            <tbody>
                            <?php
                            if (!empty($mail_data)) {
                                ?>
                               
                                <?php
                                foreach ($mail_data as $data) {
                                    ?>
                                    
                                        <tr class="gradeX">
                                            <td>
                                                <?php echo $data->name; ?>
                                            </td>
                                            <td>
                                                <?php echo $data->email; ?>
                                            </td>
                                            <td>
                                                <?php echo $data->message; ?>
                                            </td>

                                            <td>
                                                <?php echo $data->sent_date; ?>
                                            </td>
                                           
                                         
                                            <!--<td class="custom-action-btn">

                                                <a href="<?php echo base_url('user/views');?>/<?php echo urlencode(base64_encode($data->id));?>"><i title="View" class="fa fa-eye icon_size
                                                " aria-hidden="false" onclick=" "></i></a>

                                                &nbsp;&nbsp;

                                                <?php
                                                if ($data->is_blocked == '0') { ?>
                                                <i title="Block User" class="fa fa-check-circle icon_size" aria-hidden="false" onclick="block_user('<?php echo urlencode(base64_encode($data->id)); ?>');"></i>
                                                <?php } else if($data->is_blocked == '1') { ?>
                                                    <i title="Unblock User" class="fa fa-ban icon_size" aria-hidden="false" onclick="unblock_user('<?php echo urlencode(base64_encode($data->id)); ?>');"></i>
                                                <?php
                                                }
                                                ?>
                                                
                                                &nbsp;&nbsp;

                                                <i title="Delete" class="fa fa-trash icon_size" aria-hidden="false" onclick="delete_member('<?php echo urlencode(base64_encode($data->id)); ?>');"></i>
                                            </td>-->
                                        </tr>
                                    
                                    <?php
                                }
                                ?>
                                </tbody>
                                <?php
                            }
                            ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
                </div>
            </div>
        </div>
    </div>
</div>