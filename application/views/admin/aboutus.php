<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">CMS</a> <a href="javascript:void(0);" class="current">About Us</a> </div>
    <h1>About Us</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>About Us</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="<?php echo base_url('admin_save_about_us'); ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
            <input type="hidden" id="cms_id" name="cms_id" value="<?php echo (!empty($cms_data->id)) ? $cms_data->id : ''; ?>">

              <div class="control-group">
                <label class="control-label">Banner</label>
                <div class="controls">
                  <input type="file" name="cms_image" id="cms_image" required>
                  <?php 
                  if(!empty($cms_data->cms_image)) {
                  ?>
                    <img src="<?php echo base_url(); ?>assets/front/cms/<?php echo $cms_data->cms_image; ?>" height="100" width="150">
                  <?php
                  } 
                  ?>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Content</label>
                <div class="controls custon_textarea">
                  <textarea name="cms_content" id="cms_content" required><?php echo (!empty($cms_data->cms_content)) ? $cms_data->cms_content : ''; ?></textarea>
                </div>
              </div>
              
              <div class="form-actions">
                <input type="submit" value="Submit" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
CKEDITOR.replace( 'cms_content', {
  language: 'en',
  uiColor: '#9AB8F3'
});
</script>