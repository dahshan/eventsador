

<div style="font:11px/1.35em Arial, Helvetica, sans-serif;">
    <table cellspacing="0" cellpadding="0" border="0" width="98%" style="margin-top:10px; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; margin-bottom:10px; padding: 25px; background: #f1f1f1;">
        <tr>
            <td align="center" valign="top">
                <table cellspacing="0" cellpadding="0" border="0" width="620" style="background:url({siteurl}assets/images/bg.jpg repeat; border-radius: 6px;">
                    <tr>
                        <td style="text-align: center; font-size: 12px; padding: 0;">
                            <a href="#"><img src="{logo}" alt="DAY POS" style="width:122px; padding: 20px;"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                            <table width='90%' align='center' style="background:#59a245; border-radius: 5px; padding:20px 10px 10px; margin-bottom: 30px;">
                                <tr style="background: #a6cf3d">
                                    <td>
                                        <p style="color: #fff;font-size: 20px; text-transform: uppercase; text-align: center !important;"><span style="font-size: 25px; font-weight: bold; white-space: nowrap; text-align: center;">DAY POS</span></p>                                       
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td style="padding: 10px 0 0 0;">
                                        <p style="font-size: 14px; line-height: 18px;  color: #ffffff; text-align: center;">{message}
                                        </p>
                                        
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td style="font-size: 14px;  line-height: 18px; color: #fff; text-align: center;">
                                        &copy; {year} <?php echo getsitename();?><br/>
                                        All Rights Reserved. Terms of Service.<br/>
                                    </td>
                                </tr>
                                
                                <tr><td style="line-height: 38px !important; padding: 26px 0 0 0;"></td></tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>