<!doctype html>
<!--
COPYRIGHT by HighHay/Mivfx
Before using this theme, you should accept themeforest licenses terms.
http://themeforest.net/licenses
-->

<html class="no-js" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">

	<!-- Page Title Here -->
	<title>Crystal-Matix -> Design, Build, Test ... Repeat</title>

	<!-- Page Description Here -->
	<meta name="description" content="Out goal is to improve our current standard of living and engage with other professionals so that together we can grow">

	<!-- Disable screen scaling-->
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, user-scalable=0">

	<!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

    <!-- Web fonts and Web Icons -->
    <link rel="stylesheet" href="./fonts/opensans/stylesheet.css">
    <link rel="stylesheet" href="./fonts/roboto/stylesheet.css">
    <link rel="stylesheet" href="./fonts/ionicons.min.css">
    
    <!-- Vendor CSS style -->
    <link rel="stylesheet" href="./css/pageloader.css">
    
    <!-- Uncomment below to load individualy vendor CSS -->
    <link rel="stylesheet" href="./css/foundation.min.css">
    <link rel="stylesheet" href="./js/vendor/swiper.min.css">
    <link rel="stylesheet" href="./js/vendor/jquery.fullpage.min.css">
    <link rel="stylesheet" href="./js/vegas/vegas.min.css">
    
    <!-- Main CSS files -->
    <link rel="stylesheet" href="./css/main.css">
    <!-- alt layout -->
    <link rel="stylesheet" href="./css/style-color1.css">

	<script src="./js/vendor/modernizr-2.7.1.min.js"></script>
</head>

<body id="menu" class="hh-body alt-bg left-light">
	<!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

	<!-- Page Loader -->
	<div class="page-loader" id="page-loader">
		<div>
			<div class="icon ion-spin"></div>
			<p>loading</p>
		</div>
	</div>

    <!-- BEGIN OF site header Menu -->
    <header class="hh-header header-top">
        <!-- Begin of logo -->
        <div class="logo-wrapper">
            <a href="./">
                <h2 class="logo">
                    <span class="logo-img">
                        <img class="light-logo" src="img/logo.png" alt="Logo">
                    </span>
                    <span class="logo-text">
                        <span class="title">Crystal-Matrix</span>
                        <span class="desc">Design Build Test ... Repeat</span>
                    </span>
                </h2>
            </a>
        </div>
        <!-- End of logo -->
        
        <!-- Begin of menu icon -->
        <a class="menu-icon">
            <div class="bars">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </a>
        <!-- End of menu icon -->
        
        <!-- Begin of menu -->
        <nav class="menu-links">
            <ul class="links">
                <li><a href="#home">Home</a></li>
                <li><a href="#Video">Video</a></li>
                <li><a href="#Projects">Projects</a></li>
                <li><a href="html/index.html">Campaign</a></li>
                <li><a href="#About">About</a></li>
                <li><a href="#contact">Contact</a></li>
                <li class="cta"><a href="#Donate" target="_blank">Donate</a></li>
            </ul>
        </nav>
        <!-- End of menu -->
    </header>
    <!-- END OF site header Menu-->

    <!-- BEGIN OF page cover -->
    <div class="hh-cover page-cover">
        <!-- Cover Background -->
        <div class="cover-bg pos-abs full-size bg-img  bg-blur-0" data-image-src="img/bg-default1.jpg"></div>


        <!-- Solid color as background - uncomment below to use solid color background -->
        <!--<div class="cover-bg pos-abs full-size bg-color" data-bgcolor="#00B4C2"></div>-->

        
        <!-- Transluscent mask as filter -->
        <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(32, 32, 32, 0.7)"></div>

    </div>
    <!--END OF page cover -->

    	
	<!-- BEGIN OF page main content -->
	<main class="page-main fullpg" id="mainpage">

        <!-- Begin of home section -->
        <div class="section section-home fp-auto-height-responsive" data-section="home">
            <div class="content">
                <!-- Begin of Left Content -->
                <section class="c-left anim">
                    <div class="wrapper fit">
        
                        <!-- Logo Instead of clock -->
                        <!--<div class="home-logo ">
                            <img class="" src="img/logo.png" alt="Logo">
                        </div>-->
                        
                        <!-- Coutdown Clock -->
                        <div class="clock clock-countdown">
                            <div class="site-config"
                                    data-date="01/01/2018 00:00:00" 
                                    data-date-timezone="+0"
                                    ></div>
                            <div class="clock-wrapper">
                                <div class="tile tile-days">
                                    <span class="days">00</span>
                                    <span class="txt">days</span>
                                </div>
                                <div class="clock-hms">
                                    <div class="tile tile-hours">
                                        <span class="hours">00</span>
                                        <span class="txt">hours</span>
                                    </div>
                                    <div class="tile tile-minutes">
                                        <span class="minutes">00</span>
                                        <span class="txt">minutes</span>
                                    </div>
                                    <div class="tile tile-seconds">
                                        <span class="seconds">00</span>
                                        <span class="txt">seconds</span>
                                    </div>
                                </div>
                            </div>
                        </div>
        
                    </div>
                </section>
                <!-- End of Left Content -->
        
                <!-- Begin of Right Content -->
                <section class="c-right anim">
                    <div class="wrapper">
        
                        <!-- Title and description -->
                        <div class="title-desc">
                            <div class="t-wrapper">
                                <!-- Logo -->
                                <!--<div class="logo home-logo ">
                                    <img class="" src="img/logo.png" alt="Logo">
                                </div>-->
                                <!-- Title -->
                                <header class="title">
                                    <h2><strong>Crystal</strong> <br> Matrix</h2>
                                    <h3>Design Build Test ... Repeat.</h3>
                                </header>
                                <!-- desc -->
                                <div class="desc">
                                    <p>We want to build bridges through an open platform that brings entrepreneurs and designers together in an international community</p>    
                                </div>
                            </div>
                        </div>
        
                        <!-- Action button -->
                        <div class="cta-btns">
                            <a class="btn arrow-circ-btn"  href="#Video">
                                <span class="txt">Video</span>
                                <span class="arrow-icon"></span>
                            </a>
                            
                            <a class="btn arrow-circ-btn" target="_blank"  href="#Projects">
                                <span class="txt">Projects</span>
                                <span class="arrow-icon"></span>
                            </a>
                            <a class="btn arrow-circ-btn" target="_blank"  href="#About">
                                <span class="txt">About</span>
                                <span class="arrow-icon"></span>
                            </a>
                        </div>
        
                    </div>
                </section>
                <!-- End of Right Content -->
            </div>
        </div>
        <!-- End of home section -->
        
        <!-- Begin of video section -->
        <div class="section section-about section-cent fp-auto-height-responsive" data-section="Video">
        
            <section class="content clearfix">
                <!-- content title -->
                <!--<div class="c-title">
                    <h2>Our company</h2>
                </div>-->
                
                <!-- left elements -->
                <div class="c-left anim">
                    <div class="wrapper">
                        <!-- title and description -->
                        <header class="title-desc">
                            <h3 class="title">Video</h3>
                            <h2>Ruth Lorenzo Knocking On Heavens Door X Factor</h2>
                            <h4>Design, Build, Test ... Repeat ... until perfect</h4>
                        </header>
                        
                        <!-- Action button -->
                        <div class="cta-btns">
                            <a class="btn arrow-circ-btn"  href="#Video">
                                <span class="txt">Video</span>
                                <span class="arrow-icon"></span>
                            </a>
                            <a class="btn arrow-circ-btn"  href="#Projects">
                                <span class="txt">Projects</span>
                                <span class="arrow-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- end of left elements -->
                
                <!-- right elements -->
                <div class="c-right anim">
                    <!-- title and texts wrapper-->
                    <div class="wrapper">
                        <!-- title and description -->
                        <iframe width="420" height="315" class="video"
                        src="https://www.youtube.com/embed/BNHR6IQJGZs">
                        </iframe>
                        
                    </div>
                </div>
                <!-- end of right elements -->
            </section>
        </div>
        <!-- End of video section -->
        
        <!-- Begin of Works/projects section -->
        <div class="section section-projects fp-auto-height-responsive" data-section="Projects">
        
            <section class="content clearfix">
                
                <!-- left elements -->
                <div class="c-left anim">
                    <!-- title and texts wrapper-->
                    <div class="wrapper">
                        <!-- title -->
                        <header class="title-desc">
                            <h2 class="title page-title">Projects</h2>
                            <p>Please contact us for more details. Just a few samples among our projects.</p>
                        </header>
                        
                        
                        <!-- Action button -->
                        <div class="cta-btns">
                            <a class="btn arrow-circ-btn"  href="#contact">
                                <span class="txt">Contact Us</span>
                                <span class="arrow-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- end of left elements -->
                
                <!-- left elements -->
                <div class="c-right anim">
                    <div class="wrapper clip-overflow">
                        <!-- Begin of works/services/products -->
                        <div class="slider-wrapper">
                            <!-- pagination -->
                            <div class="items-pagination"></div>
                            <div class="items-nav-container">
                                <div class="items-button items-button-prev"><a>Previous</a></div>
                                <div class="items-button items-button-next"><a>Next</a></div>
                            </div>
                            <!-- slider -->
                            <div class="items-slide">
                                <ul class="c-features-list row swiper-wrapper my-gallery">
                                    <!-- item -->
                                    <li class="column swiper-slide">
                                        <a href="#project_url_1">
                                            <div class="item-img">
                                                <div class="img bg-img" data-image-src="img/items/img-sample1.jpg"></div>
                                            </div>
                                            <div class="item-desc">
                                                <h4>Category / Website</h4>
                                                <h3 class="title">Work management</h3>
                                                <div class="desc">
                                                    <p>Help us DISRUPT and REVOLUTIONIZE modern jobs.</p>
                                                    <p>Out goal is to improve our current standard of living by engaging with other professionals so that together we can grow</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- item -->
                                    <li class="column swiper-slide">
                                        <a href="#project_url_2">
                                            <div class="item-img">
                                                <div class="img bg-img" data-image-src="img/items/img-sample2.jpg"></div>
                                            </div>
                                            <div class="item-desc">
                                                <h4>Category / Website</h4>
                                                <h3 class="title">Project management</h3>
                                                <div class="desc">
                                                    <p>We want to facilitate leaders in finding the best people for there project and also designers in finding the best leader for them.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- item -->
                                    <li class="column swiper-slide">
                                        <a href="#project_url_3">
                                            <div class="item-img">
                                                <div class="img bg-img" data-image-src="img/items/img-sample3.jpg"></div>
                                            </div>
                                            <div class="item-desc">
                                                <h4>Arts / Painting</h4>
                                                <h3 class="title">Painting</h3>
                                                <div class="desc">
                                                    <p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum a sit amet nulla.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- item -->
                                    <li class="column swiper-slide">
                                        <a href="#project_url_2">
                                            <div class="item-img">
                                                <div class="img bg-img" data-image-src="img/items/img-sample4.jpg"></div>
                                            </div>
                                            <div class="item-desc">
                                                <h4>Photo / Instagram</h4>
                                                <h3 class="title">Macro Lens</h3>
                                                <div class="desc">
                                                    <p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum a sit amet nulla.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- simple link to an url -->
                                    <!-- <li class="column swiper-slide">
                                        <a href="#project_url_2">
                                            <div class="item-img">
                                                <div class="img bg-img" data-image-src="img/items/img-sample4.jpg"></div>
                                            </div>
                                            <div class="item-desc">
                                                <h4>Photo / Instagram</h4>
                                                <h3 class="title">Macro Lens</h3>
                                                <div class="desc">
                                                    <p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum a sit amet nulla.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                        <!-- End of works/services/products -->
                    </div>
                </div>
                <!-- end of left elements -->
            </section>
        </div>
        <!-- End of Works/projects section -->        
        

        <!-- Begin of services section -->
        <div class="section section-services fp-auto-height-responsive" data-section="">
        
            <section class="content clearfix">
                
                <!-- right elements -->
                <div class="c-right anim">
                    <div class="wrapper">
                        <!-- title -->
                        <header class="title-desc">
                            <h2>Our Focus</h2>
                            <p>Let’s reinvent engineering together</p>
                        </header>
                        
                        <!-- Action button -->
                        <div class="cta-btns">
                            <a class="btn arrow-circ-btn"  href="#contact">
                                <span class="txt">Contact Us</span>
                                <span class="arrow-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- end of right elements -->
                
                <!-- left elements -->
                <div class="c-left anim">
                    <div class="wrapper">
                        <!-- Begin of features/services/offers -->
                        <ul class="item-list row small-up-1 medium-up-2 large-up-2">
                            <!-- item -->
                            <li class="column anim">
                                <div class="item-desc">
                                    <h3>Work management</h3>
                                    <div class="desc">
                                        <p>It is clear that remote working is the future.</p>
                                        <p>This is why we are building a webplatform where designers and companies can create communities with the purpose of working together on achiving a common goal.</p>
                                    </div>
                                </div>
                            </li>
                            <!-- item -->
                            <li class="column anim">
                                <div class="item-desc">
                                    <h3>Artificial Intelligence</h3>
                                    <div class="desc">
                                        <p>The next BIG revolution in mankinds history is going to be the development of AI.</p> 
                                        <p>We are at the moment working in developing an AI capable of creating designs based on client requirements.</p>
                                    </div>
                                </div>
                            </li>
                            <!-- item -->
                            <li class="column anim">
                                <div class="item-desc">
                                    <h3>Robotics</h3>
                                    <div class="desc">
                                        <p>Is becoming more and more part of our world.</p>
                                        <p>This is why we are working in developing the next actuation technology for robotics.</p>
                                    </div>
                                </div>
                            </li>
                            <!-- item -->
                            <li class="column anim">
                                <div class="item-desc">
                                    <h3>Energy management</h3>
                                    <div class="desc">
                                        <p>We believe that with the increase use of wearable devices and the increased trend for “all things electric” from your car to your automated house, efficient management of power has become a must have is most industries.</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <!-- End of features/services/offers -->
                    </div>
                </div>
                <!-- end of left elements -->
                
            </section>
        
        </div>
        <!-- End of services section -->
        
        <!-- Begin of about us section -->
        <div class="section section-about section-cent fp-auto-height-responsive" data-section="About">
        
            <section class="content clearfix">
                <!-- content title -->
                <!--<div class="c-title">
                    <h2>Our company</h2>
                </div>-->
                
                <!-- left elements -->
                <div class="c-left anim">
                    <div class="wrapper">
                        <!-- title and description -->
                        <header class="title-desc">
                            <h3 class="title">About Us</h3>
                            <h2>Who is Crystal-Matrix?</h2>
                            <h4>Design, Build, Test ... Repeat ... until perfect</h4>
                        </header>
                        
                        <!-- Action button -->
                        <div class="cta-btns">
                            <a class="btn arrow-circ-btn"  href="#Video">
                                <span class="txt">Video</span>
                                <span class="arrow-icon"></span>
                            </a>
                            <a class="btn arrow-circ-btn"  href="#Projects">
                                <span class="txt">Projects</span>
                                <span class="arrow-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- end of left elements -->
                
                <!-- right elements -->
                <div class="c-right anim">
                    <!-- title and texts wrapper-->
                    <div class="wrapper">
                        <!-- title and description -->
                        <div class="title-desc">
                            <h4>Everything you should know</h4>
                            <p>We are a team of engineers that challenges design.</p>
                            <p>We question the standard.</p>
                            <p>We are driven by the need to innovate, to shape and optimize the world around us.</p>
                            <p>We challenge together and from challenge we grow.</p>
                            <p>We offer a new view on how to develop and optimize.</p>
                            <p>Design is an art, it implies going the extra mile to obtain conclusive results, it implies dedication and joy of designing. </p>
                            <p>We simplify and perfect … and only then we sign our work. </p>
                            <p>We take pride in our design.</p> 
                            <p>Let’s reinvent engineering together</p>
                            <!--<div class="illustr">
                                <img src="/img/bg-default4.jpg" alt="Illustration">
                            </div>-->
                        </div>
                    </div>
                </div>
                <!-- end of right elements -->
            </section>
        </div>
        <!-- End of about us section -->

        

        <!-- Begin of contact section -->
        <div class="section section-contact fp-auto-height-responsive" data-section="contact">
        
            <section class="content clearfix">
                
                <!-- Begin of  left elements -->
                <div class="c-left anim">
                    <div class="wrapper contact-wrapper">
                        <!-- title -->
                        <header class="title-desc">
                            <h2 class="page-title">Contact Us</h2>
                            <p>Need help or just want to say hello?</p>
                        </header>
                        
                        <!-- Begin of contact list -->
                        <ul class="contact-list c-features row small-up-1 medium-up-1 large-up-2">
                            <!-- item -->
                            <li class="column anim">
                                <div class="item-desc">
                                    <h3 class="title">Address</h3>
                                    <div class="desc">
                                        <p>
                                            Company address
                                            <br>Str. Johann Heinrich Pestalozzi, Nr. 22, Room 34
                                            <br>Timisoara, Romania
                                            <br>Postal code: 300115
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <!-- item -->
                            <li class="column anim">
                                <div class="item-desc">
                                    <h3 class="title">Contact</h3>
                                    <div class="desc">
                                        <p>
                                            Call : +4 07 368 90633
                                            <br> Email : <a href="">mihai.luchian@crystal-matrix.com</a>
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <!-- End of contact list -->
                        
                        <!-- Action button -->
                        <div class="cta-btns">
                            <a class="btn arrow-circ-btn" target="_blank"  href="#Donate">
                                <span class="txt">Donate</span>
                                <span class="arrow-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End of left elements -->
                
                
                <!-- Begin of right elements -->
                <div class="c-right">
                    <div class="wrapper c-form">
                        <!-- begin of contact form content -->
                        <div class="c-content card-wrapper">
                            <form class="message form send_message_form" method="post" action="ajaxserver/serverfile.php" id="message_form">
                                <div class="form-header clearfix">
                                    <h3>
                                        Write to us
                                    </h3>
                                    <div class="btns ">
                                        <button id="submit-message" class="btn arrow-circ-btn email_b" name="submit_message">
                                            <span class="txt">Send</span>
                                            <span class="arrow-icon"></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="fields clearfix">
                                    <div class="input name">
                                        <label for="mes-name">Name :</label>
                                        <input id="mes-name" name="name" type="text" placeholder="" class="form-success-clean" required>
                                    </div>
                                </div>
                                <div class="fields clearfix">
                                    <div class="input bottom">
                                        <label for="mes-email">Email :</label>
                                        <input id="mes-email" type="email" placeholder="" name="email" class="form-success-clean" required>
                                    </div>
                                </div>
                                <div class="fields clearfix no-border">
                                    <label for="mes-text">Message</label>
                                    <textarea id="mes-text" placeholder="..." name="message" class="form-success-clean" required></textarea>
        
                                    <div>
                                        <p class="message-ok invisible form-text-feedback form-success-visible">Your message has been sent, thank you.</p>
                                    </div>
                                </div>
        
                            </form>
                                                
                        </div>
                        <!-- end of contact form content -->
                    </div>
                </div>
                <!-- End of right elements -->
            </section>
        
        </div>
        <!-- End of contact section -->

	</main>

    <!-- BEGIN OF page footer -->
    <footer id="site-footer" class="site-footer">
        <!-- social networks -->
        <div class="contact">
            <ul class="socials">
                <li><a class="circ-btn" href="http://facebook.com/highhay"><i class="icon ion-social-facebook"></i></a></li>
                <li><a class="circ-btn" href="http://twitter.com/miradontsoa"><i class="icon ion-social-twitter"></i></a></li>
            </ul>
        </div>
        
        <!-- Notes -->
<!--         <div class="note">
            <p>Website by Highhay contact@highhay.com</p>
            <p>&copy; Copyright <a href="http://highhay.com"><span class="marked">MiVFX</span></a> 2017</p>
        </div>
         -->
        <!-- subscription form -->
        <div class="subscription">
            <h3 class="title">Subscribe to<br> Newsletter</h3>
            <!-- Begin Ajax subscription form  subscription-form -->
            <form id="subscription-form" class="form send_email_form" method="post" action="ajaxserver/serverfile.php">
                <p class="feedback gone form-success-visible">Thank you for your subscription. We will inform you.</p>
                <input id="reg-email" class="input form-success-invisible" name="email" type="email" required placeholder="your@email.address" data-validation-type="email">
                <button id="submit-email" class="btn circ-btn form-success-invisible" name="submit_email">
                    <span class="ion-checkmark"></span>
                </button>
            </form>
            <!-- End of Ajax subscription form  subscription-form -->
            
            <!-- Begin Native MailChimp Signup Form mc_embed_signup -->
            <!--<div id="mc_embed_signup">
            <form  action="//yebbiz.us3.list-manage.com/subscribe/post?u=6f9aaebaea67b078fc33b4f01&amp;id=02235833b4" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
    
            <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
            <div class="mc-field-group">
                <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
            </label>
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6f9aaebaea67b078fc33b4f01_02235833b4" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </div>
            </form>
            </div>-->
            <!--End Native MailChimp Signup Form mc_embed_signup-->
        </div>
        
        
        <!-- Arrows Scroll down arrow -->
        <!-- Move it next to fp nav using javascript -->
        <div class="s-footer scrolldown">
            <a class="down btn">
                <span class="text">Scroll</span>
            </a>
        </div>
        <!-- End of Scroll down arrow -->
    </footer>
    <!-- END OF site footer -->

    
    <!-- scripts -->
    <!-- All Javascript plugins goes here -->
    <!--		<script src="//code.jquery.com/jquery-1.12.4.min.js"></script>-->
    <script src="./js/vendor/jquery-1.12.4.min.js"></script>
    <!-- All vendor scripts -->
    <script src="./js/vendor/all.js"></script>
    <script src="./js/particlejs/particles.min.js"></script>
    
    
    <!-- Detailed vendor scripts : for manual update -->
    <!--<script src="./js/vendor/swiper.min.js"></script>
    <script src="./js/vendor/scrolloverflow.min.js"></script>
    <script src="./js/vendor/jquery.fullPage.min.js"></script>
    <script src="./js/vegas/vegas.min.js"></script>
    <script src="./js/vendor/jquery.maximage.js"></script>-->
    
    <!-- Countdown script -->
    <script src="./js/jquery.downCount.js"></script>
    
    <!-- Form script -->
    <script src="./js/form_script.js"></script>
    
    <!-- Javascript main files -->
    <script src="./js/main.js"></script>
    
        
    
    <!-- Google Analytics: Uncomment and change UA-XXXXX-X to be your site"s ID. -->
    <!--<script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src="//www.google-analytics.com/analytics.js";
            r.parentNode.insertBefore(e,r)}(window,document,"script","ga"));
            ga("create","UA-XXXXX-X");ga("send","pageview");
        </script>-->
    
        
    <!-- Uncomment below to enable particles scripts -->
    <!--<script src="./js/particlejs/particles-init.js"></script>-->
    <!--<script src="./js/particlejs/particles-init-snow.js"></script>-->
</body>

</html>

