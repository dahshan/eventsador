<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Eventsador Admin</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/fullcalendar.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/uniform.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/select2.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/matrix-style.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/matrix-media.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jasny-bootstrap.min.css" />
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap-datetimepickerr.min.css" /> -->
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap-datepicker.css" />  -->
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap-timepicker.min.css" />
        <link href="<?php echo base_url(); ?>assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet" />

        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jquery.gritter.css" />-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
         <script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
        <!-- <script src="<?php echo base_url(); ?>assets/ckeditor4/ckeditor.js"></script> -->

        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
        <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.ui.custom.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.uniform.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/select2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/matrix.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/matrix.tables.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/jasny-bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.flot.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.flot.resize.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.peity.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/fullcalendar.min.js"></script>
        <!--<script src="<?php echo base_url(); ?>assets/admin/js/jquery.gritter.min.js"></script>-->
        <script src="<?php echo base_url(); ?>assets/admin/js/matrix.chat.js"></script>
        <!-- <script src="<?php echo base_url(); ?>assets/admin/js/moment-with-locales.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap-datetimepickerr.js"></script> -->
        <!-- <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap-datepicker.js"></script> -->
        
        <!--<script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>-->
        <!--<script src="<?php echo base_url(); ?>assets/admin/js/matrix.form_validation.js"></script>-->
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.wizard.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/matrix.popover.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap-timepicker.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
        

        <script src="<?php echo base_url(); ?>assets/admin/js/lc_switch.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/lc_switch.css" />



    </head>

    <body>

        <div class="message_box succ_msg" style="display:none;">
            <div class="alert alert-success message_row succ_msg_html"></div>
            <div class="message_backdrop"></div>
        </div>
        <div class="message_box err_msg" style="display:none;">
            <div class="alert alert-danger message_row err_msg_html"></div>
            <div class="message_backdrop"></div>
        </div>


        <?php if ($header) echo $header; ?>
        <?php if ($sidebar) echo $sidebar; ?>
        <?php if ($middle) echo $middle; ?>
        <?php if ($footer) echo $footer; ?>
    



    <?php if($this->uri->segment(1) == 'add_project' || $this->uri->segment(1) == 'edit_project'): ?>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script>
            $(document).ready(function(){
                $('.project_date').datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: '0',
                });
            });
        </script>

    <?php endif; ?>

    <script type="text/javascript">
        // This function is called from the pop-up menus to transfer to
        // a different page. Ignore if the value returned is a null string:
        function goPage(newURL) {

            // if url is empty, skip the menu dividers and reset the menu selection to default
            if (newURL != "") {

                // if url is "-", it is this page -- reset the menu:
                if (newURL == "-") {
                    resetMenu();
                }
                // else, send page to designated URL
                else {
                    document.location.href = newURL;
                }
            }
        }

        // resets the menu selection upon entry to this page:
        function resetMenu() {
            document.gomenu.selector.selectedIndex = 2;
        }

    </script>
    <?php if ($this->session->flashdata('successmessage')) { ?>
        <script>
            var msg = '<strong>Success!</strong> <?php echo $this->session->flashdata('successmessage'); ?>.';
            $(".succ_msg_html").html(msg);
            $(".succ_msg").fadeIn();
            $(document).ready(function () {
                if ($(".succ_msg").is(":visible")) {
                    setTimeout(function () {
                        $(".succ_msg").fadeOut();
                    }, 1500);
                }
            })
        </script>
    <?php } ?>
    <?php if ($this->session->flashdata('errormessage')) { ?>
        <script>
            var msg = '<?php echo $this->session->flashdata('errormessage'); ?>.';
            $(".err_msg_html").html(msg);
            $(".err_msg").fadeIn();
            $(document).ready(function () {
                if ($(".err_msg").is(":visible")) {
                    setTimeout(function () {
                        $(".err_msg").fadeOut();
                    }, 1500);
                }
            })
        </script>
    <?php } ?>



    <style type="text/css">
        .select2-container .select2-choice abbr{
            display: none !important;
        }
        td.imagethumb img { height: 50px; width: 50px; }
        td.imagethumb img:hover { height: auto; width: auto;position: absolute; }
    </style>



    <script type="text/javascript">
$('.selectallCheckbox').click(function() {
    if ($(this).is(':checked')) {
        $('.checkBoxClass').attr('checked', true);
        $('.checkBoxClass').parent().addClass('checked');
    } else {
        $('.checkBoxClass').attr('checked', false);
        $('.checkBoxClass').parent().removeClass('checked');
    }
});
</script>
</body>
</html>
