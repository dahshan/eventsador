<style type="text/css">
    #sidebar > ul {
    max-height: 691px;
   /* max-height: none !important;*/
}
</style>
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
    <ul id="sidemenu">
        <?php if ($this->session->userdata('admin_role_type')==1): ?>
            <li class="<?php echo ($this->router->fetch_method() == 'dashboard') ? 'active' : ''; ?>"><a href="<?php echo base_url('admin_dashboard'); ?>"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
        <?php endif ?>

        <!-- /.dropdown -->
                <!-- <li class="dropdown">
                
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-caret-down"></i><span class="user-img">123</span>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo site_url('profile');?>">User Profile</a></li>                  
                        <li><a href="<?php echo site_url('logout');?>">Logout</a></li>
                    </ul>
                </li> -->
            <?php 
            $this->load->model('admin/Adminauthmodel');
            $all_modules = $this->Adminauthmodel->get_result_data('modules', ['sub_module' => 0,'module_id' => 0,'is_active' => 'Y', 'delete_flag' => 'N'], $arrange=1);
            if (count($all_modules) > 0) {
                
                foreach ($all_modules AS $all_modules_val) {
                    $admin_u_type = $this->session->userdata('admin_role_type');//echo $admin_u_type; exit;
                    $admin_uid = $this->session->userdata('admin_uid');
                    $this_user_role = $this->Adminauthmodel->get_row_data('role', ['id' => $admin_u_type]);
                    //echo $this_user_role->permited_module;exit;
                    if (isset($this_user_role->permited_module) && $this_user_role->permited_module != '') {
                        $permited_module = explode(',', $this_user_role->permited_module);
                    } else {
                        $permited_module = [];
                    }
                    // if ($all_modules_val->id == 17){
                    // var_dump($admin_u_type == 1); var_dump($all_modules_val->has_sub_module=="1"); var_dump(in_array($all_modules_val->id, $permited_module));exit;
                    // }
                    //print_r($permited_module);exit;
                    if (($admin_u_type == 1) || $all_modules_val->has_sub_module=="1" || (in_array($all_modules_val->id, $permited_module))) {
                        if($all_modules_val->has_sub_module=="1" ){
                            if(in_array($all_modules_val->id, $permited_module)){
                        ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                            <i class="fa fa-caret-down"></i><span class="user-img"><?php echo ucfirst($all_modules_val->module_name); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                        <?php $get_sub_module=$this->Adminauthmodel->get_result_data('modules', ['sub_module' => $all_modules_val->id,'is_active' => 'Y', 'delete_flag' => 'N']);
                            if(!empty($get_sub_module)){
                                foreach($get_sub_module as $val){
                                    if(($admin_u_type == 1) || in_array($val->id, $permited_module)) {?>
                            <li class="<?php echo ($this->router->fetch_module() == $val->module_name || $this->uri->segment(1) == $val->url_slag) ? 'active' : ''; ?>">
                                <a href="<?php echo base_url($val->url_slag); ?>">
                                    <?php echo ucfirst($val->module_icon); ?>
                                    <span>
                                        <?php echo ucfirst($val->module_name); ?>
                                    </span>
                                </a>
                            </li>
                            <?php } } }?>
                        </ul>
                    </li>
                    <?php } }else{ 
                    $this_user_role = $this->Adminauthmodel->get_row_data('role', ['id' => $admin_u_type]);
                    $permitted = $this_user_role->permited_module; $permitted_arr = explode(',', $permitted);//echo $all_modules_val->module_id; exit;//echo $permitted;exit;
                    // if (in_array($all_modules_val->id, $permitted_arr)) :
                    ?>
                    <li class="<?php echo ($this->router->fetch_module() == $all_modules_val->module_name || $this->uri->segment(1) == $all_modules_val->url_slag) ? 'active' : ''; ?>">
                        <a href="<?php echo base_url($all_modules_val->url_slag); ?>">
                            <?php echo ucfirst($all_modules_val->module_icon); ?>
                            <span>
                                <?php echo ucfirst($all_modules_val->module_name); ?>
                            </span>
                        </a>
                    </li>
                    <?php
                    // endif;
                    }
                }
            }
        }
        ?>
    </ul>
</div>
