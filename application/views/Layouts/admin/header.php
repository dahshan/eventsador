<!--Header-part-->
<div id="header" style="background-color: #FFF;">

  <!--<h3><a href="dashboard.html">Eventsador</a></h3>-->
  <h3><img style="height: 19px;width: 180px;margin: 29px 20px;" src="<?php echo base_url(); ?>assets/front/images/Logo.png" alt="Logo" /></h3>

</div>
<!--close-Header-part-->

<?php
$admin_detail = json_decode($this->session->userdata("admin_detail"));
?>

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse" style="background-color: #1f262d;">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>
    <span class="text">Welcome <?php if(!empty($admin_detail->fname)) echo $admin_detail->fname; ?></span>
    <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo base_url('admin_edit_profile'); ?>"><i class="icon-user"></i> My Profile</a></li>
        <li><a href="<?php echo base_url('admin_change_password'); ?>"><i class="icon-key"></i> Change Password</a></li>
      </ul>
    </li>

    <li class="">
      <a title="" href="<?php echo base_url('admin_logout'); ?>"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a>
    </li>
  </ul>
</div>
<!--close-top-Header-menu-->
