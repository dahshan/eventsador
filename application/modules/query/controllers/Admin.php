<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for Query [HMVC]. Handles all the datatypes and methodes required for Query section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Querymodel');
    }

    /**
     * Index Page for this Query controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Querymodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'query/admin/list';
        $this->admin_layout();
    }

    /**
     * View function of Query module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function view($id) {
        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data'] = $this->Querymodel->get_row_data('query', ['id' => $id]);;
        $this->middle = 'query/admin/view';
        $this->admin_layout();
    }
    
}

/* End of file admin.php */
/* Location: ./application/modules/query/controllers/admin.php */
