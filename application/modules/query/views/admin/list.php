<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-query"></i>Query</a>
        </div>

        <h1>Query</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Sent By</th>
                                    <th>Sender Email</th>
                                    <th>Sent to</th>
                                    <th>Reply status</th>
                                    <th>Sent at</th>
                                    <th>action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <?php
                                                echo (isset($data->name) && $data->name != '') ? $data->name : '';
                                                ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php
                                                echo (isset($data->email) && $data->email != '') ? $data->email : '';
                                                ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php
                                                $organizer_data = $this->Querymodel->get_row_data('organizer', ['id' => $data->sent_to]);
                                                echo ((isset($organizer_data->f_name) && $organizer_data->f_name != '') ? $organizer_data->f_name : '') . ' ' . ((isset($organizer_data->l_name) && $organizer_data->l_name != '') ? $organizer_data->l_name : '');
                                                ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo ($data->reply_status == 'Y') ? 'Yes' : "No"; ?>
                                            </td>
                                            <td class="custom-action-btn"><?php echo date("jS M, Y", strtotime($data->entry_date)); ?></td>
                                            <td class="custom-action-btn">
                                                <a href="<?php echo base_url('admin_view_query' . '/' . $data->id); ?>">
                                                    <i title="View" class="fa fa-eye" aria-hidden="false"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>