<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom">
                <i class="icon-home"></i> Home
            </a> 
            <a href="<?php echo base_url('admin_query'); ?>">Student</a> 
        <h1>View Student</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>View Student</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal">

                            <div class="control-group">
                                <label class="control-label">Sent By:</label>
                                <div class="controls">
                                    <?php
                                    echo (isset($data->name) && $data->name != '') ? $data->name : '';
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Sender Email:</label>
                                <div class="controls">
                                    <?php
                                    echo (isset($data->email) && $data->email != '') ? $data->email : '';
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Sent to:</label>
                                <div class="controls">
                                    <?php
                                    $organizer_data = $this->Querymodel->get_row_data('organizer', ['id' => $data->sent_to]);
                                    echo ((isset($organizer_data->f_name) && $organizer_data->f_name != '') ? $organizer_data->f_name : '') . ' ' . ((isset($organizer_data->l_name) && $organizer_data->l_name != '') ? $organizer_data->l_name : '');
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Query:</label>
                                <div class="controls">
                                    <?php
                                    echo (isset($data->query) && $data->query != '') ? $data->query : '';
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Reply status:</label>
                                <div class="controls" id="all_alergy_checkbox">
                                    <?php echo ($data->reply_status == 'Y') ? 'Yes' : "No"; ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Reply:</label>
                                <div class="controls">
                                    <?php
                                    echo (isset($data->reply) && $data->reply != '') ? $data->reply : 'NA';
                                    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>