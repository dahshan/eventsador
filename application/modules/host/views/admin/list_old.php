<style type="text/css">
    /*.fg-toolbar {width: 100vw;}*/
    div.dataTables_wrapper .ui-widget-header {
    width: 105.8%;
}
    .table-responsive{
                width: 100%;
    margin-bottom: 15px;
    overflow-y: hidden;
    -ms-overflow-style: -ms-autohiding-scrollbar;
    }
</style>
<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-street-view"></i>Host</a>
        </div>

        <h1>Host</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <?php if(in_array(61, $getRole)){ ?><button class="btn btn-success btn-cls" type="button" onclick="add();">Add Host</button><?php } ?>
                    </div>
                    <div class="widget-content nopadding table-responsive">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Image</th>
                                    <th>Location</th>
                                    <th>Company</th>
                                    <th>Website</th>
                                    <th>Position Title</th>
                                    <?php if(in_array(63, $getRole)){ ?><th>Status</th><?php } ?>
                                    <?php if(in_array(64, $getRole)){ ?><th>Action</th><?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <?php if(in_array(62, $getRole)){ ?><u><a href="<?php echo base_url('admin_update_host' . '/' . $data->id); ?>">
                                                    <?php echo (isset($data->name) & $data->name!='')?$data->name:''; ?>
                                                </a></u><?php }else{ echo (isset($data->name) & $data->name!='')?$data->name:''; } ?>
                                            </td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->emailid)) ? $data->emailid : "NA"; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->phoneno)) ? $data->phoneno : "NA"; ?></td>
                                            <td><img style="height: 50px;width: 50px;" <?php if(!empty($data->profile_image)){ ?> src="<?php echo base_url('assets/upload/appuser'); ?>/<?php echo $data->profile_image; ?>" <?php }else{ ?> src="<?php echo base_url('assets/upload/default_man.png'); ?>" <?php } ?> ></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->location)) ? $data->location : "NA"; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->company)) ? $data->company : "NA"; ?></td>
                                             <td class="custom-action-btn"><?php echo (!empty($data->website)) ? $data->website : "NA"; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->position_title)) ? $data->position_title : "NA"; ?></td>
                                            <?php if(in_array(63, $getRole)){ ?><td class="custom-action-btn">
                                                <?php
                                                if ($data->is_active == "Y") {
                                                    echo '<i title="De-activate" class="fa fa-close" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                } else {
                                                    echo '<i title="Activate" class="fa fa-check" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                }
                                                ?>
                                            </td><?php } ?>

                                            <?php if(in_array(64, $getRole)){ ?><td class="custom-action-btn">
                                                <!-- <i title="Reset Password" class="fa fa-repeat" style="margin:0 3px 2px 0;" aria-hidden="false" onclick="reset_passwd(<?php echo $data->id; ?>);"></i> -->
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                            </td><?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add Sponsor Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_host'); ?>";
    }

    /**
     * Status Change Function
     *
     * @param   rec_id
     * @return  redirects and gives control to status handler
     */
    function change_status(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_status_host'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to change status of this host?");
        }).modal("show");
    }

    /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_delete_host'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this host?");
        }).modal("show");
    }
</script>
