<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<!-- datatable for pdf -->

<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
 <!-- <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script> --> 

<style type="text/css">
    .dataTables_filter{
        margin-top: -36px;
    }
</style>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-calendar-o"></i>Host</a>
        </div>

        <h1>Host</h1>
    </div>
    <div class="container-fluid">
        <?php if(in_array(61, $getRole)){ ?><button class="btn btn-success btn-cls" type="button" onclick="add();">Add Host</button><?php } ?>
        <div class="row-fluid">

            <div class="span12">
                <div class="widget-box">
                	<div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Select Event</h5>
                    </div>


                    <div class="widget-content nopadding">
                       	<form class="form-horizontal" method="post">
                       		<div class="control-group">                            
	                       		<!-- <div class="span1">&nbsp;</div> -->
		                        <div class="span12">
		                            <label class="control-label">Event Name</label>
		                            <div class="controls">
		                                <select name="event" id="event">
		                                 <option value="">Select</option>
		                                <?php
		                            if (!empty($all_data)) {
		                                foreach ($all_data as $data) {
		                                    ?>
		                                <option value="<?php echo $data->id; ?>"><?php echo $data->event_name; ?></option>
		                            <?php } } ?>
		                                </select>
		                            </div>
		                        </div>
                            </div>                           
                       	</form>  






                    </div>
                </div>    
            </div> 
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box"  id="result_rp">                    
                </div>
            </div>
        </div>
    </div>
</div>


<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add Sponsor Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_host'); ?>";
    }

    /**
     * Status Change Function
     *
     * @param   rec_id
     * @return  redirects and gives control to status handler
     */
    function change_status(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_status_host'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to change status of this host?");
        }).modal("show");
    }

    /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_delete_host'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this host?");
        }).modal("show");
    }
</script>


<script type="text/javascript">
var myGlyph = new Image();
myGlyph.src = 'myglyph.png';

function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    return canvas.toDataURL("image/png");
}


$("#event").on('change', function(){
    if($("#event").val()!=''){
        $.post("<?php echo base_url('admin_host_search'); ?>", {event: this.value}).done(function(result){        
        //console.log(result);
        $("#result_rp").html(result);
        var namee = 'Event Report : '+$("#event option:selected").text()+')';

      var table =$('.data-table').DataTable( {
                           bSortCellsTop: true, // for add multifle tr in thead
                            dom: 'lBfrtip', // lBfrtip = sort filter, Bfrtip = no sort filter
                           buttons : [
                            //{ extend: 'pdf' }
                            { extend: 'pdfHtml5', title : function() {
                                    return namee;
                                },filename: function(){ 
                                   return 'Event Report';
                                } },
                          ]
                        } );
                      if ( ! table.data().count() ) {
                        $('.data-table').dataTable().fnDestroy();
                            $('.data-table').DataTable( {
                            searching: false,
                            bLengthChange: false
                        })
                    }
    }).fail(function(xhr, status, error){
        console.log(xhr);
        console.log(status);
        console.log(error);
    });
    }else{
        alert("Select Event");
    }
});




</script>
