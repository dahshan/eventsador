<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for accomodation [HMVC]. Handles all the datatypes and methodes required for accomodation section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Accomodationmodel');
    }

    /**
     * Index Page for this accomodation controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Accomodationmodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'accomodation/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of accomodation module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($eid,$id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('details', 'Accomodation Overview', 'trim|required');
            $this->form_validation->set_rules('category', 'Accomodation Category', 'trim|required');
            $this->form_validation->set_rules('price', 'Accomodation Price', 'trim|required');
            $this->form_validation->set_rules('currency', 'Accomodation Currency', 'trim|required');
            $this->form_validation->set_rules('latitude', 'Latitude', 'trim|required');
            $this->form_validation->set_rules('longitude', 'Longitude', 'trim|required');
            if(!empty($this->input->post('is_deal')))
            {
                $this->form_validation->set_message('deal_text', 'The is already taken');
            }
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Accomodationmodel->modify($eid,$id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Accomodation already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Accomodation modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_view_accomodation').'/'.$eid);
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        if (!empty($id)) {
            $this->data['data_single'] = $this->Accomodationmodel->load_single_data($id);
        }
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
                }
            }
        }
        else
        {
            $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
        }




        //  $this->data['all_currency'] = $this->Accomodationmodel->get_result_data('currency', ['is_active' => 'Y', 'delete_flag' => 'N']);
        $this->data['all_currency'] = $this->Accomodationmodel->load_currency();
        $role_id = $this->session->userdata('admin_role_type');
        $permission = $this->Accomodationmodel->load_permission($role_id);
        $this->data['per_add_currency'] = in_array('137', $permission) ? true : false; // add currency permission

        $this->middle = 'accomodation/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for organizer selected or not
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function organizers_check($organizers) {
        $organizers = $this->input->post('organizers');
        if (!isset($organizers) || count($organizers) == 0) {
            $this->form_validation->set_message('organizers_check', '{field} Cannotbe blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $event_id = $this->input->post('event_id');
            $event_data = $this->Accomodationmodel->get_row_data('accomodation', ['id<>' => $event_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($event_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of accomodation module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Accomodationmodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Accomodation status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Delete function of accomodation module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Accomodationmodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Accomodation deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function listaccomodation($eid) 
    {
        $all_data = $this->Accomodationmodel->get_result_data('accomodation',array("delete_flag"=>'N',"event_id"=>$eid));
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;
        $this->middle = 'accomodation/admin/listaccomodation';
        $this->admin_layout();
    }

    public function save_currency()
    {
       // echo "<pre>"; print_r($_POST); die;
        $cur_name = $_POST["cur_name"];
        $cur_code = $_POST["cur_code"];
        //$cur_symbol = $_POST["cur_symbol"];

        $data=array(
            'name' => $cur_name,
            'code' => $cur_code,
            //'symbol' => $cur_symbol,
            );
        echo $res = $this->db->insert('currency',$data);
        exit();
    }

}

/* End of file admin.php */
/* Location: ./application/modules/accomodation/controllers/admin.php */
