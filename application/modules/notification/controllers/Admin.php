<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for Event [HMVC]. Handles all the datatypes and methodes required for Event section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Notificationmodel');
    }

    /**
     * Index Page for this Event controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {

        if (!empty($_POST['delete'])) {
            admin_authenticate();
            foreach ($_POST['delete'] as $key => $value) {
                $flg = $this->Notificationmodel->delete($value);
            }
            $this->session->set_flashdata('successmessage', 'Notifications deleted successfully');
        }

        $all_data = $this->Notificationmodel->load_events();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'notification/admin/list';
        $this->admin_layout();
    }

    public function search() {
        $this->load->model('../models/admin/Adminauthmodel');
        $eventid = $_POST['event'];

        $all_data = [];
        if ($eventid) {
            $all_data = $this->Notificationmodel->load_notifications($eventid);
        }

        $data['all_data'] = $all_data;
        $data['all_data_count'] = count($all_data);
        $this->data['permitted'] = $this->Notificationmodel->get_permitted_role(); 
        $this->load->view('notification/admin/ajax_notifications',$data);
    }




    /**
     * Add/Edit function of Event module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id=NULL) {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('notification_text', 'Message', 'trim|required');
            $this->form_validation->set_rules('receivers[]', 'Receiver', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Notificationmodel->modify($id);
                if (!empty($flg)) {
                    $this->session->set_flashdata('successmessage', 'Notification sent successfully');
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_notification'));
            }
        }
        $this->data = array();
        $whr = [];
        if ($this->session->userdata('admin_role_type') == 1) $whr = ['panel_status' => '1', 'for_admin'=> '1'];
        else $whr = ['panel_status' => '1', 'for_org'=> '1'];
        $this->data['all_receiver'] = $this->Notificationmodel->get_result_data('user_type',$whr);
        // if($this->session->userdata('admin_role_type')!=1)
        // {
        //     if($this->session->userdata('admin_role_type')==2)
        //     {
        //         $this->data['all_event'] = $this->Notificationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_start_date >=' => date("Y-m-d")]);
        //     }
        //     else
        //     {
        //         if($this->session->userdata('admin_org_id')!=1)
        //         {
        //             $this->data['all_event'] = $this->Notificationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_start_date >=' => date("Y-m-d")]);
        //         }
        //     }
        // }
        // else
        // {
        //     $this->data['all_event'] = $this->Notificationmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_start_date >=' => date("Y-m-d")]);
        // }

        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('delete_flag', 'N');
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
            }
        }
        $this->db->order_by("id", "desc");

        $query = $this->db->get();
        $this->data['all_event']  = $query->result();
        
        //echo $this->db->last_query(); exit;

        $this->middle = 'notification/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for organizer selected or not
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function organizers_check($organizers) {
        $organizers = $this->input->post('organizers');
        if (!isset($organizers) || count($organizers) == 0) {
            $this->form_validation->set_message('organizers_check', '{field} Cannotbe blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $event_id = $this->input->post('event_id');
            $event_data = $this->Notificationmodel->get_row_data('event', ['id<>' => $event_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($event_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of Event module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Notificationmodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Event status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_notification'));
    }

    /**
     * Delete function of Event module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Notificationmodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Notification deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_notification'));
    }

}

/* End of file admin.php */
/* Location: ./application/modules/event/controllers/admin.php */
