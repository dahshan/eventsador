<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>

                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>                       
                    </div>
                    <div class="widget-content nopadding">
                        <div>
                            <h4 style="margin-left: 10px;">
                                Total Messages: <?php echo $all_data_count; ?>
                            </h4>
                        </div>
                        <form id="delete" method="post" action="<?php echo base_url('admin_notification'); ?>">

                        <table id="table" class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Title</th>
                                    <th>Message</th>
                                    <th>Receivers</th>
                                    <th>Date</th>
                                    <?php if(in_array(127, $getRole)){ ?><th>Action</th><?php } ?>
                                    <?php if(in_array(127, $getRole)){ ?> <th>Select</th><?php } ?>

                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->notification_title)) ? $data->notification_title : "NA"; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->notification_text)) ? $data->notification_text : "NA"; ?></td>
                                            <td class="custom-action-btn">
                                                <?php //echo $data->receivers;
                                                if (isset($data->receivers) && $data->receivers != '' && $data->receivers > 0) {
                                                    if($data->receivers=="all")
                                                    {
                                                        echo "All";

                                                    }else
                                                    {
                                                        $receivers = explode(',', $data->receivers);
                                                        foreach ($receivers AS $rcv) {
                                                            $receiver_data = $this->Notificationmodel->get_row_data('user_type', ['id' => $rcv]);
                                                            if(!empty($receiver_data))
                                                            {
                                                                echo $receiver_data->type_name.'<br>';
                                                            }
                                                        }
                                                    }
                                                }else if ($data->receivers == 0){
                                                    echo $user = $this->Notificationmodel->get_user_of_notification($data->id);
                                                } else {
                                                    echo 'NA';
                                                }
                                                ?>
                                            </td>
                                            <td class="custom-action-btn"><?php echo date("jS M, Y", strtotime($data->created_date)); ?></td>
                                            <?php if(in_array(127, $getRole)){ ?><td class="custom-action-btn">
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                            </td><?php } ?>
                                            <?php if(in_array(127, $getRole)){ ?> 
                                                <td class="custom-action-btn">
                                                    <input type='checkbox' name='delete[]' value='<?php echo $data->id; ?>' >
                                                </td>
                                            <?php } ?> 

                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php if(in_array(127, $getRole)){ ?><button class="btn btn-danger btn-cls" type="button" style="margin-top: 20px;" onclick="delete_notifications();">Delete Notifications</button><?php } ?>
                        </form>


                    </div>