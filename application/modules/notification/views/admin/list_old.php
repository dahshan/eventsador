<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-envelope-square"></i>Notification</a>
        </div>

        <h1>Notification</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                       <?php if(in_array(126, $getRole)){ ?> <button class="btn btn-success btn-cls" type="button" onclick="add();">Add Notification</button><?php } ?>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Event</th>
                                    <th>Title</th>
                                    <th>Message</th>
                                    <th>Receivers</th>
                                    <th>Date</th>
                                    <?php if(in_array(127, $getRole)){ ?><th>Action</th><?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->event_name; ?>
                                            </td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->notification_title)) ? $data->notification_title : "NA"; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->notification_text)) ? $data->notification_text : "NA"; ?></td>
                                            <td class="custom-action-btn">
                                                <?php //echo $data->receivers;
                                                if (isset($data->receivers) && $data->receivers != '' && $data->receivers > 0) {
                                                    if($data->receivers=="all")
                                                    {
                                                        echo "All";

                                                    }else
                                                    {
                                                        $receivers = explode(',', $data->receivers);
                                                        foreach ($receivers AS $rcv) {
                                                            $receiver_data = $this->Notificationmodel->get_row_data('user_type', ['id' => $rcv]);
                                                            if(!empty($receiver_data))
                                                            {
                                                                echo $receiver_data->type_name.'<br>';
                                                            }
                                                        }
                                                    }
                                                }else if ($data->receivers == 0){
                                                    echo $user = $this->Notificationmodel->get_user_of_notification($data->id);
                                                } else {
                                                    echo 'NA';
                                                }
                                                ?>
                                            </td>
                                            <td class="custom-action-btn"><?php echo date("jS M, Y", strtotime($data->created_date)); ?></td>
                                            <?php if(in_array(127, $getRole)){ ?><td class="custom-action-btn">
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                            </td><?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add Event Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_notification'); ?>";
    }

    /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_delete_notification'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this Notification?");
        }).modal("show");
    }

</script>
