<style>
    .children-inline *{
        display: inline-block;
    }
</style>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_notification'); ?>" ><i class="fa fa-envelope-square"></i>Notification</a>
            <a href="javascript:void(0);" class="current">Notification Management</a>
        </div>

        <h1>Notification</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Notification</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('admin_add_notification'); ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Select Event</label>
                                <div class="controls">
                                    <select name="event_id" >
                                        <?php
                                        if (count($all_event) > 0) {
                                            foreach ($all_event AS $event) {
                                                $users = explode(',',$event->users);
                                                if(($this->session->userdata('admin_role_type')==1) || (in_array($this->session->userdata('admin_uid'),$users)) || ($this->session->userdata('admin_role_type')==2) ){
                                                ?>
                                                <option value="<?php echo (isset($event->id) && $event->id != '') ? $event->id : ''; ?>"
                                                        <?php if(!empty($data_single)){ if($event->id==$data_single->event_id){ ?> selected <?php } }?>>
                                                            <?php echo (isset($event->event_name) && $event->event_name != '') ? $event->event_name : ''; ?> 
                                                </option>
                                                <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('event_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Title</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Title" type="text" id="notification_title" name="notification_title" value="<?php echo (!empty(set_value('notification_title'))) ? set_value('notification_title') : ((!empty($data_single->notification_title)) ? $data_single->notification_title : ''); ?>" >
                                    <?php echo form_error('notification_title', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Message</label>
                                <div class="controls">
                                    <textarea class="span11" id="notification_text" name="notification_text"><?php echo (!empty(set_value('notification_text'))) ? set_value('notification_text') : ((!empty($data_single->notification_text)) ? $data_single->notification_text : ''); ?></textarea>
                                    <?php echo form_error('notification_text', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Select Group</label>
                                <div class="controls">
                                    <select name="receivers[]" multiple >
                                    <option value="all">Select All</option>
                                        <?php
                                        if (count($all_receiver) > 0) {
                                            if (isset($data_single->receivers) && $data_single->receivers != '') {
                                                $rcvr_selected = explode(',',$data_single->receivers);
                                            } else {
                                                $rcvr_selected = [];
                                            }
                                            foreach ($all_receiver AS $rcv) {
                                                ?>
                                                <option value="<?php echo (isset($rcv->id) && $rcv->id != '') ? $rcv->id : ''; ?>"
                                                        <?php echo in_array($rcv->id, $rcvr_selected)?'selected':''; ?>>
                                                            <?php echo (isset($rcv->type_name) && $rcv->type_name != '') ? $rcv->type_name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('receivers[]', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label">How to</label>
                                <div class="controls children-inline">
                                    <input id="via_email" name="via_email" value="1" type="checkbox"/><label for="via_email" style="margin-right: 15px;">Via Email</label>
                                    <input id="via_noti" name="via_noti" value="1" type="checkbox" style="margin-left: 0px;"/><label for="via_noti">Via Notification</label>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        // $('.datepicker').datepicker();
        // $('.timepicker').timepicker({defaultTime: false});
    })
</script>