<?php

/**
 * Event Model Class. Handles all the datatypes and methodes required for handling Event
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Notificationmodel extends CI_Model
{

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the Event that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data()
    {
        $this->db->select('n.*,e.event_name');
        $this->db->from(tablename('notification as n'));
        $this->db->join("event as e", "n.event_id = e.id");
        $this->db->order_by("id", "desc");
        if ($this->session->userdata('admin_role_type') != 1) {
            if ($this->session->userdata('admin_role_type') == 2) {
                $this->db->where('e.organizers', $this->session->userdata('admin_uid'));
            } else {
                if ($this->session->userdata('admin_org_id') != 1) {
                    $this->db->where('e.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(" . $this->session->userdata('admin_uid') . ", users) !=", 0);
            }


        }
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }


    /**
     * Used for loading functionality of Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the Event that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_events() {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('delete_flag', 'N');
       // echo "<pre>";print_r($this->session->userdata('admin_role_type'));exit;
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(".$this->session->userdata('admin_uid').", users) !=", 0);
            } 
        }
        $this->db->order_by("id", "desc");

        $query = $this->db->get();
        $result = $query->result();
        //echo $this->db->last_query();
        // echo "<pre>";print_r($result);exit;
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

   
    public function load_event($eventid) {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('id', $eventid);
        
        $query = $this->db->get();
        $result = $query->first_row();

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

   
    public function load_notifications($eventid) {
        $this->db->select('*');
        $this->db->from(tablename('notification'));
        $this->db->where("event_id", $eventid);
        
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }




    /**
     * Used to get the admin's permitted module arr
     */
    public function get_permitted_role()
    {
        $role_type = $this->session->userdata('admin_role_type');
        $role = $this->get_row_data('role', ['id' => $role_type]);
        return explode(',', $role->permited_module);
    }


    /**
     * Used for loading functionality of single Event by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single Event by id that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "")
    {
        $this->db->select('*');
        $this->db->from(tablename('notification'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1", $optional_where = NULL)
    {
        if (empty($optional_where)) {
            $query = $this->db->get_where(tablename($table), $where);
        } else {
            $this->db->from(tablename($table));
            if (!empty($where)) $this->db->where($where);
            $this->db->where_in('id', $optional_where);
            $query = $this->db->get();
        }
        // return $where;
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single Event w.r.t. current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify()
    {
        $data['notification_title'] = $this->input->post('notification_title');
        $data['notification_text'] = $this->input->post('notification_text');
        $data['event_id'] = $this->input->post('event_id');
        $receivers = $this->input->post('receivers');
        $data['receivers'] = implode(',', $receivers);

        // var_dump($this->input->post('via_email')).'<br>';
        // var_dump(!empty($this->input->post('via_noti'))); exit;

        foreach ($receivers as $val) {
            if ($val == "all") {
                $data['receivers'] = "all";
                break;
            }
        }
        $data['created_date'] = date("Y-m-d H:i:s");
        $this->db->insert(tablename('notification'), $data);
        $last_id = $this->db->insert_id();
        if (!empty($last_id)) {
            //var_dump($receivers);exit;
            if ($data['receivers'] == 'all') {
                $type_list = $this->get_result_data("user_type", array("panel_status" => '1'));
            } else {
                $type_list = $this->get_result_data("user_type", '', $receivers);
            }

            if (!empty($type_list)) {
                //print_r($type_list);exit;
                $event_info = $this->get_row_data("event", array("id" => $this->input->post('event_id')));
                $users = [];
                $user_ids = [];
                foreach ($type_list as $val) {
                    //$details=$this->get_result_data($val->table_slug,'',explode(",",$event_info->{strtolower($val->type_name)}));

                    //   if($val->table_slug!='appuser'){
                    if ($val->table_slug == 'apiappuser') {
                        $val->table_slug = 'user';
                        $val->type_name = 'attendees';
                    }
                    $val->table_slug = 'user';
                    if ($val->type_name == 'Organizers') $val->type_name = 'users';
                    //echo 'hello'.$val->type_name;exit;

                    $details = $this->get_result_data($val->table_slug, '', explode(",", $event_info->{strtolower($val->type_name)}));
                    if ($details) {
                        foreach ($details as $one) {
                            if (!in_array($one->id, $user_ids)) {
                                $user_ids[] = $one->id;
                                $users[] = $one;
                            }
                        }
                    }
                    //   }else{
                    //     $details=array();
                    //   }

                    //var_dump($details);exit;


                }

                $via_email = !empty($this->input->post('via_email'));
                $via_notification = !empty($this->input->post('via_noti'));

                if (!empty($users)) {
                    foreach ($users as $value) {
                        if ($via_notification) {
                            $title = $this->input->post('notification_title');
                            $message = $this->input->post('notification_text');
                            $payload = ['title' => $title, 'message' => $message, 'callback' => "ANNOUNCE", 'event_id' => $this->input->post('event_id'), 'user_id' => $value->id];
                            $this->processNotification($last_id, $value->id);
                            $this->notification($value->fcm_reg_token, "", "", $payload, $value->device_type);
                        }

                        if ($via_email) {
                            if ($val->table_slug == "user") {
                                $value->email = $value->emailid;
                            }
                            $subject = "Eventsador - Notification Received";
                            $message = '<p>Greetings ' . $value->name . ",</p>";
                            $message .= '<p>' . $this->input->post('notification_title') . '</p>';
                            $message .= '<p>' . $this->input->post('notification_text') . '</p>';
                            // $message .= '<br><strong>Password: </strong>' . $password . "</p>";
                            $message .= '<p>Thank you,</p>';
                            $message .= '<p>Eventsador Admin</p>';

                            $mail_data = [
                                'name' => $value->name,
                                'body' => $message,
                            ];
                            $this->load->helper('email');
                            send_email($value->email, $subject, $mail_data);
                        }
                    }
                }

            }
            return $last_id;
        } else {
            return "";
        }
    }

    /**
     * Used for delete functionality of Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_event]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id)
    {
        $this->db->delete(tablename('notification_touser'), ['noti_id' => $id]);
        $this->db->where('id', $id);
        if ($this->db->delete(tablename('notification'), ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where)
    {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

    public function notification_old($fcm, $text, $title, $payload = false, $type = 'android')
    {
        $msg = $title;
        $msgg = $text;
        $this->load->library('gcmpushmessage', 'AAAAQQY2SPA:APA91bHXm3KVLxckAVs2-hYNfbif6fxdN-eXTEHNUnBG3efje1vvv2aYNTJs2usgZNmDFeHy5EH0wwynQ4vtG6t8DYb-MuDfJC4KOd_Y1kgOowCJOh1pe9ZF8avPpI80oCM_ffWGl6Mu');
        // $gcmregtoken = "ckpnfYyp5jc:APA91bFd8ZVoSHFd9URKne44NqgOMOZZk6Ix1aBTmfY8Y2bUloY3Sgl4tHjJ5uJgoLmUUuMVCBSC6vggK4ZbhtoFPfjLFn1lOmuISGwIETV-hw2UT_rKw57em5-XLGTdsLbP9GvNBBly";
        $gcmregtoken = $fcm;
        $this->gcmpushmessage->setDevices($gcmregtoken);

        $r = $this->gcmpushmessage->send($text, array('title' => $title, 'body' => "Eventsador", 'payload' => $payload));
        // print_r($r);die;
        return $r;

    }

    public function notification($fcm, $text, $title, $payload = false, $type = 'android')
    {
        $this->load->library('PushNotification');
        $r = false;
        if ($type == 'android') $r = $this->pushnotification->sendNotificationAndroid($fcm, 'Eventsador', $text, $payload);
        else if ($type == 'ios') $r = $this->pushnotification->sendNotificationIos($fcm, $title, $text, $payload);
        else $r = false;
        return $r;
    }

    public function processNotification($noti_id, $receiver_id)
    {
        $this->db->insert('notification_touser', ['noti_id' => $noti_id, 'user_id' => $receiver_id]);
        return $this->db->insert_id();
    }

    public function processNotification_old($payload)
    {
        $sender = $payload['sender_id'];
        $receiver = $payload['receiver_id'];
        $whr = "((receiver_id=$receiver AND sender_id=$sender) OR (sender_id=$receiver AND receiver_id=$sender))";
        $chat_head = $this->db->select('*')->where($whr)->where('event_id', $payload['event_id'])->get('chat_head')->row();

        if ($payload['sender_id'] == $payload['receiver_id']) return;
        $chat_data = ['event_id' => $payload['event_id'], 'sender_id' => $payload['sender_id'], 'receiver_id' => $payload['receiver_id'],
            'message' => '<' . $payload['title'] . '>' . $payload['message'], 'say_hi' => '0', 'flag' => '0', 'created_date' => date('Y-m-d H:i:s')];
        if (!$chat_head) {
            $chat_head_data = ['event_id' => $payload['event_id'], 'sender_id' => $payload['sender_id'], 'receiver_id' => $payload['receiver_id']
                , 'message' => '<' . $payload['title'] . '> ' . $payload['message'], 'say_hi' => '1', 'created_date' => date('Y-m-d H:i:s')];
            $this->db->insert('chat_head', $chat_head_data);

            $chat_data['flag'] = '0';
            $chat_data['say_hi'] = '1';
        }
        $this->db->insert('attendees_chat', $chat_data);
    }

    public function updateToken($user_id, $token, $platform = 'android')
    {
        $this->db->where('id', $user_id);
        $this->db->update('user', ['fcm_reg_token' => $token, 'device_type' => $platform]);
        return $this->db->affected_rows();
    }

    public function deletePreviousTokens($token)
    {
        $this->db->where('fcm_reg_token', $token);
        $this->db->update('user', ['fcm_reg_token' => '']);
    }

    public function get_user_of_notification($noti_id)
    {
        $query = $this->db->where('noti_id', $noti_id)->get('notification_touser');//return $query->num_rows();
        if ($query) {
            $rows = $query->result();
            $user_names = [];
            foreach ($rows as $row) {
                $user = $this->db->where('id', $row->user_id)->get('user')->row();
                if ($user) {
                    $user_names[] = $user->name;
                    return $user->name;
                }
            }
            return trim(implode(',', $user_names));
        } else {
            return '-';
        }
    }

}



/* End of file Eventmodel.php */
/* Location: ./application/modules/event/models/admin/Eventmodel.php */
