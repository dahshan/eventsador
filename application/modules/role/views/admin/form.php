<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_role'); ?>" ><i class="fa fa-user-secret"></i>Role Management</a><a href="javascript:void(0);" class="current"></i>Role Management Form</a>
        </div>

        <h1>Role</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Role</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_role') : base_url('admin_update_role') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Designation</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Designation" type="text" id="f_name" name="name" value="<?php echo (!empty(set_value('name'))) ? set_value('name') : ((!empty($data_single->name)) ? $data_single->name : ''); ?>" >
                                    <?php echo form_error('name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Choose Modules to give permission:</label>
                                <div class="controls">

                                <input class="selectallCheckbox"  name="all_role" type="checkbox" value="1" <?php echo ((!empty($data_single->all_role) && $data_single->all_role==1) ? 'checked' : ''); ?>>Select All
                                    <?php
                                    if (count($all_module) > 0) { ?>
                                    <ul>
                                    <?php
                                        foreach ($all_module AS $all_module_val) {
                                            if($all_module_val->has_sub_module==0){
                                                if (isset($data_single->permited_module) && $data_single->permited_module != '') {
                                                    $permited_module = explode(',', $data_single->permited_module);
                                                } else {
                                                    $permited_module = [];
                                                }
                                                //if($all_module_val->has_sub_module==0){
                                                ?>
                                                <li>
                                                <input class="span11 checkBoxClass" type="checkbox" name="modules[]" value="<?php echo (isset($all_module_val->id) && $all_module_val->id != '') ? $all_module_val->id : ''; ?>" <?php echo (in_array($all_module_val->id, $permited_module) ? 'checked' : ''); ?>>

                                                <?php //}else{ ?>
                                               <!--  <input disabled checked class="span11" type="checkbox" name="modules[]" value="<?php echo (isset($all_module_val->id) && $all_module_val->id != '') ? $all_module_val->id : ''; ?>" > -->
                                                <?php

                                                    //} 
                                                    echo (isset($all_module_val->module_name) && $all_module_val->module_name != '') ? '<b>'.ucwords($all_module_val->module_name).'</b>' : ''; ?>
                                                
                                                <?php // get functions
                                                $modules_functions = $this->Adminauthmodel->get_result_data('modules', ['module_id' => $all_module_val->id,'is_active' => 'Y', 'delete_flag' => 'N']);
                                                if(!empty($modules_functions)){ ?>
                                                <ul>
                                                <?php
                                                foreach ($modules_functions AS $functions) { ?>
                                                    
                                                <li>
                                                <input class="span11 checkBoxClass" type="checkbox" name="modules[]" value="<?php echo (isset($functions->id) && $functions->id != '') ? $functions->id : ''; ?>" <?php echo (in_array($functions->id, $permited_module) ? 'checked' : ''); ?>> <?php echo (isset($functions->module_name) && $functions->module_name != '') ? $functions->module_name : ''; ?>


                                                    <?php // get functions
                                                    $modules_functions_inside = $this->Adminauthmodel->get_result_data('modules', ['module_id' => $functions->id,'is_active' => 'Y', 'delete_flag' => 'N']);
                                                    if(!empty($modules_functions_inside)){ ?>
                                                    <ul>
                                                    <?php
                                                    foreach ($modules_functions_inside AS $functions_inside) { ?>
                                                        
                                                    <li>
                                                    <input class="span11 checkBoxClass" type="checkbox" name="modules[]" value="<?php echo (isset($functions_inside->id) && $functions_inside->id != '') ? $functions_inside->id : ''; ?>" <?php echo (in_array($functions_inside->id, $permited_module) ? 'checked' : ''); ?>> <?php echo (isset($functions_inside->module_name) && $functions_inside->module_name != '') ? $functions_inside->module_name : ''; ?>

                                                    </li>
                                                   <?php } ?> </ul>



                                                    <?php }


                                                     ?>




                                                </li>
                                               <?php } ?> </ul>



                                                <?php }


                                                 ?>

                                                </li>
                                                <?php
                                            }
                                        } ?> 
                                        </ul>
                                <?php }
                                    ?>
                                    <?php echo form_error('modules[]', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <input type="hidden" name="role_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    ul {
  list-style-type: none;
}
</style>
<!-- <script type="text/javascript">
    function CheckAllChk(all){
        console.log(all);
    $(".checkBoxClass").prop('checked', $(all).prop('checked'));
}
</script> -->