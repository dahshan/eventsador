<?php

/**
 * role Model Class. Handles all the datatypes and methodes required for handling role
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Rolemodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of role for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the role that has been added by current admin [Table:  ets_role]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('*');
        $this->db->from(tablename('role'));
        $this->db->where('delete_flag', 'N');
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    public function load_data() {
        $this->db->select('*');
        $this->db->from(tablename('role'));
        $this->db->where('delete_flag', 'N');
        $this->db->where('is_active', '1');
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single role by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single role by id that has been added by current admin [Table:  ets_role]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('role'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1", $arrange=null) {
        if ($arrange) {
            $query = $this->db->order_by('arrange', 'DESC')->get_where(tablename($table), $where);
        } else {
            $query = $this->db->get_where(tablename($table), $where);
        }
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single role for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single role w.r.t. current admin [Table:  ets_role]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        
       // echo "<pre>"; print_r($_POST); die;
        $name = $this->input->post('name');
        $date = date("Y-m-d H:i:s");
        $modules_post = $this->input->post('modules');

        $all_role = isset($_POST['all_role']) ? $_POST['all_role'] : 0;
        // for logistic show
        $logistic_submenus = array("0"=>"20","1"=>"21","3"=>"22","4"=>"23","5"=>"24","6"=>"30","7"=>"31");

        $check = array_intersect($logistic_submenus, $modules_post);
        //echo "<pre>"; print_r($check); die;
        if(!empty($check)){
            array_push($modules_post, 13);
        }

        
        if (count($modules_post) > 0) {
            $modules = implode(',', $modules_post);
        } else {
            $modules = '';
        }

        if (!empty($id)) {
            $data = array(
                'name' => $name,
                'permited_module' => $modules,
                'modified_date' => $date,
                'all_role' => $all_role,
            );

            $this->db->where('id', $id)->update(tablename('role'), $data);
            return $id;
        } else {
            // Check for Duplicate
            $data = array(
                'name' => $name,
                'permited_module' => $modules,
                'all_role' => $all_role,
                'entry_date' => $date
            );

            $this->db->insert(tablename('role'), $data);
            $last_id = $this->db->insert_id();

            if (!empty($last_id)) {
                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of role for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current role status
     * and change it the the opposite [Table: pb_role]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('role'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('role'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of role for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_role]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('role'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }
}

/* End of file rolemodel.php */
/* Location: ./application/modules/role/models/admin/rolemodel.php */
