<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for User [HMVC]. Handles all the datatypes and methodes required for User section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        
        $this->load->model('admin/Rolemodel');
    }

    /**
     * Index Page for this role controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Rolemodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'role/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of role module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
           // echo "<pre>"; print_r($_POST); die;
            $this->form_validation->set_rules('name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('modules[]', 'module', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Rolemodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Role already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Role modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_role'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Rolemodel->load_single_data($id);
        $this->data['all_module'] = $this->Rolemodel->get_result_data('modules', ['url_slag!='=>'admin_role','is_active' => 'Y', 'delete_flag' => 'N'], $arrange = 1);
        $this->middle = 'role/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function new_email_check($email) {
        if (isset($email) && $email != '') {
            $user_data = $this->Rolemodel->get_row_data('user', ['emailid' => $email, 'delete_flag' => 'N']);
            if (count($user_data) > 0) {
                $this->form_validation->set_message('new_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $user_id = $this->input->post('user_id');
            $user_data = $this->Rolemodel->get_row_data('user', ['id<>' => $user_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($user_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of rolerole module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Rolemodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Role status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_role'));
    }

    /**
     * Delete function of role module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Rolemodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Role deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_role'));
    }

}

/* End of file admin.php */
/* Location: ./application/modules/user/controllers/admin.php */
