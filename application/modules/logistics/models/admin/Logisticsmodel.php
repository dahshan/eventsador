<?php

/**
 * logistics Model Class. Handles all the datatypes and methodes required for handling logistics
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Logisticsmodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of logistics for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the logistics that has been added by current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('logistics.*,event.event_name');
        $this->db->from(tablename('logistics'));
        $this->db->join(tablename('event'),"event_id=event.id");
        // $this->db->where('delete_flag', 'N');
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single logistics by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single logistics by id that has been added by current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('logistics'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1") {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    public function get_result_data1($table,$whr,$table1=NULL,$condition=NULL,$field=NULL,$tag=NULL) {
        if(!empty($table1))
        {
            $this->db->select('logistics.*,exhibitor_type.name as type_name');
            $this->db->join($table1,$condition);
        }
        if(!empty($tag))
        {
            $this->db->where("logistics.name LIKE '%$tag%' or booth LIKE '%$tag%'");
        }
        $query =$this->db->where_in($field,$whr)->get(tablename($table));
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single logistics for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single logistics w.r.t. current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        // $data['first_floor_map_description'] = $this->input->post('first_floor_map_description');
        // $data['second_floor_map_description'] = $this->input->post('second_floor_map_description');
        // $data['wireless_access'] = $this->input->post('wireless_access');
        // $data['local_attractions'] = $this->input->post('local_attractions');
        // $data['parking_instruction_description'] = $this->input->post('parking_instruction_description');
        // $data['parking_latitude'] = $this->input->post('parking_latitude');
        // $data['parking_longitude'] = $this->input->post('parking_longitude');
        // $data['transportation'] = $this->input->post('transportation');
        // $data['accomodation'] = $this->input->post('accomodation');
        $data['event_id'] = $this->input->post('event_id');
        $data['instagram_link'] = $this->input->post('instagram_link');
        $data['facebook_link'] = $this->input->post('facebook_link');
        $data['twitter_link'] = $this->input->post('twitter_link');
        $data['google_plus_link'] = $this->input->post('google_plus_link');
        $data['youtube_link'] = $this->input->post('youtube_link');
        $data['flickr_link'] = $this->input->post('flickr_link');
        // if (!empty($_FILES['first_floor_map_image']['name'])) 
        // {
        //     $this->load->library('upload');
        //     $config['upload_path'] = './assets/upload/logistics/';
        //     $config['allowed_types'] = 'gif|jpg|jpeg|png';
        //     $config['file_name'] = time();

        //     $this->upload->initialize($config);
        //     if($_FILES['first_floor_map_image']['error']==0)
        //     {
        //         $profilepic=$_FILES['first_floor_map_image']['name'];
        //         $profilepic=explode('.',$profilepic);
        //         $ext=end($profilepic);
        //         if($ext=="jpg" or $ext=="jpeg" or $ext=="png" or $ext=="bmp")
        //         {
        //             $profilepic=time().rand().".".$ext;
        //             $destination="./assets/upload/logistics/".$profilepic;
        //             if (move_uploaded_file($_FILES['first_floor_map_image']['tmp_name'] ,$destination))
        //             {
        //                 $data['first_floor_map_image']=$profilepic;
        //                 $olddestination="./assets/upload/logistics/".$oldfile;
        //                 @unlink($olddestination);
        //             }
        //         }
        //         else
        //         {
        //             $this->session->set_flashdata('errormessage', 'Only .jpg,.jpeg,.bmp and .png image extensions are supported');
        //             $redirect = site_url('admin/admin_logistics');
        //             redirect($redirect);
        //         }
        //     }
        // }
        // if (!empty($_FILES['second_floor_map_image']['name'])) 
        // {
        //     $this->load->library('upload');
        //     $config['upload_path'] = './assets/upload/logistics/';
        //     $config['allowed_types'] = 'gif|jpg|jpeg|png';
        //     $config['file_name'] = time();

        //     $this->upload->initialize($config);
        //     if($_FILES['second_floor_map_image']['error']==0)
        //     {
        //         $profilepic=$_FILES['second_floor_map_image']['name'];
        //         $profilepic=explode('.',$profilepic);
        //         $ext=end($profilepic);
        //         if($ext=="jpg" or $ext=="jpeg" or $ext=="png" or $ext=="bmp")
        //         {
        //             $profilepic=time().rand().".".$ext;
        //             $destination="./assets/upload/logistics/".$profilepic;
        //             if (move_uploaded_file($_FILES['second_floor_map_image']['tmp_name'] ,$destination))
        //             {
        //                 $data['second_floor_map_image ']=$profilepic;
        //                 $olddestination="./assets/upload/logistics/".$oldfile;
        //                 @unlink($olddestination);
        //             }
        //         }
        //         else
        //         {
        //             $this->session->set_flashdata('errormessage', 'Only .jpg,.jpeg,.bmp and .png image extensions are supported');
        //             $redirect = site_url('admin/admin_logistics');
        //             redirect($redirect);
        //         }
        //     }
        // }
        // if (!empty($_FILES['parking_instruction_map_image']['name'])) 
        // {
        //     $this->load->library('upload');
        //     $config['upload_path'] = './assets/upload/logistics/';
        //     $config['allowed_types'] = 'gif|jpg|jpeg|png';
        //     $config['file_name'] = time();

        //     $this->upload->initialize($config);
        //     if($_FILES['parking_instruction_map_image']['error']==0)
        //     {
        //         $profilepic=$_FILES['parking_instruction_map_image']['name'];
        //         $profilepic=explode('.',$profilepic);
        //         $ext=end($profilepic);
        //         if($ext=="jpg" or $ext=="jpeg" or $ext=="png" or $ext=="bmp")
        //         {
        //             $profilepic=time().rand().".".$ext;
        //             $destination="./assets/upload/logistics/".$profilepic;
        //             if (move_uploaded_file($_FILES['parking_instruction_map_image']['tmp_name'] ,$destination))
        //             {
        //                 $data['parking_instruction_map_image ']=$profilepic;
        //                 $olddestination="./assets/upload/logistics/".$oldfile;
        //                 @unlink($olddestination);
        //             }
        //         }
        //         else
        //         {
        //             $this->session->set_flashdata('errormessage', 'Only .jpg,.jpeg,.bmp and .png image extensions are supported');
        //             $redirect = site_url('admin/admin_logistics');
        //             redirect($redirect);
        //         }
        //     }
        // }
        if (!empty($id)) {
            $this->db->where('id', $id)->update(tablename('logistics'), $data);
            return $id;
        } else {
            $this->db->insert(tablename('logistics'), $data);
            $last_id = $this->db->insert_id();
            if (!empty($last_id)) {
               return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of logistics for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current logistics status
     * and change it the the opposite [Table: pb_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('logistics'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('logistics'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of logistics for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('logistics'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

}

/* End of file Organizermodel.php */
/* Location: ./application/modules/logistics/models/admin/Organizermodel.php */
