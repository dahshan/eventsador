<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-link"></i>Social Media Channels</a>
        </div>

        <h1>Social Media Link</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Links</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_logistics') : base_url('admin_update_logistics') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Select Event</label>
                                <div class="controls">
                                    <select name="event_id" id="event_id">
                                    <option></option>
                                        <?php
                                        if (count($all_event) > 0) {
                                            foreach ($all_event AS $all_event_val) {
                                                $users = explode(',',$all_event_val->users);
                                                if(($this->session->userdata('admin_role_type')==1) || (in_array($this->session->userdata('admin_uid'),$users)) || ($this->session->userdata('admin_role_type')==2) ){
                                                ?>
                                                <option value="<?php echo (isset($all_event_val->id) && $all_event_val->id != '') ? $all_event_val->id : ''; ?>"
                                                        <?php if(!empty($data_single)){ if($all_event_val->id==$data_single->event_id)echo 'selected'; }?>>
                                                            <?php echo (isset($all_event_val->event_name) && $all_event_val->event_name != '') ? $all_event_val->event_name : ''; ?> 
                                                </option>
                                                <?php 
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('event_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span6">
                                    <label class="control-label">Instagram Link</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Instagram" type="text" id="instagram_link" name="instagram_link" value="<?php echo (!empty(set_value('instagram_link'))) ? set_value('instagram_link') : ((!empty($data_single->instagram_link)) ? $data_single->instagram_link : ''); ?>">
                                        <?php echo form_error('instagram_link', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">Facebook Link</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Facebook" type="text" id="facebook_link" name="facebook_link" value="<?php echo (!empty(set_value('facebook_link'))) ? set_value('facebook_link') : ((!empty($data_single->facebook_link)) ? $data_single->facebook_link : ''); ?>" >
                                        <?php echo form_error('facebook_link', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span6">
                                    <label class="control-label">Twitter Link</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Twitter" type="text" id="twitter_link" name="twitter_link" value="<?php echo (!empty(set_value('twitter_link'))) ? set_value('twitter_link') : ((!empty($data_single->twitter_link)) ? $data_single->twitter_link : ''); ?>">
                                        <?php echo form_error('twitter_link', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">Google Plus Link</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Google Plus" type="text" id="google_plus_link" name="google_plus_link" value="<?php echo (!empty(set_value('google_plus_link'))) ? set_value('google_plus_link') : ((!empty($data_single->google_plus_link)) ? $data_single->google_plus_link : ''); ?>" >
                                        <?php echo form_error('google_plus_link', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span6">
                                    <label class="control-label">Youtube Link</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Youtube" type="text" id="youtube_link" name="youtube_link" value="<?php echo (!empty(set_value('youtube_link'))) ? set_value('youtube_link') : ((!empty($data_single->youtube_link)) ? $data_single->youtube_link : ''); ?>">
                                        <?php echo form_error('youtube_link', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">Flickr Link</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Flickr" type="text" id="flickr_link" name="flickr_link" value="<?php echo (!empty(set_value('flickr_link'))) ? set_value('flickr_link') : ((!empty($data_single->flickr_link)) ? $data_single->flickr_link : ''); ?>" >
                                        <?php echo form_error('flickr_link', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="logistics_id" id="logistics_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('change','#event_id',function(){
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url('logistics/admin/fetch_data').'/';?>' + id,
            type: "GET",
            data: {},
            success: function (response) {
                response = JSON.parse(response);
                if(response)
                {
                    $("#logistics_id").val(response.id);
                    if(response.instagram_link)
                    {
                        $("#instagram_link").val(response.instagram_link);
                    }
                    else
                    {
                        $("#instagram_link").val("");
                    }
                    if(response.facebook_link)
                    {
                        $("#facebook_link").val(response.facebook_link);
                    }
                    else
                    {
                       $("#facebook_link").val("");
                    }
                    if(response.twitter_link)
                    {
                        $("#twitter_link").val(response.twitter_link);
                    }
                    else
                    {
                        $("#twitter_link").val("");
                    }
                    if(response.google_plus_link)
                    {
                        $("#google_plus_link").val(response.google_plus_link);
                    }
                    else
                    {
                        $("#google_plus_link").val("");
                    }
                    if(response.youtube_link)
                    {
                        $("#youtube_link").val(response.youtube_link);
                    }
                    else
                    {
                        $("#youtube_link").val("");
                    }
                    if(response.flickr_link)
                    {
                        $("#flickr_link").val(response.flickr_link);
                    }
                    else
                    {
                        $("#flickr_link").val("");
                    }
                }
                else
                {
                    $("#instagram_link").val("");
                    $("#facebook_link").val("");
                    $("#twitter_link").val("");
                    $("#google_plus_link").val("");
                    $("#youtube_link").val("");
                    $("#flickr_link").val(""); 
                }
            },
            asyns: false
        });
        return false;
    });
</script>