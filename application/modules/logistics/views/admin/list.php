<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-Logistics"></i>Logistics</a>
        </div>

        <h1>Logistics</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <button class="btn btn-success btn-cls" type="button" onclick="add();">Add Logistics</button>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Event</th>
                                    <th>First Floor Map Description</th>
                                    <th>Second Floor Map Description</th>
                                    <th>Wireless Access</th>
                                    <th>Local Attractions</th>
                                    <th>Parking Instruction</th>
                                    <th>Transportation</th>
                                    <th>Accomodation</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php
                            $sl = 1;
                            if (!empty($all_data)) {
                                foreach ($all_data as $data) {
                            ?>
                                <tr class="gradeX">
                                    <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                    <td class="custom-action-btn">
                                        <a href="<?php echo base_url('admin_update_logistics' . '/' . $data->id); ?>">
                                            <?php echo ucfirst($data->event_name); ?>
                                        </a>
                                    </td>
                                    <td class="custom-action-btn"><a data-info="<?php echo $data->first_floor_map_description; ?>" data-image="<?php if(!empty($data->first_floor_map_image)) echo base_url('assets/upload/logistics').'/'.$data->first_floor_map_image;?>" class="second_floor" href="javascript:void(0);"><?php echo (!empty($data->first_floor_map_description)) ? word_limit($data->first_floor_map_description) : "NA"; ?></a></td>
                                    <td class="custom-action-btn"><a data-info="<?php echo $data->second_floor_map_description; ?>" data-image="<?php if(!empty($data->second_floor_map_image)) echo base_url('assets/upload/logistics').'/'.$data->second_floor_map_image;?>" class="second_floor" href="javascript:void(0);"><?php echo (!empty($data->second_floor_map_description)) ? word_limit($data->second_floor_map_description) : "NA"; ?></a></td>
                                    <td class="custom-action-btn"><a data-info="<?php if(!empty($data->wireless_access)) echo $data->wireless_access;?>" class="wifi" href="javascript:void(0);"><?php echo (!empty($data->wireless_access)) ? word_limit($data->wireless_access) : "NA"; ?></a></td>
                                    <td class="custom-action-btn"><a data-info="<?php if(!empty($data->local_attractions)) echo $data->local_attractions;?>" class="attraction" href="javascript:void(0);"><?php echo (!empty($data->local_attractions)) ? word_limit($data->local_attractions) : "NA"; ?></a></td>
                                    <td class="custom-action-btn"><a data-info_lat="<?php echo $data->parking_latitude; ?>" data-info_lng="<?php echo $data->parking_longitude; ?>" data-info="<?php echo $data->parking_instruction_description; ?>" data-image="<?php if(!empty($data->parking_instruction_map_image)) echo base_url('assets/upload/logistics').'/'.$data->parking_instruction_map_image;?>" class="parking_instruction" href="javascript:void(0);"><?php echo (!empty($data->parking_instruction_description)) ? word_limit($data->parking_instruction_description) : "NA"; ?></td></a></td>
                                    <td class="custom-action-btn"><a data-info="<?php if(!empty($data->transportation)) echo $data->transportation;?>" class="transportation" href="javascript:void(0);"><?php echo (!empty($data->transportation)) ? word_limit($data->transportation) : "NA"; ?></a></td>
                                    <td class="custom-action-btn"><a data-info="<?php if(!empty($data->accomodation)) echo $data->accomodation;?>" class="accomodation" href="javascript:void(0);"><?php echo (!empty($data->accomodation)) ? word_limit($data->accomodation) : "NA"; ?></a></td>
                                    <td class="custom-action-btn">
                                        <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                    </td>
                                </tr>
                            <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add Logistics Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_logistics'); ?>";
    }

    $(document).on("click",".parking_instruction",function(){
        var im=$(this).data("image");
        var info=$(this).data("info");
        var info_lat=$(this).data("info_lat");
        var info_lng=$(this).data("info_lng");
        if(im)
        {
            $("#myAlert").on('shown.bs.modal', function() {
                // $('.modal').css({
                //       'width':'250px'
                // });
                $("#notification_heading").html("Parking Instruction");
                $("#modal_confirm").hide();
                $("#notification_body").html("<p><b>Description:</b></p><p>"+info+"</p><br><p><b>Image:</b></p><p><img style='height:250px;width:250px;' src='"+im+"'></img></p><p><b>Latitude:</b></p><p>"+info_lat+"</p><br><p><b>Longitude:</b></p><p>"+info_lng+"</p><br>");
            }).modal("show");
        }
        else
        {
            $("#myAlert").on('shown.bs.modal', function() {
                // $('.modal').css({
                //       'width':'250px'
                // });
                $("#notification_heading").html("Parking Instruction");
                $("#modal_confirm").hide();
                $("#notification_body").html("<p><b>Description:</b></p><p>"+info+"</p>");
            }).modal("show");
        }
    });
    $(document).on("click",".wifi",function(){
        var info=$(this).data("info");
        if(info)
        {
            $("#myAlert").on('shown.bs.modal', function() {
                // $('.modal').css({
                //       'width':'250px'
                // });
                $("#notification_heading").html("Wireless Access");
                $("#modal_confirm").hide();
                $("#notification_body").html("<p>"+info+"</p>");
            }).modal("show");
        }
    });
    $(document).on("click",".attraction",function(){
        var info=$(this).data("info");
        if(info)
        {
            $("#myAlert").on('shown.bs.modal', function() {
                // $('.modal').css({
                //       'width':'250px'
                // });
                $("#notification_heading").html("Local Attractions");
                $("#modal_confirm").hide();
                $("#notification_body").html("<p>"+info+"</p>");
            }).modal("show");
        }
    });
    $(document).on("click",".transportation",function(){
        var info=$(this).data("info");
        if(info)
        {
            $("#myAlert").on('shown.bs.modal', function() {
                // $('.modal').css({
                //       'width':'250px'
                // });
                $("#notification_heading").html("Transportation");
                $("#modal_confirm").hide();
                $("#notification_body").html("<p>"+info+"</p>");
            }).modal("show");
        }
    });
    $(document).on("click",".accomodation",function(){
        var info=$(this).data("info");
        if(info)
        {
            $("#myAlert").on('shown.bs.modal', function() {
                // $('.modal').css({
                //       'width':'250px'
                // });
                $("#notification_heading").html("Accomodation");
                $("#modal_confirm").hide();
                $("#notification_body").html("<p>"+info+"</p>");
            }).modal("show");
        }
    });
    $(document).on("click",".second_floor",function(){
        var im=$(this).data("image");
        var info=$(this).data("info");
        if(im)
        {
            $("#myAlert").on('shown.bs.modal', function() {
                // $('.modal').css({
                //       'width':'250px'
                // });
                $("#notification_heading").html("Second Floor");
                $("#modal_confirm").hide();
                $("#notification_body").html("<p><b>Description:</b></p><p>"+info+"</p><br><p><b>Image:</b></p><p><img style='height:250px;width:250px;' src='"+im+"'></img></p>");
            }).modal("show");
        }
        else
        {
            $("#myAlert").on('shown.bs.modal', function() {
                // $('.modal').css({
                //       'width':'250px'
                // });
                $("#notification_heading").html("Parking Instruction");
                $("#modal_confirm").hide();
                $("#notification_body").html("<p><b>Description:</b></p><p>"+info+"</p>");
            }).modal("show");
        }
    });

</script>
