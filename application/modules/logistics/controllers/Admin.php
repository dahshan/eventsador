<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for logistics [HMVC]. Handles all the datatypes and methodes required for logistics section of Optus
 *
 * @author  
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Logisticsmodel');
    }

    /**
     * Index Page for this logistics controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Logisticsmodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'logistics/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of logistics module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if(!empty($this->input->post('logistics_id')))
        {
            $id = $this->input->post('logistics_id');
        }
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('event_id', 'Event', 'trim|required');
            // $this->form_validation->set_rules('second_floor_map_description', 'Second Floor Map Description', 'trim|required');
            // $this->form_validation->set_rules('parking_instruction_description', 'Second Floor Map Description', 'trim|required');
            // if ($param == 'A') {
            //     if (empty($_FILES['first_floor_map_description']['name']))
            //     {
            //         $this->form_validation->set_rules('first_floor_map_description', 'First Floor Map Description', 'trim|required');
            //     }
            //     if (empty($_FILES['second_floor_map_description']['name']))
            //     {
            //         $this->form_validation->set_rules('second_floor_map_description', 'Second Floor Map Description', 'trim|required');
            //     }
            // } 
            
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Logisticsmodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Logistics already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Logistics modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_logistics'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Logisticsmodel->load_single_data($id);
        // echo $this->session->userdata('admin_role_type'); die;
        if($this->session->userdata('admin_role_type') != 1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                // $this->data['all_event'] = $this->Logisticsmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_start_date >=' => date("Y-m-d")]);
                $this->data['all_event'] = $this->Logisticsmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N']);
            }
            else
            {
                if($this->session->userdata('admin_org_id') != 1)
                {
                    $this->data['all_event'] = $this->Logisticsmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N']);//, 'event_start_date >=' => date("Y-m-d")
                    // $organizer = $this->Logisticsmodel->get_row_data('user', ['id'=> $this->session->userdata('admin_org_id')]);
                    // if ($organizer->role_id == 1){ // In case of admin representative
                    //     $this->data['all_event'] = $this->Logisticsmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N']);
                    // }else{ // in case of organizer representative
                    //     $this->data['all_event'] = $this->Logisticsmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N']);                        
                    // }

                }else{
                    $this->data['all_event'] = $this->Logisticsmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N']);
                }
            }
        }
        else
        {
            // $this->data['all_event'] = $this->Logisticsmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_start_date >=' => date("Y-m-d")]);
            $this->data['all_event'] = $this->Logisticsmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N']);
        }
        //var_dump($this->data['all_event']);exit;
        $this->middle = 'logistics/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function new_email_check($email) {
        if (isset($email) && $email != '') {
            $exhibitor_data = $this->Logisticsmodel->get_row_data('logistics', ['email' => $email, 'delete_flag' => 'N']);
            if (count($exhibitor_data) > 0) {
                $this->form_validation->set_message('new_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $exhibitor_id = $this->input->post('exhibitor_id');
            $exhibitory_data = $this->Logisticsmodel->get_row_data('logistics', ['id<>' => $exhibitor_id, 'email' => $email, 'delete_flag' => 'N']);
            if (!empty($exhibitor_data) ) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of logistics module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Logisticsmodel->status($id);
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'logistics status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_exhibitor'));
    }

    /**
     * Delete function of logistics module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Logisticsmodel->delete($id);
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'logistics deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_exhibitor'));
    }

    public function fetch_data($id) {
        $social_data = $this->Logisticsmodel->get_row_data('logistics', ['event_id' => $id]);
        echo json_encode($social_data);
    }

}

/* End of file admin.php */
/* Location: ./application/modules/logistics/controllers/admin.php */
