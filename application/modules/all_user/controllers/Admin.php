<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for User [HMVC]. Handles all the datatypes and methodes required for User section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/AllUsermodel');
        $this->load->model('speaker/admin/Speakermodel');
        $this->load->model('exhibitor/admin/Exhibitormodel');
    }

    /**
     * Index Page for this User controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->AllUsermodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->data['all_speaker'] = $this->Speakermodel->load_all_data();
        $this->data['all_exhibitor'] = $this->Exhibitormodel->load_all_data();


        //echo "<pre>"; print_r($this->data['all_exhibitor']); die;
        $this->middle = 'all_user/admin/list';
        $this->admin_layout();
    }
}

/* End of file admin.php */
/* Location: ./application/modules/all_user/controllers/admin.php */
