<?php

/**
 * User Model Class. Handles all the datatypes and methodes required for handling User
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class AllUsermodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of User for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the User that has been added by current admin [Table:  ets_user]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('user.*,role.name as role_name,role.delete_flag as deleteflag');
        $this->db->from(tablename('user'));
        $this->db->join(tablename('role'),"role.id = user.role_id");
        $this->db->where('user.delete_flag', 'N');
        $this->db->where('user.id!=', 1);
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('organizer_id', $this->session->userdata('admin_uid'));
            }
            else
            {
                $this->db->where('organizer_id', $this->session->userdata('admin_org_id'));
            }
        }
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }
}

/* End of file AllUsermodel.php */
/* Location: ./application/modules/user/models/admin/AllUsermodel.php */
