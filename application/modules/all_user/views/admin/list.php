<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-user"></i>Users</a>
        </div>

        <h1>Users</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>emailid</th>
                                    <th>phoneno No</th>
                                    <th>Role</th>
                                    <th>Joined on</th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php                            
                            $sl = 1;
                            // Organizer Representative   &   Organizer                       
                            if (!empty($all_data)) {
                                foreach ($all_data as $data) {
                            ?>
                                <tr class="gradeX">
                                    <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                    <td class="custom-action-btn">
                                        <?php 
                                            echo ucfirst($data->name);
                                            ?>
                                    </td>
                                   
                                    <td class="custom-action-btn"><?php echo (!empty($data->emailid)) ? $data->emailid : "NA"; ?></td>
                                    <td class="custom-action-btn"><?php echo (!empty($data->phoneno)) ? $data->phoneno : "NA"; ?></td>
                                    <td class="custom-action-btn"><?php echo ucfirst($data->role_name); ?></td>
                                    <td class="custom-action-btn"><?php echo date("jS M, Y", strtotime($data->entry_date)); ?></td>
                                </tr>
                            <?php
                                }
                            }
                            // speaker  
                            if (!empty($all_speaker)) {
                                foreach ($all_speaker as $data) {
                            ?>
                                <tr class="gradeX">
                                    <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                    <td class="custom-action-btn">
                                        <?php 
                                            echo ucfirst($data->name);
                                            ?>
                                    </td>
                                   
                                    <td class="custom-action-btn"><?php echo (!empty($data->emailid)) ? $data->emailid : "NA"; ?></td>
                                    <td class="custom-action-btn"><?php echo (!empty($data->phoneno)) ? $data->phoneno : "NA"; ?></td>
                                    <td class="custom-action-btn">Speaker</td>
                                    <td class="custom-action-btn"><?php echo date("jS M, Y", strtotime($data->entry_date)); ?></td>
                                </tr>
                            <?php
                                }
                            }
                            //Exhibitor
                             if (!empty($all_exhibitor)) {
                                foreach ($all_exhibitor as $data) {
                            ?>
                                <tr class="gradeX">
                                    <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                    <td class="custom-action-btn">
                                        <?php 
                                            echo ucfirst($data->name);
                                            ?>
                                    </td>
                                   
                                    <td class="custom-action-btn"><?php echo (!empty($data->emailid)) ? $data->emailid : "NA"; ?></td>
                                    <td class="custom-action-btn"><?php echo (!empty($data->phoneno)) ? $data->phoneno : "NA"; ?></td>
                                    <td class="custom-action-btn">Exhibitor</td>
                                    <td class="custom-action-btn"><?php echo date("jS M, Y", strtotime($data->entry_date)); ?></td>
                                </tr>
                            <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add User Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_user'); ?>";
    }

    /**
     * Status Change Function
     *
     * @param   rec_id
     * @return  redirects and gives control to status handler
     */
    function change_status(rec_id) {
        $("#myAlert").on('shown.bs.modal', function() {
            $("#modal_confirm").click(function() {
                window.location.href = "<?php echo base_url('admin_status_user'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to change status of this user?");
        }).modal("show");
    }

    /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function() {
            $("#modal_confirm").click(function() {
                window.location.href = "<?php echo base_url('admin_delete_user'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this User?");
        }).modal("show");
    }

    /**
     * Reset Password for an User Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function reset_passwd(rec_id) {
        $("#myAlert").on('shown.bs.modal', function() {
            $("#modal_confirm").click(function() {
                window.location.href = "<?php echo base_url('admin_reset_passwd'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to reset password for this user? The user will be notified via emailid.");
        }).modal("show");
    }
</script>
