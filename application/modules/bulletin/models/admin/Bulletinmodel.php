<?php
class Bulletinmodel extends CI_Model
{
    public function __construct()
	{
        parent::__construct();
        $this->load->database();
	}
    
    public function get_row_data($tbl_name, $where){
        return $this->db->where($where)->get($tbl_name)->row();
    }
    
	public function getAll()
	{
		$list = $this->db->get(tablename('bulletin'))->result();
		return $list;
	}

	public function getAll_where($tablename,$where)
	{
		$list = $this->db->where($where)->get(tablename($tablename))->result();
		return $list;
	}
	
	public function get($id)
	{
		$list = $this->db->where(array('id'=>$id))->get(tablename('bulletin'))->result();
		return !empty($list) ? $list[0] : array();
	}

	public function getarch($val)
	{
		$list = $this->db->where(array('archieve'=>$val))->get(tablename('bulletin'))->result();
		return !empty($list) ? $list : array();
	}
	
	public function delete($id)
	{
		$r = $this->db->delete(tablename('bulletin_topic_chat'),array('id'=>$id));
		return $r;
	}
    
    public function delete_topic($id)
    {
        $r = $this->db->delete(tablename('bulletin_topic'),array('id'=>$id));
        $this->db->delete(tablename('bulletin_topic_chat'),array('bulletin_topic_id'=>$id));
        return $r;
    }

	public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('bulletin_topic'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    public function modify($eid,$id = '') 
    {
        $data['event_id'] = $eid;
        $data['topic_name'] = $this->input->post('topic_name');
        if (!empty($id)) 
        {
            $this->db->where('id', $id)->update(tablename('bulletin_topic'), $data);
            return $id;
        } 
        else 
        {
        	$data['created_date'] = date("Y-m-d H:i:s");
            $this->db->insert(tablename('bulletin_topic'), $data);
            $last_id = $this->db->insert_id();
            if (!empty($last_id)) {
                return $last_id;
            } else {
                return "";
            }
        }
    }


	public function update($data,$where)
	{
		$r = $this->db->update(tablename('bulletin'),$data,$where);
		return $r;
	}

	public function updatearch($data,$where)
	{
		$r = $this->db->update(tablename('bulletin'),$data,$where);
		return $r;
	}

	public function insertdata($tablename,$data)
	{
		$r = $this->db->insert(tablename($tablename),$data);
		return $this->db->insert_id();
	}

	public function bulk_del($where)
	{
		echo $sql="delete from ". tablename('bulletin') ." where id in($where) ";
		$query=$this->db->query($sql);
		if(!empty($query))
		{
			return true;
		}
		else
		{
			return;
		}
	}

	public function bulk_arch($where,$val)
	{
		$sql="update ". tablename('bulletin') ." set archieve='".$val."' where id in($where) ";
		$query=$this->db->query($sql);
		if(!empty($query))
		{
			return true;
		}
		else
		{
			return;
		}
	}

	public function get_detailed_result_data($table,$table1,$join_condition,$where = "1=1")
    {
        $this->db->select('bulletin.*,user.name,user.emailid,user.phoneno,user.company,user.position_title');
        $this->db->join($table1,$join_condition);
        $query=$this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    public function get_detailed_result_data1($table,$table1,$join_condition,$where = "1=1")
    {
        // $this->db->select('bulletin.*,user.name,user.email,user.phone_no,user.company,user.position_title');
        $this->db->join($table1,$join_condition);
        $query=$this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    public function get_bulletin_chat($where)
    {
        $this->db->select('bulletin_topic_chat.*,user.name,user.emailid,user.phoneno,user.company,user.position_title,user.profile_image');
        $this->db->join('user','user.id=user_id');
        $this->db->order_by("bulletin_topic_chat.created_date","Desc");
        $query=$this->db->get_where(tablename('bulletin_topic_chat'), $where);
        return $query->result();
    }


    public function get_bulletin_chat_new($where)
    {
        $this->db->select('bulletin_topic_chat.*,user.name,user.emailid,user.phoneno,user.company,user.position_title,user.profile_image');
        $this->db->join('user','user.id=user_id');
        $this->db->order_by("bulletin_topic_chat.created_date","Asc");
        $query=$this->db->get_where(tablename('bulletin_topic_chat'), $where);
        return $query->result();
    }

    public function load_all_data() {
        $this->db->select('id,event_name');
        $this->db->from(tablename('event'));
        $this->db->where('event.delete_flag', 'N');
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(".$this->session->userdata('admin_uid').", users) !=", 0);
            }
            
            
        }
        $this->db->order_by("event.id", "asc");

        $query = $this->db->get();
        $result = $query->result();
        if(!empty($result))
        {
            $fina_result=array();
            foreach($result as $val)
            {
                $val->bulletin_topic_count=count($this->get_result_data("bulletin_topic", array("event_id"=>$val->id)));
                $fina_result[]=$val;
            }
            $result=$fina_result;
        }
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    public function get_result_data($table, $where = "1=1",$optional_where=NULL) 
    {
        if(empty($optional_where))
        {
            $query = $this->db->get_where(tablename($table), $where);
        }
        else
        {
            $this->db->where_in('id', $optional_where);
            $query = $this->db->get(tablename($table), $where);
        }

        //echo $this->db->last_query(); exit;
        return $query->result();
    }
    
    public function get_announcement1($event_id){
        return $this->db->select('id, event_id, title, description, created_date')->where('event_id', $event_id)->get('announcement')->result();
    }
    
    public function get_announcement2($event_id, $user_id){
        $announcements;
        if ($user_id){
            $this->db->select('notification.id, notification.event_id, notification.notification_title as title, notification.notification_text as description, notification.created_date')->from('notification_touser');
            $this->db->join('notification', 'notification_touser.noti_id = notification.id');
            $this->db->where('notification_touser.user_id', $user_id);
            // $this->db->where('option_noti !=', 'app_email');
            $this->db->where('notification.event_id', $event_id);
            $query = $this->db->get();
            return $query->result();
        }else{
            $this->db->select('id, event_id, notification_title as title, notification_text as description, created_date');
            $this->db->where('event_id', $event_id);
            // $this->db->where('option_noti !=', 'app_email');
            $this->db->order_by('created_date', "ASC");
            return $this->db->get('notification')->result();
        }
    }
    
    public function get_bulletinChatRead(){
        
    }
    
    public function get_last_chat_id($data){
        $this->db->select('id');
        $this->db->from('bulletin_topic_chat');
        $this->db->where($data);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) return 0;
        $row = $query->row();
        return $row->id;
    }
    
    public function update_last_read_chat($where, $data){
        if ($this->db->where($where)->get('bulletin_read')->num_rows() == 0){
            $this->db->insert('bulletin_read', $where);
        }
        $this->db->where($where);
        $this->db->update('bulletin_read', $data);
    }
    
    public function get_unread_count($user_id, $event_id){
        $this->db->select('notification_touser.id');
        $this->db->from('notification_touser');
        $this->db->join('notification', 'noti_id=notification.id');
        $this->db->where('notification.event_id', $event_id);
        $this->db->where('user_id', $user_id);
        $this->db->where('is_read', '0');
        $query = $this->db->get();
        return $query ? $query->num_rows() : 0;
    }
    
    public function get_last_read_bull_chat_id($data){
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('event_id', $data['event_id']);
        $this->db->where('topic_id', $data['topic_id']);
        $query = $this->db->get('bulletin_read');
        if ($query->num_rows() == 0) return 0;
        $row = $query->row();
        return $row->bull_chat_id;
    }
    
    public function get_unread_of_bull_topic($data){
        $this->db->where('bulletin_topic_id', $data['topic_id']);
        $this->db->where('event_id', $data['event_id']);
        $this->db->where('id >', $data['min_id']);
        $query = $this->db->get('bulletin_topic_chat');
        return $query->num_rows() > 0 ? $query->num_rows : 0;
    }
    
    public function update_data($tbl_name, $where, $data){
        $this->db->where($where);
        $this->db->update($tbl_name, $data);
        return $this->db->affected_rows();
    }
    
    public function update_announce_read_status($event_id, $user_id){
        $announces = $this->db->select('id')->where('event_id', $event_id)->get('notification'); 
        if ($announces){
            $rows = $announces->result();//echo json_encode($rows);exit;
            foreach ($rows as $row){
                $this->db->where('user_id', $user_id)->where('noti_id', $row->id)->update('notification_touser', ['is_read'=> '1']);
            }
        }
        
        
        // $strquery = "update `ets_notification_touser` INNER JOIN `ets_notification` ON `ets_notification`.`id` = `ets_notification_touser`.`noti_id` set `is_read` = 1  where `event_id` = $event_id and `ets_notification_touser.user_id=$user_id";
        // $this->db->query($strquery);
    }
    
	
}