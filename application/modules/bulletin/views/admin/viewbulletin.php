<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo site_url('superadmin/dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="<?php echo site_url('admin/contact-us'); ?>" class="current">Contact Us</a> </div>
        <h1>Manage Contact-us</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Contact Us Reply</h5>
                    </div>
                    <div class="widget-content nopadding form-horizontal">
                        <div class="control-group">
                      <label class="control-label">Query</label>
                      <div class="controls">
                      <textarea readonly class="span11" name="query" placeholder="Query..."><?php if(!empty($bulletin->query)) { echo html_entity_decode($bulletin->query,ENT_QUOTES,'utf-8'); } ?></textarea>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Reply</label>
                      <div class="controls">
                      <textarea readonly class="span11" name="reply" placeholder="Enter Reply to the query above"><?php if(!empty($bulletin->reply)) { echo html_entity_decode($bulletin->reply,ENT_QUOTES,'utf-8'); } ?></textarea>
                      </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  $("document").ready(function(){
    $("#reply_contact").validate({
      rules:{
        reply:"required"
      },
      messages:{
        reply:"Please Enter Reply..!!",
      }
    });
  });
</script>
<style>
  .error{
    color: #dd0000;
  }
</style>
