<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_bulletin'); ?>" title="" class="tip-bottom"><i class="fa fa-headphones"></i> Bulletin</a>
            <a href="<?php echo base_url('admin_bulletin_topic_list').'/'.$this->uri->segment(2); ?>" ><i class="fa fa-bars"></i>Bulletin Topic</a>
            <a href="javascript:void(0);" class="current">Bulletin Topic Management</a>
        </div>

        <h1>Bulletin Topic</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Bulletin Topic</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_bulletin_topic').'/'.$this->uri->segment(2) : base_url('admin_update_bulletin_topic').'/'.$this->uri->segment(2) . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <!-- <div class="control-group">
                                <label class="control-label">Event</label>
                                <div class="controls">
                                    <select name="event_id" id="event_id">
                                        <?php
                                        if (count($all_event) > 0) {
                                            foreach ($all_event as $all_event_val) {
                                                ?>
                                                <option value="<?php echo $all_event_val->id; ?>"
                                                        <?php if(!empty($data_single)){if($all_event_val->id==$data_single->event_id)echo 'selected'; }?>>
                                                            <?php echo (isset($all_event_val->event_name) && $all_event_val->event_name != '') ? $all_event_val->event_name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('event_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div> -->
                            <div class="control-group">
                                <label class="control-label">Topic Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Topic Name" type="text" id="topic_name" name="topic_name" value="<?php echo (!empty(set_value('topic_name'))) ? set_value('title') : ((!empty($data_single->topic_name)) ? $data_single->topic_name : ''); ?>" >
                                    <?php echo form_error('topic_name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            
                            <!-- <input type="hidden" name="eventid" id="eventid" value="<?php if(!empty($data_single->id)) echo $data_single->id; ?>"> -->
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

