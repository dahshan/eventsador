<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>

<style type="text/css">
.btn-circle {
  width: 10px;
  height: 10px;
  text-align: center;
  padding: 4px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
.table-bordered td{
    text-align: center;
}

.category-color {
    width: 40px;
    height: 40px;
    border-radius: 50%;
}

</style>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo site_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="<?php echo base_url('admin_bulletin'); ?>" title="Bulletin" class="tip-bottom"><i class="fa fa-headphones"></i> Bulletin</a><a href="<?php echo (!empty($list[0]->event_id))?base_url('admin_bulletin_topic_list').'/'.$list[0]->event_id:''; ?>" title="Bulletin" class="tip-bottom"><i class="fa fa-headphones"></i> Bulletin Topic</a><a href="javascript:void(0);" class="current">Bulletin Chat List</a> </div>
        
    </div>
    <div class="container-fluid">
       
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> 
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                      
                    </div>
                    <div class="widget-content">
                      <form id="delete" method="post">
                      <table class="table table-bordered data-table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Message</th>
                            <th>Date</th>
                            <?php if(in_array(115, $getRole)){ ?> <th>Select</th><?php } ?>

                            <!-- <th>Action</th> -->
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if(!empty($list))
                          {
                            $x = 0;
                            // echo"<pre>";
                            // print_r($list);die;
                           foreach($list as $contact)
                           {
                            // $cls = "";
                            //       if($contact->reply_status == 1)
                            //           $cls = "active";

                              
                           ?>
                            <!-- <tr class="<?= $cls; ?>"> -->
                            <tr>
                              <td><?php echo stripslashes($contact->name) ; ?></td>
                              <td><?php echo stripslashes($contact->message);?></td>
                              <td><?php echo date('jS M Y h:i:s A',strtotime($contact->created_date));?></td>
                              <?php if(in_array(115, $getRole)){ ?> 
                                  <td class="custom-action-btn">
                                      <input type='checkbox' name='delete[]' value='<?php echo $contact->id; ?>' >
                                  </td>
                              <?php } ?> 


                              <!-- <td><?php if($contact->reply_status == 0){ ?><a href="<?php echo $this->config->item('base_url');?>admin_bulletin_reply/<?php echo $contact->id;?>"><span><i class="fa fa-reply"></i></span></a> <?php } else { ?> <a href="<?php echo $this->config->item('base_url');?>admin_view-bulletin_reply/<?php echo $contact->id;?>"><span><i class="fa fa-eye"></i></span></a> <?php } ?></td> -->
                            </tr>
                            <?php
                           }
                          }
                          ?>
                        </tbody>
                        
                      </table>
                      <?php if(in_array(115, $getRole)){ ?><button class="btn btn-danger btn-cls" type="button" style="margin-top: 20px;" onclick="delete_chats()">Delete Chats</button><?php } ?>
                      </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body">Lorem ipsum dolor sit amet...</p>
    </div>

    <div class="modal-footer"> 
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a> 
        <a data-dismiss="modal" class="btn" href="#">Cancel</a> 
    </div>
</div>
<!-- [end] Notification DIV -->



<script type="text/javascript">


   

    function delete_chats(event) {

         var chats = $('input[name^=delete]:checked').map(function(idx, elem) {
           return $(elem).val();
         }).get();

         console.log(chats);

         $("#myAlert").on('shown.bs.modal', function() {
             $("#modal_confirm").click(function() {
                 $('form#delete').submit();
             });

             $("#notification_heading").html("Confirmation");
             $("#notification_body").html("Do you want to delete " + chats.length + " items?");
         }).modal("show");
     }


</script>


