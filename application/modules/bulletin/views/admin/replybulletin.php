<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="<?php echo base_url('admin_bulletin_list'); ?>/<?php echo $bulletin->event_id; ?>" title="Bulletin" class="tip-bottom"><i class="fa fa-headphones"></i> Bulletin List</a><a href="<?php echo site_url('admin/bulletin-us'); ?>" class="current">Bulletin Reply</a> </div>
        <h1>Manage bulletin-us</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Bulletin Feedback</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form role="form" class="form-horizontal" action="<?php echo base_url('admin_update-bulletin_reply').'/'.$bulletin->id; ?>" method="post" id="reply_contact" name="reply_contact">
                    <!-- text input -->
                    <div class="control-group">
                      <label  class="control-label">Topic</label>
                      <div class="controls">
                         <input class="span11" readonly placeholder="Topic" type="text" id="topic" name="topic" value="<?php echo (!empty(set_value('topic'))) ? set_value('topic') : ((!empty($bulletin->topic)) ? $bulletin->topic : ''); ?>" >
                      </div>
                    </div>

                    <div class="control-group">
                      <label  class="control-label">Meesage</label>
                      <div class="controls">
                        <textarea readonly class="span11" name="query" placeholder="Query..."><?php if(!empty($bulletin->query)) { echo html_entity_decode($bulletin->query,ENT_QUOTES,'utf-8'); } ?></textarea>
                      </div>
                    </div>

                    <div class="control-group">
                      <label  class="control-label">Reply</label>
                      <div class="controls">
                        <textarea class="span11" name="reply" placeholder="Enter Reply to the above query"></textarea>
                      </div>
                    </div>
                    
                    <div class="form-actions">
                      <input class="btn btn-success" type="submit" value="Reply"/>
                    </div>
                  </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script>
  $("document").ready(function(){
    $("#reply_contact").validate({
      rules:{
        reply:"required"
      },
      messages:{
        reply:"Please Enter Reply..!!",
      }
    });
  });
</script>
<style>
  .error{
    color: #dd0000;
  }
</style>
    