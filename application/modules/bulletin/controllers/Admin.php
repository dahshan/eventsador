<?php
/**
 *
 * Main Admin controller 
 * for managing the cms pages. 
 * All the methods containg 
 * all the detailed functionalities 
 * used for managing cms pages.
 *
 * <p>
 *  @author : 
 *  @package : contactus, replycontact, replyviewcontact, deletecontact, updatecontact
 *  @copyright : Sketch Web Solutions
 * </p>
 *
 **/
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    /**
     * This is the Constructor 
     **/
    public function __construct() {
        parent::__construct();
        $this->load->helper('url', 'form');
        $this->load->library('form_validation');
        $this->load->model('admin/bulletinmodel');
        $this->load->model('accomodation/admin/Accomodationmodel');
        admin_authenticate();
    }

    /**
     * Functionality for 
     * listing of the 
     * contact us details
     *
     * <p>
     *   @author : 
     *   @param : None
     * </p>
     *
     **/
    public function index() {
        $list = $this->bulletinmodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $list;
        $this->middle = 'bulletin/admin/list';
        $this->admin_layout();
    }

    public function form($eid,$id = NULL) 
    {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('topic_name', 'Name', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->bulletinmodel->modify($eid,$id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Bulletin Topic already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Bulletin Topic modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_bulletin_topic_list').'/'.$eid);
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        if (!empty($id)) {
            $this->data['data_single'] = $this->bulletinmodel->load_single_data($id);
        }
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
                }
            }
        }
        else
        {
            $this->data['all_event'] = $this->bulletinmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
        }
        $this->middle = 'bulletin/admin/form';
        $this->admin_layout();
    }

    public function bulletin_topic_list($eid) {
        $list = $this->bulletinmodel->get_result_data("bulletin_topic",array("event_id"=>$eid));
      //  echo "<pre>"; print_r($list);
      //  echo $this->db->last_query();
        if(!empty($list))
        {
            $fina_result=array();
            foreach($list as $val)
            {   
                $where1['bulletin_topic_id']=$val->id;
                $where1['event_id']=$val->event_id;
                $bulletin_chat = $this->bulletinmodel->get_bulletin_chat($where1);
                $val->bulletin_chat_count=count($bulletin_chat);

                $fina_result[]=$val;
            }
            $list=$fina_result;
        }
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['list'] = $list;
        $this->data['eid'] = $eid;
        $this->middle = 'bulletin/admin/bulletintopiclist';
        $this->admin_layout();
    }

    public function bulletin_list($id,$event) {

        if (!empty($_POST['delete'])) {
            admin_authenticate();
            // echo "<pre>";
            // print_r($_POST['delete']);
            // exit;
            foreach ($_POST['delete'] as $key => $value) {
                $flg = $this->bulletinmodel->delete($value);
            }
            $this->session->set_flashdata('successmessage', 'Messages deleted successfully');
        }

        $list = $this->bulletinmodel->get_bulletin_chat(array("bulletin_topic_id"=>$id,"event_id"=>$event));
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['list'] = $list;
        $this->middle = 'bulletin/admin/bulletinlist';
        $this->admin_layout();
    }

    /**
     * Functionality for 
     * replying to a contact
     *
     * <p>
     *   @author : 
     *   @param : contact-id
     * </p>
     *
     **/
    public function bulletin_reply($id) {
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['bulletin'] = $this->bulletinmodel->get($id);
        $this->middle = 'bulletin/admin/replybulletin';
        $this->admin_layout();
    }

    /**
     * Functionality for 
     * viewing the detail 
     * of a contact
     *
     * <p>
     *   @author : 
     *   @param : contact-id
     * </p>
     *
     **/
    public function viewbulletin($id) {
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['bulletin'] = $this->bulletinmodel->get($id);
        $this->middle = 'bulletin/admin/viewbulletin';
        $this->admin_layout();
    }

    /**
     * Functionality for 
     * deleting a contact 
     * listed in the 
     * contact us management
     *
     * <p>
     *   @author : 
     *   @param : contact-id
     * </p>
     *
     **/
    public function deletecontact($id) 
    {
        $id = base64_decode(urldecode($id));
        if ($this->bulletinmodel->delete($id)) {
            $this->session->set_flashdata('successmessage', 'Message deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! something went wrong. Please try again');
        }

        $redirect=$_SERVER['HTTP_REFERER'];
        redirect($redirect);
    }

    public function delete_topic($id) 
    {
        $id = base64_decode(urldecode($id));
        if ($this->bulletinmodel->delete_topic($id)) {
            $this->session->set_flashdata('successmessage', 'Topic deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! something went wrong. Please try again');
        }

        $redirect=$_SERVER['HTTP_REFERER'];
        redirect($redirect);
    }

    
    /**
     * Functionality for 
     * updating the contact 
     * on the contact us management
     *
     * <p>
     *   @param : None
     * </p>
     *
     **/
    public function updatebulletin($id = NULL) 
    {
        $data['reply']=$this->input->post('reply');
        $data['reply_status']='1';
        $where['id']=$id;
        $flg = $this->bulletinmodel->update($data,$where);
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Bulletin reply sent');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_bulletin'));
    }

   ////////////Newsletter////////////////

    public function newsletter() 
    {
        $this->load->library('MailChimp');
        $email=$this->input->post('nwsltr_email');
        $list_id = 'ed788b6a62';
        $result = $this->mailchimp->post("lists/$list_id/members", [ 'email_address' => $email, 'merge_fields' => ['FNAME'=>'Test', 'LNAME'=>'Check'], 'status' => 'subscribed', ]);
        if(!empty($result))
        {
            $this->session->set_flashdata('successmessage', 'You have successfully subscribed to Botler App');
        } 
        else 
        {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong. Please try again.');
        }
        redirect($this->agent->referrer());
    }
}