<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for currency [HMVC]. Handles all the datatypes and methodes required for currency section of Optus
 *
 * @author  <sketch.dev22@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Currencymodel');
    }

    /**
     * Index Page for this currency controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Currencymodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'currency/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of currency module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('code', 'currency Code', 'trim|required');
           // $this->form_validation->set_rules('symbol', 'currency Symbol', 'trim|required');
           
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Currencymodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! currency already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'currency modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_currency'));
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        if (!empty($id)) {
            $this->data['data_single'] = $this->Currencymodel->load_single_data($id);
        }
        
        $this->middle = 'currency/admin/form';
        $this->admin_layout();
    }

    

    /**
     * Status Change function of currency module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Currencymodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'currency status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Delete function of currency module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Currencymodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'currency deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

}

/* End of file admin.php */
/* Location: ./application/modules/currency/controllers/admin.php */
