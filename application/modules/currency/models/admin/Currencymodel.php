<?php

/**
 * currency Model Class. Handles all the datatypes and methodes required for handling currency
 *
 * @author <sketch.dev22@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Currencymodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of currency for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the currency that has been added by current admin [Table:  ets_currency]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('*');
        $this->db->from(tablename('currency'));
        $this->db->where('delete_flag', 'N');
       $this->db->order_by('code', 'asc'); 
        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

    /**
     * Used for loading functionality of single currency by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single currency by id that has been added by current admin [Table:  ets_currency]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('currency'));
        if(!empty($id))
        {
            $this->db->where('id', $id);
        }

        $query = $this->db->get();
        $result = $query->row();
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1",$optional_where=NULL) {
        if(empty($optional_where))
        {
            $query = $this->db->get_where(tablename($table), $where);
        }
        else
        {
            $this->db->where_in('id', $optional_where);
            $query = $this->db->get(tablename($table), $where);
        }

        //echo $this->db->last_query(); exit;
        return $query->result();
    }


    /**
     * Used for Save(Insert/Update) functionality of single currency for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single currency w.r.t. current admin [Table:  ets_currency]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
      //  echo "<pre>"; print_r($this->input->post()); die;
        $data['name'] = $this->input->post('name');
        $data['code'] = $this->input->post('code');
       // $data['symbol'] = $this->input->post('symbol');

       
        if (!empty($id)) 
        {
            $this->db->where('id', $id)->update(tablename('currency'), $data);
            return $id;
        } 
        else 
        {
            $this->db->insert(tablename('currency'), $data);
            $last_id = $this->db->insert_id();
            if (!empty($last_id)) {
                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of currency for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current currency status
     * and change it the the opposite [Table: pb_currency]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('currency'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('currency'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of currency for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_currency]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('currency'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

    public function insert_data($table,$data1)
    {
        $this->db->insert(tablename($table),$data1);
        return $this->db->insert_id();
    }

    public function delete_data($table,$where)
    {
        $this->db->delete(tablename($table),$where);
        return $this->db->affected_rows();
    }

    public function update_data($table,$where,$data)
    {
        $this->db->where($where);
        $this->db->update($table, $data); 
        return $this->db->affected_rows();
    } 
   
    public function get_detailed_result_data($table,$table1,$join_condition,$where = "1=1")
    {
        $this->db->join($table1,$join_condition);
        $query=$this->db->get_where(tablename($table), $where);
        return $query->result();
    }


}

/* End of file currencymodel.php */
/* Location: ./application/modules/currency/models/admin/currencymodel.php */
