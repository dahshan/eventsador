<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_currency'); ?>" ><i class="fa fa-building"></i>Currency</a>
            <a href="javascript:void(0);" class="current">Currency Management</a>
        </div>

        <h1>Currency</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Currency</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_currency') : base_url('admin_update_currency'). '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Name" type="text" id="name" name="name" value="<?php echo (!empty(set_value('name'))) ? set_value('title') : ((!empty($data_single->name)) ? $data_single->name : ''); ?>" >
                                    <?php echo form_error('name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Code</label>
                                <div class="controls">
                                    <textarea class="span11" id="code" name="code"><?php echo (!empty(set_value('code'))) ? set_value('code') : ((!empty($data_single->code)) ? $data_single->code : ''); ?></textarea>
                                    <?php echo form_error('code', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>    



                         <!--    <div class="control-group">
                                <label class="control-label">Symbol</label>
                                <div class="controls">
                                    <textarea class="span11" id="symbol" name="symbol"><?php echo (!empty(set_value('symbol'))) ? set_value('symbol') : ((!empty($data_single->symbol)) ? $data_single->symbol : ''); ?></textarea>
                                    <?php echo form_error('symbol', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>  
 -->

                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div id="myModalCurr" class="modal" role="dialog" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Currency</h4>
      </div>
      <div class="modal-body">
            <form class="form-horizontal">
                           
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="" placeholder="Enter Name" type="text" id="cur_name" name="cur_name" value="" >
                                    <div id="name_err" style="color:red;"></div>
                                </div>
                            </div>

                             <div class="control-group">
                                <label class="control-label">Code</label>
                                <div class="controls">
                                    <input class="" placeholder="Enter Code" type="text" id="cur_code" name="cur_code" value="" >
                                    <div id="code_err" style="color:red;"></div>
                                </div>
                            </div>


                             <div class="control-group">
                                <label class="control-label">Symbol</label>
                                <div class="controls">
                                    <input class="" placeholder="Enter Symbol" type="text" id="cur_symbol" name="cur_symbol" value="" >
                                    <div id="symbol_err" style="color:red;"></div>
                                </div>
                            </div>                           
                        </form>

      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" onclick="save_curr();" >Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrhTMHvt2CLbSI_GnMoEpNz7Z2s6SOxdE"></script>
<script type="text/javascript">
    function initMap() {
        var lat=Number($("#latitude").val());
        var lng=Number($("#longitude").val());
        var myLatLng = {lat: lat, lng: lng};
        // if(lat && lng)
        // {
        //     var myLatLng = {lat: lat, lng: lng};
        // }
        // else
        // {
        //     var myLatLng = {lat: lat, lng: lng};
        // }
        $("#div_map").removeAttr("style");
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: 15,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map
          // title: 'Hello World!'
        });
    }
    $(document).on("change","#is_deal",function(){
        if (this.checked) {
            $("#deal_text").val("");
            $("#deal_tab").removeAttr("style");
        } else {
            $('#deal_tab').css('display','none');
        }
    });
    

    function save_curr() {
        var cur_name = $("#cur_name").val();
        var cur_code = $("#cur_code").val();
        var cur_symbol = $("#cur_symbol").val();

        if((cur_name!='') && (cur_code!='') && (cur_symbol!='')){
            $("#name_err").html('');
            $("#code_err").html('');
            $("#symbol_err").html('');

            $.post("<?php echo base_url('admin_save_currency'); ?>",{cur_name:cur_name,cur_code:cur_code,cur_symbol:cur_symbol},function(result) {
                //console.log(result);
                $("#currency").append('<option value="'+cur_code+'('+cur_symbol+')'+'">'+cur_code+'('+cur_symbol+')'+'</option>');
                $("#myModalCurr").modal('hide');
            });
        }else{
            if(cur_name==''){
                $("#name_err").html('Name Field is required.');
            }
            if(cur_code==''){
                $("#code_err").html('Code Field is required.');
            }
            if(cur_symbol==''){
                $("#symbol_err").html('Symbol Field is required.');
            }
        }
    }

</script>