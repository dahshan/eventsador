<?php

/**
 * faq Model Class. Handles all the datatypes and methodes required for handling faq
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Faqmodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of faq for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the faq that has been added by current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('*');
        $this->db->from(tablename('faq'));
        $this->db->where('delete_flag', 'N');
        // if($this->session->userdata('admin_role_type')!=1)
        // {
        //     if($this->session->userdata('admin_role_type')==2)
        //     {
        //         $this->db->where('organizer_id', $this->session->userdata('admin_uid'));
        //     }
        //     else
        //     {
        //         $this->db->where('organizer_id', $this->session->userdata('admin_org_id'));
        //     }
        // }
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single faq by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single faq by id that has been added by current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('faq'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1") {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    public function get_result_data1($table,$whr,$table1=NULL,$condition=NULL,$field=NULL,$tag=NULL) 
    {
        if(!empty($table1))
        {
            // $this->db->select('exhibitor.*,exhibitor_type.name as type_name');
            // $this->db->join($table1,$condition);
        }
        $query =$this->db->where_in($field,$whr)->get(tablename($table));
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single faq for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single faq w.r.t. current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        $data['question'] = $this->input->post('question');
        $data['answer'] = $this->input->post('answer');
        if (!empty($id)) 
        {
            $this->db->where('id', $id)->update(tablename('faq'), $data);
            $file = $_FILES['img_file'];
            if (isset($file['name']) && $file['name'] != '' && count($file['name'])) {
                foreach ($file['name'] AS $file_name_key => $file_name) {
                    if (!empty($file['name'][$file_name_key])) {
                        $info = pathinfo($file['name'][$file_name_key]);
                        $ext = $info['extension']; // get the extension of the file
                        $newname = "media_" . time() . $file_name_key . "." . $ext;
                        $target = __DIR__ . '/../../../../../assets/upload/faq/' . $newname;
                        move_uploaded_file($_FILES['img_file']['tmp_name'][$file_name_key], $target);
                        $data = array(
                            'faq_id' => $id,
                            'image' => $newname,
                            'created_date' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert(tablename('faq_images'), $data);
                    }
                }
            }
            return $id;
        } 
        else 
        {
            $data['created_date'] = date("Y-m-d H:i:s");
            $this->db->insert(tablename('faq'), $data);
            $last_id = $this->db->insert_id();
            if (!empty($last_id)) {
                $file = $_FILES['img_file'];
                if (isset($file['name']) && $file['name'] != '' && count($file['name'])) {
                    foreach ($file['name'] AS $file_name_key => $file_name) {
                        if (!empty($file['name'][$file_name_key])) {
                            $info = pathinfo($file['name'][$file_name_key]);
                            $ext = $info['extension']; // get the extension of the file
                            $newname = "media_" . time() . $file_name_key . "." . $ext;
                            $target = __DIR__ . '/../../../../../assets/upload/faq/' . $newname;
                            move_uploaded_file($_FILES['img_file']['tmp_name'][$file_name_key], $target);
                            $data = array(
                                'faq_id' => $last_id,
                                'image' => $newname,
                                'created_date' => date('Y-m-d H:i:s')
                            );
                            $this->db->insert(tablename('faq_images'), $data);
                        }
                    }
                }
                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of faq for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current faq status
     * and change it the the opposite [Table: pb_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('faq'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('faq'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of faq for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('faq'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

    public function delete_data($table,$where)
    {
        $this->db->delete(tablename($table),$where);
        return $this->db->affected_rows();
    }

    public function faq_images($faq_id) 
    {
        $sql="select concat('".base_url('assets/upload/faq').'/'."',image) as faq_image from ".tablename('faq_images')." where faq_id='".$faq_id."' ";
        $query=$this->db->query($sql);
        $result=$query->result();
        return $result;
    }

}

/* End of file Sponsormodel.php */
/* Location: ./application/modules/faq/models/admin/Sponsormodel.php */
