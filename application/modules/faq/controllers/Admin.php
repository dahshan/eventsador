<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for faq [HMVC]. Handles all the datatypes and methodes required for faq section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Faqmodel');
    }

    /**
     * Index Page for this faq controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Faqmodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'faq/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of faq module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('question', 'Question', 'trim|required');
            $this->form_validation->set_rules('answer', 'Answer', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Faqmodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! faq already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'faq modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_faq'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Faqmodel->load_single_data($id);
        $this->middle = 'faq/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function new_email_check($email) {
        if (isset($email) && $email != '') {
            $sponsor_data = $this->Faqmodel->get_row_data('faq', ['email' => $email, 'delete_flag' => 'N']);
            if (count($sponsor_data) > 0) {
                $this->form_validation->set_message('new_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $speaker_id = $this->input->post('speaker_id');
            $speaker_data = $this->Faqmodel->get_row_data('faq', ['id<>' => $speaker_id, 'email' => $email, 'delete_flag' => 'N']);
            if (count($sspeaker_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of faq module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Faqmodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'faq status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_faq'));
    }

    /**
     * Delete function of faq module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Faqmodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'faq deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_faq'));
    }

    public function delete_file($id) {
        $file = $this->Faqmodel->get_row_data('faq_images', ['id' => $id]);
        $flg = $this->Faqmodel->delete_data('faq_images',array("id"=>$id));
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Picture deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_update_faq/').$file->faq_id);
    }

}

/* End of file admin.php */
/* Location: ./application/modules/faq/controllers/admin.php */
