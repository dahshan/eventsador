<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_faq'); ?>" ><i class="fa fa-question-circle"></i>FAQ</a>
            <a href="javascript:void(0);" class="current">FAQ Management</a>
        </div>

        <h1>FAQ</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>FAQ</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_faq') : base_url('admin_update_faq') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Question</label>
                                <div class="controls">
                                    <textarea class="span11" id="question" name="question"><?php echo (!empty(set_value('question'))) ? set_value('question') : ((!empty($data_single->question)) ? $data_single->question : ''); ?></textarea>
                                    <?php echo form_error('question', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Answer</label>
                                <div class="controls">
                                    <textarea class="span11" id="answer" name="answer"><?php echo (!empty(set_value('answer'))) ? set_value('answer') : ((!empty($data_single->answer)) ? $data_single->answer : ''); ?></textarea>
                                    <?php echo form_error('answer', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Choose Pictures</label>
                                <div class="controls">
                                    <input type="file" name="img_file[]" id="img_file" accept="image/*" multiple>
                                    <?php echo form_error('img_file', '<div style="color:red;">', '</div>'); ?>
                                </div>
                                <div class="controls">
                                    <?php
                                    if (!empty($data_single->id)) {
                                        $all_images = $this->Faqmodel->get_result_data('faq_images', ['faq_id' => $data_single->id]);
                                        if (count($all_images) > 0) {
                                            foreach ($all_images AS $all_images_val) {
                                                ?>
                                                <div style="position: relative; display: inline-block;">
                                                    <span style="position: absolute; z-index: 9; top: 5px; right: 10px;">
                                                        <a href="javascript:void(0);" onclick="delete_file('<?php echo $all_images_val->id; ?>')"><i class="fa fa-times"></i></a>
                                                    </span>
                                                    <img src="<?php echo base_url(); ?>assets/upload/faq/<?php echo $all_images_val->image; ?>" height="100" width="150">
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <!-- <input type="hidden" name="eventid" id="eventid" value="<?php if(!empty($data_single->id)) echo $data_single->id; ?>"> -->
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<script type="text/javascript">
    function delete_file(id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_delete_faq_image'); ?>/" + id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this picture?");
        }).modal("show");
    }
</script>

