<?php

/**
 * lead Model Class. Handles all the datatypes and methodes required for handling lead
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Leadmodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of lead for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the lead that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('id,event_name');
        $this->db->from(tablename('event'));
        $this->db->where('event.delete_flag', 'N');
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(".$this->session->userdata('admin_uid').", users) !=", 0);
            }
            
            
        }
        $this->db->order_by("event.id", "asc");

        $query = $this->db->get();
        $result = $query->result();
        if(!empty($result))
        {
            $fina_result=array();
            foreach($result as $val)
            {
                $val->lead_count=count($this->get_result_data("leads", array("delete_flag"=>'N',"event_id"=>$val->id)));
                $fina_result[]=$val;
            }
            $result=$fina_result;
        }
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single lead by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single lead by id that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    // public function load_single_data($id = "") {
    //     $this->db->select('*');
    //     $this->db->from(tablename('lead'));
    //     if(!empty($id))
    //     {
    //         $this->db->where('id', $id);
    //     }

    //     $query = $this->db->get();
    //     $result = $query->row();
    //     if (!empty($result)) {
    //         return $result;
    //     } else {
    //         return "";
    //     }
    // }

    public function load_single_data($id = ""){
        if (empty($id)) return "";
        $lead = $this->db->where('id', $id)->get('leads')->row();
        $lead_id = $lead->id;
        $company_info = $this->get_result_data('lead_company', ['lead_id'=> $lead_id]);
        if (count($company_info) > 0){
            $lead->company = $company_info[0]->company;
            $lead->title = $company_info[0]->position;
        }
        return $lead;
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1",$optional_where=NULL) {
        if(empty($optional_where))
        {
            $query = $this->db->get_where(tablename($table), $where);
        }
        else
        {
            $this->db->where_in('id', $optional_where);
            $query = $this->db->get(tablename($table), $where);
        }

        //echo $this->db->last_query(); exit;
        return $query->result();
    }


    public function get_lead_result_data($event_id) {
        
        $this->db->select("app.name,count(user_id) as total_leads, app.profile_image"); 
        $this->db->from("leads as ld");
        $this->db->join("user as app","app.id = ld.user_id");  
        $this->db->where("ld.delete_flag",'N');
        $this->db->where("ld.event_id",$event_id);
        $this->db->where("ld.event_id",$event_id);
        $this->db->group_by('user_id'); 
        $this->db->order_by('total_leads', 'desc'); 
        $this->db->limit(5);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        $result =  $query->result();

        return $result;
    }


    public function get_my_events($table, $where =NULL) {
        
        $query = $this->db->get_where(tablename($table), $where);
        
        //echo $this->db->last_query(); exit;
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single lead for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single lead w.r.t. current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify_old($eid,$id = '') {
        $data['event_id'] = $eid;
        $data['title'] = $this->input->post('title');
        $data['note'] = $this->input->post('note');
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phone');
        $data['company'] = $this->input->post('company');
        if (!empty($id)) 
        {
            $this->db->where('id', $id)->update(tablename('lead'), $data);
            return $id;
        } 
        else 
        {
            $data['created_date'] = date("Y-m-d H:i:s");
            $this->db->insert(tablename('lead'), $data);
            $last_id = $this->db->insert_id();
            if (!empty($last_id)) {
                return $last_id;
            } else {
                return "";
            }
        }
    }

    public function modify($eid,$id = '') {
        $data['event_id'] = $eid;
        $data['note'] = $this->input->post('note');
        $data['name'] = $this->input->post('name');
        $data['emailid'] = $this->input->post('email');
        $data['phoneno'] = $this->input->post('phone');
        
        $company['company'] = $this->input->post('company');
        $company['position'] = $this->input->post('title');
        if (!empty($id)) 
        {
            $this->db->where('id', $id)->update(tablename('leads'), $data);
            
            $company = $this->get_row_data('lead_company', ['lead_id'=> $id]);
            if (count($company) > 0){
                $this->db->where('id', $company->id)->update('lead_company', $company);
            }else{
                if (!empty($company['company']) && !empty($company['position'])){
                    //$lead = $this->db->where('id', $id)->get('leads')->row();
                    $company['lead_id'] = $id; $company['current_job'] = 0;
                    $this->db->insert('lead_company', $company);
                }
            }
            return $id;
        } 
        else 
        {
            $data['created_date'] = date("Y-m-d H:i:s");
            $this->db->insert(tablename('leads'), $data);
            $last_id = $this->db->insert_id();
            $company['lead_id'] = $last_id; $company['current_job'] = 0;
            $this->db->isert('lead_company', $company);
            if (!empty($last_id)) {
                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of lead for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current lead status
     * and change it the the opposite [Table: pb_event]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('lead'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('lead'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of lead for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_event]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('lead'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

    public function insert_data($table,$data1)
    {
        $this->db->insert(tablename($table),$data1);
        return $this->db->insert_id();
    }

    public function delete_data($table,$where)
    {
        $this->db->delete(tablename($table),$where);
        return $this->db->affected_rows();
    }

    public function update_data($table,$where,$data)
    {
        $this->db->where($where);
        $this->db->update($table, $data); 
        //echo $this->db->last_query(); exit;
        return $this->db->affected_rows();
    } 

    public function get_detailed_result_data($table,$table1,$join_condition,$where = "1=1")
    {
        $this->db->join($table1,$join_condition);
        $query=$this->db->get_where(tablename($table), $where);
        return $query->result();
    }


    public function lead_list_order_by_name($event_id,$id) 
    {
       $sql="select * from ".tablename('lead')." where event_id='".$event_id."'  and user_id='".$id."' and delete_flag = 'N' order by name asc";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }
    
    public function lead_list_new($event_id, $user_id){
        return $this->db->select('id, is_scan, lead_id')->where('event_id', $event_id)->where('user_id', $user_id)->order_by('name', 'ASC')->get('leads')->result();
    }

    public function process_lead_company($rows, $lead_id, $update = false){
        if ($update) $this->db->where('lead_id', $lead_id)->delete('lead_company');
        if ($rows && is_array($rows)){
            foreach ($rows as $row){
                unset($row->id); unset($row->user_id);  
                $row->lead_id = $lead_id; $row->created_date = date('Y-m-d H:i:s');
                $this->db->insert('lead_company', $row);
            }
        }
    }
    
    public function process_lead_education($rows, $lead_id, $update = false){
        if ($update) $this->db->where('lead_id', $lead_id)->delete('lead_education');
        if ($rows && is_array($rows)){
            foreach ($rows as $row){
                unset($row->id); unset($row->user_id);  
                $row->lead_id = $lead_id;   $row->created_date = date('Y-m-d H:i:s');
                $this->db->insert('lead_education', $row);
            }
        }
    }
    
    public function process_lead_link($rows, $lead_id, $update = false){
        if ($update) $this->db->where('lead_id', $lead_id)->delete('lead_link');
        if ($rows && is_array($rows)){
            foreach ($rows as $row){
                unset($row->id); unset($row->user_id);  
                $row->lead_id = $lead_id; $row->created_date = date('Y-m-d H:i:s');
                $this->db->insert('lead_link', $row);
            }
        }
    }

}

/* End of file Eventmodel.php */
/* Location: ./application/modules/lead/models/admin/Eventmodel.php */
