<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="<?php echo base_url('admin_lead'); ?>" ><i class="fa fa-flag-checkered"></i>Event Lead</a>
            <a href="<?php echo base_url('admin_view_lead').'/'.$this->uri->segment(2); ?>" ><i class="fa fa-flag"></i>Lead</a>
            <a href="javascript:void(0);" class="current">Lead Management</a>
        </div>

        <h1>Lead</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Lead</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_lead').'/'.$this->uri->segment(2) : base_url('admin_update_lead').'/'.$this->uri->segment(2) . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <!-- <div class="control-group">
                                <label class="control-label">Event</label>
                                <div class="controls">
                                    <select name="event_id" id="event_id">
                                        <?php
                                        if (count($all_event) > 0) {
                                            foreach ($all_event as $all_event_val) {
                                                ?>
                                                <option value="<?php echo $all_event_val->id; ?>"
                                                        <?php if(!empty($data_single)){if($all_event_val->id==$data_single->event_id)echo 'selected'; }?>>
                                                            <?php echo (isset($all_event_val->event_name) && $all_event_val->event_name != '') ? $all_event_val->event_name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('event_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div> -->
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Name" type="text" id="name" name="name" value="<?php echo (!empty(set_value('name'))) ? set_value('name') : ((!empty($data_single->name)) ? $data_single->name : ''); ?>" >
                                    <?php echo form_error('name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Email</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Email" type="text" id="email" name="email" value="<?php echo (!empty(set_value('email'))) ? set_value('email') : ((!empty($data_single->emailid)) ? $data_single->emailid : ''); ?>" >
                                    <?php echo form_error('email', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Phone</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Phone" type="text" id="phone" name="phone" value="<?php echo (!empty(set_value('phone'))) ? set_value('phone') : ((!empty($data_single->phoneno)) ? $data_single->phoneno : ''); ?>" >
                                    <?php echo form_error('phone', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">company</label>
                                <div class="controls">
                                    <input class="span11" placeholder="company" type="text" id="company" name="company" value="<?php echo (!empty(set_value('company'))) ? set_value('company') : ((!empty($data_single->company)) ? $data_single->company : ''); ?>" >
                                    <?php echo form_error('company', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Title</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Agenda Title" type="text" id="title" name="title" value="<?php echo (!empty(set_value('title'))) ? set_value('title') : ((!empty($data_single->title)) ? $data_single->title : ''); ?>" >
                                    <?php echo form_error('title', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Note</label>
                                <div class="controls">
                                    <textarea class="span11" id="note" name="note"><?php echo (!empty(set_value('note'))) ? set_value('note') : ((!empty($data_single->note)) ? $data_single->note : ''); ?></textarea>
                                    <?php echo form_error('note', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <input type="hidden" name="eventid" id="eventid" value="<?php if(!empty($data_single->id)) echo $data_single->id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

