<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="<?php echo base_url('admin_lead'); ?>" ><i class="fa fa-flag-checkered"></i>Event Lead</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-flag"></i>Lead</a>
        </div>

        <h1>Lead</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <?php if(in_array(129, $getRole)){ ?><button class="btn btn-success btn-cls" type="button" onclick="add();">Add Lead</button><?php } ?>
                        <button class="btn btn-success btn-cls" type="button" onclick="export_csv();">Export</button>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Company</th>
                                    <th>Title</th>
                                    <th>Lead Generation</th>
                                    <!--<?php if(in_array(131, $getRole)){ ?><th>Status</th><?php } ?>-->
                                    <?php if(in_array(132, $getRole)){ ?><th>Action</th><?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                               <?php if(in_array(130, $getRole)){ ?> <u><a href="<?php echo base_url('admin_update_lead'.'/'.$this->uri->segment(2) . '/' . $data->id); ?>">
                                                <?php echo $data->name; ?>
                                                </a></u><?php }else{ echo $data->name; } ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->emailid; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->phoneno; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->company; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->title; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php if($data->is_scan==1){echo "Scan";}else{echo "Manual";} ?>
                                            </td>
                                           <?php //if(in_array(131, $getRole)){ ?> 
                                           <!--<td class="custom-action-btn">-->
                                                <?php
                                                //if ($data->is_active == "Y") {
                                                    //echo '<i title="De-activate" class="fa fa-close" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                //} else {
                                                //    echo '<i title="Activate" class="fa fa-check" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                //}
                                                ?>
                                            <!--</td>-->
                                            <?php //} ?>
                                            <?php if(in_array(132, $getRole)){ ?><td class="custom-action-btn">
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                            </td><?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add Event Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_lead').'/'.$this->uri->segment(2); ?>";
    }
    
    function export_csv(){
        window.location.href= "<?=base_url('export_leadlist_as_csv').'/'.$this->uri->segment(2)?>";
    }

    /**
     * Status Change Function
     *
     * @param   rec_id
     * @return  redirects and gives control to status handler
     */
    function change_status(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_status_lead'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to change status of this Lead?");
        }).modal("show");
    }

    /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_delete_lead'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this Lead?");
        }).modal("show");
    }

    
</script>
