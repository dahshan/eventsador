<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for lead [HMVC]. Handles all the datatypes and methodes required for lead section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Leadmodel');
    }

    /**
     * Index Page for this lead controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Leadmodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'lead/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of lead module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($eid,$id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('note', 'Note', 'trim|required');
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric');
            $this->form_validation->set_rules('company', 'Company', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Leadmodel->modify($eid,$id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! lead already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Lead modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_view_lead').'/'.$eid);
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        if (!empty($id)) {
            $this->data['data_single'] = $this->Leadmodel->load_single_data($id);
            $this->data['all_room'] = $this->Leadmodel->get_result_data('agenda_location', ['event_id' => $this->data['data_single']->event_id]);
        }
        
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->data['all_event'] = $this->Leadmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->data['all_event'] = $this->Leadmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
                }
            }
        }
        else
        {
            $this->data['all_event'] = $this->Leadmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
        }
        $this->middle = 'lead/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for organizer selected or not
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function organizers_check($organizers) {
        $organizers = $this->input->post('organizers');
        if (!isset($organizers) || count($organizers) == 0) {
            $this->form_validation->set_message('organizers_check', '{field} Cannotbe blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $event_id = $this->input->post('event_id');
            $event_data = $this->Leadmodel->get_row_data('lead', ['id<>' => $event_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($event_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of lead module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Leadmodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Lead status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Delete function of lead module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Leadmodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Lead deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

   public function listlead($eid) 
    {
        $all_data = $this->Leadmodel->get_result_data('leads',array("event_id"=>$eid));
        foreach ($all_data as $lead){
            $lead_id = $lead->id;
            $company_info = $this->Leadmodel->get_result_data('lead_company', ['lead_id'=> $lead_id]);
            if (count($company_info) > 0){
                $lead->company = $company_info[0]->company;
                $lead->title = $company_info[0]->position;
            }
        }
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;
        $this->middle = 'lead/admin/listlead';
        $this->admin_layout();
    }

    public function export_as_csv($event_id){
        $event = $this->Leadmodel->get_row_data('event', ['id'=> $event_id]);
        $all_data = $this->Leadmodel->get_result_data('leads',array("delete_flag"=>'N',"event_id"=>$event_id));
        // $getRole = getRole();
        // echo $event_id;
        // echo "<pre>";
        // print_r($all_data);
        // exit;
		    $filename = "Lead - ".$event->event_description.".csv";
            header("Content-Type: application/xls");    
            header("Content-Disposition: attachment; filename=$filename");  
            header("Pragma: no-cache"); 
            header("Expires: 0");
            echo  'S.No.,'.'Name,'. 'Email,'.'Phone,'.'Company,'.'Title, '.'Lead Generation,'."\r\n";
            $sn=1;
            if(!empty($all_data)){
                $index = 0;
			    foreach($all_data as $data){
			        $index ++;
			     //   echo '"' . $index . '", ' . '"' . $data->name . '", ' . '"' . $data->email . '", ' . '"' . $data->phone . '", ' . '"'. $data->company . '", ' . '"' . $data->title . '", ' .  '"' . $data->note . '", ' . '"'. ($data->is_scan == 1 ? 'Scan' : "Mannual") . '",' . "\r\n";
			     $name = $data->name; $email = $data->emailid; $phone = $data->phoneno ; $company = $data->company; $title = $data->title; $gen = $data->is_scan == 1 ? 'Scan' : "Mannual";
			        echo "$index, $name, $email, $phone ,$company, $title, $gen ,\r\n";

			    }
            }
            exit;
    }

}

/* End of file admin.php */
/* Location: ./application/modules/lead/controllers/admin.php */
