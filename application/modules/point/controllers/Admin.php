<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for point [HMVC]. Handles all the datatypes and methodes required for point section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Pointmodel');
    }

    /**
     * Index Page for this point controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Pointmodel->load_events();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'point/admin/list';
        $this->admin_layout();
    }

    public function search() {
        $this->load->model('../models/admin/Adminauthmodel');
        $eventid = $_POST['event'];

        $all_data = [];
        if ($eventid) {
            $all_data = $this->Pointmodel->load_points($eventid);
        }

        $data['all_data'] = $all_data;
        $this->load->view('point/admin/ajax_points',$data);
    }


    /**
     * Add/Edit function of point module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('question', 'Question', 'trim|required');
            $this->form_validation->set_rules('answer', 'Answer', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Pointmodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! point already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'point modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_faq'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Pointmodel->load_single_data($id);
        $this->middle = 'point/admin/form';
        $this->admin_layout();
    }

    public function delete($id) {
        $flg = $this->Pointmodel->delete_point($id);
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Point deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_point'));
    }

}

/* End of file admin.php */
/* Location: ./application/modules/point/controllers/admin.php */
