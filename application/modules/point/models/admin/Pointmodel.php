<?php

/**
 * point Model Class. Handles all the datatypes and methodes required for handling point
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Pointmodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of point for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the point that has been added by current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $final_array = array();
        $sql="select distinct user_id,name from ".tablename('point').",".tablename('user')." app where user_id=app.id ";
        $query=$this->db->query($sql);
        $result=$query->result();
        if(!empty($result))
        {
            $final_array=[];
            foreach($result as $val)
            {
                $val->picture_count=$this->point_sum($val->user_id,"Picture");
                $val->video_count=$this->point_sum($val->user_id,"Video");
                $val->commnet_speaker_count=$this->point_sum($val->user_id,"Comment/Speaker");
                $val->like_count=$this->point_sum($val->user_id,"Like");
                $final_array[]=$val;
            }
        }
        $result=$final_array;
        return $result;
    }



    /**
     * Used for loading functionality of Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the Event that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_events() {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('delete_flag', 'N');
       // echo "<pre>";print_r($this->session->userdata('admin_role_type'));exit;
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(".$this->session->userdata('admin_uid').", users) !=", 0);
            } 
        }
        $this->db->order_by("id", "desc");

        $query = $this->db->get();
        $result = $query->result();
        //echo $this->db->last_query();
        // echo "<pre>";print_r($result);exit;
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

   
    public function load_event($eventid) {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('id', $eventid);
        
        $query = $this->db->get();
        $result = $query->first_row();

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

   
    public function load_points($eventid) {
        $final_array = array();
        $sql="select distinct user_id,name from ".tablename('point').",".tablename('user')." app where user_id=app.id and event_id={$eventid}";
        $query=$this->db->query($sql);
        $result=$query->result();
        if(!empty($result))
        {
            $final_array=[];
            foreach($result as $val)
            {
                $val->picture_count=$this->point_sum($val->user_id,"Picture");
                $val->video_count=$this->point_sum($val->user_id,"Video");
                $val->commnet_speaker_count=$this->point_sum($val->user_id,"Comment/Speaker");
                $val->like_count=$this->point_sum($val->user_id,"Like");
                $final_array[]=$val;
            }
        }
        $result=$final_array;
        return $result;
    }




    /**
     * Used for loading functionality of single point by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single point by id that has been added by current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('point'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    public function load_user_points_data($id = "") {
      
        $sql="select SUM(point) as points_earned from ".tablename('point')." where user_id=".$id." ";
        $query=$this->db->query($sql);
        $result=$query->result();

        return $result;
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1") {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    public function get_result_data1($table,$whr,$table1=NULL,$condition=NULL,$field=NULL,$tag=NULL) 
    {
        if(!empty($table1))
        {
            // $this->db->select('exhibitor.*,exhibitor_type.name as type_name');
            // $this->db->join($table1,$condition);
        }
        $query =$this->db->where_in($field,$whr)->get(tablename($table));
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single point for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single point w.r.t. current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        $data['question'] = $this->input->post('question');
        $data['answer'] = $this->input->post('answer');
        if (!empty($id)) 
        {
            $this->db->where('id', $id)->update(tablename('point'), $data);
            $file = $_FILES['img_file'];
            if (isset($file['name']) && $file['name'] != '' && count($file['name'])) {
                foreach ($file['name'] AS $file_name_key => $file_name) {
                    if (!empty($file['name'][$file_name_key])) {
                        $info = pathinfo($file['name'][$file_name_key]);
                        $ext = $info['extension']; // get the extension of the file
                        $newname = "media_" . time() . $file_name_key . "." . $ext;
                        $target = __DIR__ . '/../../../../../assets/upload/point/' . $newname;
                        move_uploaded_file($_FILES['img_file']['tmp_name'][$file_name_key], $target);
                        $data = array(
                            'faq_id' => $id,
                            'image' => $newname,
                            'created_date' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert(tablename('faq_images'), $data);
                    }
                }
            }
            return $id;
        } 
        else 
        {
            $data['created_date'] = date("Y-m-d H:i:s");
            $this->db->insert(tablename('point'), $data);
            $last_id = $this->db->insert_id();
            if (!empty($last_id)) {
                $file = $_FILES['img_file'];
                if (isset($file['name']) && $file['name'] != '' && count($file['name'])) {
                    foreach ($file['name'] AS $file_name_key => $file_name) {
                        if (!empty($file['name'][$file_name_key])) {
                            $info = pathinfo($file['name'][$file_name_key]);
                            $ext = $info['extension']; // get the extension of the file
                            $newname = "media_" . time() . $file_name_key . "." . $ext;
                            $target = __DIR__ . '/../../../../../assets/upload/point/' . $newname;
                            move_uploaded_file($_FILES['img_file']['tmp_name'][$file_name_key], $target);
                            $data = array(
                                'faq_id' => $last_id,
                                'image' => $newname,
                                'created_date' => date('Y-m-d H:i:s')
                            );
                            $this->db->insert(tablename('faq_images'), $data);
                        }
                    }
                }
                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of point for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current point status
     * and change it the the opposite [Table: pb_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('point'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('point'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of point for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('point'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

    public function delete_data($table,$where)
    {
        $this->db->delete(tablename($table),$where);
        return $this->db->affected_rows();
    }

    public function delete_one($table, $where){
        $row = $this->get_row_data($table, $where);
        if ($row){
            $this->db->delete(tablename($table), ['id'  => $row->id]);
            return $this->db->affected_rows();
        }else{
            return 0;
        }
        
    }

    public function point_sum($user_id,$point_type) 
    {
        $sql="select SUM(`point`) as sum_total FROM ".tablename('point')." WHERE `user_id`='".$user_id."' and `point_type`='".$point_type."'";
        $query=$this->db->query($sql);
        $result=$query->row();
        return $result->sum_total;
    }

    public function update_data($table,$where,$data)
    {
        $this->db->where($where);
        $this->db->update($table, $data); 
        return $this->db->affected_rows();
    } 

    public function insert_data($table,$data1)
    {
        $this->db->insert(tablename($table),$data1);
        return $this->db->insert_id();
    }

    public function delete_point($id){
      $this->db->where('user_id', $id);
      $this->db->delete('point');
      return true;
    }

}

/* End of file Sponsormodel.php */
/* Location: ./application/modules/point/models/admin/Sponsormodel.php */
