<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-arrow-up"></i>Point</a>
        </div>

        <h1>Point</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <!-- <button class="btn btn-success btn-cls" type="button" onclick="add();">Add FAQ</button> -->
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>User</th>
                                    <th>Picture</th>
                                    <!--<th>Video</th>-->
                                    <th>Like</th>
                                    <th>Comment/Speaker</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                               // echo "<pre>"; print_r($all_data); die;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->name)) ? $data->name : "NA"; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->picture_count)) ? $data->picture_count : "0"; ?></td>
                                            <!--<td class="custom-action-btn"><?php echo (!empty($data->video_count)) ? $data->video_count : "0"; ?></td>-->
                                            <td class="custom-action-btn"><?php echo (!empty($data->like_count)) ? $data->like_count : "0"; ?></td><td class="custom-action-btn"><?php echo (!empty($data->commnet_speaker_count)) ? $data->commnet_speaker_count : "0"; ?></td>

                                            <td class="custom-action-btn">
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->user_id; ?>);"></i>
                                            </td> 
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add Sponsor Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    // function add() {
    //     window.location.href = "<?php echo base_url('admin_add_faq'); ?>";
    // }

    /**
     * Status Change Function
     *
     * @param   rec_id
     * @return  redirects and gives control to status handler
     */
    // function change_status(rec_id) {
    //     $("#myAlert").on('shown.bs.modal', function () {
    //         $("#modal_confirm").click(function () {
    //             window.location.href = "<?php echo base_url('admin_status_faq'); ?>/" + rec_id;
    //         });

    //         $("#notification_heading").html("Confirmation");
    //         $("#notification_body").html("Do you want to change status of this faq?");
    //     }).modal("show");
    // }

    /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_point_delete'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this Point?");
        }).modal("show");
    }
</script>
