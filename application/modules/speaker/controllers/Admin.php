<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for speaker [HMVC]. Handles all the datatypes and methodes required for speaker section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Speakermodel');
    }

    /**
     * Index Page for this speaker controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Speakermodel->load_events();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'speaker/admin/list';
        $this->admin_layout();
    }

    public function search() {
        $this->load->model('../models/admin/Adminauthmodel');
        $eventid = $_POST['event'];

        $event = $this->Speakermodel->load_event($eventid);
        $all_data = [];
        if ($event->speakers) {
            $all_data = $this->Speakermodel->load_speakers($event->speakers);
        }

        $data['all_data'] = $all_data;
        $this->load->view('speaker/admin/ajax_speakers',$data);
    }



    /**
     * Index Page for this Organizer controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function speakers_by_event() {
        if (is_numeric($this->uri->segment(2))) {
            $event = $this->Speakermodel->load_event_data($this->uri->segment(2));
            $all_data = $this->Speakermodel->load_all_data_in($event->speakers);
            $uri = $this->uri->segment(1);
            $this->data = array();
            $this->data['uri'] = $uri;
            $this->data['all_data'] = $all_data;
            $this->middle = 'speaker/admin/list';
            $this->admin_layout();
        }
    }

    /**
     * Add/Edit function of speaker module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            if ($param == 'A') {
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_new_email_check');
            } else {
                $this->form_validation->set_rules('email', 'Email', 'trim|required|callback_old_email_check');
            }
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Speakermodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Speaker already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Speaker modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_speaker'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Speakermodel->load_single_data($id);
        $this->middle = 'speaker/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function new_email_check($email) {
        if (isset($email) && $email != '') {
            $sponsor_data = $this->Speakermodel->get_row_data('user', ['emailid' => $email, 'delete_flag' => 'N']);
            if (count($sponsor_data) > 0) {
                $this->form_validation->set_message('new_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $speaker_id = $this->input->post('speaker_id');
            $speaker_data = $this->Speakermodel->get_row_data('user', ['id<>' => $speaker_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (isset($speaker_data) && count($speaker_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of speaker module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Speakermodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Speaker status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_speaker'));
    }

    /**
     * Delete function of speaker module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $user = $this->Speakermodel->load_single_data($id);
        $flg = $this->Speakermodel->delete($user);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Speaker deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_speaker'));
    }

}

/* End of file admin.php */
/* Location: ./application/modules/speaker/controllers/admin.php */
