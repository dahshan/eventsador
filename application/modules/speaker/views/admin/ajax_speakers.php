<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>

                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>                       
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Country</th>
                                    <?php if(in_array(59, $getRole)){ ?><th>Status</th><?php } ?>
                                    <?php if(in_array(60, $getRole)){ ?><th>Action</th><?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <?php if(in_array(58, $getRole)){ ?><u><a href="<?php echo base_url('admin_update_speaker' . '/' . $data->id); ?>">
                                                    <?php echo (isset($data->name) & $data->name!='')?$data->name:''; ?>
                                                </a></u><?php }else{ echo (isset($data->name) & $data->name!='')?$data->name:''; } ?>
                                            </td>
                                            <td><img style="height: 50px;width: 50px;" <?php if(!empty($data->profile_image)){ ?> src="<?php echo base_url('assets/upload/appuser'); ?>/200_<?php echo $data->profile_image; ?>" <?php }else{ ?> src="<?php echo base_url('assets/upload/default_man.png'); ?>" <?php } ?> ></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->emailid)) ? $data->emailid : "NA"; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->phoneno)) ? $data->phoneno : "NA"; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->country)) ? $data->country_name : "NA"; ?></td>
                                            <?php if(in_array(59, $getRole)){ ?><td class="custom-action-btn">
                                                <?php
                                                if ($data->is_active == "Y") {
                                                    echo '<i title="De-activate" class="fa fa-close" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                } else {
                                                    echo '<i title="Activate" class="fa fa-check" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                }
                                                ?>
                                            </td><?php } ?>

                                            <?php if(in_array(60, $getRole)){ ?><td class="custom-action-btn">
                                                <!-- <i title="Reset Password" class="fa fa-repeat" style="margin:0 3px 2px 0;" aria-hidden="false" onclick="reset_passwd(<?php echo $data->id; ?>);"></i> -->
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                            </td><?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>