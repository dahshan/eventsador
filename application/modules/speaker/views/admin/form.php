<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_speaker'); ?>"><i class="fa fa-microphone"></i>Speaker</a>
            <a href="javascript:void(0);" class="current">Speaker Management</a>
        </div>

        <h1>Speaker</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Speaker</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_speaker') : base_url('admin_update_speaker') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Name" type="text" id="name" name="name" value="<?php echo (!empty(set_value('name'))) ? set_value('name') : ((!empty($data_single->name)) ? $data_single->name : ''); ?>" >
                                    <?php echo form_error('name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Email</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Email" type="text" id="email" name="email" value="<?php echo (!empty(set_value('email'))) ? set_value('email') : ((!empty($data_single->emailid)) ? $data_single->emailid : ''); ?>">
                                    <?php echo form_error('email', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Phone</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Phone" type="text" id="phoneno" name="phone" value="<?php echo (!empty(set_value('phone'))) ? set_value('phone') : ((!empty($data_single->phoneno)) ? $data_single->phoneno : ''); ?>" >
                                    <?php echo form_error('phone', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Image</label>
                                <div class="controls">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                      <img  src="<?php if(!empty($data_single->profile_image)){ echo base_url('assets/upload/appuser')."/".$data_single->profile_image; } else {echo base_url('assets/upload/default_man.png');}?>" data-src="<?php if(!empty($data_single->profile_image)){ echo base_url('assets/upload/appuser')."/".$data_single->profile_image; } else {echo base_url('assets/upload/default_man.png');}?>" alt="...">
                                    </div>
                                    <div>
                                      <div class="controls custon_textarea" style="margin-left: 0;">
                                        <input type="file" name="image" style="line-height:1px;">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>










                            <div class="control-group">
                                <label class="control-label">Position</label>
                                <div class="controls">
                                    <input class="span11" placeholder="position" type="text" id="position" name="position" value="<?php echo (!empty(set_value('position'))) ? set_value('position') : ((!empty($data_single->position_title)) ? $data_single->position_title : ''); ?>" >
                                    <?php echo form_error('position', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Company</label>
                                <div class="controls">
                                    <input class="span11" placeholder="company" type="text" id="company" name="company" value="<?php echo (!empty(set_value('company'))) ? set_value('company') : ((!empty($data_single->company)) ? $data_single->company : ''); ?>" >
                                    <?php echo form_error('company', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">City</label>
                                <div class="controls">
                                    <input class="span11" placeholder="city" type="text" id="city" name="city" value="<?php echo (!empty(set_value('city'))) ? set_value('city') : ((!empty($data_single->city_name)) ? $data_single->city_name : ''); ?>" >
                                    <?php echo form_error('city', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Country</label>
                                <div class="controls">
                                    <input class="span11" placeholder="country" type="text" id="country" name="country" value="<?php echo (!empty(set_value('country'))) ? set_value('country') : ((!empty($data_single->country_name)) ? $data_single->country_name : ''); ?>" >
                                    <?php echo form_error('country', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>



                            <input type="hidden" name="speaker_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
