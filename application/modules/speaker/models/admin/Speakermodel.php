<?php

/**
 * speaker Model Class. Handles all the datatypes and methodes required for handling speaker
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Speakermodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of speaker for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the speaker that has been added by current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('delete_flag', 'N');
        $this->db->where('type_id', 3);
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('organizer_id', $this->session->userdata('admin_uid'));
            }
            // else
            // {
            //     $this->db->where('organizer_id', $this->session->userdata('admin_org_id'));
            // }
        }
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the Event that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_events() {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('delete_flag', 'N');
       // echo "<pre>";print_r($this->session->userdata('admin_role_type'));exit;
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(".$this->session->userdata('admin_uid').", users) !=", 0);
            } 
        }
        $this->db->order_by("id", "desc");

        $query = $this->db->get();
        $result = $query->result();
        //echo $this->db->last_query();
        // echo "<pre>";print_r($result);exit;
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

   
    public function load_event($eventid) {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('id', $eventid);
        
        $query = $this->db->get();
        $result = $query->first_row();

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

   
    public function load_speakers($speakers) {
        $this->db->select('user.*, countries.name as country_name');
        $this->db->from(tablename('user'));
        $this->db->join('countries', 'countries.id=user.country', 'LEFT');
        $this->db->where("user.id in ($speakers)");
        
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

    /**
     * Used for loading functionality of single speaker by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single speaker by id that has been added by current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of user for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the user that has been added by current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data_in($where_str) {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('delete_flag', 'N');
        $this->db->where('role_id', 2);
        $this->db->where_in('id', $where_str);
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single Event by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single Event by id that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_event_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1") {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single speaker for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single speaker w.r.t. current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        $type_id = 3;

        if($this->session->userdata('admin_role_type')==2)
        {
            $organizer_id=$this->session->userdata('admin_uid');
        }
        else
        {
            $organizer_id=0;
        }
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $image = "";
        if (!empty($_FILES['image']['name'])) 
        {
            $this->load->library('upload');
            $config['upload_path'] = './assets/upload/appuser/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['file_name'] = time();

            $this->upload->initialize($config);
            if($_FILES['image']['error']==0)
            {
                $profilepic=$_FILES['image']['name'];
                $profilepic=explode('.',$profilepic);
                $ext=end($profilepic);
                if($ext=="jpg" or $ext=="jpeg" or $ext=="png" or $ext=="bmp")
                {
                    $profilepic=time().rand().".".$ext;
                    $destination="./assets/upload/appuser/".$profilepic;
                    if (move_uploaded_file($_FILES['image']['tmp_name'] ,$destination))
                    {
                        $image=$profilepic;
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $destination; 
                        //echo $origin.file_exists($origin).'<br>';
                        $config['new_image'] = './assets/upload/appuser/200_' . $profilepic;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = 200;
                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();

                        if (!empty($id)) {
                            $old_info = $this->db->get_where(tablename('user'),['id'=>$id])->first_row();
                            $olddestination="./assets/upload/appuser/".$old_info->profile_image;
                            $olddestinationthumb="./assets/upload/appuser/200_".$old_info->profile_image;
                            if (file_exists($olddestination)) {
                                @unlink($olddestination);
                            }
                            if (file_exists($olddestinationthumb)) {
                                @unlink($olddestinationthumb);
                            }

                        }

                    }
                }
                else
                {
                    $this->session->set_flashdata('errormessage', 'Only .jpg,.jpeg,.bmp and .png image extensions are supported');
                    $redirect = site_url('admin/admin_sponsor');
                    redirect($redirect);
                }
            }
        }

   
        $position = $this->input->post('position');
        $company = $this->input->post('company');
        $city = $this->input->post('city');
        $country = $this->input->post('country');


         $this->load->helper('common');
        $password = generateRandomString(10);
        $passwordEnc = md5($password);


         $date = date("Y-m-d H:i:s");
        if (!empty($id)) {
            if(!empty($image))
            {
                $data = array(
                    'organizer_id' => $organizer_id,
                    'name' => $name,
                    'emailid' => $email,
                    'phoneno' => $phone,
                    'profile_image' => $image,
                    'position_title' => $position,
                    'company' => $company,
                    'city_name' => $city,
                    'country_name' => $country,
                    'modified_date' => $date,
                );
            }
            else
            {
                $data = array(
                    'organizer_id' => $organizer_id,
                    'name' => $name,
                    'emailid' => $email,
                    'phoneno' => $phone,
                    'position_title' => $position,
                    'company' => $company,
                    'city_name' => $city,
                    'country_name' => $country,
                    'modified_date' => $date,
                );
            }

            $this->db->where('id', $id)->update(tablename('user'), $data);
            return $id;
        } else {
            // Check for Duplicate
            $data = array(
                'organizer_id' => $organizer_id,
                'name' => $name,
                'emailid' => $email,
                'phoneno' => $phone,
                'profile_image' => $image,
                'position_title' => $position,
                'company' => $company,
                'city_name' => $city,
                'country_name' => $country,
                'entry_date' => $date,
                'password' => $passwordEnc,
                'type_id'=>$type_id
            );

            $this->db->insert(tablename('user'), $data);
            $last_id = $this->db->insert_id();

            if (!empty($last_id)) {

               $subject = "Eventsador - Account Created as Speaker";
               $message = '<p>Greetings ' . $name . ",</p>";
               $message .= '<p>Your account has been created with Eventsador. Please use the following credential to login to your profile.</p>';
               $message .= '<p><strong>user-id: </strong>' . $email;
               $message .= '<br><strong>Password: </strong>' . $password . "</p>";
               $message .= '<p>Thank you,</p>';
               $message .= '<p>Eventsador Admin</p>';

               $mail_data = [
                   'name' => $name,
                   'body' => $message,
               ];

               $this->load->helper('email');
               send_email($email, $subject, $mail_data);

                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of speaker for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current speaker status
     * and change it the the opposite [Table: pb_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('user'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of speaker for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($user) {
        $delete_faq = array('delete_flag' => 'Y', 'name' => $user->name . '[deleted]', 'emailid' => 'deleted'.rand ( 10000 , 99999 ).'.'.$user->emailid);
        $this->db->where('id', $user->id);

        if ($this->db->update(tablename('user'), $delete_faq, ['id' => $user->id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

}

/* End of file Sponsormodel.php */
/* Location: ./application/modules/speaker/models/admin/Sponsormodel.php */
