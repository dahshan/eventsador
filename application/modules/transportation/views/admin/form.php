<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_transportation'); ?>" ><i class="fa fa-bus"></i>Transportation</a>
            <a href="<?php echo base_url('admin_view_transportation').'/'.$this->uri->segment(2); ?>" ><i class="fa fa-car"></i>Transportation List</a>
            <a href="javascript:void(0);" class="current">Transportation Management</a>
        </div>

        <h1>Transportation</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Transportation</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_transportation').'/'.$this->uri->segment(2) : base_url('admin_update_transportation').'/'.$this->uri->segment(2) . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Transportation Company</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Transportation Company" type="text" id="transportation_company" name="transportation_company" value="<?php echo (!empty(set_value('transportation_company'))) ? set_value('title') : ((!empty($data_single->transportation_company)) ? $data_single->transportation_company : ''); ?>" >
                                    <?php echo form_error('transportation_company', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Details</label>
                                <div class="controls">
                                    <textarea class="span11" id="details" name="details"><?php echo (!empty(set_value('details'))) ? set_value('details') : ((!empty($data_single->details)) ? $data_single->details : ''); ?></textarea>
                                    <?php echo form_error('details', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="span6">
                                    <label class="control-label">Type of Car</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Type of Car" type="text" id="type_of_car" name="type_of_car" value="<?php echo (!empty(set_value('type_of_car'))) ? set_value('title') : ((!empty($data_single->type_of_car)) ? $data_single->type_of_car : ''); ?>" >
                                        <?php echo form_error('type_of_car', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">Model of Car</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Model Of A Car" type="text" id="model_of_car" name="model_of_car" value="<?php echo (!empty(set_value('model_of_car'))) ? set_value('title') : ((!empty($data_single->model_of_car)) ? $data_single->model_of_car : ''); ?>" >
                                        <?php echo form_error('model_of_car', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="span4">
                                    <label class="control-label">Link</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Enter Link" type="text" id="link" name="link" value="<?php echo (!empty(set_value('link'))) ? set_value('title') : ((!empty($data_single->link)) ? $data_single->link : ''); ?>" >
                                        <?php echo form_error('link', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span4">
                                    <label class="control-label">Price</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Enter Price" type="text" id="price" name="price" value="<?php echo (!empty(set_value('price'))) ? set_value('title') : ((!empty($data_single->price)) ? $data_single->price : ''); ?>" >
                                        <?php echo form_error('price', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>

                               <!--  <div class="span4">
                                    <label class="control-label">Currency</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Enter Currency" type="text" id="currency" name="currency" value="<?php echo (!empty(set_value('currency'))) ? set_value('title') : ((!empty($data_single->currency)) ? $data_single->currency : ''); ?>" >
                                        <?php echo form_error('currency', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div> -->
                            </div>


                            <div class="control-group">
                                <label class="control-label">Currency</label>
                                <div class="controls">
                                    <select name="currency" id="currency">
                                    <option value="">Select Currency</option>
                                        <?php
                                        if (count($all_currency) > 0) {
                                            foreach ($all_currency as $cur) {
                                                $ccc = $cur->code;
                                                ?>
                                                <option value="<?php echo $ccc; ?>"
                                                        <?php if(!empty($data_single)){if($ccc==$data_single->currency)echo 'selected'; }?>>
                                                            <?php echo (isset($cur->code) && $cur->code != '') ? $ccc : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                        
                                        
                                    </select>
                                    <br><br>
                                    <?php if ($per_add_currency) :?>
                                    <a style="cursor: pointer;font-size: 14px;" data-toggle="modal" data-target="#myModalCurr">+ Add Currency</a>
                                    <?php echo form_error('currency', '<div style="color:red;">', '</div>'); ?>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span6">
                                    <label class="control-label">Is Deal</label>
                                    <div class="controls">
                                        <input type="checkbox" <?php if(!empty($data_single->is_deal)){ echo "checked"; }?> value="1" name="is_deal" id="is_deal">
                                    </div>
                                </div>
                                <div class="span5" id="deal_tab" <?php if(empty($data_single->is_deal)){ ?> style="display: none" <?php } ?>>
                                    <label class="control-label">Deal</label>
                                    <div class="controls">
                                        <input maxlength="100" class="span11" placeholder="Enter Deal" type="text" id="deal_text" name="deal_text" value="<?php echo (!empty(set_value('deal_text'))) ? set_value('title') : ((!empty($data_single->deal_text)) ? $data_single->deal_text : ''); ?>" >
                                        <?php echo form_error('deal_text', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <?php echo form_error('deal_text', '<div style="color:red;">', '</div>'); ?>
                            </div>

                            <!--<div class="control-group">-->
                            <!--    <div class="span5">-->
                            <!--        <label class="control-label">Latitude</label>-->
                            <!--        <div class="controls">-->
                            <!--            <input class="span11" placeholder="Enter Latitude" type="text" id="latitude" name="latitude" value="<?php echo (!empty(set_value('latitude'))) ? set_value('title') : ((!empty($data_single->latitude)) ? $data_single->latitude : ''); ?>" >-->
                            <!--            <?php echo form_error('latitude', '<div style="color:red;">', '</div>'); ?>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--    <div class="span5">-->
                            <!--        <label class="control-label">Longitude</label>-->
                            <!--        <div class="controls">-->
                            <!--            <input class="span11" placeholder="Enter Longitude" type="text" id="longitude" name="longitude" value="<?php echo (!empty(set_value('longitude'))) ? set_value('title') : ((!empty($data_single->longitude)) ? $data_single->longitude : ''); ?>" >-->
                            <!--            <?php echo form_error('longitude', '<div style="color:red;">', '</div>'); ?>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--    <div class="span2">-->
                            <!--        <button class="btn btn-success btn-cls" type="button" onclick="initMap();">Map</button>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <!--<div class="control-group" id="div_map" style="display: none;">-->
                            <!--    <div id="map_canvas" style="height:330px;max-width:950px !important;margin-top: 12px;margin-left: 20px;margin-bottom: 12px;"></div>-->
                            <!--</div>-->
                            <div class="control-group">
                                <label class="control-label">Image</label>
                                <div class="controls">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                      <img  src="<?php if(!empty($data_single->image)){ echo base_url('assets/upload/transportation')."/".$data_single->image; } else {echo base_url('assets/upload/placeholder.png');}?>" data-src="<?php if(!empty($data_single->image)){ echo base_url('assets/upload/transportation')."/".$data_single->image; } else {echo base_url('assets/upload/placeholder.png');}?>" alt="...">
                                    </div>
                                    <div>
                                      <div class="controls custon_textarea" style="margin-left: 0;">
                                        <input type="file" name="image" style="line-height:1px;">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <!-- <input type="hidden" name="eventid" id="eventid" value="<?php if(!empty($data_single->id)) echo $data_single->id; ?>"> -->
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModalCurr" class="modal" role="dialog" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Currency</h4>
      </div>
      <div class="modal-body">
            <form class="form-horizontal">
                           
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="" placeholder="Enter Name" type="text" id="cur_name" name="cur_name" value="" >
                                    <div id="name_err" style="color:red;"></div>
                                </div>
                            </div>

                             <div class="control-group">
                                <label class="control-label">Code</label>
                                <div class="controls">
                                    <input class="" placeholder="Enter Code" type="text" id="cur_code" name="cur_code" value="" >
                                    <div id="code_err" style="color:red;"></div>
                                </div>
                            </div>


                             <!-- <div class="control-group">
                                <label class="control-label">Symbol</label>
                                <div class="controls">
                                    <input class="" placeholder="Enter Symbol" type="text" id="cur_symbol" name="cur_symbol" value="" >
                                    <div id="symbol_err" style="color:red;"></div>
                                </div>
                            </div>  -->                          
                        </form>

      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" onclick="save_curr();" >Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrhTMHvt2CLbSI_GnMoEpNz7Z2s6SOxdE"></script>
<script type="text/javascript">
    var marker
    function initMap() {
        var lat=Number($("#latitude").val());
        var lng=Number($("#longitude").val());
        var myLatLng = {lat: lat, lng: lng};
        // if(lat && lng)
        // {
        //     var myLatLng = {lat: lat, lng: lng};
        // }
        // else
        // {
        //     var myLatLng = {lat: lat, lng: lng};
        // }
        $("#div_map").removeAttr("style");
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: 15,
          center: myLatLng
        });

        marker = new google.maps.Marker({
          position: myLatLng,
          map: map
          // title: 'Hello World!'
        });
        
        google.maps.event.addListener(map, 'click', function(event) {
            addMarker(event.latLng);
        });
    }
    $(document).on("change","#is_deal",function(){
        if (this.checked) {
            $("#deal_text").val("");
            $("#deal_tab").removeAttr("style");
        } else {
            $('#deal_tab').css('display','none');
        }
    });


    function save_curr() {
        var cur_name = $("#cur_name").val();
        var cur_code = $("#cur_code").val();
      //  var cur_symbol = $("#cur_symbol").val();

        if((cur_name!='') && (cur_code!='') ){
            $("#name_err").html('');
            $("#code_err").html('');
           // $("#symbol_err").html('');

            $.post("<?php echo base_url('admin_save_currency'); ?>",{cur_name:cur_name,cur_code:cur_code},function(result) {
                //console.log(result);
                $("#currency").append('<option value="'+cur_code+'">'+cur_code+'</option>');
                $("#myModalCurr").modal('hide');
            });
        }else{
            if(cur_name==''){
                $("#name_err").html('Name Field is required.');
            }
            if(cur_code==''){
                $("#code_err").html('Code Field is required.');
            }
            // if(cur_symbol==''){
            //     $("#symbol_err").html('Symbol Field is required.');
            // }
        }
    }
    
    function addMarker(position) {
        marker.setPosition(position);
        $("#latitude").val(position.lat());
        $('#longitude').val(position.lng());
    }
</script>
