<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for transportation [HMVC]. Handles all the datatypes and methodes required for transportation section of Optus
 *
 * @author  
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Transportationmodel');
        $this->load->model('accomodation/admin/Accomodationmodel');
    }

    /**
     * Index Page for this transportation controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Transportationmodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'transportation/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of transportation module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($eid,$id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('transportation_company', 'Transportation Company', 'trim|required');
            $this->form_validation->set_rules('details', 'Transportation Details', 'trim|required');
            $this->form_validation->set_rules('type_of_car', 'Type of Car', 'trim|required');
            $this->form_validation->set_rules('model_of_car', 'Model of Car', 'trim|required');
            $this->form_validation->set_rules('price', 'Price', 'trim|required|numeric');
            $this->form_validation->set_rules('currency', 'Currency', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Transportationmodel->modify($eid,$id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Transportation already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Transportation modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_view_transportation').'/'.$eid);
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        if (!empty($id)) {
            $this->data['data_single'] = $this->Transportationmodel->load_single_data($id);
        }
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
                }
            }
        }
        else
        {
            $this->data['all_event'] = $this->Transportationmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
        }


        //$this->data['all_currency'] = $this->Transportationmodel->get_result_data('currency', ['is_active' => 'Y', 'delete_flag' => 'N']);
        $this->data['all_currency'] = $this->Transportationmodel->load_currency();

        $role_id = $this->session->userdata('admin_role_type');
        $permission = $this->Transportationmodel->load_permission($role_id);
        $this->data['per_add_currency'] = in_array('137', $permission) ? true : false; // add currency permission

        $this->middle = 'transportation/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for organizer selected or not
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function organizers_check($organizers) {
        $organizers = $this->input->post('organizers');
        if (!isset($organizers) || count($organizers) == 0) {
            $this->form_validation->set_message('organizers_check', '{field} Cannotbe blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $event_id = $this->input->post('event_id');
            $event_data = $this->Transportationmodel->get_row_data('transportation', ['id<>' => $event_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($event_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of transportation module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Transportationmodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Transportation status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Delete function of transportation module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Transportationmodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Transportation deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function listtransportation($eid) 
    {
        $all_data = $this->Transportationmodel->get_result_data('transportation',array("delete_flag"=>'N',"event_id"=>$eid));
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;
        $this->middle = 'transportation/admin/listtransportation';
        $this->admin_layout();
    }

}

/* End of file admin.php */
/* Location: ./application/modules/transportation/controllers/admin.php */
