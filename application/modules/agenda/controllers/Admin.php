<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for agenda [HMVC]. Handles all the datatypes and methodes required for agenda section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Agendamodel');
        $this->load->model('event/admin/Eventmodel');
        $this->load->model('accomodation/admin/Accomodationmodel');

        $this->load->helper("common_helper");
    }

    /**
     * Index Page for this agenda controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Agendamodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'agenda/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of agenda module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($eid,$id = NULL) {

        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Agenda Title', 'trim|required');
            $this->form_validation->set_rules('details', 'Agenda Overview', 'trim|required');
            $this->form_validation->set_rules('date', 'Agenda Date', 'trim|required');
            $this->form_validation->set_rules('start_time', 'Agenda Start Time', 'trim|required');
            $this->form_validation->set_rules('end_time', 'Agenda End Time', 'trim|required');
            $this->form_validation->set_rules('web_link', 'Web Link', 'trim|callback_valid_url');

            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Agendamodel->modify($eid,$id);
                if (!empty($flg)) {

                    $where = array();
                    $where['un.agenda_notify']='Y';
                    $users_list = $this->Eventmodel->get_user_details_result_data($where);
                   
                    $notification_text  = 'Agenda has been added';
                    $notification_title = 'Agenda Added';

                    foreach($users_list as $user)
                    {
                        notification($user->fcm_reg_token,$notification_text,$notification_title);
                    }


                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! agenda already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'agenda modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_view_agenda').'/'.$eid);
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        if (!empty($id)) {
            $this->data['data_single'] = $this->Agendamodel->load_single_data($id);
            
        }else{
            $this->data['data_single'] = array();
        }

       // echo $eid; die;
        //$this->data['all_room'] = $this->Agendamodel->get_result_data('agenda_location', ['event_id' => $eid]);
        $this->data['all_room'] = $this->Agendamodel->get_result_data('floor_map', ['event_id' => $eid,"delete_flag"=>'N',"is_active"=>'Y']);
        


          // echo $this->db->last_query(); die;

        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
                }
            }
        }
        else
        {
            $this->data['all_event'] = $this->Agendamodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
        }
        if($this->session->userdata('admin_role_type')==2)
        {
            $this->data['all_speaker'] = $this->Agendamodel->get_result_data('user', ['organizer_id' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N','type_id'=>3]);
        }
        else
        {
            $this->data['all_speaker'] = $this->Agendamodel->get_result_data('user', ['is_active' => 'Y', 'delete_flag' => 'N','type_id'=>3]);
        }

        //echo $this->uri->segment(2); die;
        $date = $this->Agendamodel->get_row_data('event', ['id' => $this->uri->segment(2)]);
        $this->data['event_start_date'] = $date->event_start_date;
        $this->data['event_end_date'] = $date->event_end_date;
       // echo "<pre>"; print_r($this->data['event_start_date']); die;
        $this->middle = 'agenda/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for organizer selected or not
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function organizers_check($organizers) {
        $organizers = $this->input->post('organizers');
        if (!isset($organizers) || count($organizers) == 0) {
            $this->form_validation->set_message('organizers_check', '{field} Cannotbe blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $event_id = $this->input->post('event_id');
            $event_data = $this->Agendamodel->get_row_data('agenda', ['id<>' => $event_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($event_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of agenda module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Agendamodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Agenda status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Delete function of agenda module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Agendamodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'agenda deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function room() 
    {
     //   $data = $this->Agendamodel->get_result_data("agenda_location",array("event_id"=>$this->input->post('eid')));
        $data = $this->Agendamodel->get_result_data("floor_map",array("event_id"=>$this->input->post('eid'),"delete_flag"=>'N',"is_active"=>'Y'));
        
        $option="";
        if (!empty($data)) 
        {
            foreach($data as $val)
            {
                //$option.='<option value="'.$val->id.'">'.$val->name.'</option>';
                $option.='<option value="'.$val->id.'">'.$val->floor_map_name.'</option>';
            }
        }
        echo $option;
    }

    public function listagenda($eid) 
    {
        $all_data = $this->Agendamodel->get_result_data('agenda',array("delete_flag"=>'N',"event_id"=>$eid,"agenda_owner_type"=>'admin'));
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;
        $this->middle = 'agenda/admin/listagenda';
        $this->admin_layout();
    }


    public function valid_url($str) {
           return (filter_var($str, FILTER_VALIDATE_URL) !== FALSE);
       }

}

/* End of file admin.php */
/* Location: ./application/modules/agenda/controllers/admin.php */
