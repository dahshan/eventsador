<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="<?php echo base_url('admin_agenda'); ?>" ><i class="fa fa-calendar-plus-o"></i>Event Agenda</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-calendar-plus-o"></i>Agenda List</a>
        </div>

        <h1>Agenda</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                         <?php if(in_array(78, $getRole)){ ?><button class="btn btn-success btn-cls" type="button" onclick="add();">Add Agenda</button> <?php } ?>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Title</th>
                                    <th>Overview</th>
                                    <th>Date</th>
                                    <th>Timings</th>
                                    <th>Location</th>
                                    <th>Speakers</th>
                                    <?php if(in_array(80, $getRole)){ ?><th>Status</th><?php } ?>
                                    <?php if(in_array(81, $getRole)){ ?><th>Action</th><?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <?php if(in_array(79, $getRole)){ ?><u><a href="<?php echo base_url('admin_update_agenda'.'/'.$this->uri->segment(2) . '/' . $data->id); ?>">
                                                <?php echo $data->title; ?>
                                                </a></u><?php }else{ echo $data->title; } ?>
                                            </td>
                                            <td class="custom-action-btn" width="15%">
                                                <p class="addReadMore showlesscontent"><?php echo $data->details; ?></p>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo date("M,d Y",strtotime($data->start_date_time)); ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo date("H:i:s",strtotime($data->start_date_time))." - ".date("H:i:s",strtotime($data->end_date_time)); ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->location; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php
                                                if (isset($data->speakers) && $data->speakers != '') {
                                                    $speakers = explode(',', $data->speakers);
                                                    foreach ($speakers AS $speakers_val) {
                                                        $speaker_data = $this->Agendamodel->get_row_data('user', ['id' => $speakers_val]);
                                                        if(!empty($speaker_data))
                                                        {
                                                            echo $speaker_data->name.'<br>';
                                                        }
                                                    }
                                                } else {
                                                    echo 'NA';
                                                }
                                                ?>
                                            </td>
                                            <?php if(in_array(80, $getRole)){ ?><td class="custom-action-btn">
                                                <?php
                                                if ($data->is_active == "Y") {
                                                    echo '<i title="De-activate" class="fa fa-close" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                } else {
                                                    echo '<i title="Activate" class="fa fa-check" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                }
                                                ?>
                                            </td><?php } ?>
                                            <?php if(in_array(81, $getRole)){ ?><td class="custom-action-btn">
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                            </td><?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<style>
    .addReadMore.showlesscontent .SecSec,
    .addReadMore.showlesscontent .readLess {
        display: none;
    }

    .addReadMore.showmorecontent .readMore {
        display: none;
    }

    .addReadMore .readMore,
    .addReadMore .readLess {
        font-weight: bold;
        margin-left: 2px;
        color: blue;
        cursor: pointer;
    }

    .addReadMoreWrapTxt.showmorecontent .SecSec,
    .addReadMoreWrapTxt.showmorecontent .readLess {
        display: block;
    }
</style>

<script type="text/javascript">
    /**
     * Add Event Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_agenda').'/'.$this->uri->segment(2); ?>";
    }

    /**
     * Status Change Function
     *
     * @param   rec_id
     * @return  redirects and gives control to status handler
     */
    function change_status(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_status_agenda'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to change status of this Agenda?");
        }).modal("show");
    }


    function AddReadMore() {
        //This limit you can set after how much characters you want to show Read More.
        var carLmt = 30;
        // Text to show when text is collapsed
        var readMoreTxt = " ... Read More";
        // Text to show when text is expanded
        var readLessTxt = " Read Less";


        //Traverse all selectors with this class and manupulate HTML part to show Read More
        $(".addReadMore").each(function() {
            if ($(this).find(".firstSec").length)
                return;

            var allstr = $(this).text();
            if (allstr.length > carLmt) {
                var firstSet = allstr.substring(0, carLmt);
                var secdHalf = allstr.substring(carLmt, allstr.length);
                var strtoadd = firstSet + "<span class='SecSec'>" + secdHalf + "</span><br><span class='readMore'  title='Click to Show More'>" + readMoreTxt + "</span><span class='readLess' title='Click to Show Less'>" + readLessTxt + "</span>";
                $(this).html(strtoadd);
            }

        });
        //Read More and Read Less Click Event binding
        $(document).on("click", ".readMore,.readLess", function() {
            $(this).closest(".addReadMore").toggleClass("showlesscontent showmorecontent");
        });
    }
    $(function() {
        //Calling function after Page Load
        AddReadMore();
    });

    /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_delete_agenda'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this Agenda?");
        }).modal("show");
    }

    
</script>
