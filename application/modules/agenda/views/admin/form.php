<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_agenda'); ?>" ><i class="fa fa-calendar-plus-o"></i>Event Agenda</a>
            <a href="<?php echo base_url('admin_view_agenda').'/'.$this->uri->segment(2); ?>" ><i class="fa fa-calendar-plus-o"></i>Agenda List</a>
            <a href="javascript:void(0);" class="current">Agenda Management</a>
        </div>

        <h1>Agenda</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Agenda</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_agenda').'/'.$this->uri->segment(2) : base_url('admin_update_agenda').'/'.$this->uri->segment(2) . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <!-- <div class="control-group">
                                <label class="control-label">Event</label>
                                <div class="controls">
                                    <select name="event_id" id="event_id">
                                        <?php
                                        if (count($all_event) > 0) {
                                            foreach ($all_event as $all_event_val) {
                                                ?>
                                                <option value="<?php echo $all_event_val->id; ?>"
                                                        <?php if(!empty($data_single)){if($all_event_val->id==$data_single->event_id)echo 'selected'; }?>>
                                                            <?php echo (isset($all_event_val->event_name) && $all_event_val->event_name != '') ? $all_event_val->event_name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('event_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div> -->
                            <div class="control-group">
                                <label class="control-label">Title</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Agenda Title" type="text" id="title" name="title" value="<?php echo (!empty(set_value('title'))) ? set_value('title') : ((!empty($data_single->title)) ? $data_single->title : ''); ?>" >
                                    <?php echo form_error('title', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Overview</label>
                                <div class="controls">
                                    <textarea class="span11" id="details" maxlength="500" name="details"><?php echo (!empty(set_value('details'))) ? set_value('details') : ((!empty($data_single->details)) ? $data_single->details : ''); ?></textarea>
                                    <p id="details_count"></p>
                                    <?php echo form_error('details', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Date</label>
                                <div class="controls">
                                    <input onkeypress="return false" class="span11 datepicker" placeholder="Select Date" type="text" id="date" name="date" value="<?php echo (!empty(set_value('date'))) ? set_value(date('m/d/Y',strtotime($data_single->start_date_time))) : ((!empty($data_single->start_date_time)) ? date('m/d/Y',strtotime($data_single->start_date_time)) : ''); ?>" >
                                    <?php echo form_error('date', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="span5">
                                    <label class="control-label">Start Time</label>
                                    <div class="controls">
                                        <input onkeypress="return false" class="span11 timepicker1" placeholder="Start Time" type="text" id="start_time" name="start_time" value="<?php echo (!empty(set_value('start_time'))) ? set_value('start_time') : ((!empty($data_single->start_date_time)) ? date('H:i a',strtotime($data_single->start_date_time)) : ''); ?>" >
                                        <?php echo form_error('start_time', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">End Time</label>
                                    <div class="controls">
                                        <input onkeypress="return false" class="span11 timepicker2" placeholder="End Time" type="text" id="end_time" name="end_time" value="<?php echo (!empty(set_value('end_time'))) ? set_value('end_time') : ((!empty($data_single->end_date_time)) ? date('H:i a',strtotime($data_single->end_date_time)) : ''); ?>" >
                                        <?php echo form_error('end_time', '<div style="color:red;">', '</div>'); ?>
                                        <span class="tim2_err" style="color:red;"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Speaker</label>
                                <div class="controls">
                                    <select name="speakers[]" multiple >
                                        <?php
                                        if (count($all_speaker) > 0) {
                                            if (isset($data_single->speakers) && $data_single->speakers != '') {
                                                $speakers_selected = explode(',',$data_single->speakers);
                                            } else {
                                                $speakers_selected = [];
                                            }
                                            foreach ($all_speaker AS $all_speaker_val) {
                                                ?>
                                                <option value="<?php echo (isset($all_speaker_val->id) && $all_speaker_val->id != '') ? $all_speaker_val->id : ''; ?>"
                                                        <?php echo in_array($all_speaker_val->id, $speakers_selected)?'selected':''; ?>>
                                                            <?php echo (isset($all_speaker_val->name) && $all_speaker_val->name != '') ? $all_speaker_val->name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('speaker_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Location</label>
                                <div class="controls">
                                    <select name="room_id" id="room_id">
                                        <?php if(!empty($all_room)){
                                            foreach($all_room as $val)
                                            {
                                        ?>
                                            <!-- <option value="<?php echo $val->id;?>" <?php if(isset($data_single->room_id)){ if($val->id==$data_single->room_id) echo 'selected'; } ?>><?php echo $val->name;?></option> -->
                                            <option value="<?php echo $val->id;?>" <?php if(isset($data_single->room_id)){ if($val->id==$data_single->room_id) echo 'selected'; } ?>><?php echo $val->floor_map_name;?></option>
                                        <?php
                                            }
                                        }
                                        ?>  
                                    </select>
                                    <?php echo form_error('room_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Live</label>
                                <div class="controls">
                                    <input type="checkbox" <?php if(!empty($data_single->is_live) && $data_single->is_live == 'Y'){ echo "checked"; }?> value="1" name="is_live" id="is_live">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Recorded</label>
                                <div class="controls">
                                    <input type="checkbox" <?php if(!empty($data_single->is_recorded) && $data_single->is_recorded == 'Y'){ echo "checked"; }?> value="1" name="is_recorded" id="is_recorded">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Web Link</label>
                                <div class="controls">
                                    <input class="span11" placeholder="https://" type="text" id="web_link" name="web_link" value="<?php echo (!empty(set_value('web_link'))) ? set_value('web_link') : ((!empty($data_single->web_link)) ? $data_single->web_link : ''); ?>" >
                                    <?php echo form_error('web_link', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>


                            <input type="hidden" name="eventid" id="eventid" value="<?php if(!empty($data_single->id)) echo $data_single->id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
           // startDate: "today",
            startDate: new Date ("<?php echo @$event_start_date; ?>"),
            endDate: new Date ("<?php echo @$event_end_date; ?>"),
            todayHighlight: true,
            autoclose: true,
            changeDate: function(dateText) {
                // $('.datepicker2').datepicker({
                //     startDate: this.value
                // });
                // alert("ok");
            }
            // "setDate": new Date(),
            // "autoclose": true
            // startDate: '-3d'
        });


      //$('.timepicker').timepicker({defaultTime: false});



         $('.timepicker1').timepicker({
        }).on('changeTime.timepicker', function (e) {
            //var newHour =(e.time.hours)+1;
            //$('.timepicker2').val(newHour+":"+e.time.minutes+" "+e.time.meridian);
            $('.timepicker2').timepicker({
            }).on('changeTime.timepicker', function (f) {
                // console.log(e.time);
                // console.log(f.time);
                var hours = 0;
                var fhours = 0;

                if(e.time.meridian == "PM" && e.time.hours<12){
                    hours = e.time.hours+12;
                }else if(e.time.meridian == "AM" && e.time.hours==12){
                    hours = e.time.hours-12;
                }else{
                    hours = e.time.hours;
                } 

                if(f.time.meridian == "PM" && f.time.hours<12){
                    fhours = f.time.hours+12;
                }else if(f.time.meridian == "AM" && f.time.hours==12){
                    fhours = f.time.hours-12;
                }else{
                    fhours = f.time.hours;
                } 
                // console.log(hours);
                // console.log(fhours);
                if((hours >= fhours) && (e.time.minutes >= f.time.minutes))
                { 
                    $('.tim2_err').html("End time must be greater than start time.");
                    //$('#end_time').val('');
                }else{
                    $('.tim2_err').html("");
                }


            })
        });


        $(document).on("change","#event_id",function(){
            $("#s2id_room_id a").text("");
            var saveData = $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url('agenda/admin/room'); ?>",
                    data: {eid:$(this).val()},
                    dataType: "text",
                    success: function(resultData) { 
                        $("#room_id").html(resultData);
                    }
            });
        });

        $("#details_count").text("Characters left: " + (500 - $("#details").val().length));
        $("#details").keyup(function(){
          $("#details_count").text("Characters left: " + (500 - $(this).val().length));
        });

    })
</script>
