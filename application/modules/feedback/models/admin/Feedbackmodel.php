<?php

/**
 * feedback Model Class. Handles all the datatypes and methodes required for handling feedback
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Feedbackmodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of feedback for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the feedback that has been added by current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('feedback.*,user.name');
        $this->db->from(tablename('feedback'));
        $this->db->where('feedback.delete_flag', 'N');
        // if($this->session->userdata('admin_role_type')==2)
        // {
        //     $this->db->where('organizer_id', $this->session->userdata('admin_uid'));
        // }
        $this->db->join("user","user_id=user.id");
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single feedback by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single feedback by id that has been added by current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_feedback_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('feedback'));
        $this->db->join(tablename('user'), 'feedback.user_id = user.id');
        $this->db->where('feedback.id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1") {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->result();
    }


    public function send_reply_email($feedback) {
        $subject = "Eventsador - Feedback Reply";
        $message = $_POST['reply'];

        $mail_data = [
            'name' => $feedback->name,
            'body' => $message,
        ];

        $this->load->helper('email');
        if (send_email($feedback->emailid, $subject, $mail_data)) {
            return true;
        }else{
            return false;
        }

    }

    public function get_result_data1($table,$whr,$table1=NULL,$condition=NULL,$field=NULL,$tag=NULL) 
    {
        if(!empty($table1))
        {
            // $this->db->select('exhibitor.*,exhibitor_type.name as type_name');
            // $this->db->join($table1,$condition);
        }
        $query =$this->db->where_in($field,$whr)->get(tablename($table));
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single feedback for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single feedback w.r.t. current admin [Table:  ets_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        if($this->session->userdata('admin_role_type')==2)
        {
            $organizer_id=$this->session->userdata('admin_uid');
        }
        else
        {
            $organizer_id=0;
        }
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $company = $this->input->post('company');
        $position_title = $this->input->post('position_title');
        $location = $this->input->post('location');
        $details = $this->input->post('details');
        $image = "";
        if (!empty($_FILES['image']['name'])) 
        {
            $this->load->library('upload');
            $config['upload_path'] = './assets/upload/feedback/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['file_name'] = time();

            $this->upload->initialize($config);
            if($_FILES['image']['error']==0)
            {
                $profilepic=$_FILES['image']['name'];
                $profilepic=explode('.',$profilepic);
                $ext=end($profilepic);
                if($ext=="jpg" or $ext=="jpeg" or $ext=="png" or $ext=="bmp")
                {
                    $profilepic=time().rand().".".$ext;
                    $destination="./assets/upload/feedback/".$profilepic;
                    if (move_uploaded_file($_FILES['image']['tmp_name'] ,$destination))
                    {
                        $image=$profilepic;
                        $olddestination="./assets/upload/feedback/".$oldfile;
                        @unlink($olddestination);
                    }
                }
                else
                {
                    $this->session->set_flashdata('errormessage', 'Only .jpg,.jpeg,.bmp and .png image extensions are supported');
                    $redirect = site_url('admin/admin_sponsor');
                    redirect($redirect);
                }
            }
        }
        // $date = date("Y-m-d H:i:s");
        if (!empty($id)) {
            if(!empty($image))
            {
                $data = array(
                    'organizer_id' => $organizer_id,
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'company' => $company,
                    'position_title' => $position_title,
                    'location' => $location,
                    'details' => $details,
                    'image' => $image
                    // 'modified_date' => $date,
                );
            }
            else
            {
                $data = array(
                    'organizer_id' => $organizer_id,
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'company' => $company,
                    'position_title' => $position_title,
                    'location' => $location,
                    'details' => $details
                    // 'modified_date' => $date,
                );
            }

            $this->db->where('id', $id)->update(tablename('feedback'), $data);
            return $id;
        } else {
            // Check for Duplicate
            $data = array(
                'organizer_id' => $organizer_id,
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'company' => $company,
                'position_title' => $position_title,
                'location' => $location,
                'details' => $details,
                'image' => $image
                // 'entry_date' => $date
            );

            $this->db->insert(tablename('feedback'), $data);
            $last_id = $this->db->insert_id();

            if (!empty($last_id)) {
                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of feedback for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current feedback status
     * and change it the the opposite [Table: pb_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('feedback'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('feedback'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of feedback for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_sponsor]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('feedback'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

}

/* End of file Sponsormodel.php */
/* Location: ./application/modules/feedback/models/admin/Sponsormodel.php */
