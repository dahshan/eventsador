<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-street-view"></i>Feedback Reply</a>
        </div>

        <h1>Feedback Reply</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Feedback Reply</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">

                            <div class="control-group">
                                <label class="control-label">Reply</label>
                                <div class="controls">
                                    <textarea class="span11" id="reply" rows="10" name="reply">
Hi <?php echo $name ?>, ,
Reply on your feedback "<?php echo $feedback ?>"



Best Regards, 
Eventsador Team
                                    </textarea>
                                    <?php echo form_error('reply', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>






                            <input type="hidden" name="feedback_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
