<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for feedback [HMVC]. Handles all the datatypes and methodes required for feedback section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Feedbackmodel');
    }

    /**
     * Index Page for this feedback controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Feedbackmodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'feedback/admin/list';
        $this->admin_layout();
    }

    public function reply($id) {
        if (!empty($id)) {
            $feedback = $this->Feedbackmodel->load_feedback_data($id);
            // echo "<pre>";
            // print_r($feedback);
            // exit;

            if (!empty($_POST['reply'])) {
                admin_authenticate();
                if ($this->Feedbackmodel->send_reply_email($feedback)) {
                    $this->session->set_flashdata('successmessage', 'Reply Sent successfully');
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_feedback'));                
            }
            $uri = $this->uri->segment(1);
            $this->data = array();
            $this->data['uri'] = $uri;
            $this->data['feedback'] = $feedback->comment;
            $this->data['name'] = $feedback->name;
            $this->data['id'] = $id;
            $this->middle = 'feedback/admin/form';
            $this->admin_layout();
        }
    }

    public function delete($id) {
        // $feedback = $this->Feedbackmodel->load_single_data($id);

        $flg = $this->Feedbackmodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Feedback deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_feedback'));
    }


}

