<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for local_attraction [HMVC]. Handles all the datatypes and methodes required for local_attraction section of Optus
 *
 * @author  
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Localattractionmodel');
        $this->load->model('accomodation/admin/Accomodationmodel');
    }

    /**
     * Index Page for this local_attraction controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Localattractionmodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'local_attraction/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of local_attraction module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($eid,$id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('place_name', 'Place Name', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
            $this->form_validation->set_rules('link', 'Link', 'trim|required');
            $this->form_validation->set_rules('latitude', 'Latitude', 'trim|required');
            $this->form_validation->set_rules('longitude', 'Longitude', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Localattractionmodel->modify($eid,$id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Local Attraction already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Local Attraction modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_view_local_attraction').'/'.$eid);
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        if (!empty($id)) {
            $this->data['data_single'] = $this->Localattractionmodel->load_single_data($id);
            $this->data['all_images'] = $this->Localattractionmodel->get_result_data('local_attraction_pictures', ['local_attraction_id' => $id]);
        }
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
                }
            }
        }
        else
        {
            $this->data['all_event'] = $this->Localattractionmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
        }
        $this->middle = 'local_attraction/admin/form';
        $this->admin_layout();
    }

    public function delete_file($id,$event,$pid) {
        $file = $this->Localattractionmodel->get_row_data('local_attraction_pictures', ['id' => $id]);
        $flg = $this->Localattractionmodel->delete_data('local_attraction_pictures',array("id"=>$id));
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Picture deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        //redirect(base_url('admin_update_local_attraction/').$file->local_attraction_id);
        redirect(base_url('admin_update_local_attraction/').$event.'/'.$pid);
    }

    /**
     * Used for organizer selected or not
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function organizers_check($organizers) {
        $organizers = $this->input->post('organizers');
        if (!isset($organizers) || count($organizers) == 0) {
            $this->form_validation->set_message('organizers_check', '{field} Cannotbe blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $event_id = $this->input->post('event_id');
            $event_data = $this->Localattractionmodel->get_row_data('local_attraction', ['id<>' => $event_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($event_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of local_attraction module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Localattractionmodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Local Attraction status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Delete function of local_attraction module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Localattractionmodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Local Attraction deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function listlocal_attraction($eid) 
    {
        $all_data = $this->Localattractionmodel->get_result_data('local_attraction',array("delete_flag"=>'N',"event_id"=>$eid));
        if (count($all_data) > 0){
            foreach ($all_data as $one){
                $image = $this->Localattractionmodel->get_row_data('local_attraction_pictures', ['local_attraction_id'=> $one->id]);
                if ($image && !empty($image->image)){
                    $one->image = $image->image; //var_dump($one->image); 
                }else{
                    $one->image = "";
                }
            }
        }
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;
        $this->middle = 'local_attraction/admin/listlocalattraction';
        $this->admin_layout();
    }

}

/* End of file admin.php */
/* Location: ./application/modules/local_attraction/controllers/admin.php */
