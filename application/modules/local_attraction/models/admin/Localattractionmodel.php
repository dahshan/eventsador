<?php

/**
 * local_attraction Model Class. Handles all the datatypes and methodes required for handling local_attraction
 *
 * @author  
 * @access      public
 * @since Version 0.0.1
 */
class Localattractionmodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of local_attraction for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the local_attraction that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('id,event_name');
        $this->db->from(tablename('event'));
        $this->db->where('event.delete_flag', 'N');
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(".$this->session->userdata('admin_uid').", users) !=", 0);
            }
            
            
        }
        $this->db->order_by("event.id", "asc");

        $query = $this->db->get();
        $result = $query->result();
        if(!empty($result))
        {
            $fina_result=array();
            foreach($result as $val)
            {
                $val->local_attraction_count=count($this->get_result_data("local_attraction", array("delete_flag"=>'N',"event_id"=>$val->id)));
                $fina_result[]=$val;
            }
            $result=$fina_result;
        }
         if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single local_attraction by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single local_attraction by id that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('local_attraction'));
        if(!empty($id))
        {
            $this->db->where('id', $id);
        }

        $query = $this->db->get();
        $result = $query->row();
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1",$optional_where=NULL) {
        if(empty($optional_where))
        {
            $query = $this->db->get_where(tablename($table), $where);
        }
        else
        {
            $this->db->where_in('id', $optional_where);
            $query = $this->db->get(tablename($table), $where);
        }

        //echo $this->db->last_query(); exit;
        return $query->result();
    }


    public function get_my_events($table, $where =NULL) {
        
        $query = $this->db->get_where(tablename($table), $where);
        
        //echo $this->db->last_query(); exit;
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single local_attraction for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single local_attraction w.r.t. current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($eid,$id = '') {
        $data['event_id'] = $eid;
        $data['place_name'] = $this->input->post('place_name');
        $data['description'] = $this->input->post('description');
        $data['link'] = $this->input->post('link');
        $data['latitude'] = $this->input->post('latitude');
        $data['longitude'] = $this->input->post('longitude');
        if (!empty($_FILES['map']['name'])) 
        {
            $this->load->library('upload');
            $config['upload_path'] = './assets/upload/local_attraction/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['file_name'] = time();

            $this->upload->initialize($config);
            if($_FILES['map']['error']==0)
            {
                $profilepic=$_FILES['map']['name'];
                $profilepic=explode('.',$profilepic);
                $ext=end($profilepic);
                if($ext=="jpg" or $ext=="jpeg" or $ext=="png" or $ext=="bmp")
                {
                    $profilepic=time().rand().".".$ext;
                    $destination="./assets/upload/local_attraction/".$profilepic;
                    if (move_uploaded_file($_FILES['map']['tmp_name'] ,$destination))
                    {
                        $data['map']=$profilepic;
                        //$olddestination="./assets/upload/local_attraction/".$oldfile;
                        //@unlink($olddestination);
                    }
                }
                else
                {
                    $this->session->set_flashdata('errormessage', 'Only .jpg,.jpeg,.bmp and .png image extensions are supported');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }
        if (!empty($id)) 
        {
            $this->db->where('id', $id)->update(tablename('local_attraction'), $data);
            $file = $_FILES['img_file'];
            if (isset($file['name']) && $file['name'] != '' && !empty($file['name'])) {
                // foreach ($file['name'] AS $file_name_key => $file_name) {
                //     if (!empty($file['name'][$file_name_key])) {
                //         $info = pathinfo($file['name'][$file_name_key]);
                //         $ext = $info['extension']; // get the extension of the file
                //         $newname = "media_" . time() . $file_name_key . "." . $ext;
                //         $target = __DIR__ . '/../../../../../assets/upload/local_attraction/' . $newname;
                //         move_uploaded_file($_FILES['img_file']['tmp_name'][$file_name_key], $target);
                //         $data = array(
                //             'local_attraction_id' => $id,
                //             'image' => $newname,
                //             'created_date' => date('Y-m-d H:i:s')
                //         );
                //         $this->db->insert(tablename('local_attraction_pictures'), $data);
                //     }
                // }


                if (!empty($file['name'])) {
                        $info = pathinfo($file['name']);
                        $ext = $info['extension']; // get the extension of the file
                        $newname = "media_" . time() . "." . $ext;
                        $target = __DIR__ . '/../../../../../assets/upload/local_attraction/' . $newname;
                        if (move_uploaded_file($_FILES['img_file']['tmp_name'] ,$target))
                        {
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = $target; 
                            //echo $origin.file_exists($origin).'<br>';
                            $config['new_image'] = __DIR__ . '/../../../../../assets/upload/local_attraction/200_' . $newname;
                            $config['maintain_ratio'] = TRUE;
                            $config['width'] = 200;
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();

                            if (!empty($id)) {
                                $old_info = $this->db->get_where(tablename('local_attraction_pictures'),['local_attraction_id'=>$id])->first_row();
                                $olddestination= __DIR__ . '/../../../../../assets/upload/local_attraction/' . $old_info->image;
                                $olddestinationthumb=__DIR__ . '/../../../../../assets/upload/local_attraction/200_' . $old_info->image;
                                if (file_exists($olddestination)) {
                                    @unlink($olddestination);
                                }
                                if (file_exists($olddestinationthumb)) {
                                    @unlink($olddestinationthumb);
                                }

                            }
                        }

                        $data = array(
                            'local_attraction_id' => $id,
                            'image' => $newname,
                            'created_date' => date('Y-m-d H:i:s')
                        );
                        $this->db->where('local_attraction_id', $id)->delete('local_attraction_pictures');
                        $this->db->insert(tablename('local_attraction_pictures'), $data);
                    }
            }
            return $id;
        } 
        else 
        {
            $this->db->insert(tablename('local_attraction'), $data);
            $last_id = $this->db->insert_id();
            if (!empty($last_id)) {
                $file = $_FILES['img_file'];
                if (isset($file['name']) && $file['name'] != '' && !empty($file['name'])) {
                    //foreach ($file['name'] AS $file_name_key => $file_name) {
                        // if (!empty($file['name'][$file_name_key])) {
                        //     $info = pathinfo($file['name'][$file_name_key]);
                        //     $ext = $info['extension']; // get the extension of the file
                        //     $newname = "media_" . time() . $file_name_key . "." . $ext;
                        //     $target = __DIR__ . '/../../../../../assets/upload/local_attraction/' . $newname;
                        //     move_uploaded_file($_FILES['img_file']['tmp_name'][$file_name_key], $target);
                        //     $data = array(
                        //         'local_attraction_id' => $last_id,
                        //         'image' => $newname,
                        //         'created_date' => date('Y-m-d H:i:s')
                        //     );
                        //     $this->db->insert(tablename('local_attraction_pictures'), $data);
                        // }
                   // }


                    if (!empty($file['name'])) {
                            $info = pathinfo($file['name']);
                            $ext = $info['extension']; // get the extension of the file
                            $newname = "media_" . time() . "." . $ext;
                            $target = __DIR__ . '/../../../../../assets/upload/local_attraction/' . $newname;
                            if (move_uploaded_file($_FILES['img_file']['tmp_name'] ,$target))
                            {
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $target; 
                                //echo $origin.file_exists($origin).'<br>';
                                $config['new_image'] = __DIR__ . '/../../../../../assets/upload/local_attraction/200_' . $newname;
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = 200;
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();

                                if (!empty($id)) {
                                    $old_info = $this->db->get_where(tablename('local_attraction_pictures'),['local_attraction_id'=>$id])->first_row();
                                    $olddestination= __DIR__ . '/../../../../../assets/upload/local_attraction/' . $old_info->image;
                                    $olddestinationthumb=__DIR__ . '/../../../../../assets/upload/local_attraction/200_' . $old_info->image;
                                    if (file_exists($olddestination)) {
                                        @unlink($olddestination);
                                    }
                                    if (file_exists($olddestinationthumb)) {
                                        @unlink($olddestinationthumb);
                                    }

                                }
                            }
                            $data = array(
                                'local_attraction_id' => $last_id,
                                'image' => $newname,
                                'created_date' => date('Y-m-d H:i:s')
                            );
                            $this->db->where('local_attraction_id', $last_id)->delete('local_attraction_pictures');
                            $this->db->insert(tablename('local_attraction_pictures'), $data);
                        }
                }
                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of local_attraction for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current local_attraction status
     * and change it the the opposite [Table: pb_event]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('local_attraction'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('local_attraction'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of local_attraction for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_event]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('local_attraction'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

    public function insert_data($table,$data1)
    {
        $this->db->insert(tablename($table),$data1);
        return $this->db->insert_id();
    }

    public function delete_data($table,$where)
    {
        $this->db->delete(tablename($table),$where);
        return $this->db->affected_rows();
    }

    public function update_data($table,$where,$data)
    {
        $this->db->where($where);
        $this->db->update($table, $data); 
        return $this->db->affected_rows();
    } 

    public function search_event($tag) 
    {
        $this->db->select('*');
        $this->db->from(tablename('local_attraction'));
        $this->db->or_like('event_name', $tag);
        $this->db->or_like('event_venue', $tag);
        $this->db->or_like('event_description', $tag);
        $this->db->order_by("entry_date", "desc");
        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) 
        {
            // echo $this->db->last_query();die;
            return $result;
        } 
        else 
        {
            return "";
        }
    }

    public function get_detailed_result_data($table,$table1,$join_condition,$where = "1=1")
    {
        $this->db->join($table1,$join_condition);
        $query=$this->db->get_where(tablename($table), $where);
        return $query->result();
    }


}

/* End of file Eventmodel.php */
/* Location: ./application/modules/local_attraction/models/admin/Eventmodel.php */
