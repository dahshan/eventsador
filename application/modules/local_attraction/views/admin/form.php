<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_local_attraction'); ?>" ><i class="fa fa-map-signs"></i>Local Attraction</a>
            <a href="<?php echo base_url('admin_view_local_attraction').'/'.$this->uri->segment(2); ?>" ><i class="fa fa-map-marker"></i>Local Attraction List</a>
            <a href="javascript:void(0);" class="current">Local Attraction</a>
        </div>

        <h1>Local Attraction</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Local Attraction</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_local_attraction').'/'.$this->uri->segment(2) : base_url('admin_update_local_attraction').'/'.$this->uri->segment(2) . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Place Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Place Name" type="text" id="place_name" name="place_name" value="<?php echo (!empty(set_value('place_name'))) ? set_value('title') : ((!empty($data_single->place_name)) ? $data_single->place_name : ''); ?>" >
                                    <?php echo form_error('place_name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Description</label>
                                <div class="controls">
                                    <textarea class="span11" id="description" name="description"><?php echo (!empty(set_value('description'))) ? set_value('description') : ((!empty($data_single->description)) ? $data_single->description : ''); ?></textarea>
                                    <?php echo form_error('description', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Link</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Link" type="text" id="link" name="link" value="<?php echo (!empty(set_value('link'))) ? set_value('title') : ((!empty($data_single->link)) ? $data_single->link : ''); ?>" >
                                    <?php echo form_error('link', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="span5">
                                    <label class="control-label">Latitude</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Enter Latitude" type="text" id="latitude" name="latitude" value="<?php echo (!empty(set_value('latitude'))) ? set_value('title') : ((!empty($data_single->latitude)) ? $data_single->latitude : ''); ?>" >
                                        <?php echo form_error('latitude', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">Longitude</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Enter Longitude" type="text" id="longitude" name="longitude" value="<?php echo (!empty(set_value('longitude'))) ? set_value('title') : ((!empty($data_single->longitude)) ? $data_single->longitude : ''); ?>" >
                                        <?php echo form_error('longitude', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span2">
                                    <button class="btn btn-success btn-cls" type="button" onclick="initMap();">Map</button>
                                </div>
                            </div>
                            <div class="control-group" id="div_map" style="display: none;">
                                <div id="map_canvas" style="height:330px;max-width:950px !important;margin-top: 12px;margin-left: 20px;margin-bottom: 12px;"></div>
                            </div>
                            <!--<div class="control-group">-->
                            <!--    <label class="control-label">Map</label>-->
                            <!--    <div class="controls">-->
                            <!--      <div class="fileinput fileinput-new" data-provides="fileinput">-->
                            <!--        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">-->
                            <!--          <img  src="<?php if(!empty($data_single->map)){ echo base_url('assets/upload/local_attraction')."/".$data_single->map; } else {echo base_url('assets/upload/placeholder.png');}?>" data-src="<?php if(!empty($data_single->map)){ echo base_url('assets/upload/local_attraction')."/".$data_single->map; } else {echo base_url('assets/upload/placeholder.png');}?>" alt="...">-->
                            <!--        </div>-->
                            <!--        <div>-->
                            <!--          <div class="controls custon_textarea" style="margin-left: 0;">-->
                            <!--            <input type="file" name="map" style="line-height:1px;">-->
                            <!--          </div>-->
                            <!--        </div>-->
                            <!--      </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <div class="control-group">
                                <label class="control-label">Choose Pictures</label>
                                <div class="controls">
                                   <!--  <input type="file" name="img_file[]" id="img_file" accept="image/*" multiple> -->
                                    <input type="file" name="img_file" id="img_file" >
                                    <?php echo form_error('img_file', '<div style="color:red;">', '</div>'); ?>
                                </div>
                                <div class="controls">
                                    <?php
                                    if (!empty($data_single->id)) {
                                        $all_images = $this->Localattractionmodel->get_result_data('local_attraction_pictures', ['local_attraction_id' => $data_single->id]);
                                        if (count($all_images) > 0) {
                                            foreach ($all_images AS $all_images_val) {
                                                ?>
                                                <div style="position: relative; display: inline-block;">
                                                    <span style="position: absolute; z-index: 9; top: 5px; right: 10px;">
                                                        <a href="javascript:void(0);" onclick="delete_file('<?php echo $all_images_val->id; ?>')"><i class="fa fa-times"></i></a>
                                                    </span>
                                                    <img src="<?php echo base_url(); ?>assets/upload/local_attraction/<?php echo $all_images_val->image; ?>" height="100" width="150">
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <!-- <input type="hidden" name="eventid" id="eventid" value="<?php if(!empty($data_single->id)) echo $data_single->id; ?>"> -->
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<script type="text/javascript">
    function delete_file(id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_delete_attraction_image'); ?>/" + id + "/<?php echo $this->uri->segment(2); ?>/<?php echo $this->uri->segment(3); ?>";
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this picture?");
        }).modal("show");
    }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrhTMHvt2CLbSI_GnMoEpNz7Z2s6SOxdE"></script>
<script type="text/javascript">
    var marker;
    function initMap() {
        var lat=Number($("#latitude").val());
        var lng=Number($("#longitude").val());
        var myLatLng = {lat: lat, lng: lng};
        // if(lat && lng)
        // {
        //     var myLatLng = {lat: lat, lng: lng};
        // }
        // else
        // {
        //     var myLatLng = {lat: lat, lng: lng};
        // }
        $("#div_map").removeAttr("style");
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: 15,
          center: myLatLng
        });

        marker = new google.maps.Marker({
          position: myLatLng,
          map: map
          // title: 'Hello World!'
        });
        
        google.maps.event.addListener(map, 'click', function(event) {
            addMarker(event.latLng);
        });
    }
    $(document).on("change","#is_deal",function(){
        if (this.checked) {
            $("#deal_text").val("");
            $("#deal_tab").removeAttr("style");
        } else {
            $('#deal_tab').css('display','none');
        }
    });    
    function addMarker(position) {
        marker.setPosition(position);
        $("#latitude").val(position.lat());
        $('#longitude').val(position.lng());
    }
</script>
