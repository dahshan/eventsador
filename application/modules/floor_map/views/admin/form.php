<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_floor_map'); ?>" ><i class="fa fa-map"></i>Floor Map</a>
            <a href="<?php echo base_url('admin_view_floor_map').'/'.$this->uri->segment(2); ?>" ><i class="fa fa-map-pin"></i>Floor Map List</a>
            <a href="javascript:void(0);" class="current">Floor Map Management</a>
        </div>

        <h1>Floor Map</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Floor Map</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_floor_map').'/'.$this->uri->segment(2) : base_url('admin_update_floor_map').'/'.$this->uri->segment(2) . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <!-- <div class="control-group">
                                <label class="control-label">Event</label>
                                <div class="controls">
                                    <select name="event_id" id="event_id">
                                        <?php
                                        if (count($all_event) > 0) {
                                            foreach ($all_event as $all_event_val) {
                                                ?>
                                                <option value="<?php echo $all_event_val->id; ?>"
                                                        <?php if(!empty($data_single)){if($all_event_val->id==$data_single->event_id)echo 'selected'; }?>>
                                                            <?php echo (isset($all_event_val->event_name) && $all_event_val->event_name != '') ? $all_event_val->event_name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('event_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div> -->
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Floor Map Name" type="text" id="floor_map_name" name="floor_map_name" value="<?php echo (!empty(set_value('floor_map_name'))) ? set_value('title') : ((!empty($data_single->floor_map_name)) ? $data_single->floor_map_name : ''); ?>" >
                                    <?php echo form_error('floor_map_name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Description</label>
                                <div class="controls">
                                    <textarea class="span11" id="floor_map_description" name="floor_map_description"><?php echo (!empty(set_value('floor_map_description'))) ? set_value('description') : ((!empty($data_single->floor_map_description)) ? $data_single->floor_map_description : ''); ?></textarea>
                                    <?php echo form_error('floor_map_description', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Map</label>
                                <div class="controls">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                      <img  src="<?php if(!empty($data_single->floor_map)){ echo base_url('assets/upload/floor_map')."/".$data_single->floor_map; } else {echo base_url('assets/upload/placeholder.png');}?>" data-src="<?php if(!empty($data_single->floor_map)){ echo base_url('assets/upload/floor_map')."/".$data_single->floor_map; } else {echo base_url('assets/upload/placeholder.png');}?>" alt="...">
                                    </div>
                                    <div>
                                      <div class="controls custon_textarea" style="margin-left: 0;">
                                        <input type="file" name="floor_map" style="line-height:1px;">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <!-- <input type="hidden" name="eventid" id="eventid" value="<?php if(!empty($data_single->id)) echo $data_single->id; ?>"> -->
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

