<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for Organizer [HMVC]. Handles all the datatypes and methodes required for Organizer section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Organizermodel');
    }

    /**
     * Index Page for this Organizer controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Organizermodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'organizer/admin/list';
        $this->admin_layout();
    }

    /**
     * Index Page for this Organizer controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function organizers_by_event() {
        if (is_numeric($this->uri->segment(2))) {
            $event = $this->Organizermodel->load_event_data($this->uri->segment(2));
            $all_data = $this->Organizermodel->load_all_data_in($event->organizers);
            $uri = $this->uri->segment(1);
            $this->data = array();
            $this->data['uri'] = $uri;
            $this->data['all_data'] = $all_data;
            $this->middle = 'organizer/admin/list';
            $this->admin_layout();
        }
    }

    /**
     * Add/Edit function of Organizer module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('company', 'Company', 'trim|required');
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            if ($param == 'A') {
                $this->form_validation->set_rules('emailid', 'Email', 'trim|required|valid_email|callback_new_email_check');
            } else {
                // $this->form_validation->set_rules('emailid', 'Email', 'trim|required|callback_old_email_check');
            }
            $this->form_validation->set_rules('phoneno', 'Phone no', 'trim|required|numeric');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Organizermodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Organizer already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Organizer modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_organizer'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Organizermodel->load_single_data($id);
        $this->data['all_module'] = $this->Organizermodel->get_result_data('modules', ['is_active' => 'Y', 'delete_flag' => 'N']);
        $this->data['all_company'] = $this->Organizermodel->get_result_data('company_admin', ['is_active' => 'Y', 'delete_flag' => 'N']);
        $this->middle = 'organizer/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function new_email_check($email) {
        if (isset($email) && $email != '') {
            $organizer_data = $this->Organizermodel->get_row_data('user', ['emailid' => $email, 'delete_flag' => 'N']);
            if (count((array) $organizer_data) > 0) {
                $this->form_validation->set_message('new_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $organizer_id = $this->input->post('organizer_id');
            $organizer_data = $this->Organizermodel->get_row_data('user', ['id<>' => $organizer_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count((array) $organizer_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of Organizer module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Organizermodel->status($id);
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Organizer status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_organizer'));
    }

    /**
     * Status Change function of User module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function resend_email($id) {
        $flg = $this->Organizermodel->reset_pwd_and_resend_email($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'credentials sent successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_organizer'));
    }

    /**
     * Delete function of Organizer module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $user = $this->Organizermodel->load_single_data($id);

        $flg = $this->Organizermodel->delete($user);
        
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Organizer deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_organizer'));
    }

}

/* End of file admin.php */
/* Location: ./application/modules/organizer/controllers/admin.php */
