<?php

/**
 * user Model Class. Handles all the datatypes and methodes required for handling user
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Organizermodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of user for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the user that has been added by current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('delete_flag', 'N');
        $this->db->where('role_id', 2);
        if (is_numeric($this->uri->segment(2))) {
            $event = $this->load_single_data($this->uri->segment(2));
            echo "<pre>";
            print_r($event);
            exit;
            $this->db->where_in('id', $event->organizer);
        }
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of user for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the user that has been added by current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data_in($where_str) {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('delete_flag', 'N');
        $this->db->where('role_id', 2);
        $this->db->where_in('id', $where_str);
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single Event by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single Event by id that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_event_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single user by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single user by id that has been added by current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1") {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single user for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single user w.r.t. current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        $type_id = 2;
        $name = $this->input->post('name');
        $phoneno = $this->input->post('phoneno');
        $address = $this->input->post('address');
        if(!empty($this->input->post('emailid')))
        {
            $emailid = $this->input->post('emailid');
        } else{
            $emailid = null;
        }

        if(!empty($this->input->post('company')))
        {
            $company = $this->input->post('company');
        }
        else
        {
            $company = 0;
        }
        $profile_image = "";
        if (!empty($_FILES['profile_image']['name'])) 
        {
            $this->load->library('upload');
            $config['upload_path'] = './assets/upload/profimg/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['file_name'] = time();

            $this->upload->initialize($config);
            if($_FILES['profile_image']['error']==0)
            {
                $profilepic=$_FILES['profile_image']['name'];
                $profilepic=explode('.',$profilepic);
                $ext=end($profilepic);
                if($ext=="jpg" or $ext=="jpeg" or $ext=="png" or $ext=="bmp")
                {
                    $profilepic=time().rand().".".$ext;
                    $destination="./assets/upload/profimg/".$profilepic;
                    if (move_uploaded_file($_FILES['profile_image']['tmp_name'] ,$destination))
                    {
                        $profile_image=$profilepic;
                        //$olddestination="./assets/upload/profimg/".$oldfile;
                        //@unlink($olddestination);
                    }
                }
                else
                {
                    $this->session->set_flashdata('errormessage', 'Only .jpg,.jpeg,.bmp and .png image extensions are supported');
                    $redirect = site_url('admin/admin_organizer');
                    redirect($redirect);
                }
            }
        }
        $date = date("Y-m-d H:i:s");

        $this->load->helper('common');
        $password = generateRandomString(10);
        $passwordEnc = md5($password);

        if (!empty($id)) {
            if(!empty($profile_image))
            {
                $data = array(
                    'role_id' => 2,
                    'company_id' => $company,
                    'name' => $name,
                    // 'emailid' => $emailid,
                    'password' => $passwordEnc,
                    'phoneno' => $phoneno,
                    'address' => $address,
                    'profile_image' => $profile_image,
                    'modified_date' => $date,
                );
            }
            else
            {
                $data = array(
                    'role_id' => 2,
                    'company_id' => $company,
                    'name' => $name,
                    // 'emailid' => $emailid,
                    'password' => $passwordEnc,
                    'phoneno' => $phoneno,
                    'address' => $address,
                    'modified_date' => $date,
                );
            }

            $this->db->where('id', $id)->update(tablename('user'), $data);
            return $id;
        } else {
            // Check for Duplicate
            $data = array(
                'role_id' => 2,
                'company_id' => $company,
                'name' => $name,
                'emailid' => $emailid,
                'password' => $passwordEnc,
                'phoneno' => $phoneno,
                'address' => $address,
                'profile_image' => $profile_image,
                'entry_date' => $date,
                'type_id'=>$type_id
            );

            $this->db->insert(tablename('user'), $data);
            $last_id = $this->db->insert_id();

            if (!empty($last_id)) {
               $subject = "Eventsador - Account Created";
               $message = '<p>Greetings ' . $name . ",</p>";
               $message .= '<p>Your account has been created with Eventsador. Please use the following credential to login to your profile.</p>';
               $message .= '<p><strong>user-id: </strong>' . $emailid;
               $message .= '<br><strong>Password: </strong>' . $password . "</p>";
               $message .= '<p>Thank you,</p>';
               $message .= '<p>Eventsador Admin</p>';

               $mail_data = [
                   'name' => $name,
                   'body' => $message,
               ];

               $this->load->helper('email');
               send_email($emailid, $subject, $mail_data);

                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of user for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current user status
     * and change it the the opposite [Table: pb_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('user'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }


    /**
     * Used for resend credintials functionality of User for an admin
     *
     * <p>Description</p>
     *
     * This function takes id as input
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function reset_pwd_and_resend_email($id) {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $new_password = rand(100000, 999999);

            $update = array('password' => md5($new_password));
            $this->db->where('id', $id);

            if ($this->db->update(tablename('user'), $update)) {

                $subject = "Eventsador - Account Password Reset";
                $message = '<p>Greetings ' . $result->name . ",</p>";
                $message .= '<p>Your password has been reset with Eventsador. Please use the following credential to login to your profile.</p>';
                $message .= '<p><strong>User-id: </strong>' . $result->emailid;
                $message .= '<br><strong>Password: </strong>' . $new_password . "</p>";
                $message .= '<p>Thank you,</p>';
                $message .= '<p>Eventsador Admin</p>';

                $mail_data = [
                    'name' => $result->name,
                    'body' => $message,
                ];

                $this->load->helper('email');
                send_email($result->emailid, $subject, $mail_data);


                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }


    /**
     * Used for delete functionality of user for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($user) {
        $delete_faq = array('delete_flag' => 'Y', 'name' => $user->name . '[deleted]', 'emailid' => 'deleted'.rand ( 10000 , 99999 ).'.'.$user->emailid);
        $this->db->where('id', $user->id);

        if ($this->db->update(tablename('user'), $delete_faq, ['id' => $user->id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

}

/* End of file Organizermodel.php */
/* Location: ./application/modules/user/models/admin/Organizermodel.php */
