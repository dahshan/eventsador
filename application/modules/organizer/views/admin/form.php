<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_organizer'); ?>"><i class="fa fa-user"></i>Organizer</a>
            <a href="javascript:void(0);" class="current"></i>Organizer Management</a>
        </div>

        <h1>Organizer</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Organizer</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_organizer') : base_url('admin_update_organizer') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Select Company</label>
                                <div class="controls">
                                    <select name="company" id="company">
                                    <option></option>
                                        <?php
                                        if (count($all_company) > 0) {
                                            foreach ($all_company AS $company) {
                                                ?>
                                                <option value="<?php echo (isset($company->id) && $company->id != '') ? $company->id : ''; ?>"
                                                        <?php if(!empty($data_single)){ if($company->id==$data_single->company_id) echo 'selected'; } ?>>
                                                            <?php echo (isset($company->company_name) && $company->company_name != '') ? $company->company_name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('company', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Name" type="text" id="name" name="name" value="<?php echo (!empty(set_value('name'))) ? set_value('name') : ((!empty($data_single->name)) ? $data_single->name : ''); ?>" >
                                    <?php echo form_error('name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Email</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Email" type="text" id="emailid" name="emailid" value="<?php echo (!empty(set_value('emailid'))) ? set_value('emailid') : ((!empty($data_single->emailid)) ? $data_single->emailid : ''); ?>" 
                                    <?php if ($id != ''): ?>
                                        disabled="disabled"
                                    <?php endif ?>
                                     >
                                    <?php echo form_error('emailid', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Phone</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Phone" type="text" id="phoneno" name="phoneno" value="<?php echo (!empty(set_value('phoneno'))) ? set_value('phoneno') : ((!empty($data_single->phoneno)) ? $data_single->phoneno : ''); ?>" >
                                    <?php echo form_error('phoneno', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Address</label>
                                <div class="controls">
                                    <textarea class="span11" id="address" name="address"><?php echo (!empty(set_value('address'))) ? set_value('address') : ((!empty($data_single->address)) ? $data_single->address : ''); ?></textarea>
                                    <?php echo form_error('address', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Image</label>
                                <div class="controls">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                      <img  src="<?php if(!empty($data_single->profile_image)){ echo base_url('assets/upload/profimg')."/".$data_single->profile_image; } else {echo base_url('assets/upload/default_man.png');}?>" data-src="<?php if(!empty($data_single->profile_image)){ echo base_url('assets/upload/profimg')."/".$data_single->profile_image; } else {echo base_url('assets/upload/default_man.png');}?>" alt="...">
                                    </div>
                                    <div>
                                      <div class="controls custon_textarea" style="margin-left: 0;">
                                        <input type="file" name="profile_image" style="line-height:1px;">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <input type="hidden" name="organizer_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
