<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for exhibitor [HMVC]. Handles all the datatypes and methodes required for exhibitor section of Optus
 *
 * @author  
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Exhibitormodel');
    }

    /**
     * Index Page for this exhibitor controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Exhibitormodel->load_events();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'exhibitor/admin/list';
        $this->admin_layout();
    }

    public function search() {
        $this->load->model('../models/admin/Adminauthmodel');
        $eventid = $_POST['event'];

        $event = $this->Exhibitormodel->load_event($eventid);
        $all_data = [];
        if ($event->exhibitors) {
            $all_data = $this->Exhibitormodel->load_exhibitors($event->exhibitors);
        }

        $data['all_data'] = $all_data;
        $this->load->view('exhibitor/admin/ajax_exhibitor',$data);
    }

    /**
     * Index Page for this Organizer controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function exhibitors_by_event() {
        if (is_numeric($this->uri->segment(2))) {
            $event = $this->Exhibitormodel->load_event_data($this->uri->segment(2));
            $all_data = $this->Exhibitormodel->load_all_data_in($event->exhibitors);
            $uri = $this->uri->segment(1);
            $this->data = array();
            $this->data['uri'] = $uri;
            $this->data['all_data'] = $all_data;
            $this->middle = 'exhibitor/admin/list';
            $this->admin_layout();
        }
    }

    /**
     * Add/Edit function of exhibitor module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {

            //echo "<pre>"; print_r($_POST); die;


            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('booth', 'Booth', 'trim|required|numeric');
            if ($param == 'A') {
                if (empty($_FILES['floor_map']['name']))
                {
                    $this->form_validation->set_rules('floor_map', 'floor_map', 'trim|required');
                }
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_new_email_check');
            } else {
                $this->form_validation->set_rules('email', 'Email', 'trim|required|callback_old_email_check');
            }
            $this->form_validation->set_rules('phone', 'Phone no', 'trim|required|numeric');
            // $this->form_validation->set_rules('location', 'Address', 'trim|required');


            if($_POST['type_id']==0)
            {
                $this->form_validation->set_rules('exhibitor_name', 'exhibitor name', 'trim|required');
            }

            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Exhibitormodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Exhibitor already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Exhibitor modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_exhibitor'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Exhibitormodel->load_single_data($id);
        $this->data['exhibitor_type'] = $this->Exhibitormodel->get_result_data("exhibitor_type", array("is_active"=>'Y'));
        $this->data['all_module'] = $this->Exhibitormodel->get_result_data('modules', ['is_active' => 'Y', 'delete_flag' => 'N']);
        $this->middle = 'exhibitor/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function new_email_check($email) {
        if (isset($email) && $email != '') {
            $exhibitor_data = $this->Exhibitormodel->get_row_data('user', ['emailid' => $email, 'delete_flag' => 'N']);
            if (count($exhibitor_data) > 0) {
                $this->form_validation->set_message('new_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $exhibitor_id = $this->input->post('exhibitor_id');
            $exhibitory_data = $this->Exhibitormodel->get_row_data('user', ['id<>' => $exhibitor_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (!empty($exhibitor_data) && count($exhibitor_data) > 0 ) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of exhibitor module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Exhibitormodel->status($id);
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'exhibitor status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_exhibitor'));
    }

    /**
     * Delete function of exhibitor module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $user = $this->Exhibitormodel->load_single_data($id);
        $flg = $this->Exhibitormodel->delete($user);
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'exhibitor deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_exhibitor'));
    }

}

/* End of file admin.php */
/* Location: ./application/modules/exhibitor/controllers/admin.php */
