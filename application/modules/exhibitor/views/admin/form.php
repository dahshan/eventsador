<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_exhibitor'); ?>"><i class="fa fa-flag"></i>Exhibitor</a>
            <a href="javascript:void(0);" class="current">Exhibitor Management</a>
        </div>

        <h1>Exhibitor</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Exhibitor</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_exhibitor') : base_url('admin_update_exhibitor') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Name" type="text" id="name" name="name" value="<?php echo (!empty(set_value('name'))) ? set_value('name') : ((!empty($data_single->name)) ? $data_single->name : ''); ?>" >
                                    <?php echo form_error('name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Select Exhibitor Type</label>
                                <div class="controls">
                                <input type="hidden" id="type_field" value="1">
                                    <select name="type_id" class="select-class" onchange="add_new(this)">
                                        <?php
                                        if (count($exhibitor_type) > 0) {
                                            foreach ($exhibitor_type AS $all_exhibitor_val) {
                                                ?>
                                                <option value="<?php echo (isset($all_exhibitor_val->id) && $all_exhibitor_val->id != '') ? $all_exhibitor_val->id : ''; ?>"
                                                        <?php if(!empty($data_single)){ if($all_exhibitor_val->id==$data_single->exhibitor_type)echo 'selected'; }?>>
                                                            <?php echo (isset($all_exhibitor_val->name) && $all_exhibitor_val->name != '') ? $all_exhibitor_val->name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        if($this->session->userdata('admin_role_type')==1){
                                        ?>

                                        <option value="0">Other</option>
                                        <?php } ?>
                                        <input class="span11 text-class exhibitor-class"  id="exhibitor_text" placeholder="Exhibitor Type" type="text" name="exhibitor_name" style="display:none;">
                                    &nbsp;&nbsp;
                                    <a class="exhibitor-class" style="display: none;" href="javascript:void(0);" onclick="revert_add(this)">
                                        <i class="fa fa-undo" aria-hidden="true"></i>
                                    </a>
                                   <?php echo form_error('exhibitor_name', '<div style="color:red;">', '</div>'); ?>


                                    </select>
                                    <?php echo form_error('type_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Details</label>
                                <div class="controls">
                                    <textarea class="span11" id="details" name="details"><?php echo (!empty(set_value('details'))) ? set_value('details') : ((!empty($data_single->details)) ? $data_single->details : ''); ?></textarea>
                                    <?php echo form_error('details', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span6">
                                    <label class="control-label">Location</label>
                                    <div class="controls">
                                        <textarea class="span11" id="location" name="location"><?php echo (!empty(set_value('location'))) ? set_value('location') : ((!empty($data_single->location)) ? $data_single->location : ''); ?></textarea>
                                        <?php echo form_error('location', '<div style="color:red;">', '</div>'); ?>
                                    </div>   
                                </div>
                                <div class="span5">
                                    <label class="control-label">Booth</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Booth" type="text" id="booth" name="booth" value="<?php echo (!empty(set_value('booth'))) ? set_value('booth') : ((!empty($data_single->booth)) ? $data_single->booth : ''); ?>" >
                                        <?php echo form_error('booth', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span6">
                                    <label class="control-label">Email</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Email" type="text" id="email" name="email" value="<?php echo (!empty(set_value('email'))) ? set_value('email') : ((!empty($data_single->emailid)) ? $data_single->emailid : ''); ?>">
                                        <?php echo form_error('email', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">Phone</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Phone" type="text" id="phone" name="phone" value="<?php echo (!empty(set_value('phone'))) ? set_value('phone') : ((!empty($data_single->phoneno)) ? $data_single->phoneno : ''); ?>" >
                                        <?php echo form_error('phone', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span6">
                                    <label class="control-label">Website</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Website" type="text" id="website" name="website" value="<?php echo (!empty(set_value('website'))) ? set_value('website') : ((!empty($data_single->website)) ? $data_single->website : ''); ?>">
                                        <?php echo form_error('website', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">Contact Person</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Contact Person" type="text" id="contact_person" name="contact_person" value="<?php echo (!empty(set_value('contact_person'))) ? set_value('contact_person') : ((!empty($data_single->contact_person)) ? $data_single->contact_person : ''); ?>" >
                                        <?php echo form_error('contact_person', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span6">
                                    <label class="control-label">Logo</label>
                                    <div class="controls">
                                      <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                          <img  src="<?php if(!empty($data_single->profile_image)){ echo base_url('assets/upload/appuser')."/".$data_single->profile_image; } else {echo base_url('assets/upload/default_logo.png');}?>" data-src="<?php if(!empty($data_single->profile_image)){ echo base_url('assets/upload/appuser')."/".$data_single->profile_image; } else {echo base_url('assets/upload/default_logo.png');}?>" alt="...">
                                        </div>
                                        <div>
                                          <div class="controls custon_textarea" style="margin-left: 0;">
                                            <input type="file" name="logo" style="line-height:1px;">
                                          </div>
                                          <?php echo form_error('logo', '<div style="color:red;">', '</div>'); ?>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">Floor Map</label>
                                    <div class="controls">
                                      <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                          <img  src="<?php if(!empty($data_single->floor_map)){ echo base_url('assets/upload/exhibitor')."/".$data_single->floor_map; } else {echo base_url('assets/upload/default_floor.png');}?>" data-src="<?php if(!empty($data_single->floor_map)){ echo base_url('assets/upload/exhibitor')."/".$data_single->floor_map; } else {echo base_url('assets/upload/default_floor.png');}?>" alt="...">
                                        </div>
                                        <div>
                                          <div class="controls custon_textarea" style="margin-left: 0;">
                                            <input type="file" name="floor_map" style="line-height:1px;">
                                          </div>
                                          <?php echo form_error('floor_map', '<div style="color:red;">', '</div>'); ?>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="exhibitor_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function add_new(obj) {
        var div_name = $(obj).val();
        if (div_name == '0') {
            $(".exhibitor-class").toggle();
            $(".text-class").val('');
            $("#type_field").val(2);
        } else {
            $("#type_field").val(1);
        }
    }
    function revert_add(obj) {
        $("#type_field").val(1);
        $(".exhibitor-class").toggle();
        $('.select-class option:eq(0)').attr('selected', 'selected');
        $(".text-class").val('');
    }
    
</script>