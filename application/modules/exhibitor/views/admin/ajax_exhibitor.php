<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>

                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>                       
                    </div>
                    <div class="widget-content nopadding">

                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Contact Person</th>
                                    <th>Logo</th>
                                    <!-- <th>Website</th> -->
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Location</th>
                                    <th>Booth</th>
                                    <?php if(in_array(67, $getRole)){ ?><th>Status</th><?php } ?>
                                    <?php if(in_array(68, $getRole)){ ?><th>Action</th><?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                            <?php
                            $sl = 1;
                            //echo "<pre>"; print_r($all_data);
                            if (!empty($all_data)) {
                                foreach ($all_data as $data) {
                            ?>
                                <tr class="gradeX">
                                    <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                    <td class="custom-action-btn">
                                        <?php if(in_array(66, $getRole)){ ?><u><a href="<?php echo base_url('admin_update_exhibitor' . '/' . $data->id); ?>">
                                            <?php echo ucfirst($data->name); ?>
                                        </a></u><?php }else{ echo ucfirst($data->name); } ?>
                                    </td>
                                    <td class="custom-action-btn"><?php echo (!empty($data->type)) ? $data->type : "NA"; ?></td>
                                    <td class="custom-action-btn"><?php echo (!empty($data->contact_person)) ? $data->contact_person : "NA"; ?></td>
                                    <td class="imagethumb"><img style="" <?php if(!empty($data->profile_image)){ ?> src="<?php echo base_url('assets/upload/appuser'); ?>/<?php echo $data->profile_image; ?>" <?php }else{ ?> src="<?php echo base_url('assets/upload/default_logo.png'); ?>" <?php } ?> ></td>
                                    <!-- <td class="custom-action-btn"><?php echo (!empty($data->website)) ? $data->website : "NA"; ?></td> -->
                                    <td class="custom-action-btn"><?php echo (!empty($data->emailid)) ? $data->emailid : "NA"; ?></td>
                                    <td class="custom-action-btn"><?php echo (!empty($data->phoneno)) ? $data->phoneno : "NA"; ?></td>
                                    <td class="custom-action-btn"><?php echo (!empty($data->location)) ? $data->location : "NA"; ?></td>
                                    <td class="custom-action-btn"><u><a data-image="<?php if(!empty($data->floor_map)) echo base_url('assets/upload/exhibitor').'/'.$data->floor_map;?>" class="floor_map" href="javascript:void(0);"><?php echo (!empty($data->booth)) ? $data->booth : "NA"; ?></a></u></td>
                                   <?php if(in_array(67, $getRole)){ ?> <td class="custom-action-btn">
                                        <?php
                                        if ($data->is_active == "Y") {
                                            echo '<i title="De-activate" class="fa fa-close" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                        } else {
                                            echo '<i title="Activate" class="fa fa-check" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                        }
                                        ?>
                                    </td><?php } ?>

                                    <?php if(in_array(68, $getRole)){ ?><td class="custom-action-btn">
                                        <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                    </td><?php } ?>
                                </tr>
                            <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>


                    </div>