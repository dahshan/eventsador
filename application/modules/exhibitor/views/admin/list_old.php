<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-flag"></i>Exhibitor</a>
        </div>

        <h1>Exhibitor</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <?php if(in_array(65, $getRole)){ ?><button class="btn btn-success btn-cls" type="button" onclick="add();">Add Exhibitor</button><?php } ?>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Contact Person</th>
                                    <th>Logo</th>
                                    <!-- <th>Website</th> -->
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Location</th>
                                    <th>Booth</th>
                                    <?php if(in_array(67, $getRole)){ ?><th>Status</th><?php } ?>
                                    <?php if(in_array(68, $getRole)){ ?><th>Action</th><?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                            <?php
                            $sl = 1;
                            //echo "<pre>"; print_r($all_data);
                            if (!empty($all_data)) {
                                foreach ($all_data as $data) {
                            ?>
                                <tr class="gradeX">
                                    <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                    <td class="custom-action-btn">
                                        <?php if(in_array(66, $getRole)){ ?><u><a href="<?php echo base_url('admin_update_exhibitor' . '/' . $data->id); ?>">
                                            <?php echo ucfirst($data->name); ?>
                                        </a></u><?php }else{ echo ucfirst($data->name); } ?>
                                    </td>
                                    <td class="custom-action-btn"><?php echo (!empty($data->type)) ? $data->type : "NA"; ?></td>
                                    <td class="custom-action-btn"><?php echo (!empty($data->contact_person)) ? $data->contact_person : "NA"; ?></td>
                                    <td><img style="height: 50px;width: 50px;" <?php if(!empty($data->profile_image)){ ?> src="<?php echo base_url('assets/upload/appuser'); ?>/<?php echo $data->profile_image; ?>" <?php }else{ ?> src="<?php echo base_url('assets/upload/default_logo.png'); ?>" <?php } ?> ></td>
                                    <!-- <td class="custom-action-btn"><?php echo (!empty($data->website)) ? $data->website : "NA"; ?></td> -->
                                    <td class="custom-action-btn"><?php echo (!empty($data->emailid)) ? $data->emailid : "NA"; ?></td>
                                    <td class="custom-action-btn"><?php echo (!empty($data->phoneno)) ? $data->phoneno : "NA"; ?></td>
                                    <td class="custom-action-btn"><?php echo (!empty($data->location)) ? $data->location : "NA"; ?></td>
                                    <td class="custom-action-btn"><u><a data-image="<?php if(!empty($data->floor_map)) echo base_url('assets/upload/exhibitor').'/'.$data->floor_map;?>" class="floor_map" href="javascript:void(0);"><?php echo (!empty($data->booth)) ? $data->booth : "NA"; ?></a></u></td>
                                   <?php if(in_array(67, $getRole)){ ?> <td class="custom-action-btn">
                                        <?php
                                        if ($data->is_active == "Y") {
                                            echo '<i title="De-activate" class="fa fa-close" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                        } else {
                                            echo '<i title="Activate" class="fa fa-check" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                        }
                                        ?>
                                    </td><?php } ?>

                                    <?php if(in_array(68, $getRole)){ ?><td class="custom-action-btn">
                                        <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                    </td><?php } ?>
                                </tr>
                            <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add exhibitor Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_exhibitor'); ?>";
    }

    /**
     * Status Change Function
     *
     * @param   rec_id
     * @return  redirects and gives control to status handler
     */
    function change_status(rec_id) {
        $("#myAlert").on('shown.bs.modal', function() {
            $("#modal_confirm").click(function() {
                window.location.href = "<?php echo base_url('admin_status_exhibitor'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to change status of this exhibitor?");
        }).modal("show");
    }

    /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function() {
            $("#modal_confirm").click(function() {
                window.location.href = "<?php echo base_url('admin_delete_exhibitor'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this exhibitor?");
        }).modal("show");
    }

    $(document).on("click",".floor_map",function(){
        var im=$(this).data("image");
        if(im)
        {
            $("#myAlert").on('shown.bs.modal', function() {
                $('.modal').css({
                      'width':'250px'
                });
                $("#notification_heading").html("Floor Map");
                $("#modal_confirm").hide();
                $("#notification_body").html("<img style='height:50px;width:250px;' src='"+im+"'></img>");
            }).modal("show");
        }
    });
</script>
