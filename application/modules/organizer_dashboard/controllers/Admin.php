<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for Event [HMVC]. Handles all the datatypes and methodes required for Event section of Optus
 *
 * @author  
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Organizerdashboardmodel');
        $this->load->helper("common_helper");
        
    }

    /**
     * Index Page for this Event controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Organizerdashboardmodel->load_all_data();
       
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'organizer_dashboard/admin/list';
        $this->admin_layout();
    }

    
    public function reportt_search() {
        $eventid = $_POST['event'];

        $event = $this->Organizerdashboardmodel->load_single_data($eventid);

        $event->accommodations = $this->Organizerdashboardmodel->load_other_tables_data('accomodation', ['event_id' => $event->id]);

        $event->transportations = $this->Organizerdashboardmodel->load_other_tables_data('transportation', ['event_id' => $event->id]);

        $event->attractions = $this->Organizerdashboardmodel->load_other_tables_data('local_attraction', ['event_id' => $event->id]);

        $event->exhibitors = $this->Organizerdashboardmodel->load_other_tables_data('exhibitor', ['organizer_id' => $event->id]);

        $data['event'] = $event;
        $this->load->view('organizer_dashboard/admin/organizer_dashboard_ajax',$data);
        // echo "<pre>";
        // print_r($event);
        // exit;

    }

    public function event_attendees() {
        $eventid = $_POST['event'];

        $event = $this->Organizerdashboardmodel->load_single_data($eventid);
        $all_data = $this->Organizerdashboardmodel->event_attendees($event->attendees);

        $data['all_data'] = $all_data;
        $this->load->view('organizer_dashboard/admin/organizer_dashboard_ajax_attendees',$data);
    }

    public function event_exhibitors() {
        $eventid = $_POST['event'];

        $all_data = $this->Organizerdashboardmodel->event_exhibitors($eventid);

        $data['all_data'] = $all_data;
        $this->load->view('organizer_dashboard/admin/organizer_dashboard_ajax_exhibitors',$data);
    }

    public function event_accommodations() {
        $eventid = $_POST['event'];

        $all_data = $this->Organizerdashboardmodel->event_accommodations($eventid);

        $data['all_data'] = $all_data;
        $this->load->view('organizer_dashboard/admin/organizer_dashboard_ajax_accommodations',$data);
    }

    public function event_transportations() {
        $eventid = $_POST['event'];

        $all_data = $this->Organizerdashboardmodel->event_transportations($eventid);

        $data['all_data'] = $all_data;
        $this->load->view('organizer_dashboard/admin/organizer_dashboard_ajax_transportations',$data);
    }

    public function event_hosts() {
        $eventid = $_POST['event'];

        $event = $this->Organizerdashboardmodel->load_single_data($eventid);
        $all_data = $this->Organizerdashboardmodel->event_hosts($event->hosts);

        $data['all_data'] = $all_data;
        $this->load->view('organizer_dashboard/admin/organizer_dashboard_ajax_hosts',$data);
    }

    public function event_sponsors() {
        $eventid = $_POST['event'];

        $event = $this->Organizerdashboardmodel->load_single_data($eventid);
        $all_data = $this->Organizerdashboardmodel->event_sponsors($event->sponsors);

        $data['all_data'] = $all_data;
        $this->load->view('organizer_dashboard/admin/organizer_dashboard_ajax_sponsors',$data);
    }

    public function event_attractions() {
        $eventid = $_POST['event'];

        $all_data = $this->Organizerdashboardmodel->event_attractions($eventid);

        $data['all_data'] = $all_data;
        $this->load->view('organizer_dashboard/admin/organizer_dashboard_ajax_attractions',$data);
    }

}
