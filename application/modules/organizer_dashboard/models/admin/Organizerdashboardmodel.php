<?php

/**
 * Event Model Class. Handles all the datatypes and methodes required for handling Event
 *
 * @author  <sketch.dev22@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Organizerdashboardmodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the Event that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('delete_flag', 'N');
       // echo "<pre>";print_r($this->session->userdata('admin_role_type'));exit;
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(".$this->session->userdata('admin_uid').", users) !=", 0);
            } 
        }
        $this->db->order_by("id", "desc");

        $query = $this->db->get();
        $result = $query->result();
        //echo $this->db->last_query();
        // echo "<pre>";print_r($result);exit;
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }


    public function load_other_tables_data($tbl, $where) {
        $this->db->select('id');
        $this->db->from(tablename($tbl));
        $this->db->where('delete_flag', 'N');
        $this->db->where($where);
        $this->db->order_by("id", "desc");

        $query = $this->db->get();
        $result = $query->num_rows();
        return $result;
    }

   
    public function load_single_data($eventid) {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('id', $eventid);
        
        $query = $this->db->get();
        $result = $query->first_row();

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }
   
    public function load_single_data_old($eventid) {
        $this->db->select('t1.*,t2.name,t2.emailid,t2.phoneno,t2.profile_image,t2.company,t2.position_title');
        $this->db->from(tablename('event_access_request'). ' as t1');
        $this->db->join(tablename('user') . ' as t2', 't1.user_id = t2.id', 'left');  
           
        $this->db->where('t1.event_id', $eventid);
        
        $query = $this->db->get();
        $result = $query->result();

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }
   
    public function event_attendees($attendees) {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where("id in ($attendees)");
        
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }
   
    public function event_exhibitors($eventid) {
        $this->db->select('*');
        $this->db->from(tablename('exhibitor'));
        $this->db->where('event_id', $eventid);
        
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }
   
    public function event_accommodations($eventid) {
        $this->db->select('*');
        $this->db->from(tablename('accomodation'));
        $this->db->where('event_id', $eventid);
        
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }
   
    public function event_transportations($eventid) {
        $this->db->select('*');
        $this->db->from(tablename('transportation'));
        $this->db->where('event_id', $eventid);
        
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }
   
    public function event_hosts($hosts) {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where("id in ($hosts)");
        
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }
   
    public function event_sponsors($sponsors) {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where("id in ($sponsors)");
        
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }
   
    public function event_attractions($eventid) {
        $this->db->select('*');
        $this->db->from(tablename('local_attraction'));
        $this->db->where('event_id', $eventid);
        
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

    
}
