
                    <div class="row-fluid">
                        <div class="span12">
                            <ul class="site-stats">

                                <?php if($this->session->userdata('admin_role_type')==1){ ?>
                                <a href="javascript:void" id="attendees"><li class="bg_lh"><i class="icon-building"></i> <strong><?php echo empty($event->attendees) ? 0 : count(explode(',', $event->attendees)); ?></strong>
                                    <small>
                                        Attendees
                                    </small>
                                </li></a>
                                <?php } ?>

                                <?php if($this->session->userdata('admin_role_type')==1){ ?>
                                <a href="javascript:void" id="exhibitors"><li class="bg_lh"><i class="icon-building"></i> <strong><?php echo $event->exhibitors; ?></strong>
                                    <small>
                                        Exhibitors
                                    </small>
                                </li></a>
                                <?php } ?>

                                <?php if($this->session->userdata('admin_role_type')==1){ ?>
                                <a href="javascript:void" id="accommodations"><li class="bg_lh"><i class="icon-building"></i> <strong><?php echo $event->accommodations; ?></strong>
                                    <small>
                                        Accommodation
                                    </small>
                                </li></a>
                                <?php } ?>

                                <?php if($this->session->userdata('admin_role_type')==1){ ?>
                                <a href="javascript:void" id="transportations"><li class="bg_lh"><i class="icon-building"></i> <strong><?php echo $event->transportations; ?></strong>
                                    <small>
                                        Transportation
                                    </small>
                                </li></a>
                                <?php } ?>

                                <?php if($this->session->userdata('admin_role_type')==1){ ?>
                                <a href="javascript:void" id="hosts"><li class="bg_lh"><i class="icon-building"></i> <strong><?php echo empty($event->hosts) ? 0 : count(explode(',', $event->hosts)); ?></strong>
                                    <small>
                                        Host
                                    </small>
                                </li></a>
                                <?php } ?>

                                <?php if($this->session->userdata('admin_role_type')==1){ ?>
                                <a href="javascript:void" id="sponsors"><li class="bg_lh"><i class="icon-building"></i> <strong><?php echo empty($event->sponsors) ? 0 : count(explode(',', $event->sponsors)); ?></strong>
                                    <small>
                                        Sponsors
                                    </small>
                                </li></a>
                                <?php } ?>

                                <?php if($this->session->userdata('admin_role_type')==1){ ?>
                                <a href="javascript:void" id="attractions"><li class="bg_lh"><i class="icon-building"></i> <strong><?php echo $event->attractions; ?></strong>
                                    <small>
                                        Attractions
                                    </small>
                                </li></a>
                                <?php } ?>


                            </ul>
                        </div>
                    </div>

<script>

$("#attendees").click(function(){
    console.log($("#event").val());
    if($("#event").val()!=''){
        $.post("<?php echo base_url('admin_organizer_dashboard_attendees'); ?>", {event: $("#event").val()}).done(function(result){        
        $("#fields_rp").html(result);
    }).fail(function(xhr, status, error){
        console.log(xhr);
        console.log(status);
        console.log(error);
    });
    }else{
        alert("Select Event");
    }
});

$("#exhibitors").click(function(){
    console.log($("#event").val());
    if($("#event").val()!=''){
        $.post("<?php echo base_url('admin_organizer_dashboard_exhibitors'); ?>", {event: $("#event").val()}).done(function(result){        
        $("#fields_rp").html(result);
    }).fail(function(xhr, status, error){
        console.log(xhr);
        console.log(status);
        console.log(error);
    });
    }else{
        alert("Select Event");
    }
});

$("#accommodations").click(function(){
    console.log($("#event").val());
    if($("#event").val()!=''){
        $.post("<?php echo base_url('admin_organizer_dashboard_accommodations'); ?>", {event: $("#event").val()}).done(function(result){        
        $("#fields_rp").html(result);
    }).fail(function(xhr, status, error){
        console.log(xhr);
        console.log(status);
        console.log(error);
    });
    }else{
        alert("Select Event");
    }
});

$("#transportations").click(function(){
    console.log($("#event").val());
    if($("#event").val()!=''){
        $.post("<?php echo base_url('admin_organizer_dashboard_transportations'); ?>", {event: $("#event").val()}).done(function(result){        
        $("#fields_rp").html(result);
    }).fail(function(xhr, status, error){
        console.log(xhr);
        console.log(status);
        console.log(error);
    });
    }else{
        alert("Select Event");
    }
});

$("#hosts").click(function(){
    console.log($("#event").val());
    if($("#event").val()!=''){
        $.post("<?php echo base_url('admin_organizer_dashboard_hosts'); ?>", {event: $("#event").val()}).done(function(result){        
        $("#fields_rp").html(result);
    }).fail(function(xhr, status, error){
        console.log(xhr);
        console.log(status);
        console.log(error);
    });
    }else{
        alert("Select Event");
    }
});

$("#sponsors").click(function(){
    console.log($("#event").val());
    if($("#event").val()!=''){
        $.post("<?php echo base_url('admin_organizer_dashboard_sponsors'); ?>", {event: $("#event").val()}).done(function(result){        
        $("#fields_rp").html(result);
    }).fail(function(xhr, status, error){
        console.log(xhr);
        console.log(status);
        console.log(error);
    });
    }else{
        alert("Select Event");
    }
});

$("#attractions").click(function(){
    console.log($("#event").val());
    if($("#event").val()!=''){
        $.post("<?php echo base_url('admin_organizer_dashboard_attractions'); ?>", {event: $("#event").val()}).done(function(result){        
        $("#fields_rp").html(result);
    }).fail(function(xhr, status, error){
        console.log(xhr);
        console.log(status);
        console.log(error);
    });
    }else{
        alert("Select Event");
    }
});

</script>

