<!-- datatable for pdf -->

<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
 <!-- <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script> --> 

<style type="text/css">
    .dataTables_filter{
        margin-top: -36px;
    }
</style>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-calendar-o"></i>Organizer Dashboard</a>
        </div>

        <h1>Organizer Dashboard</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                	<div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Site Analytics</h5>
                    </div>
                    <div class="widget-content nopadding">
                       	<form class="form-horizontal" method="post">
                       		<div class="control-group">                            
	                       		<!-- <div class="span1">&nbsp;</div> -->
		                        <div class="span12">
		                            <label class="control-label">Event Name</label>
		                            <div class="controls">
		                                <select name="event" id="event">
		                                 <option value="">Select</option>
		                                <?php
		                            if (!empty($all_data)) {
		                                foreach ($all_data as $data) {
		                                    ?>
		                                <option value="<?php echo $data->id; ?>"><?php echo $data->event_name; ?></option>
		                            <?php } } ?>
		                                </select>
		                            </div>
		                        </div>
                            </div>                           
                       	</form>  






                    </div>
                </div>    
            </div> 
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box"  id="result_rp">                    
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box"  id="fields_rp">                    
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
var myGlyph = new Image();
myGlyph.src = 'myglyph.png';

function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    return canvas.toDataURL("image/png");
}


$("#event").on('change', function(){
    if($("#event").val()!=''){
        $.post("<?php echo base_url('admin_organizer_dashboard_search'); ?>", {event: this.value}).done(function(result){        
        //console.log(result);
        $("#result_rp").html(result);
        $("#fields_rp").html('');
        var namee = 'Event Report : '+$("#event option:selected").text()+')';

      var table =$('.data-table').DataTable( {
                           bSortCellsTop: true, // for add multifle tr in thead
                            dom: 'lBfrtip', // lBfrtip = sort filter, Bfrtip = no sort filter
                           buttons : [
                            //{ extend: 'pdf' }
                            { extend: 'pdfHtml5', title : function() {
                                    return namee;
                                },filename: function(){ 
                                   return 'Event Report';
                                } },
                          ]
                        } );
                      if ( ! table.data().count() ) {
                        $('.data-table').dataTable().fnDestroy();
                            $('.data-table').DataTable( {
                            searching: false,
                            bLengthChange: false
                        })
                    }
    }).fail(function(xhr, status, error){
        console.log(xhr);
        console.log(status);
        console.log(error);
    });
    }else{
        alert("Select Event");
    }
});





</script>
