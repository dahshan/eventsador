
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>                       
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <!-- <th>Image</th> -->
                                    <th>Company</th>
                                    <th>Position</th>
                                    <th>Status</th>
                                    <!-- <th>Print Badge</th> -->
                                    <th>Logined By</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->name; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->emailid; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->phoneno; ?>
                                            </td>
                                            <!-- <td><img style="height: 50px;width: 50px;" <?php //if(!empty($data->profile_image)){ ?> src="<?php //echo base_url('assets/upload/appuser'); ?>/<?php //echo $data->profile_image; ?>" <?php //}else{ ?> src="<?php //echo base_url('assets/upload/default_man.png'); ?>" <?php //} ?> ></td> -->
                                            <td class="custom-action-btn">
                                                <?php echo $data->company; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->position_title; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php if($data->checked_in=='1'){ echo "Checked in"; }else{  echo "Check in"; } ?>
                                            </td>
                                            <!-- <td class="custom-action-btn">
                                                <i title="Print Badge" class="fa fa-print" aria-hidden="false" onclick="print_badge(<?php echo $data->event_id; ?>,<?php echo $data->user_id; ?>);"></i>
                                            </td> -->
                                             <td class="custom-action-btn">
                                                <?php echo $data->requested_by; ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>