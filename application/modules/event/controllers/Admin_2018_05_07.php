<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for Event [HMVC]. Handles all the datatypes and methodes required for Event section of Optus
 *
 * @author  
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Eventmodel');
        $this->load->helper("common_helper");
        
    }

    /**
     * Index Page for this Event controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Eventmodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'event/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of Event module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('event_name', 'Event name', 'trim|required');
            $this->form_validation->set_rules('event_start_date', 'Event start date', 'trim|required');
            $this->form_validation->set_rules('event_end_date', 'Event end date', 'trim|required');
            $this->form_validation->set_rules('event_start_time', 'Event start time', 'trim|required');
            $this->form_validation->set_rules('event_end_time', 'Event end time', 'trim|required');
            // $this->form_validation->set_rules('event_venue', 'Event venue', 'trim|required');
            if($this->session->userdata('admin_role_type')!=1)
            {
                $this->form_validation->set_rules('autocomplete', 'Event venue', 'trim|required');
                $this->form_validation->set_rules('event_description', 'Event Designation', 'trim|required');
                if($this->session->userdata('admin_role_type')!=2)
                {
                    $this->form_validation->set_rules('organizers', 'Organizer', 'callback_organizers_check');
                }
                $this->form_validation->set_rules('sponsors', 'Sponsor', 'callback_sponsors_check');
                $this->form_validation->set_rules('hosts', 'Host', 'callback_hosts_check');
            }
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Eventmodel->modify($id);
                if (!empty($flg)) {

                    //Send Notification To Users

                    //$users_list = $this->Eventmodel->get_result_data('user_notification', ['event_notify' => 'Y']);

                    $where = array();
                    $where['un.event_notify']='Y';
                    $users_list = $this->Eventmodel->get_user_details_result_data($where);
                   
                    $notification_text  = 'Event has been added';
                    $notification_title = 'Event Added';

                    foreach($users_list as $user)
                    {
                        notification($user->fcm_reg_token,$notification_text,$notification_title);
                    }


                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Event already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Event modified successfully');
                    }



                    
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_event'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Eventmodel->load_single_data($id);
        $this->data['all_organizer'] = $this->Eventmodel->get_result_data('user', ['role_id' => 2,'is_active' => 'Y', 'delete_flag' => 'N']);
        if($this->session->userdata('admin_role_type')==2)
        {
            $this->data['all_sponsor'] = $this->Eventmodel->get_result_data('sponsor', ['organizer_id' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N']);
        }
        else
        {
            $this->data['all_sponsor'] = $this->Eventmodel->get_result_data('sponsor', ['is_active' => 'Y', 'delete_flag' => 'N']);
        }
        if($this->session->userdata('admin_role_type')==2)
        {
            $this->data['all_speaker'] = $this->Eventmodel->get_result_data('speaker', ['organizer_id' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N']);
        }
        else
        {
            $this->data['all_speaker'] = $this->Eventmodel->get_result_data('speaker', ['is_active' => 'Y', 'delete_flag' => 'N']);
        }
        if($this->session->userdata('admin_role_type')==2)
        {
            $this->data['all_host'] = $this->Eventmodel->get_result_data('host', ['organizer_id' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N']);
        }
        else
        {
            $this->data['all_host'] = $this->Eventmodel->get_result_data('host', ['is_active' => 'Y', 'delete_flag' => 'N']);
        }
        if($this->session->userdata('admin_role_type')==2)
        {
            $this->data['all_exhibitor'] = $this->Eventmodel->get_result_data('exhibitor', ['organizer_id' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N']);
        }
        else
        {
            $this->data['all_exhibitor'] = $this->Eventmodel->get_result_data('exhibitor', ['is_active' => 'Y', 'delete_flag' => 'N']);
        }
        $this->middle = 'event/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for organizer selected or not
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function organizers_check($organizers) {
        $organizers = $this->input->post('organizers');
        if (!isset($organizers) || count($organizers) == 0) {
            $this->form_validation->set_message('organizers_check', '{field} Cannot be blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function hosts_check($hosts) {
        $hosts = $this->input->post('hosts');
        if (!isset($hosts) || count($hosts) == 0) {
            $this->form_validation->set_message('hosts_check', '{field} Cannot be blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function sponsors_check($sponsors) {
        $sponsors = $this->input->post('sponsors');
        if (!isset($sponsors) || count($sponsors) == 0) {
            $this->form_validation->set_message('sponsors_check', '{field} Cannot be blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $event_id = $this->input->post('event_id');
            $event_data = $this->Eventmodel->get_row_data('event', ['id<>' => $event_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($event_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of Event module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Eventmodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Event status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_event'));
    }

    /**
     * Delete function of Event module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Eventmodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Event deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_event'));
    }

}

/* End of file admin.php */
/* Location: ./application/modules/event/controllers/admin.php */
