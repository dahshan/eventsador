<?php

/**
 * Event Model Class. Handles all the datatypes and methodes required for handling Event
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Eventmodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the Event that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('delete_flag', 'N');
        //var_dump($this->session->userdata());exit;
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
            }
        }
        $this->db->order_by("arrange", "desc");

        $query = $this->db->get();
        $result = $query->result();
        //var_dump($result); exit;
        // echo "<pre>";print_r($result);exit;
        if(!empty($result))
        {
            $fina_result=array();
            foreach($result as $val)
            {
                $val->pending_count=count($this->get_result_data("event_access_request", array("status"=>'not_approved',"event_id"=>$val->id)));

                $val->pending_count_not_join=count($this->get_result_data("event_access_request", array("status"=>'approved',"event_id"=>$val->id,"event_join"=>'0')));

                $val->pending_count = $this->get_count_of_pending_attendees($val->id);// $val->pending_count_not_join;//$val->pending_count + 

                $val->attendees_count=$this->get_attendee_only($val->id);//count($this->get_result_data("event_access_request", array("event_id"=>$val->id,"event_join"=>'1')));//"status"=>'approved',
                // if(!empty($val->attendees))
                // {
                //     $val->attendees_count=count(explode(',',$val->attendees));
                // }
                // else
                // {
                //     $val->attendees_count=0;
                // }
                $fina_result[]=$val;
            }
            $result=$fina_result;
        }

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single Event by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single Event by id that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1",$optional_where=NULL) {
        if(empty($optional_where))
        {
            $query = $this->db->get_where(tablename($table), $where);
        }
        else
        {
            $this->db->where_in('id', $optional_where);
            $query = $this->db->get(tablename($table), $where);
        }

        //echo $this->db->last_query(); exit;
        return $query->result();
    }


    public function get_result_data_of_role_users() {
        
        $this->db->select("us.*,rl.name as role_name"); 
        $this->db->from("user as us");
        $this->db->join("role as rl","rl.id = us.role_id");  
        $this->db->where("rl.id IN ('5','6','7','8')");
        //$this->db->where("un.event_notify",'Y');
     
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        $result =  $query->result();

      /*  echo "<pre>";
        print_r($result);
        exit;*/

        return $result;
    }

    public function get_result_data1($table,$whr,$table1=NULL,$condition=NULL,$field=NULL,$tag=NULL){
        if(!empty($table1))
        {
            // $this->db->select('exhibitor.*,exhibitor_type.name as type_name');
            // $this->db->join($table1,$condition);
        }
        $query =$this->db->where_in($field,$whr)->get(tablename($table));
        return $query->result();
    }


    public function get_user_details_result_data($where) {
        
        $this->db->select("un.user_id,app.*"); 
        $this->db->from("user_notification as un");
        $this->db->join("user as app","app.id = un.user_id");  
        $this->db->where($where);
        //$this->db->where("un.event_notify",'Y');
     
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        $result =  $query->result();

        return $result;
    }


    public function get_my_events($table, $where =NULL) {
        
        $query = $this->db->get_where(tablename($table), $where);
        
        //echo $this->db->last_query(); exit;
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single Event w.r.t. current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {

     // echo "<pre>"; print_r($_POST); die;
        $dd = $this->input->post('event_start_date');
        $date = str_replace('/', '-', $dd);

        $dd2 = $this->input->post('event_end_date');
        $date2 = str_replace('/', '-', $dd2);

        $data['event_name'] = $this->input->post('event_name');
        //$data['event_date'] = date("Y-m-d");
        $data['event_date'] = date("Y-m-d",strtotime($date));
        $data['event_start_date'] = date("Y-m-d",strtotime($date));
        $data['event_end_date'] = date("Y-m-d",strtotime($date2));
        $data['event_start_time'] = $this->input->post('event_start_time');
        $data['event_end_time'] = $this->input->post('event_end_time');

        //echo "<pre>"; print_r($data); die;

        if(!empty($this->input->post('latlong'))){
            $latlong=json_decode($this->input->post('latlong'));
            if(!empty($latlong))
            {
                $data['lat']= $latlong->lat;
                $data['lng']= $latlong->lng;
            }
            else
            {
                $latlong=explode(",",$this->input->post('latlong'));
                $data['lat']= $latlong[0];
                $data['lng']= $latlong[1];
            }
        }
        $data['event_venue']=$this->input->post('autocomplete');

        if(!empty($this->input->post('street_number'))){
            $data['street_no']= $this->input->post('street_number');
            //$data['event_venue'].=$data['street_no'];
        }else{
            $data['street_no']= '';
        }
        
        if(!empty($this->input->post('route'))){
            // if(!empty($this->input->post('street_number')))
            // {
            //     $data['event_venue'].=", ";
            // }
            $data['street_name'] = $this->input->post('route');
            //$data['event_venue'].=$data['street_name'];
        }else{
            $data['street_name']= '';
        }
        
        if(!empty($this->input->post('locality'))){
            // if(!empty($this->input->post('route')))
            // {
            //     $data['event_venue'].=", ";
            // }
            $data['city']= $this->input->post('locality');
            //$data['event_venue'].=$data['city'];
        }else{
            $data['city']= '';
        }
        
        if(!empty($this->input->post('administrative_area_level_1'))){
            // if(!empty($this->input->post('locality')))
            // {
            //     $data['event_venue'].=", ";
            // }
            $data['state'] = $this->input->post('administrative_area_level_1');
            //$data['event_venue'].=$data['state'];
        }else{
            $data['state']= '';
        }
        
        if(!empty($this->input->post('postal_code'))){
            // if(!empty($this->input->post('administrative_area_level_1')))
            // {
            //     $data['event_venue'].=", ";
            // }
            $data['zip_code']= $this->input->post('postal_code');
            //$data['event_venue'].=$data['zip_code'];
        }else{
            $data['zip_code']= '';
        }
        
        if(!empty($this->input->post('country'))){
            // if(!empty($this->input->post('postal_code')))
            // {
            //     $data['event_venue'].=", ";
            // }
            $data['country'] = $this->input->post('country');
            //$data['event_venue'].=$data['country'];
        }else{
            $data['country']= '';
        }
        
        // $data['event_venue'] = $this->input->post('event_venue');
        $data['event_description'] = $this->input->post('event_description');
        
        $organizers = [];
        if(!empty($this->input->post('organizers'))){
            $organizers = $this->input->post('organizers');
        }
        
        $sponsors = [];
        if(!empty($this->input->post('sponsors'))){
            $sponsors = $this->input->post('sponsors');
        }
        
        $speakers = [];
        if(!empty($this->input->post('speakers'))){
            $speakers = $this->input->post('speakers');
        }
        
        $hosts = [];
        if(!empty($this->input->post('hosts'))){
            $hosts = $this->input->post('hosts');
        }
        
        $users = [];
        if(!empty($this->input->post('users'))){
            $users = $this->input->post('users');
        }
        
        $exhibitors = [];
        if(!empty($this->input->post('is_exhibition'))){
            $data['is_exhibition']= "Y";
            if(!empty($this->input->post('exhibitors')))
            {
                $exhibitors = $this->input->post('exhibitors');
            }
        }else{
            $data['is_exhibition']= "N";
        }

        if(!empty($this->input->post('is_featured'))){
            $data['is_featured']= "Y";
            if(!empty($this->input->post('is_featured')))
            {
                $featured = $this->input->post('is_featured');
            }
        }else{
            if ($id == '') $data['is_featured']= "N";
        }
        
        $data['dress_code'] = $this->input->post('dress_code');


        if(!empty($this->input->post('area'))){
            $data['area'] = $this->input->post('area');
        }else{
            $data['area'] = '';
        }


        if (!empty($_FILES['dress_code_image']['name'])){
            $this->load->library('upload');
            $config['upload_path'] = './assets/upload/event/dress_code/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['file_name'] = time();

            $this->upload->initialize($config);
            if($_FILES['dress_code_image']['error']==0)
            {
                $profilepic=$_FILES['dress_code_image']['name'];
                $profilepic=explode('.',$profilepic);
                $ext=end($profilepic);
                if($ext=="jpg" or $ext=="jpeg" or $ext=="png" or $ext=="bmp")
                {
                    $profilepic=time().rand().".".$ext;
                    $destination="./assets/upload/event/dress_code/".$profilepic;
                    if (move_uploaded_file($_FILES['dress_code_image']['tmp_name'] ,$destination))
                    {
                        $data['dress_code_image']=$profilepic;
                        //$olddestination="./assets/upload/event/dress_code/".$oldfile;
                        //@unlink($olddestination);
                    }
                }
                else
                {
                    $this->session->set_flashdata('errormessage', 'Only .jpg,.jpeg,.bmp and .png image extensions are supported');
                    $redirect = site_url('admin/admin_event');
                    redirect($redirect);
                }
            }
        }
        
        $event_logo="";
        if (!empty($_FILES['event_logo']['name'])){
            $this->load->library('upload');
            $config['upload_path'] = './assets/upload/event/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['file_name'] = time();

            $this->upload->initialize($config);
            if($_FILES['event_logo']['error']==0)
            {
                $profilepic=$_FILES['event_logo']['name'];
                $profilepic=explode('.',$profilepic);
                $ext=end($profilepic);
                if($ext=="jpg" or $ext=="jpeg" or $ext=="png" or $ext=="bmp")
                {
                    $profilepic=time().rand().".".$ext;
                    $destination="./assets/upload/event/".$profilepic;
                    if (move_uploaded_file($_FILES['event_logo']['tmp_name'] ,$destination))
                    {
                        $data['event_logo']=$profilepic;
                        //$olddestination="./assets/upload/event/".$oldfile;
                        //@unlink($olddestination);
                    }
                }
                else
                {
                    $this->session->set_flashdata('errormessage', 'Only .jpg,.jpeg,.bmp and .png image extensions are supported');
                    $redirect = site_url('admin/admin_event');
                    redirect($redirect);
                }
            }
        }
        
        $date = date("Y-m-d H:i:s");
        if(!empty($organizers) && is_array($organizers)){
            $data['organizers'] = implode(',',$organizers);
        }else{
            $data['organizers']= @$organizers;
        }
        
        if(!empty($sponsors)){
            $data['sponsors'] = implode(',',$sponsors);
        }else{
            $data['sponsors']= '';
        }
        
        if(!empty($speakers)){
            $data['speakers'] = implode(',',$speakers);
        }else{
            $data['speakers']= '';
        }
        
        if(!empty($hosts)){
            $data['hosts'] = implode(',',$hosts);
        }else{
            $data['hosts']= '';
        }
        
        if(!empty($users)){
            $data['users'] = implode(',',$users);
        }else{
            $data['users']= '';
        }
        
        if(!empty($exhibitors)){
            $data['exhibitors'] = implode(',',$exhibitors);
        }else{
            $data['exhibitors']= '';
        }

        
        $data['attendees']= $data['sponsors'].','.$data['hosts'].','.$data['exhibitors'].','.$data['speakers'].','.$data['users'];
        $new_employees = array_filter(explode(',', $data['attendees']));  // $this->_debug($new_employees, false);
        

        if (!empty($id)){
            $event_attendees = $this->Eventmodel->get_row_data('event',array('id'=>$id));
            $old_employees_str = $event_attendees->sponsors . ',' . $event_attendees->hosts . ',' . $event_attendees->exhibitors . ',' . $event_attendees->speakers . ',' . $event_attendees->users;
            $old_employees = array_filter(explode(',', $old_employees_str));  // $this->_debug($old_employees, false);

            $omitted_emps = $this->ge_omit_employees($new_employees, $old_employees); //$this->_debug($omitted_emps, false);

            $new_attendees = explode(',', $data['attendees']);
            $old_attendees = explode(',', $event_attendees->attendees);

            $data['attendees'] = array_unique(array_merge($new_attendees,$old_attendees), SORT_REGULAR);
            if (count($omitted_emps)) $data['attendees'] = $this->get_new_attendees($data['attendees'], $omitted_emps);
            //$this->_debug($data['attendees']);
            //var_dump($data['attendees']); exit;
            
            $data['attendees'] = implode(',', $data['attendees']);

            $data['modified_date'] = $date;
            // echo "<pre>"; print_r($data); die;
            $this->db->where('id', $id)->update(tablename('event'), $data);

             // update join host/speaker/sponser and exhibitor for this event
                $where11['user_type'] =  'sponsors';
                $where11['event_id'] =  $id;
                $where12['user_type'] =  'hosts';
                $where12['event_id'] =  $id;
                $where13['user_type'] =  'exhibitors';
                $where13['event_id'] =  $id;
                $where14['user_type'] =  'speakers';
                $where14['event_id'] =  $id;

                $where15['user_type'] =  'admin_user';
                $where15['event_id'] =  $id;
                
               //echo  "<pre>"; print_r($where11); die;
                $request_delete1 = $this->Eventmodel->delete_data("event_access_request",$where11);
                $request_delete2 = $this->Eventmodel->delete_data("event_access_request",$where12);
                $request_delete3 = $this->Eventmodel->delete_data("event_access_request",$where13);
                $request_delete4 = $this->Eventmodel->delete_data("event_access_request",$where14);
                $request_delete5 = $this->Eventmodel->delete_data("event_access_request",$where15);

                if(!empty($sponsors)){
                    foreach ($sponsors as $valueap) {
                        $data11['event_id']=$id;
                        $data11['user_id']=$valueap;
                        $data11['status']='approved';
                        $data11['event_join']='1';
                        $data11['is_request']='1';                        
                        $data11['created_date']=date("Y-m-d H:i:s");
                        $data11['user_type']='sponsors';
                        $request_insert = $this->Eventmodel->insert_data("event_access_request",$data11);
                    }
                }

                if(!empty($hosts)){
                    foreach ($hosts as $value2) {
                        $data112['event_id']=$id;
                        $data112['user_id']=$value2;
                        $data112['status']='approved';
                        $data112['event_join']='1';
                        $data112['is_request']='1';                        
                        $data112['created_date']=date("Y-m-d H:i:s");
                        $data112['user_type']='hosts';
                        $request_insert2 = $this->Eventmodel->insert_data("event_access_request",$data112);
                    }
                }

                if(!empty($exhibitors)){
                    foreach ($exhibitors as $value3) {
                        $data113['event_id']=$id;
                        $data113['user_id']=$value3;
                        $data113['status']='approved';
                        $data113['event_join']='1';
                        $data113['is_request']='1';                        
                        $data113['created_date']=date("Y-m-d H:i:s");
                        $data113['user_type']='exhibitors';
                        $request_insert3 = $this->Eventmodel->insert_data("event_access_request",$data113);
                    }
                }

                if(!empty($speakers)){
                    foreach ($speakers as $value4) {
                        $data114['event_id']=$id;
                        $data114['user_id']=$value4;
                        $data114['status']='approved';
                        $data114['event_join']='1';
                        $data114['is_request']='1';                        
                        $data114['created_date']=date("Y-m-d H:i:s");
                        $data114['user_type']='speakers';
                        $request_insert4 = $this->Eventmodel->insert_data("event_access_request",$data114);
                    }
                }

               if(!empty($users)){
                    foreach ($users as $value5) {
                        $data115['event_id']=$id;
                        $data115['user_id']=$value5;
                        $data115['status']='approved';
                        $data115['event_join']='1';
                        $data115['is_request']='1';                        
                        $data115['created_date']=date("Y-m-d H:i:s");
                        $data115['user_type']='admin_user';
                        $request_insert5 = $this->Eventmodel->insert_data("event_access_request",$data115);
                    }
                }


            //die('*****************');


            return $id;
        } 
        else 
        {
            
            // Check for Duplicate
            $data['entry_date'] = $date;
            $this->db->insert(tablename('event'), $data);
            $last_id = $this->db->insert_id();

            if (!empty($last_id)) {

                // join host/speaker/sponser and exhibitor for this event                

                if(!empty($sponsors)){
                    foreach ($sponsors as $valueap) {
                        $data11['event_id']=$last_id;
                        $data11['user_id']=$valueap;
                        $data11['status']='approved';
                        $data11['event_join']='1';
                        $data11['is_request']='1';                        
                        $data11['created_date']=date("Y-m-d H:i:s");
                        $data11['user_type']='sponsors';
                        $request_insert = $this->Eventmodel->insert_data("event_access_request",$data11);
                    }
                }

                if(!empty($hosts)){
                    foreach ($hosts as $value2) {
                        $data112['event_id']=$last_id;
                        $data112['user_id']=$value2;
                        $data112['status']='approved';
                        $data112['event_join']='1';
                        $data112['is_request']='1';                        
                        $data112['created_date']=date("Y-m-d H:i:s");
                        $data112['user_type']='hosts';
                        $request_insert2 = $this->Eventmodel->insert_data("event_access_request",$data112);
                    }
                }

                if(!empty($exhibitors)){
                    foreach ($exhibitors as $value3) {
                        $data113['event_id']=$last_id;
                        $data113['user_id']=$value3;
                        $data113['status']='approved';
                        $data113['event_join']='1';
                        $data113['is_request']='1';                        
                        $data113['created_date']=date("Y-m-d H:i:s");
                        $data113['user_type']='exhibitors';
                        $request_insert3 = $this->Eventmodel->insert_data("event_access_request",$data113);
                    }
                }

                if(!empty($speakers)){
                    foreach ($speakers as $value4) {
                        $data114['event_id']=$last_id;
                        $data114['user_id']=$value4;
                        $data114['status']='approved';
                        $data114['event_join']='1';
                        $data114['is_request']='1';                        
                        $data114['created_date']=date("Y-m-d H:i:s");
                        $data114['user_type']='speakers';
                        $request_insert4 = $this->Eventmodel->insert_data("event_access_request",$data114);
                    }
                }

               if(!empty($users)){
                    foreach ($users as $value5) {
                        $data115['event_id']=$last_id;
                        $data115['user_id']=$value5;
                        $data115['status']='approved';
                        $data115['event_join']='1';
                        $data115['is_request']='1';                        
                        $data115['created_date']=date("Y-m-d H:i:s");
                        $data115['user_type']='admin_user';
                        $request_insert5 = $this->Eventmodel->insert_data("event_access_request",$data115);
                    }
                }


            //die('*****************');
                               
                return $last_id;
            } else {
                return "";
            }
        }
    }

    private function ge_omit_employees($new, $old){
        if (!is_array($new)){
            return $old;
        }else if (is_array($old)){
            $ans = [];
            foreach ($old as $one){
                if (!in_array($one, $new)) $ans[] = $one;
            }
            return array_filter($ans);
        }else{
            return [];
        }
    }

    private function get_new_attendees($new, $omit){ 
        //var_dump($omit); exit;
        $ans = [];
        foreach ($new as $one){ //var_dump($one); exit;
            if (!in_array($one, $omit) && !empty($one) && $one!="") { $ans[] = $one; }
        }
        return $ans;
    }

    /**
     * Used for change status functionality of Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current Event status
     * and change it the the opposite [Table: pb_event]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('event'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_event]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('event'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for rearrange functionality of Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_event]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function rearrange($id, $arrange) {
        $data = array('arrange' => $arrange);
        $this->db->where('id', $id);

        if ($this->db->update(tablename('event'), $data)) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

    public function insert_data($table,$data1)
    {
        $this->db->insert(tablename($table),$data1);
        //echo $this->db->last_query(); exit;
        return $this->db->insert_id();
    }

    public function delete_data($table,$where)
    {
        $this->db->delete(tablename($table),$where);
        return $this->db->affected_rows();
    }

    public function update_data($table,$where,$data)
    {
        $this->db->where($where);
        $this->db->update($table, $data); 
        //echo $this->db->last_query(); exit;
        return $this->db->affected_rows();
    } 

    public function search_event($tag) 
    {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->or_like('event_name', $tag);
        $this->db->or_like('event_venue', $tag);
        $this->db->or_like('event_description', $tag);
        $this->db->where("delete_flag","N");
        $this->db->order_by("entry_date", "desc");
        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) 
        {
            // echo $this->db->last_query();die;
            return $result;
        } 
        else 
        {
            return "";
        }
    }

    public function getchat($table,$where) 
    {
        $whr_event = empty($where['event_id']) || $where['event_id'] == 0 ? " " : " event_id='".$where['event_id']."' and ";
        $sql="select id,event_id,sender_id,receiver_id,message,created_date,DATE(created_date) as chat_date 
            from ".$table." 
            where ". $whr_event. " ((sender_id='".$where['sender_id']."' and receiver_id='".$where['receiver_id']."') or (sender_id='".$where['receiver_id']."' and receiver_id='".$where['sender_id']."')) order by created_date asc";
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $result = $query->result();
/*
        $chatArr = array();
        
         $i = 0;

          echo "<pre>";
        print_r($result);
        exit;

        foreach($result as $chat)
        {
            $chatArr[$chat->chat_date][$i]['id'] = $chat->id;
            // $chatArr[$chat->chat_date][$i]['event_id'] = $chat->event_id;
            // $chatArr[$chat->chat_date][$i]['receiver_id'] = $chat->receiver_id;
            // $chatArr[$chat->chat_date][$i]['message'] = $chat->message;
            // $chatArr[$chat->chat_date][$i]['flag'] = $chat->flag;
            // $chatArr[$chat->chat_date][$i]['created_date'] = $chat->created_date;
            $i++;
        }

        echo "<pre>";
        print_r($chatArr);
        exit;*/
        return $result;
    }



    public function getchatdates($table,$where) 
    {
        $whr_event = empty($where['event_id']) || $where['event_id'] == 0 ? " " : " event_id='".$where['event_id']."' and ";
        $sql="select DATE(created_date) as chat_date 
            from ".$table." 
            where ".$whr_event." ((sender_id='".$where['sender_id']."' and receiver_id='".$where['receiver_id']."') or (sender_id='".$where['receiver_id']."' and receiver_id='".$where['sender_id']."')) group by DATE(created_date) ";
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $result = $query->result();

       
        return $result;
    }

    public function chat_messages($id) 
    {
        $this->db->select('chat_head.*, event.event_name');
        $this->db->from('chat_head');
        $this->db->join('event', 'event.id=chat_head.event_id', 'left');
        $this->db->where('sender_id', $id);
        $this->db->or_where('receiver_id', $id);
        $this->db->order_by('created_date', 'DESC');
        $query = $this->db->get();
        
        return $query->result();
    }

    public function getcontact($table,$where) 
    {
        $sql="select * 
            from ".$table." 
            where ((user_id='".$where['user_id']."' and friend_id='".$where['friend_id']."') or (user_id='".$where['friend_id']."' and friend_id='".$where['user_id']."')) 
            order by created_date desc";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function contact_list($id) 
    {
        $sql="select * from ".tablename('contact')." where (user_id='".$id."' or friend_id='".$id."') and status='accepted' and contact_type='request' order by created_date desc";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function contact_list_order_by_name_1($id) 
    {
        $sql="select * from ".tablename('contact')." where (user_id='".$id."' or friend_id='".$id."') and contact_type='request' order by name asc";// and status='accepted'
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function contact_list_order_by_name($id) 
    {
       $sql="select * from ".tablename('contact')." where user_id='".$id."' and contact_type <> 'request' order by name asc";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function update_chat($table,$where) 
    {
        $whr_event = empty($where['event_id']) || $where['event_id'] == 0 ? " " : " event_id='".$where['event_id']."' and ";
        $sql="update ".$table." set flag='1' where ".$whr_event." ((sender_id='".$where['sender_id']."' and receiver_id='".$where['receiver_id']."')) ";// or (sender_id='".$where['receiver_id']."' and receiver_id='".$where['sender_id']."')
        $query = $this->db->query($sql);
        return $this->db->affected_rows();
    }

    public function chat_count($where) 
    {
        $sql="select count(id) as total_unread 
            from ".tablename('attendees_chat')." 
            where event_id='".$where['event_id']."' and ((sender_id='".$where['sender_id']."' and receiver_id='".$where['receiver_id']."')) and flag='0' ";// or (sender_id='".$where['receiver_id']."' and receiver_id='".$where['sender_id']."')
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result->total_unread;
    }

    public function my_agenda($where, $date) 
    {
        $this->db->select('agenda.*');
        $this->db->from(tablename('my_agenda'));
        $this->db->join(tablename('agenda'),'my_agenda.agenda_id=agenda.id');
        $this->db->where("DATE_FORMAT(`ets_agenda`.`start_date_time`, '%Y-%m-%d') = '".$date."'");
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result();
        // echo $this->db->last_query();die;
        if (!empty($result)) 
        {
            return $result;
        } 
        else 
        {
            return "";
        }
    }

    public function agenda_available_dates($where) 
    {
        $sql="select date_format(start_date_time,'%Y-%m-%d') as agenda_date from ".tablename('agenda')." where (event_id='".$where['event_id']."' and user_id=0) or (event_id='".$where['event_id']."' and user_id='".$where['user_id']."' ) group by date_format(start_date_time,'%Y-%m-%d') order by date_format(start_date_time,'%Y-%m-%d') ASC";
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $result = $query->result();
        // print_r($result);die;
        return $result;
    }


    public function agenda_available_dates_admin($where) 
    {
        $sql="select date_format(start_date_time,'%Y-%m-%d') as agenda_date from ".tablename('agenda')." where (event_id='".$where['event_id']."' and user_id=0) and is_active='Y' and delete_flag='N' group by date_format(start_date_time,'%Y-%m-%d') order by date_format(start_date_time,'%Y-%m-%d') ASC";
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $result = $query->result();
        // print_r($result);die;
        return $result;
    }

    public function agenda_available_dates_user($where) 
    {
        // $sql="select date_format(start_date_time,'%Y-%m-%d') as agenda_date from ".tablename('agenda')." where (event_id='".$where['event_id']."' and user_id='".$where['user_id']."' ) and is_active='Y' and delete_flag='N' group by date_format(start_date_time,'%Y-%m-%d') order by date_format(start_date_time,'%Y-%m-%d') ASC";
        $sql="select date_format(start_date_time,'%Y-%m-%d') as agenda_date from ".tablename('agenda')." where (event_id='".$where['event_id']."' ) and is_active='Y' and delete_flag='N' group by date_format(start_date_time,'%Y-%m-%d') order by date_format(start_date_time,'%Y-%m-%d') ASC";

        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $result = $query->result();
        // print_r($result);die;
        return $result;
    }

    public function get_detailed_result_data($table,$table1,$join_condition,$where = "1=1")
    {
        $this->db->join($table1,$join_condition);
        $query=$this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    public function checked_assigned_events($id,$field) 
    {
        $sql="SELECT group_concat(`event_name`) as events FROM ".tablename('event')." WHERE FIND_IN_SET(".$id.",".$field.") and event_end_date > '".date('Y-m-d')."' AND delete_flag='N' AND is_active='Y'";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result->events;
    }


    public function get_role_users() {
        
        $this->db->select("*"); 
        $this->db->from("role");
        $this->db->where("is_active","Y");
        $this->db->where("delete_flag","N");
        $this->db->order_by("id");
        $query = $this->db->get();
        $result =  $query->result();
        return $result;
    }

    public function get_role_users_by_id($id) {
        
        $this->db->select("*"); 
        $this->db->from("user");
        $this->db->where("role_id",$id);
        $this->db->where("is_active","Y");
        $this->db->where("delete_flag","N");
        $this->db->order_by("id");
        $query = $this->db->get();
        $result =  $query->result();
        return $result;
    }
    
    public function get_new_message_count($data){
        $this->db->select('id');
        $this->db->from("attendees_chat");
        $this->db->where('receiver_id', $data['user_id']);
        $this->db->where('flag', "0");
        $this->db->group_by(array("event_id", "sender_id"));
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function update_msg_status($data){
        $this->db->where($data);
        return $this->db->update('attendees_chat', ['flag'=> '1']);
    }
    
    public function get_city_name_by_id($id){
        if (empty($id) || $id == '') return "";
        $this->db->select('*');
        $this->db->from('cities');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            $row = $query->row(); return $row->name;
        }else{
            return "";
        }
    }
    
    public function get_country_name_by_id($id){
        if (empty($id) || $id == '') return "";
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            $row  = $query->row(); return $row->name;
        }else{
            return "";
        }
    }
    
    public function get_new_exchange_request_count($user_id){
        $this->db->select('id');
        $this->db->from('contact');
        $this->db->where('friend_id', $user_id);
        $this->db->where('status', 'pending');
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function get_user_app_downloaded($tot_attendees){
        $this->db->select('id');
        $this->db->from('user');
        $this->db->where_in('id', $tot_attendees);
        $this->db->where('fcm_reg_token!=', '');
        return $this->db->get()->num_rows();
    }
    
    public function check_admin_status($user_id, $event_id = ''){
        $user = $this->get_row_data('user', ['id'=> $user_id]);
        if ($user->role_id == '1') return true;
        if (empty($event_id)) return false;
        $event = $this->get_row_data('event', ['id'=> $event_id, 'delete_flag'=> 'N']);
        if (!$event) return FALSE;
        $organizers = explode(',', $event->organizers);
        $users = explode(',', $event->users);
        if (in_array($user_id, $organizers) || in_array($user_id, $users)) return TRUE;
        return FALSE;
        
    }
    
    public function getContactWithUserId($user_id){
        $where = "(user_id=$user_id OR friend_id=$user_id)";
        $list = $this->db->from('contact')->where($where)->where('status!=', 'pending')->get()->result();
        foreach ($list as $item){
            if ($item->contact_type == 'request'){
                $f_id = $item->user_id == $user_id ? $item->friend_id : $item->user_id;
                $user = $this->db->from('user')->where('id', $f_id)->get()->row();
                $item->name = $user->name;  
                $item->email = $user->emailid;    
                $item->phone = $user->phoneno;
                $item->skype_id = $user->skype_id;  
                $item->wechat_id = $user->wechat_id;
                $item->fax_no = $user->fax_no;
                $item->location = $user->location;
                $item->mobile = $user->mobile_no;
                $company = $this->db->from('company')->where('user_id', $f_id)->get()->row();
                
                $item->position     = $company ? $company->position : $user->position_title;  
                $item->company      = $company ? $company->company :$user->company;
                // $item->title        = '';  
            }
        }
        return $list;
    }


    function get_count_of_pending_attendees($event_id){
        $this->db->select('user_id');
        $this->db->from('event_access_request');
        $this->db->join('user', 'user_id=user.id');
        $this->db->where('event_id', $event_id);
        $this->db->where('event_join', '0');
        $this->db->where('user.role_id', 0);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_attendee_only($event_id){
        $this->db->select('user_id');
        $this->db->from('event_access_request');
        $this->db->join('user', 'user_id=user.id');
        $this->db->where('event_id', $event_id);
        $this->db->where('event_join', '1');
        $this->db->where('user.role_id', 0);
        $query = $this->db->get();
        return $query->num_rows();
    }

    
    private function _debug($data, $exit = true){
        echo '<pre>';
        print_r($data);
        echo '<br>';
        if ($exit) exit;
    }
    
}

/* End of file Eventmodel.php */
/* Location: ./application/modules/event/models/admin/Eventmodel.php */
