<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-calendar-o"></i>Event</a>
        </div>

        <h1>Event</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <?php //if($this->session->userdata('admin_role_type')==1){ ?><!-- <button class="btn btn-success btn-cls" type="button" onclick="add();">Add Event</button> --> <?php //} ?>

                        <?php if(($this->session->userdata('admin_role_type')==1) || in_array(49, $getRole)){ ?> <button class="btn btn-success btn-cls" type="button" onclick="add();">Add Event</button> <?php } ?>
                        
                        
                    </div>
                    <div class="widget-content nopadding">
                        <table id="myTable" class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Event Name</th>
                                    <th>Event Date</th>
                                    <th>Event Time</th>
                                    <th>Event Venue</th>
                                    <th>Organizers</th>
                                    <th>Sponsors</th>
                                    <th>Speakers</th>
                                    <th>Hosts</th>
                                    <th>Users</th>
                                    <th>Attendees</th>
                                    <th>Attendees Pending</th>
                                    <th>Exhibitors</th>
                                    <?php //if($this->session->userdata('admin_role_type')==1){ 
                                        if(in_array(51, $getRole)){ ?>
                                    <th>Status</th>
                                    <?php } if(in_array(52, $getRole)){ ?>
                                    <th>Action</th>
                                    <?php } ?>
                                </tr>
                            </thead>

                            <tbody class="sort">
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        $users = explode(',',$data->users);
                                        
                                        //echo '<pre>'; print_r($this->session->all_userdata());
                                        //if(($this->session->userdata('admin_role_type')==1) || (in_array($this->session->userdata('admin_uid'),$users))){
                                       if(($this->session->userdata('admin_role_type')==1) || (in_array($this->session->userdata('admin_uid'),$users)) || ($this->session->userdata('admin_role_type')==2) ){
                                        ?>
                                        <tr id="<?php echo $data->id; ?>" class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                               <?php //if($this->session->userdata('admin_role_type')==1){ 
                                                if(in_array(50, $getRole) || ($this->session->userdata('admin_role_type')==2)){?> <u><a href="<?php echo base_url('admin_update_event' . '/' . $data->id); ?>">
                                                    <?php echo $data->event_name; ?>
                                                </a></u> <?php }else{ echo $data->event_name; } ?>
                                            </td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->event_start_date)) ? date("M d,Y",strtotime($data->event_start_date))."-".date("M d,Y",strtotime($data->event_end_date)) : "NA"; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->event_start_time)) ? $data->event_start_time."-".$data->event_end_time : "NA"; ?></td>
                                            <td class="custom-action-btn"><?php echo (!empty($data->event_venue)) ? $data->event_venue : "NA"; ?></td>
                                            <td class="custom-action-btn">
                                                <a href="<?php echo base_url('admin_event_organizer' . '/' . $data->id);?>">
                                                    <?php echo empty($data->organizers) ? 0 : count(explode(',', $data->organizers)); ?>
                                                </a>
                                            </td>
                                            <td class="custom-action-btn">
                                                <a href="<?php echo base_url('admin_event_sponsor' . '/' . $data->id);?>">
                                                    <?php echo empty($data->sponsors) ? 0 : count(explode(',', $data->sponsors)); ?>
                                                </a>
                                            </td>
                                            <td class="custom-action-btn">
                                                <a href="<?php echo base_url('admin_event_speaker' . '/' . $data->id);?>">
                                                    <?php echo empty($data->speakers) ? 0 : count(explode(',', $data->speakers)); ?>
                                                </a>
                                            </td>
                                            <td class="custom-action-btn">
                                                <a href="<?php echo base_url('admin_event_host' . '/' . $data->id);?>">
                                                    <?php echo empty($data->hosts) ? 0 : count(explode(',', $data->hosts)); ?>
                                                </a>
                                            </td>
                                            <td class="custom-action-btn">
                                                <a href="<?php echo base_url('admin_event_user' . '/' . $data->id);?>">
                                                    <?php echo empty($data->users) ? 0 : count(explode(',', $data->users)); ?>
                                                </a>
                                            </td>
                                            <td class="custom-action-btn">
                                                <a href="<?php echo base_url('admin_view_attendees' . '/' . $data->id);?>">
                                                    <?php echo $data->attendees_count; ?>
                                                </a>
                                            </td>
                                            <td class="custom-action-btn">
                                                <a href="<?php echo base_url('admin_view_request' . '/' . $data->id);?>">
                                                    <?php echo $data->pending_count; ?>
                                                </a>
                                            </td>
                                            <td class="custom-action-btn">
                                                <a href="<?php echo base_url('admin_event_exhibitor' . '/' . $data->id);?>">
                                                    <?php echo empty($data->exhibitors) ? 0 : count(explode(',', $data->exhibitors)); ?>
                                                </a>
                                            </td>
                                            <?php //if($this->session->userdata('admin_role_type')==1){
                                            if(in_array(51, $getRole)){ ?>
                                            <td class="custom-action-btn">
                                                <?php
                                                if ($data->is_active == "Y") {
                                                    echo '<i title="De-activate" class="fa fa-close" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                } else {
                                                    echo '<i title="Activate" class="fa fa-check" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                }
                                                ?>
                                            </td>
                                            <?php } if(in_array(52, $getRole)){ ?>
                                            <td class="custom-action-btn">
                                                <!-- <i title="Reset Password" class="fa fa-repeat" style="margin:0 3px 2px 0;" aria-hidden="false" onclick="reset_passwd(<?php echo $data->id; ?>);"></i> -->
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add Event Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_event'); ?>";
    }

    /**
     * Status Change Function
     *
     * @param   rec_id
     * @return  redirects and gives control to status handler
     */
    function change_status(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_status_event'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to change status of this event?");
        }).modal("show");
    }

    /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_delete_event'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this Event?");
        }).modal("show");
    }

    /**
     * Reset Password for an Event Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function reset_passwd(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_reset_passwd'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to reset password for this event? The event will be notified via email.");
        }).modal("show");
    }

    $('.sort').sortable({
        cursor: 'move',
        axis:   'y',
        update: function(e, ui) {
            href = '<?php echo base_url('admin_event_rearrange'); ?>';
            $(this).sortable("refresh");
            sorted = $(this).sortable('toArray', {attribute: "id"});
            console.log(sorted);
            $.post(href, {arrange: sorted}).done(function(result){        
                console.log(result);
            }).fail(function(xhr, status, error){
                console.log(xhr);
                console.log(status);
                console.log(error);
            });
            // $.ajax({
            //     type:   'POST',
            //     url:    href,
            //     data:   {'name': sorted},
            //     dataType: 'json',
            //     contentType: 'application/json',
            //     success: function(msg) {
            //         //do something with the sorted data
            //     }
            // });
        }
    });

</script>
