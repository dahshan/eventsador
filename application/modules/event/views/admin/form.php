<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_event'); ?>"><i class="fa fa-calendar-o"></i>Event</a>
            <a href="javascript:void(0);" class="current"></i>Event Management</a>
        </div>

        <h1>Event</h1>
    </div>
    <?php //echo "<pre>"; print_r($data_single); die; ?>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Event</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_event') : base_url('admin_update_event') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Event Name</label>
                                <div class="controls">
                                    <input class="span11" <?php if($this->session->userdata('admin_role_type')!=1){ echo 'readonly'; }?> placeholder="Event Name" type="text" id="event_name" name="event_name" value="<?php echo (!empty(set_value('event_name'))) ? set_value('event_name') : ((!empty($data_single->event_name)) ? $data_single->event_name : ''); ?>" >
                                    <?php echo form_error('event_name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="span5">
                                    <label class="control-label">Event Start Date</label>
                                    <div class="controls">
                                        <input <?php if($this->session->userdata('admin_role_type')!=1){ echo 'readonly'; }?> onkeypress="return false" class="span11  <?php if($this->session->userdata('admin_role_type')==1){ echo 'datepicker1'; }?> " placeholder="Event Start Date" type="text" id="event_start_date" name="event_start_date" value="<?php echo (!empty(set_value('event_start_date'))) ? set_value('event_start_date') : ((!empty($data_single->event_start_date)) ? date('d/m/Y',strtotime($data_single->event_start_date)) : ''); ?>" >
                                        <?php echo form_error('event_start_date', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">Event End Date</label>
                                    <div class="controls">
                                        <input <?php if($this->session->userdata('admin_role_type')!=1){ echo 'readonly'; }?> onkeypress="return false" class="span11 <?php if($this->session->userdata('admin_role_type')==1){ echo 'datepicker2'; }?>" placeholder="Event End Date" type="text" id="event_end_date" name="event_end_date" value="<?php echo (!empty(set_value('event_end_date'))) ? set_value('event_end_date') : ((!empty($data_single->event_end_date)) ? date('d/m/Y',strtotime($data_single->event_end_date)) : ''); ?>" >
                                        <?php echo form_error('event_end_date', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="span5">
                                    <label class="control-label">Event Start Time</label>
                                    <div class="controls">
                                        <input <?php if($this->session->userdata('admin_role_type')!=1){ echo 'readonly'; }?> onkeypress="return false" class="span11 <?php if($this->session->userdata('admin_role_type')==1){ echo 'timepicker1'; }?> " placeholder="Event Start Time" type="text" id="event_start_time" name="event_start_time" value="<?php echo (!empty(set_value('event_start_time'))) ? set_value('event_start_time') : ((!empty($data_single->event_start_time)) ? $data_single->event_start_time : ''); ?>" >
                                        <?php echo form_error('event_start_time', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">Event End Time</label>
                                    <div class="controls">
                                        <input <?php if($this->session->userdata('admin_role_type')!=1){ echo 'readonly'; }?> onkeypress="return false" class="span11 <?php if($this->session->userdata('admin_role_type')==1){ echo 'timepicker2'; }?>" placeholder="Event End Time" type="text" id="event_end_time" name="event_end_time" value="<?php echo (!empty(set_value('event_end_time'))) ? set_value('event_end_time') : ((!empty($data_single->event_end_time)) ? $data_single->event_end_time : ''); ?>" >
                                        <?php echo form_error('event_end_time', '<div style="color:red;">', '</div>'); ?>

                                        <span class="tim2_err" style="color:red;"></span>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="control-group">
                                <label class="control-label">Event Venue</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Event Venue" type="text" id="event_venue" name="event_venue" value="<?php echo (!empty(set_value('event_venue'))) ? set_value('event_venue') : ((!empty($data_single->event_venue)) ? $data_single->event_venue : ''); ?>" >
                                    <?php echo form_error('event_venue', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div> -->
                            <div class="control-group">
                              <label class="control-label">Event Venue( Search for your Address )</label>
                              <div class="controls">
                                <input type="text" <?php if($this->session->userdata('admin_role_type')!=1){ echo 'readonly'; }?> class="span11" name="autocomplete" id="autocomplete" placeholder="Select Address From Dropdown Hints" value="<?php if(!empty($data_single)){
                                  // if(!empty($result->street_no)) 
                                  // echo $result->street_no.",";
                                  // if(!empty($result->street_name)) 
                                  // echo $result->street_name.",";
                                  // if(!empty($result->city)) 
                                  // echo $result->city." - ";
                                  // if(!empty($result->zip_code)) 
                                  // echo $result->zip_code.",";
                                  // if(!empty($result->state)) 
                                  // echo $result->state.",";
                                  // if(!empty($result->country)) 
                                  echo $data_single->event_venue ; }?> " />
                              </div>
                              <?php echo form_error('autocomplete', '<div style="color:red;">', '</div>'); ?>
                            </div>
                            
                            <!-- <div class="control-group map-section-1">  -->
                            <div class="control-group">
                              <div class="span6">
                                <!-- <b style="color:#ff0000;margin-top: 12px;margin-left: 20px;">*You Have to Search Event Venue Address . Don't Put it Manually.</b> -->
                                <div id="map_canvas" style="height:330px;max-width:950px !important;margin-top: 12px;margin-left: 20px;margin-bottom: 12px;"></div>
                              </div>
                              <div class="span6" style="margin-top: 30px;">
                                <!-- <div class="control-group">
                                  <div class="control-group">
                                    <div class="span5">
                                    <input type="text" name="street_number" placeholder="Street Number" class="span11" id="street_number" <?php if(!empty($data_single)) { ?> value="<?php echo $data_single->street_no;?>" <?php } ?>>
                                    </div>
                                    <div class="span6">
                                    <input type="text" name="route" placeholder="Street Name" class="span11" id="route" <?php if(!empty($data_single)) { ?> value="<?php echo $data_single->street_name;?>" <?php } ?>>
                                    </div>
                                  </div>
                                </div> -->


                                <!-- <div class="control-group"> 
                                  <label class="control-label" style=""></label>
                                  <input type="text" name="area" placeholder="Area" class="span11" id="sublocality_level_1" <?php if(!empty($data_single)) { ?> value="<?php echo $data_single->area;?>" <?php } ?>>
                                </div>  -->




                                 <div class="control-group"> 
                                  <label class="control-label" style=""></label>
                                  <input <?php if($this->session->userdata('admin_role_type')!=1){ echo 'readonly'; }?> type="text" name="locality" placeholder="City" class="span11" id="locality" <?php if(!empty($data_single)) { ?> value="<?php echo $data_single->city;?>" <?php } ?>>
                                </div> 
                                <!-- <div class="control-group">
                                  <label class="control-label" style=""></label>
                                    <input type="text" name="administrative_area_level_1" placeholder="State" class="span11" id="administrative_area_level_1" <?php if(!empty($data_single)) { ?> value="<?php echo $data_single->state;?>" <?php } ?>>
                                </div> -->
                                <!-- <div class="control-group">
                                  <label class="control-label" style=""></label>
                                    <input type="text" name="postal_code" placeholder="Zip Code" class="span11" id="postal_code" <?php if(!empty($data_single)) { ?> value="<?php echo $data_single->zip_code;?>" <?php } ?>>
                                </div> -->
                                <div class="control-group">
                                  <label class="control-label" style=""></label>
                                    <input <?php if($this->session->userdata('admin_role_type')!=1){ echo 'readonly'; }?> type="text" name="country" placeholder="Country" class="span11" id="country" <?php if(!empty($data_single)) { ?> value="<?php echo $data_single->country;?>" <?php } ?>>
                                </div>
                              </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Event Description</label>
                                <div class="controls">
                                    <textarea id="event_desc" maxlength="1000" <?php if($this->session->userdata('admin_role_type')!=1){ echo 'readonly'; }?> class="span11" id="event_description" name="event_description"><?php echo (!empty(set_value('event_description'))) ? set_value('event_description') : ((!empty($data_single->event_description)) ? $data_single->event_description : ''); ?></textarea>
                                    <p id="event_desc_count"></p>
                                    <?php echo form_error('event_description', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                            <div class="span6">
                                <label class="control-label">Dress Code</label>
                                <div class="controls">
                                    <textarea id="event_dress_code" maxlength="500" class="span11" id="dress_code" rows="9" name="dress_code"><?php echo (!empty(set_value('dress_code'))) ? set_value('dress_code') : ((!empty($data_single->dress_code)) ? $data_single->dress_code : ''); ?></textarea>
                                    <p id="event_dress_code_count"></p>
                                    <?php echo form_error('dress_code', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="span6">
                                <label class="control-label">Dress Code Image</label>
                                <div class="controls">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                      <img  src="<?php if(!empty($data_single->dress_code_image)){ echo base_url('assets/upload/event/dress_code')."/".$data_single->dress_code_image; } else {echo base_url('assets/upload/event/dress_code/dress_code.jpeg');}?>" data-src="<?php if(!empty($data_single->dress_code_image)){ echo base_url('assets/upload/event/dress_code')."/".$data_single->dress_code_image; } else {echo base_url('assets/upload/event/dress_code/dress_code.jpeg');}?>">
                                    </div>
                                    <div>
                                      <div class="controls custon_textarea" style="margin-left: 0;">
                                        <input type="file" name="dress_code_image" style="line-height:1px;">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            </div>
                            <?php if($this->session->userdata('admin_role_type')==2){ ?>
                            <input type="hidden" name="organizers" value="<?php echo ((!empty($data_single->organizers)) ? $data_single->organizers : ''); ?>">

                             <?php } ?>
                            <?php if($this->session->userdata('admin_role_type')!=2){ ?>
                            <div class="control-group">
                                <label class="control-label">Select Organizer</label>
                                <div class="controls">
                                    <select name="organizers[]" >
                                        <?php
                                        if (count($all_organizer) > 0) {
                                            if (isset($data_single->organizers) && $data_single->organizers != '') {
                                                $organizers_selected = explode(',',$data_single->organizers);
                                            } else {
                                                $organizers_selected = [];
                                            }
                                            foreach ($all_organizer AS $all_organizer_val) {
                                                $organizer_event=assigned_events($all_organizer_val->id,'organizers');
                                                // if(!empty($organizer_event))
                                                // {
                                                //     $organizer_event=" << ".$organizer_event." >>";
                                                // }
                                                ?>
                                                <option value="<?php echo (isset($all_organizer_val->id) && $all_organizer_val->id != '') ? $all_organizer_val->id : ''; ?>"
                                                        <?php echo in_array($all_organizer_val->id, $organizers_selected)?'selected':''; ?>>
                                                            <?php echo (isset($all_organizer_val->name) && $all_organizer_val->name != '') ? $all_organizer_val->name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('organizers', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="control-group">
                                <label class="control-label">Select Sponsor</label>
                                <div class="controls">
                                    <select name="sponsors[]" multiple >
                                        <?php
                                        if (count($all_sponsor) > 0) {
                                            if (isset($data_single->sponsors) && $data_single->sponsors != '') {
                                                $sponsors_selected = explode(',',$data_single->sponsors);
                                            } else {
                                                $sponsors_selected = [];
                                            }
                                            foreach ($all_sponsor AS $sponsor) {
                                                $sponsor_event=assigned_events($sponsor->id,'sponsors');
                                                if(!empty($sponsor_event))
                                                // {
                                                //     $sponsor_event=" << ".$sponsor_event." >>";
                                                // }
                                                ?>
                                                <option value="<?php echo (isset($sponsor->id) && $sponsor->id != '') ? $sponsor->id : ''; ?>"
                                                        <?php echo in_array($sponsor->id, $sponsors_selected)?'selected':''; ?>>
                                                            <?php echo (isset($sponsor->name) && $sponsor->name != '') ? $sponsor->name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('sponsors', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Select Speaker</label>
                                <div class="controls">
                                    <select name="speakers[]" multiple >
                                        <?php
                                        if (count($all_speaker) > 0) {
                                            if (isset($data_single->speakers) && $data_single->speakers != '') {
                                                $speakers_selected = explode(',',$data_single->speakers);
                                            } else {
                                                $speakers_selected = [];
                                            }
                                            foreach ($all_speaker AS $speaker) {
                                                $speaker_event=assigned_events($speaker->id,'speakers');
                                                // if(!empty($speaker_event))
                                                // {
                                                //     $speaker_event=" << ".$speaker_event." >>";
                                                // }
                                                ?>
                                                <option value="<?php echo (isset($speaker->id) && $speaker->id != '') ? $speaker->id : ''; ?>"
                                                        <?php echo in_array($speaker->id, $speakers_selected)?'selected':''; ?>>
                                                            <?php echo (isset($speaker->name) && $speaker->name != '') ? $speaker->name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('speakers', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Select Host</label>
                                <div class="controls">
                                    <select name="hosts[]" multiple >
                                        <?php
                                        if (count($all_host) > 0) {
                                            if (isset($data_single->hosts) && $data_single->hosts != '') {
                                                $hosts_selected = explode(',',$data_single->hosts);
                                            } else {
                                                $hosts_selected = [];
                                            }
                                            foreach ($all_host AS $host) {
                                                $host_event=assigned_events($host->id,'hosts');
                                                // if(!empty($host_event))
                                                // {
                                                //     $host_event=" << ".$host_event." >>";
                                                // }
                                                ?>
                                                <option value="<?php echo (isset($host->id) && $host->id != '') ? $host->id : ''; ?>"
                                                        <?php echo in_array($host->id, $hosts_selected)?'selected':''; ?>>
                                                            <?php echo (isset($host->name) && $host->name != '') ? $host->name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('hosts', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <?php if(!empty($all_role)){
                                    foreach ($all_role as $role) {
                                 ?>
                                <!--***users***-->
                            <div class="control-group">
                                <label class="control-label">Select <?php echo $role->name; ?></label>
                                <div class="controls">
                                <?php $get_role_user = $this->Eventmodel->get_role_users_by_id($role->id); 
                                //echo "<pre>"; print_r($get_role_user); die;
                                        ?>
                                    <select name="users[]" multiple >
                                        <?php      
                                        if (count($get_role_user) > 0) {
                                            if (isset($data_single->users) && $data_single->users != '') {
                                                $users_selected = explode(',',$data_single->users);
                                            } else {
                                                $users_selected = [];
                                            }
                                            foreach ($get_role_user AS $user) {
                                                $user_event=assigned_events($user->id,'users');
                                                // if(!empty($user_event))
                                                // {
                                                //     $user_event=" << ".$user_event." >>";
                                                // }
                                                ?>
                                                 <option value="<?php echo (isset($user->id) && $user->id != '') ? $user->id : ''; ?>"
                                                    <?php echo in_array($user->id, $users_selected)?'selected':''; ?>>
                                                            <?php echo (isset($user->name) && $user->name != '') ? $user->name : ''; ?> 
                                                </option> 
                                                <?php
                                             }
                                         }
                                        ?>
                                    </select>
                                    <?php echo form_error('users', '<div style="color:red;">', '</div>'); ?>
                                    <!--***users***-->
                                </div>
                            </div>

                             <?php } } ?>

                            <!-- <div class="control-group">
                                <label class="control-label">Select User</label>
                                <div class="controls">
                                    <select name="users[]" multiple >
                                        <?php
                                        // if (count($all_user) > 0) {
                                        //     if (isset($data_single->users) && $data_single->users != '') {
                                        //         $users_selected = explode(',',$data_single->users);
                                        //     } else {
                                        //         $users_selected = [];
                                        //     }
                                        //     foreach ($all_user AS $user) {
                                        //         $user_event=assigned_events($user->id,'users');
                                        //         if(!empty($user_event))
                                        //         {
                                        //             $user_event=" << ".$user_event." >>";
                                        //         }
                                        //         ?>
                                        //         <option value="<?php echo (isset($user->id) && $user->id != '') ? $user->id : ''; ?>"
                                        //                 <?php echo in_array($user->id, $users_selected)?'selected':''; ?>>
                                        //                     <?php echo (isset($user->name) && $user->name != '') ? $user->name.$user_event : ''; ?> 
                                        //         </option>
                                        //         <?php
                                        //     }
                                        // }
                                        ?>
                                    </select>
                                    <?php //echo form_error('users', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div> -->

                            <div class="control-group">
                                <label class="control-label">Is Exhibition</label>
                                <div class="controls">
                                    <input type="checkbox" <?php if(!empty($data_single->is_exhibition) && $data_single->is_exhibition == 'Y'){ echo "checked"; }?> value="1" name="is_exhibition" id="is_exhibition">
                                </div>
                            </div>
                            <div class="control-group" <?php if(empty($data_single->exhibitors)){ ?> style="display: none" <?php } ?> id="ex_tab">
                                <label class="control-label">Select Exhibitor</label>
                                <div class="controls">
                                    <select name="exhibitors[]" multiple >
                                        <?php
                                        if (count($all_exhibitor) > 0) {
                                            if (isset($data_single->exhibitors) && $data_single->exhibitors != '') {
                                                $exhibitors_selected = explode(',',$data_single->exhibitors);
                                            } else {
                                                $exhibitors_selected = [];
                                            }
                                            foreach ($all_exhibitor AS $exhibitor) {
                                                $exhibitor_event=assigned_events($exhibitor->id,'exhibitors');
                                                // if(!empty($exhibitor_event))
                                                // {
                                                //     $exhibitor_event=" << ".$exhibitor_event." >>";
                                                // }
                                                ?>
                                                <option value="<?php echo (isset($exhibitor->id) && $exhibitor->id != '') ? $exhibitor->id : ''; ?>"
                                                        <?php echo in_array($exhibitor->id, $exhibitors_selected)?'selected':''; ?>>
                                                            <?php echo (isset($exhibitor->name) && $exhibitor->name != '') ? $exhibitor->name: ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('exhibitors', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            
                            <?php if ($this->session->userdata('admin_role_type') == '1') :?>
                            <div class="control-group">
                               <label class="control-label">Is Featured Logo</label>
                                <div class="controls">
                                    <input type="checkbox" <?php if(!empty($data_single->is_featured) && $data_single->is_featured == 'Y'){ echo "checked"; }?> value="Y" name="is_featured" id="is_featured">
                                </div>
                            </div>
                            <?php endif; ?>
                          
                            <div class="control-group">
                                <label class="control-label">Logo</label>
                               
                                <div class="controls">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                      <img  src="<?php if(!empty($data_single->event_logo)){ echo base_url('assets/upload/event')."/".$data_single->event_logo; } else {echo base_url('assets/upload/event-default-logo.png');}?>" data-src="<?php if(!empty($data_single->event_logo)){ echo base_url('assets/upload/event')."/".$data_single->event_logo; } else {echo base_url('assets/upload/event-default-logo.png');}?>" alt="...">
                                    </div>
                                    <div>
                                      <div class="controls custon_textarea" style="margin-left: 0;">
                                        <input type="file" name="event_logo" style="line-height:1px;">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <input type="hidden" name="event_id" value="<?php echo $id; ?>">
                            <input type="hidden" name="latlong" id="latlong" <?php if(!empty($data_single)) { ?> value="<?php echo floatval($data_single->lat).",".floatval($data_single->lng);?>" <?php } ?> />
                            <input type="hidden" name="eventid" id="eventid" value="<?php if(!empty($data_single->id)) echo $data_single->id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    /*.glyphicon{position:relative;top:1px;display:inline-block;font-family:'Glyphicons Halflings';font-style:normal;font-weight:400;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.glyphicon-asterisk:before{content:"\2a"}.glyphicon-plus:before{content:"\2b"}.glyphicon-euro:before,.glyphicon-eur:before{content:"\20ac"}.glyphicon-minus:before{content:"\2212"}.glyphicon-cloud:before{content:"\2601"}.glyphicon-envelope:before{content:"\2709"}.glyphicon-pencil:before{content:"\270f"}.glyphicon-glass:before{content:"\e001"}.glyphicon-music:before{content:"\e002"}.glyphicon-search:before{content:"\e003"}.glyphicon-heart:before{content:"\e005"}.glyphicon-star:before{content:"\e006"}.glyphicon-star-empty:before{content:"\e007"}.glyphicon-user:before{content:"\e008"}.glyphicon-film:before{content:"\e009"}.glyphicon-th-large:before{content:"\e010"}.glyphicon-th:before{content:"\e011"}.glyphicon-th-list:before{content:"\e012"}.glyphicon-ok:before{content:"\e013"}.glyphicon-remove:before{content:"\e014"}.glyphicon-zoom-in:before{content:"\e015"}.glyphicon-zoom-out:before{content:"\e016"}.glyphicon-off:before{content:"\e017"}.glyphicon-signal:before{content:"\e018"}.glyphicon-cog:before{content:"\e019"}.glyphicon-trash:before{content:"\e020"}.glyphicon-home:before{content:"\e021"}.glyphicon-file:before{content:"\e022"}.glyphicon-time:before{content:"\e023"}.glyphicon-road:before{content:"\e024"}.glyphicon-download-alt:before{content:"\e025"}.glyphicon-download:before{content:"\e026"}.glyphicon-upload:before{content:"\e027"}.glyphicon-inbox:before{content:"\e028"}.glyphicon-play-circle:before{content:"\e029"}.glyphicon-repeat:before{content:"\e030"}.glyphicon-refresh:before{content:"\e031"}.glyphicon-list-alt:before{content:"\e032"}.glyphicon-lock:before{content:"\e033"}.glyphicon-flag:before{content:"\e034"}.glyphicon-headphones:before{content:"\e035"}.glyphicon-volume-off:before{content:"\e036"}.glyphicon-volume-down:before{content:"\e037"}.glyphicon-volume-up:before{content:"\e038"}.glyphicon-qrcode:before{content:"\e039"}.glyphicon-barcode:before{content:"\e040"}.glyphicon-tag:before{content:"\e041"}.glyphicon-tags:before{content:"\e042"}.glyphicon-book:before{content:"\e043"}.glyphicon-bookmark:before{content:"\e044"}.glyphicon-print:before{content:"\e045"}.glyphicon-camera:before{content:"\e046"}.glyphicon-font:before{content:"\e047"}.glyphicon-bold:before{content:"\e048"}.glyphicon-italic:before{content:"\e049"}.glyphicon-text-height:before{content:"\e050"}.glyphicon-text-width:before{content:"\e051"}.glyphicon-align-left:before{content:"\e052"}.glyphicon-align-center:before{content:"\e053"}.glyphicon-align-right:before{content:"\e054"}.glyphicon-align-justify:before{content:"\e055"}.glyphicon-list:before{content:"\e056"}.glyphicon-indent-left:before{content:"\e057"}.glyphicon-indent-right:before{content:"\e058"}.glyphicon-facetime-video:before{content:"\e059"}.glyphicon-picture:before{content:"\e060"}.glyphicon-map-marker:before{content:"\e062"}.glyphicon-adjust:before{content:"\e063"}.glyphicon-tint:before{content:"\e064"}.glyphicon-edit:before{content:"\e065"}.glyphicon-share:before{content:"\e066"}.glyphicon-check:before{content:"\e067"}.glyphicon-move:before{content:"\e068"}.glyphicon-step-backward:before{content:"\e069"}.glyphicon-fast-backward:before{content:"\e070"}.glyphicon-backward:before{content:"\e071"}.glyphicon-play:before{content:"\e072"}.glyphicon-pause:before{content:"\e073"}.glyphicon-stop:before{content:"\e074"}.glyphicon-forward:before{content:"\e075"}.glyphicon-fast-forward:before{content:"\e076"}.glyphicon-step-forward:before{content:"\e077"}.glyphicon-eject:before{content:"\e078"}.glyphicon-chevron-left:before{content:"\e079"}.glyphicon-chevron-right:before{content:"\e080"}.glyphicon-plus-sign:before{content:"\e081"}.glyphicon-minus-sign:before{content:"\e082"}.glyphicon-remove-sign:before{content:"\e083"}.glyphicon-ok-sign:before{content:"\e084"}.glyphicon-question-sign:before{content:"\e085"}.glyphicon-info-sign:before{content:"\e086"}.glyphicon-screenshot:before{content:"\e087"}.glyphicon-remove-circle:before{content:"\e088"}.glyphicon-ok-circle:before{content:"\e089"}.glyphicon-ban-circle:before{content:"\e090"}.glyphicon-arrow-left:before{content:"\e091"}.glyphicon-arrow-right:before{content:"\e092"}.glyphicon-arrow-up:before{content:"\e093"}.glyphicon-arrow-down:before{content:"\e094"}.glyphicon-share-alt:before{content:"\e095"}.glyphicon-resize-full:before{content:"\e096"}.glyphicon-resize-small:before{content:"\e097"}.glyphicon-exclamation-sign:before{content:"\e101"}.glyphicon-gift:before{content:"\e102"}.glyphicon-leaf:before{content:"\e103"}.glyphicon-fire:before{content:"\e104"}.glyphicon-eye-open:before{content:"\e105"}.glyphicon-eye-close:before{content:"\e106"}.glyphicon-warning-sign:before{content:"\e107"}.glyphicon-plane:before{content:"\e108"}.glyphicon-calendar:before{content:"\e109"}.glyphicon-random:before{content:"\e110"}.glyphicon-comment:before{content:"\e111"}.glyphicon-magnet:before{content:"\e112"}.glyphicon-chevron-up:before{content:"\e113"}.glyphicon-chevron-down:before{content:"\e114"}.glyphicon-retweet:before{content:"\e115"}.glyphicon-shopping-cart:before{content:"\e116"}.glyphicon-folder-close:before{content:"\e117"}.glyphicon-folder-open:before{content:"\e118"}.glyphicon-resize-vertical:before{content:"\e119"}.glyphicon-resize-horizontal:before{content:"\e120"}.glyphicon-hdd:before{content:"\e121"}.glyphicon-bullhorn:before{content:"\e122"}.glyphicon-bell:before{content:"\e123"}.glyphicon-certificate:before{content:"\e124"}.glyphicon-thumbs-up:before{content:"\e125"}.glyphicon-thumbs-down:before{content:"\e126"}.glyphicon-hand-right:before{content:"\e127"}.glyphicon-hand-left:before{content:"\e128"}.glyphicon-hand-up:before{content:"\e129"}.glyphicon-hand-down:before{content:"\e130"}.glyphicon-circle-arrow-right:before{content:"\e131"}.glyphicon-circle-arrow-left:before{content:"\e132"}.glyphicon-circle-arrow-up:before{content:"\e133"}.glyphicon-circle-arrow-down:before{content:"\e134"}.glyphicon-globe:before{content:"\e135"}.glyphicon-wrench:before{content:"\e136"}.glyphicon-tasks:before{content:"\e137"}.glyphicon-filter:before{content:"\e138"}.glyphicon-briefcase:before{content:"\e139"}.glyphicon-fullscreen:before{content:"\e140"}.glyphicon-dashboard:before{content:"\e141"}.glyphicon-paperclip:before{content:"\e142"}.glyphicon-heart-empty:before{content:"\e143"}.glyphicon-link:before{content:"\e144"}.glyphicon-phone:before{content:"\e145"}.glyphicon-pushpin:before{content:"\e146"}.glyphicon-usd:before{content:"\e148"}.glyphicon-gbp:before{content:"\e149"}.glyphicon-sort:before{content:"\e150"}.glyphicon-sort-by-alphabet:before{content:"\e151"}.glyphicon-sort-by-alphabet-alt:before{content:"\e152"}.glyphicon-sort-by-order:before{content:"\e153"}.glyphicon-sort-by-order-alt:before{content:"\e154"}.glyphicon-sort-by-attributes:before{content:"\e155"}.glyphicon-sort-by-attributes-alt:before{content:"\e156"}.glyphicon-unchecked:before{content:"\e157"}.glyphicon-expand:before{content:"\e158"}.glyphicon-collapse-down:before{content:"\e159"}.glyphicon-collapse-up:before{content:"\e160"}.glyphicon-log-in:before{content:"\e161"}.glyphicon-flash:before{content:"\e162"}.glyphicon-log-out:before{content:"\e163"}.glyphicon-new-window:before{content:"\e164"}.glyphicon-record:before{content:"\e165"}.glyphicon-save:before{content:"\e166"}.glyphicon-open:before{content:"\e167"}.glyphicon-saved:before{content:"\e168"}.glyphicon-import:before{content:"\e169"}.glyphicon-export:before{content:"\e170"}.glyphicon-send:before{content:"\e171"}.glyphicon-floppy-disk:before{content:"\e172"}.glyphicon-floppy-saved:before{content:"\e173"}.glyphicon-floppy-remove:before{content:"\e174"}.glyphicon-floppy-save:before{content:"\e175"}.glyphicon-floppy-open:before{content:"\e176"}.glyphicon-credit-card:before{content:"\e177"}.glyphicon-transfer:before{content:"\e178"}.glyphicon-cutlery:before{content:"\e179"}.glyphicon-header:before{content:"\e180"}.glyphicon-compressed:before{content:"\e181"}.glyphicon-earphone:before{content:"\e182"}.glyphicon-phone-alt:before{content:"\e183"}.glyphicon-tower:before{content:"\e184"}.glyphicon-stats:before{content:"\e185"}.glyphicon-sd-video:before{content:"\e186"}.glyphicon-hd-video:before{content:"\e187"}.glyphicon-subtitles:before{content:"\e188"}.glyphicon-sound-stereo:before{content:"\e189"}.glyphicon-sound-dolby:before{content:"\e190"}.glyphicon-sound-5-1:before{content:"\e191"}.glyphicon-sound-6-1:before{content:"\e192"}.glyphicon-sound-7-1:before{content:"\e193"}.glyphicon-copyright-mark:before{content:"\e194"}.glyphicon-registration-mark:before{content:"\e195"}.glyphicon-cloud-download:before{content:"\e197"}.glyphicon-cloud-upload:before{content:"\e198"}.glyphicon-tree-conifer:before{content:"\e199"}*/
</style>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script>

<?php 
        if(!empty($data_single->event_start_date)){
                $stt = date('d/m/Y',strtotime($data_single->event_start_date));
            }else{
                $stt = 'today';
            } 

        ?>
        var stt = '<?php echo $stt; ?>';
 $(".datepicker1").datepicker({
            format: 'dd/mm/yyyy',
            // startDate: stt,
            todayBtn:  1,
            autoclose: true,
        }).on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('.datepicker2').val('');
            $('.datepicker2').datepicker('setStartDate', minDate);
            $('.timepicker1').val('');
            $('.timepicker2').val('');
        });

        $(".datepicker2").datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
            }).on('changeDate', function (selected) {
               // var maxDate = new Date(selected.date.valueOf());
               // $('.datepicker1').datepicker('setEndDate', maxDate);
                $(this).datepicker('hide');
                 $('.timepicker1').val('');
            $('.timepicker2').val('');

            });

    $(document).ready(function () {
        <?php if($this->session->userdata('admin_role_type')!=2){ ?>
        
         $('.timepicker1').timepicker({
            //format: 'HH:mm',
            //showMeridian: false,
        }).on('changeTime.timepicker', function (e) {
            //var newHour =(e.time.hours)+1;
            //$('.timepicker2').val(newHour+":"+e.time.minutes+" "+e.time.meridian);
            $('.timepicker2').timepicker({
               // format: 'HH:mm',
               // showMeridian: false,
            }).on('changeTime.timepicker', function (f) {
                // console.log(e.time);
                // console.log(f.time);
               
                var hours = 0;
                var fhours = 0;

                if(e.time.meridian == "PM" && e.time.hours<12){
                    hours = e.time.hours+12;
                }else if(e.time.meridian == "AM" && e.time.hours==12){
                    hours = e.time.hours-12;
                }else{
                    hours = e.time.hours;
                } 

                if(f.time.meridian == "PM" && f.time.hours<12){
                    fhours = f.time.hours+12;
                }else if(f.time.meridian == "AM" && f.time.hours==12){
                    fhours = f.time.hours-12;
                }else{
                    fhours = f.time.hours;
                }                

                //  console.log(hours);
                // console.log(fhours);
                if(($('.datepicker1').val()) == ($('.datepicker2').val())){
                    if((hours >= fhours) && (e.time.minutes >= f.time.minutes))
                    { 
                        $('.tim2_err').html("End time must be greater than start time.");
                        //$('.timepicker2').val(newHour+":"+e.time.minutes+" "+e.time.meridian);
                    }else{
                        $('.tim2_err').html("");
                    }
                }else{
                        $('.tim2_err').html("");
                    }


            })
        });


        // $('.timepicker1').timepicker();
        // $('.timepicker2').timepicker();
        
        <?php } ?>
        $(document).on("change","#is_exhibition",function(){
            if (this.checked) {
                $("#ex_tab").removeAttr("style");
            } else {
                $('#ex_tab').css('display','none');
            }
        });



    })

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrhTMHvt2CLbSI_GnMoEpNz7Z2s6SOxdE&signed_in=true&libraries=places&callback=initAutocomplete" async defer></script>
<script>

var placeSearch, autocompletes,geocoder;;
var componentForm = {
   // street_number: 'short_name',
    //route: 'long_name',
    locality: 'long_name',
    //sublocality_level_1: 'short_name',
    //administrative_area_level_1: 'short_name',
    country: 'long_name',
    //postal_code: 'short_name'
};

function initAutocomplete() 
{
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});
  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
  if(document.getElementById("eventid").value)
  {
    var res=document.getElementById("eventid").value;
    var res1=document.getElementById("latlong").value;
    var res2 = res1.split(",");
    console.log(res2[0]+"$"+res2[1]);
    initMap(parseFloat(res2[0]),parseFloat(res2[1]));
  }
  else
  {
    initMap(37.807441841136814,-122.3966747671036);
  }
}

// [START region_fillform]
function fillInAddress() 
{
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();
  $('#placeid').val(place.place_id);
  var lat = place.geometry.location.lat();
  var lng = place.geometry.location.lng();
  initMap(lat,lng);
  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }
  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    //console.log('********'+addressType);
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

var map;
var infowindow;
var marker;

var userType = "<?php if($this->session->userdata('admin_role_type')==1){ echo '1'; }else{ echo '0'; }?>";

function initMap(lat, lng) 
{
  var myLatLng = {lat: lat, lng: lng};
  var data = JSON.stringify(myLatLng)
  $("#latlong").val(data);
  console.log(myLatLng);
  var map = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 15,
      center: myLatLng
  });

  marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      draggable: true,
  });
if(userType==1){
  google.maps.event.addListener(marker, 'dragend', function() {
    // updateMarkerStatus('Drag ended');
    geocodePosition(marker.getPosition());
  });
}

}

function geocodePosition(pos) 
{
  geocoder = new google.maps.Geocoder();
  geocoder.geocode({
    latLng: pos
  },function(responses) {
      if (responses && responses.length > 0) {
        marker.formatted_address = responses[0].formatted_address;
        $("#address").val(responses[0].formatted_address);
        //document.getElementById("street_number").value = '';
        //document.getElementById("route").value = '';
        document.getElementById("locality").value = '';
       // document.getElementById("sublocality_level_1").value = '';
       // document.getElementById("administrative_area_level_1").value = '';
        //document.getElementById("postal_code").value = '';
        document.getElementById("country").value = '';
        for (var i = 0; i < responses[0].address_components.length; i++) {
          var addressType = responses[0].address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = responses[0].address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = '';
            document.getElementById(addressType).value = val;
          }
        }
      }else{
        marker.formatted_address = 'Cannot determine address at this location.';
      }
      document.getElementById('autocomplete').value=marker.formatted_address;
      document.getElementById('latlong').value=marker.getPosition().lat()+","+marker.getPosition().lng();
      console.log(marker.getPosition().lat()+"@"+marker.getPosition().lng());
      // infowindow.setContent(marker.formatted_address+"<br>coordinates: "+marker.getPosition().lat()+","+marker.getPosition().lng());
      // infowindow.open(map, marker);
    }
  );
}
$("#event_desc_count").text("Characters left: " + (1000 - $("#event_desc").val().length));
$("#event_desc").keyup(function(){
  $("#event_desc_count").text("Characters left: " + (1000 - $(this).val().length));
});

$("#event_dress_code_count").text("Characters left: " + (500 - $("#event_dress_code").val().length));
$("#event_dress_code").keyup(function(){
  $("#event_dress_code_count").text("Characters left: " + (500 - $(this).val().length));
});


</script>