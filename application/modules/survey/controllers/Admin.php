<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for Media [HMVC]. Handles all the datatypes and methodes required for Media section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

      /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Surveymodel');
    }

    /**
     * Index Page for this Media controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {

        if (!empty($_POST['delete'])) {
            admin_authenticate();
            foreach ($_POST['delete'] as $key => $value) {
                $flg = $this->Surveymodel->delete_data($value);
            }
            $this->session->set_flashdata('successmessage', 'Surveys deleted successfully');
        }

        $all_data = $this->Surveymodel->load_events();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'survey/admin/list';
        $this->admin_layout();
    }

    public function search() {
        $this->load->model('../models/admin/Adminauthmodel');
        $eventid = $_POST['event'];

        $all_data = [];
        if ($eventid) {
            $all_data = $this->Surveymodel->load_surveys($eventid);
        }

        $data['all_data'] = $all_data;
        $this->load->view('survey/admin/ajax_surveys',$data);
    }



    /**
     * Add/Edit function of Media module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {

        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('survey_question', 'Survey Question', 'trim|required');
            //if ($param == "A") {
                //$this->form_validation->set_rules('survey_answer[]', 'Survey Answer', 'trim|required');
            //}

            $survey_answer = (isset($_POST['is_question']) ? $_POST['is_question'] : 0);
            $survey_answer = $this->input->post('survey_answer');

            if(empty($survey_answer[0]) && $survey_answer==1)
            {
                $this->session->set_flashdata('errormessage', 'Please Select Survey Answer');

                redirect(base_url('admin_survey'));

                /*if (!empty($id)) {
                 redirect(base_url('admin_update_survey' . '/' . $id));
                }
                else
                {
                    redirect(base_url('admin_add_survey'));
                }*/

            }

            if ($this->form_validation->run($this) == TRUE) {


                //echo "<pre>"; print_r($_POST);die;

                $flg = $this->Surveymodel->modify($id);
                if (!empty($flg)) {
                    $this->session->set_flashdata('successmessage', 'Survey added successfully');
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_survey'));
            }
            // else
            // {
            //     $flg = $this->Surveymodel->modify($id);
            //     if (!empty($flg)) {
            //         $this->session->set_flashdata('successmessage', 'Survey modified successfully');
            //     } else {
            //         $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
            //     }
            //     redirect(base_url('admin_survey'));

            // }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Surveymodel->load_single_data($id);

        $this->data['all_answers']  = $this->Surveymodel->fetch_all_answers($id);

        $this->data['all_event'] = $this->Surveymodel->load_all_events();
        // echo '<pre>';
        // print_r($this->data['all_event']); exit;

        $this->data['all_category'] = $this->Surveymodel->get_result_data('survey_category', ['status' => '1']);

        $this->middle = 'survey/admin/form';
        $this->admin_layout();
    }

   

    /**
     * Status Change function of Media module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Surveymodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Survey status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_survey'));
    }

    /**
     * Delete function of Media module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {

        admin_authenticate();
        $id=base64_decode(urldecode($id));

        $flg = $this->Surveymodel->delete_data($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Survey deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }
   
   
    /**
     * View function of Survey Results
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function view($id){
        $this->data = array();
        $this->data['id'] = $id;
        $this->data['survey'] = $this->Surveymodel->get_row_data('survey_questions', ['id'=> $id]);
        if ($this->data['survey']->isRating == 'false') 
            $this->data['sample_ans'] = $this->Surveymodel->fetch_all_answers($id);
        $this->data['answers'] = $this->Surveymodel->get_answers_data($id);
        
        $this->middle = 'survey/admin/view_results';
        $this->admin_layout();
    }

    public function delete_answer($id){
        if ($this->Surveymodel->delete_answer($id)){
            $this->session->set_flashdata('successmessage', 'Answer deleted successfully');
        }else{
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

}

/* End of file admin.php */
/* Location: ./application/modules/media/controllers/admin.php */
