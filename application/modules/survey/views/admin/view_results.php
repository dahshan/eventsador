<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_survey'); ?>" class="current"><i class="fa fa-users"></i>Survey</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-list-alt"></i>Results</a>
        </div>

        <h1>Survey</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>Question: <?=$survey->survey_question?></h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>User</th>
                                    <th>Answer</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                                <?php if ($answers): $index = 0; ?>
                                    <?php foreach ($answers as $one_ans): $index ++; ?>
                                    <tr>
                                        <td><?=$index?></td>
                                        <td><?=$one_ans->name?></td>
                                        <td><?=$survey->isRating == 'true' ? $one_ans->survey_rating : $one_ans->survey_answer?></td>
                                        <td><?=date('Y-m-d H:i:s', strtotime($one_ans->created_date))?></td>
                                        <td>
                                            <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_answer(<?=$one_ans->id?>);" style="font-size: 18px;"></i>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body">Lorem ipsum dolor sit amet...</p>
    </div>

    <div class="modal-footer"> 
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a> 
        <a data-dismiss="modal" class="btn" href="#">Cancel</a> 
    </div>
</div>

<script type="text/javascript">
    /**
     * Add Media Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_survey'); ?>";
    }


 ///////////////////// DELETE IMAGE /////////////////////////////////////////

   function delete_answer(id) {
        $("#myAlert").on('shown.bs.modal', function() {
            $("#modal_confirm").click(function() {
                window.location.href = "<?php echo base_url('survey/admin/delete_answer'); ?>/" + id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this data?");
        }).modal("show");
    }
    
    function view_result(id){
        console.log(id);
    }
</script>
