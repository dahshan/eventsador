<script>
$(document).ready(function () {

    $("button#add").click(function(){   
    //$(".abcd:last").clone().appendTo(".wrapper");
    $(".abcd:last").clone(true).find("input:text").val("").end().appendTo(".wrapper");   
});
$(".glyphicon-remove").click(function () {

       $(this).closest(".abcd").remove();
    });

});
</script>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_survey'); ?>" ><i class="fa fa-users"></i>Survey</a>
            <a href="javascript:void(0);" class="current">Survey</a>
        </div>

        <h1>Survey</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Survey</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_survey') : base_url('admin_update_survey') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Select Event</label>
                                <div class="controls">
                                    <select name="event_id" >
                                        <?php
                                        //var_dump($all_event);exit;
                                        if (count($all_event) > 0) {
                                            foreach ($all_event AS $event) {
                                                ?>
                                                <option value="<?php echo (isset($event->id) && $event->id != '') ? $event->id : ''; ?>"
                                                        <?php if(!empty($data_single)){ if($event->id==$data_single->event_id){ ?> selected <?php } }?>>
                                                            <?php echo (isset($event->event_name) && $event->event_name != '') ? $event->event_name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('event_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span5">
                                    <label class="control-label">Is Comment</label>
                                    <div class="controls">
                                        <input type="checkbox" <?php if(!empty($data_single->is_comment)){ echo "checked"; }?> value="1" name="is_comment" id="is_comment">
                                         <?php echo form_error('is_comment', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span6" id="comment_tab" <?php if(empty($data_single) || empty($data_single->is_comment)){?> style="display: none;" <?php } ?>>
                                    <label class="control-label">Comment</label>
                                    <div class="controls">                                       
                                     <input type="text" name="comment" placeholder="Comment" class="span11" id="comment" <?php if(!empty($data_single)) { ?> value="<?php echo $data_single->comment;?>" <?php } ?>>
                                      <?php echo form_error('comment', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span5">
                                    <label class="control-label">Is Question</label>
                                    <div class="controls">
                                        <input onclick="checkOne('question');" type="checkbox" <?php if(!empty($data_single->is_question)){ echo "checked"; }?> value="1" name="is_question" id="is_question">
                                         <?php echo form_error('is_question', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>


                            <div class="control-group">
                                <div class="span5">
                                    <label class="control-label">Is Rate</label>
                                    <div class="controls">
                                        <input onclick="checkOne('rate');" type="checkbox" <?php if(!empty($data_single->isRating) && $data_single->isRating=='true'){ echo "checked"; }?> value="1" name="isRating" id="isRating">
                                         <?php echo form_error('isRating', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>


                            <?php //echo "<pre>"; print_r($all_category);die; ?>
                            
                            <div class="control-group">
                                <label class="control-label">Select Category</label>
                                <div class="controls">
                                    <select name="category_id" >
                                        <?php
                                        if (count($all_category) > 0) {
                                            foreach ($all_category AS $category) {
                                                ?>
                                                <option value="<?php echo (isset($category->id) && $category->id != '') ? $category->id : ''; ?>"
                                                        <?php if(!empty($data_single)){ if($category->id==$data_single->category_id){ ?> selected <?php } }?>>
                                                            <?php echo (isset($category->category_name) && $category->category_name != '') ? $category->category_name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('category_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div> 

                            <div class="control-group">
                                <label class="control-label">Survey Question</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Survey Question" type="text" id="survey_question" name="survey_question" value="<?php echo (!empty(set_value('survey_question'))) ? set_value('survey_question') : ((!empty($data_single->survey_question)) ? $data_single->survey_question : ''); ?>" >
                                    <?php echo form_error('survey_question', '<div style="color:red;">', '</div>'); ?>
                                </div>
                               
                            </div>


                            <?php


                              if(!empty($all_answers))
                              {
                                for($i=0;$i<count($all_answers);$i++){
                                ?>

                                  <div class="control-group option-group">
                                      
                                      <div class="controls">


                                      <div class="form-group abcd" id="abcde">      
                                          <div class="col-sm-12" >
                                            <div class="btn-group week">
                                               <input  placeholder="Survey Answer" type="text" id="survey_answer" name="survey_answer[]" value="<?php echo (!empty($all_answers[$i]->survey_answer)) ? $all_answers[$i]->survey_answer : ''; ?>">
                                                            
                                                                   
                                           </div> 
                                         
                                              <span class="glyphicon glyphicon-remove"><i class="icon-remove"></i></span>
                                              
                                        </div>
                                     </div>
                                              
                                          <!-- <label class="remove">Delete</label> -->
                                      </div>
                                  </div>

                              <?php
                              }
                             }
                             ?>


                            <div class="control-group option-group">
                                      
                                  <div class="controls wrapper">
                                        
                                   
                                    <div class="form-group abcd" id="abcde">      
                                      <div class="col-sm-12" >
                                        <div class="btn-group week">
                                          <input  placeholder="Survey Answer" type="text" id="survey_answer" name="survey_answer[]" value="<?php echo (!empty($all_answers->survey_answer)) ? $all_answers->survey_answer : ''; ?>">          
                                                               
                                       </div> 
                                      
                                          <span class="glyphicon glyphicon-remove"><i class="icon-remove"></i></span>
                                    
                                    </div>
                                    </div>

                                </div>

                            </div>
                            

                               <div class="control-group addClss">
                                <div class="widget-content">
                                    <button type="button" id="add" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus" aria-hidden="true"></i> Add Survey Answer</button>
                                    
                                    <!-- <div class="fa fa-plus option-show" id="option-show">Add option</div> -->
                                    <!-- <input type="button" value="ADD OPTION" id="add_phone_number"> -->
                                </div>
                                </div>



                           
                            <input type="hidden" name="media_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        // $('.datepicker').datepicker();
        // $('.timepicker').timepicker({defaultTime: false});
    })
    $(document).on("change","#is_comment",function(){
        if (this.checked) {
            $("#comment_tab").removeAttr("style");
        } else {
            $('#comment_tab').css('display','none');
        }
    });


    function checkOne(type){
        console.log(type);
        if(type==='rate'){
            $('#is_question').prop('checked', false); 
            $('#is_question').parent('span').removeClass('checked');
            $('.option-group').hide();
            $('.addClss').hide();
        }else if(type==='question'){
            $('#isRating').prop('checked', false); 
            $('#isRating').parent('span').removeClass('checked');
            $('.option-group').show();
            $('.addClss').show();
        }
    }
</script>