<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>

                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>                       
                    </div>
                    <div class="widget-content nopadding">
                        <form id="delete" method="post" action="<?php echo base_url('admin_survey'); ?>">

                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Survey Question</th>
                                    <th>Survey Create Date</th>
                                    <?php if(in_array(124, $getRole) || in_array(121, $getRole)){ ?> <th>Action</th><?php } ?>
                                    <?php if(in_array(124, $getRole)){ ?> <th>Select</th><?php } ?>

                                   
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php 
                                
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <?php if(in_array(123, $getRole)){ ?><u><a href="<?php echo base_url('admin_update_survey' . '/' . $data->id); ?>">
                                                    <?php echo $data->survey_question; ?>
                                                </a></u> <?php }else{ echo $data->survey_question; } ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo date('d-m-Y',strtotime($data->survey_create_date)); ?>
                                            </td>
                                            <?php if(in_array(124, $getRole)){ ?><td class="custom-action-btn">
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_survey('<?php echo urlencode(base64_encode($data->id)); ?>');" style="font-size: 18px;"></i>
                                            <?php } ?>
                                            <?php if (in_array(121, $getRole)): ?>
                                                <i title="View Results" class="fa fa-eye" aria-hidden="false" onclick="view_result('<?=$data->id?>');" style="font-size: 18px; margin-left: 5px;"></i>
                                            <?php endif; ?>
                                            </td>
                                            <?php if(in_array(124, $getRole)){ ?> 
                                                <td class="custom-action-btn">
                                                    <input type='checkbox' name='delete[]' value='<?php echo $data->id; ?>' >
                                                </td>
                                            <?php } ?> 

                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php if(in_array(124, $getRole)){ ?><button class="btn btn-danger btn-cls" type="button" style="margin-top: 20px;" onclick="delete_surveys();">Delete Surveys</button><?php } ?>
                        </form>


                    </div>