<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-users"></i>Survey</a>
        </div>

        <h1>Survey</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                       <?php if(in_array(122, $getRole)){ ?> <button class="btn btn-success btn-cls" type="button" onclick="add();">Add Survey</button><?php } ?>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Event</th>
                                    <th>Survey Question</th>
                                    <th>Survey Create Date</th>
                                    <?php if(in_array(124, $getRole) || in_array(121, $getRole)){ ?> <th>Action</th><?php } ?>
                                   <!--  <th>Poll Status</th> -->
                                   
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php 
                                
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->event_name; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php if(in_array(123, $getRole)){ ?><u><a href="<?php echo base_url('admin_update_survey' . '/' . $data->id); ?>">
                                                    <?php echo $data->survey_question; ?>
                                                </a></u> <?php }else{ echo $data->survey_question; } ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo date('d-m-Y',strtotime($data->survey_create_date)); ?>
                                            </td>
                                            <?php if(in_array(124, $getRole)){ ?><td class="custom-action-btn">
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_survey('<?php echo urlencode(base64_encode($data->id)); ?>');" style="font-size: 18px;"></i>
                                            <?php } ?>
                                            <?php if (in_array(121, $getRole)): ?>
                                                <i title="View Results" class="fa fa-eye" aria-hidden="false" onclick="view_result('<?=$data->id?>');" style="font-size: 18px; margin-left: 5px;"></i>
                                            <?php endif; ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body">Lorem ipsum dolor sit amet...</p>
    </div>

    <div class="modal-footer"> 
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a> 
        <a data-dismiss="modal" class="btn" href="#">Cancel</a> 
    </div>
</div>

<script type="text/javascript">
    /**
     * Add Media Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_survey'); ?>";
    }


 ///////////////////// DELETE IMAGE /////////////////////////////////////////

   function delete_survey(id,type) {

        $("#myAlert").on('shown.bs.modal', function() {
            $("#modal_confirm").click(function() {
                window.location.href = "<?php echo base_url('survey/admin/delete'); ?>/" + id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this data?");
        }).modal("show");
    }
    
    function view_result(id){
        window.location.href = "<?php echo base_url('survey/admin/view'); ?>/" + id;
    }
</script>
