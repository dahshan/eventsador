<?php

/**
 * Media Model Class. Handles all the datatypes and methodes required for handling Media
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Surveymodel extends CI_Model {

  /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of Media for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the Media that has been added by current admin [Table:  ets_media]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        //echo $this->session->userdata('admin_role_type'); exit;
        //echo $this->session->userdata('admin_org_id'); exit;
        $this->db->select('s.*,e.event_name');
        $this->db->from(tablename('survey_questions as s'));
        $this->db->join("event as e","s.event_id = e.id");
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('e.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('e.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(".$this->session->userdata('admin_uid').", users)");
            }
        }
        $this->db->where('e.delete_flag', 'N');
        $this->db->group_by('s.id');
        
        $query = $this->db->get();
        $result = $query->result();
        
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }


    /**
     * Used for loading functionality of Event for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the Event that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_events() {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('delete_flag', 'N');
       // echo "<pre>";print_r($this->session->userdata('admin_role_type'));exit;
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(".$this->session->userdata('admin_uid').", users) !=", 0);
            } 
        }
        $this->db->order_by("id", "desc");

        $query = $this->db->get();
        $result = $query->result();
        //echo $this->db->last_query();
        // echo "<pre>";print_r($result);exit;
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

   
    public function load_event($eventid) {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('id', $eventid);
        
        $query = $this->db->get();
        $result = $query->first_row();

        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

   
    public function load_surveys($eventid) {
        $this->db->select('*');
        $this->db->from(tablename('survey_questions'));
        $this->db->where("event_id", $eventid);
        
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }




    public function load_all_events() {
        //echo $this->session->userdata('admin_role_type'); exit;
        //echo $this->session->userdata('admin_org_id'); exit;
        $this->db->select('e.*');
        $this->db->from(tablename('event as e'));
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('e.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('e.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(".$this->session->userdata('admin_uid').", users)");
            }
        }
        $this->db->where('e.delete_flag', 'N');
        $this->db->group_by('e.id');
        
    
        $query = $this->db->get();
        $result = $query->result();
        
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }


    /**
     * Used for loading functionality of single Media by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single Media by id that has been added by current admin [Table:  ets_media]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('survey_questions'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }


    public function fetch_all_answers($ques_id) {

        $this->db->select('*');
        $this->db->from(tablename('survey_answers'));
        $this->db->where('survey_question_id', $ques_id);
        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1") {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single Media for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single Media w.r.t. current admin [Table:  ets_media]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
       
        $event_id   = $this->input->post('event_id');
        $is_comment   = $this->input->post('is_comment');
        $comment   = $this->input->post('comment');
        $is_question   = $this->input->post('is_question');
        $survey_question = $this->input->post('survey_question');
        $survey_answer = $this->input->post('survey_answer');

         $category_id = $this->input->post('category_id');

         $isRating = isset($_POST['isRating']) ? $this->input->post('isRating') : 0;
        //echo "<pre>"; print_r($isRating); die;
       /* echo "<pre>";
        print_r($poll_answer);
        exit;*/
       /* $emailid    = $this->input->post('emailid');
        $phoneno    = $this->input->post('phoneno');
        $address    = $this->input->post('address');
        $date       = date("Y-m-d H:i:s");*/

      /*  $this->load->helper('common');
        $password = generateRandomString(10);
        $passwordEnc = md5($password);*/

        if($isRating==1){
            $isRating = "true";
        }else{
            $isRating = "false";
        }

        if (!empty($id)) {
            $data = array(
                'event_id'          => $event_id,
                'category_id'       => $category_id,
                'survey_question'   => $survey_question,
                'is_comment'        => $is_comment,
                'comment'           => $comment,
                'is_question'       => $is_question,
                'isRating'          => $isRating
                /*'poll_create_date' => date("Y-m-d H:i:s"),
                'poll_end_date' => date("Y-m-d H:i:s"),
                'poll_status'   => '1'*/
            );


            $this->db->where('id', $id)->update(tablename('survey_questions'), $data);


            ////////////////// DELETE ANSWERS //////////////////////

            $this->db->where('survey_question_id', $id);
            $this->db->delete('survey_answers');

            ///////////////// INSERT ANSWERS //////////////////////
            if(!empty($survey_answer))
                {
            foreach($survey_answer as $answers)
            {
                if(!empty($answers))
                {
                    $data = array();
                     $data = array(
                    'event_id'      => $event_id,
                    'survey_question_id' => $id,
                    'survey_answer'   => $answers
                    );
                    $this->db->insert(tablename('survey_answers'), $data);
                }
            }
        }



            return $id;
        } else {
            // Check for Duplicate
            $data = array(
                'event_id'          => $event_id,
                'category_id'       => $category_id,
                'is_comment'       => $is_comment,
                'comment'           => $comment,
                'is_question'           => $is_question,
                'survey_question'   => $survey_question,
                'survey_create_date'=> date("Y-m-d H:i:s"),
                'survey_status'     => '1',
                'isRating'       => $isRating
            );

            $this->db->insert(tablename('survey_questions'), $data);
            $last_id = $this->db->insert_id();

            if (!empty($last_id)) {

                foreach($survey_answer as $answers)
                {
                    if(!empty($answers))
                    {
                        $data = array(
                         'event_id'      => $event_id,
                         'survey_question_id' => $last_id,
                         'survey_answer'   => $answers
                        );
                        $this->db->insert(tablename('survey_answers'), $data);
                    }
                }

               
               // $answer_last_id = $this->db->insert_id();               



                return $last_id;
            } else {
                return "";
            }
        }
    }

   

    /**
     * Used for change status functionality of Media for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current Media status
     * and change it the the opposite [Table: pb_media]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('survey_questions'));
        $this->db->where('id', $id);
        //$this->db->where('poll_status', '0');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->survey_status;

            if ($is_active == "0") {
                $new_is_active = "1";
            } else {
                $new_is_active = "0";
            }

            $update = array('survey_status' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('survey_questions'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of Media for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_media]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
   public function delete_data($id) {
        //$delete_faq = array('delete_flag' => 'Y');
        //$this->db->where('id', $id);

        //echo 'Delete data';

        $this->db->where('id', $id);
        $this->db->delete('survey_questions');

        $this->db->where('survey_question_id', $id);
        $this->db->delete('survey_answers');

        $this->db->where('survey_question', $id);
        $this->db->delete('survey_questions');
        
        return true ;
        
    }


    public function media_approve_status($id)
    {
        $sql="select approval_status from ".tablename('media_files')."  where id='".$id."' ";
        $query=$this->db->query($sql);
        $adminstatus=$query->row();
        if($adminstatus->approval_status==0)
        {
            $approval_status=1;
        }
        else
        {
            $approval_status=0;
        }
        $sq="update ".tablename('media_files')."  set approval_status='".$approval_status."' where id='".$id."'";
        $qq=$this->db->query($sq);
        if(!empty($qq))
        {
            return $approval_status;
        }
        else
        {
            return $approval_status;
        }
    }

    /**
     * Used for delete functionality of Media file for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_media]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete_file($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('media_files'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }
    
    public function get_answers_data($question_id){
        $this->db->select('survey_result.*, user.name, survey_answers.survey_answer');
        $this->db->from('survey_result');
        $this->db->join('user', 'user_id = user.id');
        $this->db->join('survey_answers', 'survey_answer_id = survey_answers.id', 'left');
        $this->db->where('survey_result.survey_question_id', $question_id);
        $query = $this->db->get();
        if ($query->num_rows() == 0) return false;
        return $query->result();
    }
    
    public function delete_answer($id){
        $this->db->where('id', $id)->delete('survey_result');
        return $this->db->affected_rows();
    }

}

/* End of file Mediamodel.php */
/* Location: ./application/modules/media/models/admin/Mediamodel.php */
