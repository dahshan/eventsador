<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_company'); ?>" ><i class="fa fa-building"></i>Company</a><a href="javascript:void(0);" class="current">Company Management Form</a>
        </div>

        <h1>Company</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Company</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_company') : base_url('admin_update_company') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Name" type="text" id="company_name" name="company_name" value="<?php echo (!empty(set_value('company_name'))) ? set_value('company_name') : ((!empty($data_single->company_name)) ? $data_single->company_name : ''); ?>" >
                                    <?php echo form_error('company_name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Details</label>
                                <div class="controls">
                                    <textarea class="span11" id="company_details" name="company_details"><?php echo (!empty(set_value('company_details'))) ? set_value('company_details') : ((!empty($data_single->company_details)) ? $data_single->company_details : ''); ?></textarea>
                                    <?php echo form_error('company_details', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span6">
                                    <label class="control-label">Email</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Email" type="text" id="company_email" name="company_email" value="<?php echo (!empty(set_value('company_email'))) ? set_value('company_email') : ((!empty($data_single->company_email)) ? $data_single->company_email : ''); ?>">
                                        <?php echo form_error('company_email', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">Phone</label>
                                    <div class="controls">
                                        <input class="span11" placeholder="Phone" type="text" id="company_phone" name="company_phone" value="<?php echo (!empty(set_value('company_phone'))) ? set_value('company_phone') : ((!empty($data_single->company_phone)) ? $data_single->company_phone : ''); ?>" >
                                        <?php echo form_error('company_phone', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Address</label>
                                <div class="controls">
                                    <textarea class="span11" id="company_address" name="company_address"><?php echo (!empty(set_value('company_address'))) ? set_value('company_address') : ((!empty($data_single->company_address)) ? $data_single->company_address : ''); ?></textarea>
                                    <?php echo form_error('company_address', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Company Logo</label>
                                <div class="controls">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                      <img  src="<?php if(!empty($data_single->company_logo)){ echo base_url('assets/upload/company')."/".$data_single->company_logo; } else {echo base_url('assets/upload/default_logo.png');}?>" data-src="<?php if(!empty($data_single->company_logo)){ echo base_url('assets/upload/company')."/".$data_single->company_logo; } else {echo base_url('assets/upload/default_logo.png');}?>" alt="...">
                                    </div>
                                    <div>
                                      <div class="controls custon_textarea" style="margin-left: 0;">
                                        <input type="file" name="company_logo" style="line-height:1px;">
                                      </div>
                                      <?php echo form_error('company_logo', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <input type="hidden" name="company_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
