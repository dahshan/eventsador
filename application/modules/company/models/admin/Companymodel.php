<?php

/**
 * company_admin Model Class. Handles all the datatypes and methodes required for handling company_admin
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Companymodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of company_admin for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the company_admin that has been added by current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('*');
        $this->db->from(tablename('company_admin'));
        $this->db->where('delete_flag', 'N');
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single company_admin by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single company_admin by id that has been added by current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('company_admin'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1") {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single company_admin for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single company_admin w.r.t. current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        $company_name = $this->input->post('company_name');
        $company_email = $this->input->post('company_email');
        $company_phone = $this->input->post('company_phone');
        $company_address = $this->input->post('company_address');
        $company_details = $this->input->post('company_details');
        $company_logo = "";
        if (!empty($_FILES['company_logo']['name'])) 
        {
            $this->load->library('upload');
            $config['upload_path'] = './assets/upload/company_admin/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['file_name'] = time();

            $this->upload->initialize($config);
            if($_FILES['company_logo']['error']==0)
            {
                $profilepic=$_FILES['company_logo']['name'];
                $profilepic=explode('.',$profilepic);
                $ext=end($profilepic);
                if($ext=="jpg" or $ext=="jpeg" or $ext=="png" or $ext=="bmp")
                {
                    $profilepic=time().rand().".".$ext;
                    $destination="./assets/upload/company/".$profilepic;
                    if (move_uploaded_file($_FILES['company_logo']['tmp_name'] ,$destination))
                    {
                        $company_logo=$profilepic;
                        //$olddestination="./assets/upload/company/".$oldfile;
                        //@unlink($olddestination);
                    }
                }
                else
                {
                    $this->session->set_flashdata('errormessage', 'Only .jpg,.jpeg,.bmp and .png image extensions are supported');
                    $redirect = site_url('admin/admin_company');
                    redirect($redirect);
                }
            }
        }
        $date = date("Y-m-d H:i:s");

        $this->load->helper('common');
        $password = generateRandomString(10);
        $company_password = sha1($password);

        if (!empty($id)) {
            if(!empty($company_logo))
            {
                $data = array(
                    'company_name' => $company_name,
                    'company_email' => $company_email,
                    'company_phone' => $company_phone,
                    'company_address' => $company_address,
                    'company_details' => $company_details,
                    'company_logo' => $company_logo,
                );
            }
            else
            {
                $data = array(
                    'company_name' => $company_name,
                    'company_email' => $company_email,
                    'company_phone' => $company_phone,
                    'company_address' => $company_address,
                    'company_details' => $company_details,
                );
            }

            $this->db->where('id', $id)->update(tablename('company_admin'), $data);
            return $id;
        } else {
            // Check for Duplicate
            $data = array(
                'company_name' => $company_name,
                'company_email' => $company_email,
                'company_phone' => $company_phone,
                'company_address' => $company_address,
                'company_details' => $company_details,
                'company_logo' => $company_logo,
                'company_password' => $company_password,
                'created_date' => $date
            );

            $this->db->insert(tablename('company_admin'), $data);
            $last_id = $this->db->insert_id();

            if (!empty($last_id)) {
               // $subject = "Eventsador - Account Created";
               // $message = '<p>Greetings ' . $company_name . ",</p>";
               // $message .= '<p>Your account has been created with Eventsador. Please use the following credential to login to your profile.</p>';
               // $message .= '<p><strong>company_admin-id: </strong>' . $company_email;
               // $message .= '<br><strong>Password: </strong>' . $password . "</p>";
               // $message .= '<p>Thank you,</p>';
               // $message .= '<p>Eventsador Admin</p>';

               // $mail_data = [
               //     'name' => $company_name,
               //     'body' => $message,
               // ];

               // $this->load->helper('email');
               // send_email($company_email, $subject, $mail_data);
                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of company_admin for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current company_admin status
     * and change it the the opposite [Table: pb_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('company_admin'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('company_admin'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }


    /**
     * Used for resend credintials functionality of User for an admin
     *
     * <p>Description</p>
     *
     * This function takes id as input
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function reset_pwd_and_resend_email($id) {
        $this->db->select('*');
        $this->db->from(tablename('company_admin'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $new_password = rand(100000, 999999);

            $update = array('company_password' => md5($new_password));
            $this->db->where('id', $id);

            if ($this->db->update(tablename('company_admin'), $update)) {

                $subject = "Eventsador - Account Password Reset";
                $message = '<p>Greetings ' . $result->company_name . ",</p>";
                $message .= '<p>Your password has been reset with Eventsador. Please use the following credential to login to your profile.</p>';
                $message .= '<p><strong>User-id: </strong>' . $result->company_email;
                $message .= '<br><strong>Password: </strong>' . $new_password . "</p>";
                $message .= '<p>Thank you,</p>';
                $message .= '<p>Eventsador Admin</p>';

                $mail_data = [
                    'name' => $result->company_name,
                    'body' => $message,
                ];

                $this->load->helper('email');
                send_email($result->company_email, $subject, $mail_data);


                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }



    /**
     * Used for delete functionality of company_admin for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('company_admin'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

}

/* End of file Organizermodel.php */
/* Location: ./application/modules/company_admin/models/admin/Organizermodel.php */
