<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for company [HMVC]. Handles all the datatypes and methodes required for company section of Optus
 *
 * @author  
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Companymodel');
    }

    /**
     * Index Page for this company controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Companymodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'company/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of company module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('company_name', 'Name', 'trim|required');
            if ($param == 'A') {
                if (empty($_FILES['company_logo']['name']))
                {
                    $this->form_validation->set_rules('company_logo', 'Logo', 'trim|required');
                }
                $this->form_validation->set_rules('company_email', 'Email', 'trim|required|valid_email|callback_new_email_check');
            } else {
                $this->form_validation->set_rules('company_email', 'Email', 'trim|required|callback_old_email_check');
            }
            $this->form_validation->set_rules('company_phone', 'Phone no', 'trim|required|numeric');
            $this->form_validation->set_rules('company_address', 'Address', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Companymodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! company already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'company modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_company'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Companymodel->load_single_data($id);
        $this->data['all_module'] = $this->Companymodel->get_result_data('modules', ['is_active' => 'Y', 'delete_flag' => 'N']);
        $this->middle = 'company/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function new_email_check($email) {
        if (isset($email) && $email != '') {
            $organizer_data = $this->Companymodel->get_row_data('company_admin', ['company_email' => $email, 'delete_flag' => 'N']);
            if (count($organizer_data) > 0) {
                $this->form_validation->set_message('new_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $company_id = $this->input->post('company_id');
            $company_data = $this->Companymodel->get_row_data('company_admin', ['id<>' => $company_id, 'company_email' => $email, 'delete_flag' => 'N']);
            if (count($company_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of company module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Companymodel->status($id);
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Company status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_company'));
    }

    /**
     * Status Change function of User module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function resend_email($id) {
        $flg = $this->Companymodel->reset_pwd_and_resend_email($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'credentials sent successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_company'));
    }

    /**
     * Delete function of company module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Companymodel->delete($id);
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Company deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_company'));
    }

}

/* End of file admin.php */
/* Location: ./application/modules/company/controllers/admin.php */
