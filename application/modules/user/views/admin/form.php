<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_user'); ?>"><i class="fa fa-user"></i>User</a><a href="javascript:void(0);" class="current"></i>User Form</a>
        </div>

        <h1>User</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>User</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_user') : base_url('admin_update_user') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Name" type="text" id="name" name="name" value="<?php echo (!empty(set_value('name'))) ? set_value('name') : ((!empty($data_single->name)) ? $data_single->name : ''); ?>" >
                                    <?php echo form_error('name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Email</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Email" type="text" id="emailid" name="emailid" value="<?php echo (!empty(set_value('emailid'))) ? set_value('emailid') : ((!empty($data_single->emailid)) ? $data_single->emailid : ''); ?>">
                                    <?php echo form_error('emailid', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Phone</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Phone" type="text" id="phoneno" name="phoneno" value="<?php echo (!empty(set_value('phoneno'))) ? set_value('phoneno') : ((!empty($data_single->phoneno)) ? $data_single->phoneno : ''); ?>" >
                                    <?php echo form_error('phoneno', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Select Role</label>
                                <div class="controls">
                                    <select name="role_id">
                                        <?php
                                        if (count($all_role) > 0) {
                                            foreach ($all_role AS $role) {
                                                ?>
                                                <option value="<?php echo $role->id; ?>" <?php if(!empty($data_single)){ if($role->id==$data_single->role_id){ echo 'selected'; } } ?> >
                                                    <?php echo $role->name; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('role_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <input type="hidden" name="user_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
