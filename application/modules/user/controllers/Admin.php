<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for User [HMVC]. Handles all the datatypes and methodes required for User section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Usermodel');
    }

    /**
     * Index Page for this User controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Usermodel->load_all_data();

        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'user/admin/list';
        $this->admin_layout();
    }

    /**
     * Index Page for this Organizer controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function users_by_event() {
        if (is_numeric($this->uri->segment(2))) {
            $event = $this->Usermodel->load_event_data($this->uri->segment(2));
            $all_data = $this->Usermodel->load_all_data_in($event->users);
            $uri = $this->uri->segment(1);
            $this->data = array();
            $this->data['uri'] = $uri;
            $this->data['all_data'] = $all_data;
            $this->middle = 'user/admin/list';
            $this->admin_layout();
        }
    }

    /**
     * Add/Edit function of User module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            if ($param == 'A') {
                $this->form_validation->set_rules('emailid', 'Email', 'trim|required|valid_email|callback_new_email_check');
            } else {
                $this->form_validation->set_rules('emailid', 'Email', 'trim|required|valid_email|callback_old_email_check');
            }
            $this->form_validation->set_rules('phoneno', 'Phone no', 'trim|numeric');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Usermodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! User already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'User modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_user'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Usermodel->load_single_data($id);
        $this->data['all_role'] = $this->Usermodel->get_result_data('role', ['is_active' => 'Y', 'delete_flag' => 'N']);
        $this->middle = 'user/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function new_email_check($email) {
        if (isset($email) && $email != '') {
            $user_data = $this->Usermodel->get_row_data('user', ['emailid' => $email, 'delete_flag' => 'N']);
            if (count($user_data) > 0) {
                $this->form_validation->set_message('new_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $user_id = $this->input->post('user_id');
            $user_data = $this->Usermodel->get_row_data('user', ['id<>' => $user_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($user_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of User module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Usermodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'User status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_user'));
    }

    /**
     * Status Change function of User module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function resend_email($id) {
        $flg = $this->Usermodel->reset_pwd_and_resend_email($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'credentials sent successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_user'));
    }

    /**
     * Delete function of User module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $user = $this->Usermodel->load_single_data($id);

        $flg = $this->Usermodel->delete($user);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'User deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_user'));
    }

}

/* End of file admin.php */
/* Location: ./application/modules/user/controllers/admin.php */
