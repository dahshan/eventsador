<?php

/**
 * User Model Class. Handles all the datatypes and methodes required for handling User
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Usermodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of User for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the User that has been added by current admin [Table:  ets_user]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('user.*,role.name as role_name,role.delete_flag as deleteflag');
        $this->db->from(tablename('user'));
        $this->db->join(tablename('role'),"role.id = user.role_id");
        $this->db->where('user.delete_flag', 'N');
        $this->db->where('user.id!=', 1);
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('organizer_id', $this->session->userdata('admin_uid'));
            }
            else
            {
                $this->db->where('organizer_id', $this->session->userdata('admin_org_id'));
            }
        }
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single User by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single User by id that has been added by current admin [Table:  ets_user]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of user for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the user that has been added by current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data_in($where_str) {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('delete_flag', 'N');
        $this->db->where('role_id', 2);
        $this->db->where_in('id', $where_str);
        $this->db->order_by("id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single Event by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single Event by id that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_event_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('event'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1") {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single User for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single User w.r.t. current admin [Table:  ets_user]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        $type_id = 8;
        
        $name = $this->input->post('name');
        $role_id = $this->input->post('role_id');
        $emailid = $this->input->post('emailid');
        $phoneno = $this->input->post('phoneno');
        $organizer_id = $this->session->userdata('admin_uid');
        $date = date("Y-m-d H:i:s");
        if (count($this->input->post('modules')) > 0) {
            $modules = implode(',', $this->input->post('modules'));
        } else {
            $modules = '';
        }
        
        $this->load->helper('common');
        $password = generateRandomString(10);
        $passwordEnc = md5($password);

        if (!empty($id)) {
            $data = array(
                'name' => $name,
                'emailid' => $emailid,
                'phoneno' => $phoneno,
                'role_id' => $role_id,
                'modified_date' => $date,
            );

            $this->db->where('id', $id)->update(tablename('user'), $data);
            return $id;
        } else {
            // Check for Duplicate
            $data = array(
                'name' => $name,
                'emailid' => $emailid,
                'password' => $passwordEnc,
                'phoneno' => $phoneno,
                'role_id' => $role_id,
                'organizer_id' => $organizer_id,
                'entry_date' => $date,
                'type_id'=>$type_id
            );

            if (!empty($newimg)) {
                $data["profile_image"] = $newimg;
            }

            $this->db->insert(tablename('user'), $data);
            $last_id = $this->db->insert_id();

            if (!empty($last_id)) {
                $subject = "Eventsador - Account Created";
                $message = '<p>Greetings ' . $name . ",</p>";
                $message .= '<p>Your account has been created with Eventsador. Please use the following credential to login to your profile.</p>';
                $message .= '<p><strong>User-id: </strong>' . $emailid;
                $message .= '<br><strong>Password: </strong>' . $password . "</p>";
                $message .= '<p>Thank you,</p>';
                $message .= '<p>Eventsador Admin</p>';

                $mail_data = [
                    'name' => $name,
                    'body' => $message,
                ];

                $this->load->helper('email');
                send_email($emailid, $subject, $mail_data);

                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of User for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current User status
     * and change it the the opposite [Table: pb_user]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('user'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for resend credintials functionality of User for an admin
     *
     * <p>Description</p>
     *
     * This function takes id as input
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function reset_pwd_and_resend_email($id) {
        $this->db->select('*');
        $this->db->from(tablename('user'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $new_password = rand(100000, 999999);

            $update = array('password' => md5($new_password));
            $this->db->where('id', $id);

            if ($this->db->update(tablename('user'), $update)) {

                $subject = "Eventsador - Account Password Reset";
                $message = '<p>Greetings ' . $result->name . ",</p>";
                $message .= '<p>Your password has been reset with Eventsador. Please use the following credential to login to your profile.</p>';
                $message .= '<p><strong>User-id: </strong>' . $result->emailid;
                $message .= '<br><strong>Password: </strong>' . $new_password . "</p>";
                $message .= '<p>Thank you,</p>';
                $message .= '<p>Eventsador Admin</p>';

                $mail_data = [
                    'name' => $result->name,
                    'body' => $message,
                ];

                $this->load->helper('email');
                send_email($result->emailid, $subject, $mail_data);


                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of User for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_user]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($user) {
        $delete_faq = array('delete_flag' => 'Y', 'name' => $user->name . '[deleted]', 'emailid' => 'deleted'.rand ( 10000 , 99999 ).'.'.$user->emailid);
        $this->db->where('id', $user->id);

        if ($this->db->update(tablename('user'), $delete_faq, ['id' => $user->id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

    public function appuser_get($where)
    {
        return $this->db->where($where)->get(tablename('user'))->row();
    }

    public function appuser_insert($data1)
    {
        $this->db->insert(tablename('user'),$data1);
        return $this->db->insert_id();
    }

    public function update_data($table,$where,$data)
    {
        $this->db->where($where);
        $this->db->update($table, $data); 
        return $this->db->affected_rows();
    } 
    
    public function get_qrcode_data($user_id){
        // QR Content: name, position, company, mobile_no, phone_no, email, fax_no, address, skype_id, wechat_id, user_id, image
        $QRContent = [];
        $user = $this->get_row_data('user', ['id'=> $user_id]);
        if ($user){
            $companies = $this->db->from('company')->where('user_id', $user_id)->order_by('current_job desc, id desc')->get()->result();
            
            $QRContent['name'] = $user->name;       //$QRContent['position'] = $user->position_title;
            if ($companies){
                $QRContent['position'] = $companies[0]->position;
            }
            $QRContent['mobile_no'] = $user->mobile_no; $QRContent['phone_no'] = $user->phoneno;
            $QRContent['email'] = $user->emailid;       $QRContent['fax_no'] = $user->fax_no;
            $QRContent['address'] = $user->address;     $QRContent['skype_id'] = $user->skype_id;
            $QRContent['wechat_id'] = $user->wechat_id; $QRContent['user_id'] = $user->id;
            if (!empty($user->profile_image)) $QRContent['image'] = base_url('assets/upload/appuser').'/'.$user->profile_image;
            else $QRContent['image'] = "";
            $QRContent['qr_code'] = $user->qr_code;
        }
        return $QRContent;
    }

}

/* End of file Usermodel.php */
/* Location: ./application/modules/user/models/admin/Usermodel.php */
