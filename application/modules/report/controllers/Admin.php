<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for Event [HMVC]. Handles all the datatypes and methodes required for Event section of Optus
 *
 * @author  
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Reportmodel');
        $this->load->helper("common_helper");
        
    }

    /**
     * Index Page for this Event controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Reportmodel->load_all_data();
       
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'report/admin/list';
        $this->admin_layout();
    }

    
    public function report_search() {
       // echo "<pre>"; print_r($_POST); die;
        $eventid = $_POST['event'];
        $type = $_POST['type'];

        $all_data = $this->Reportmodel->load_single_data($eventid,$type);

        $data['all_data'] = $all_data;
    $this->load->view('report/admin/report_ajax',$data);
    }

}

/* End of file admin.php */
/* Location: ./application/modules/report/controllers/admin.php */
