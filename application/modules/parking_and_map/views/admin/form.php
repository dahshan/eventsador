<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_parking_map'); ?>" ><i class="fa fa-map"></i>Parking Map</a>
            <a href="<?php echo base_url('admin_view_parking_map').'/'.$this->uri->segment(2); ?>" ><i class="fa fa-map-pin"></i>Parking Map List</a>
            <a href="javascript:void(0);" class="current">Parking Map Management</a>
        </div>

        <h1>Parking Map</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Parking Map</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_parking_map').'/'.$this->uri->segment(2) : base_url('admin_update_parking_map').'/'.$this->uri->segment(2) . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">

                        <div class="control-group">
                                <label class="control-label">Parking Longitude</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Parking Longitude" type="text" id="parking_longitude" name="parking_longitude" value="<?php echo (!empty(set_value('parking_longitude'))) ? set_value('parking_longitude') : ((!empty($data_single->parking_longitude)) ? $data_single->parking_longitude : ''); ?>" >
                                    <?php echo form_error('parking_longitude', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>



                            <div class="control-group">
                                <label class="control-label">Parking Latitude</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Parking Latitude" type="text" id="parking_latitude" name="parking_latitude" value="<?php echo (!empty(set_value('parking_latitude'))) ? set_value('parking_latitude') : ((!empty($data_single->parking_latitude)) ? $data_single->parking_latitude : ''); ?>" >
                                    <?php echo form_error('parking_latitude', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>


                           
                            <div class="control-group">
                                <label class="control-label">Map</label>
                                <div class="controls">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                      <img  src="<?php if(!empty($data_single->map_image)){ echo base_url('assets/upload/parking_and_map')."/".$data_single->map_image; } else {echo base_url('assets/upload/placeholder.png');}?>" data-src="<?php if(!empty($data_single->map_image)){ echo base_url('assets/upload/parking_and_map')."/".$data_single->map_image; } else {echo base_url('assets/upload/placeholder.png');}?>" alt="...">
                                    </div>
                                    <div>
                                      <div class="controls custon_textarea" style="margin-left: 0;">
                                        <input type="file" name="map_image" style="line-height:1px;">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Description</label>
                                <div class="controls">
                                    <textarea class="span11 ckeditor" id="parking_description" name="parking_description"><?php echo (!empty(set_value('parking_description'))) ? set_value('description') : ((!empty($data_single->parking_description)) ? $data_single->parking_description : ''); ?></textarea>                                   
                                    <?php echo form_error('parking_description', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

