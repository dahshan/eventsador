<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-map"></i>Parking & Map</a>
        </div>

        <h1>Parking & Map</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Event Name</th>
                                    <th>Total Parking & Map</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                //echo "<pre>";print_r($all_data);
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn"><?php if(in_array(160, $getRole)){ ?><u><a href="<?php echo base_url('admin_view_parking_map' . '/' . $data->id); ?>">
                                                <?php echo $data->event_name; ?></a></u> <?php }else{ echo $data->event_name; } ?>
                                            </td>
                                            <td class="custom-action-btn"><?php if(in_array(160, $getRole)){ ?><u><a href="<?php echo base_url('admin_view_parking_map' . '/' . $data->id); ?>"><?php echo $data->parking_and_map_count; ?></a></u> <?php }else{ echo $data->parking_and_map_count; } ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    function add() {
        window.location.href = "<?php echo base_url('admin_add_parking_map'); ?>";
    }
</script>
