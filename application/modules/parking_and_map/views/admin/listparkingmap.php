<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="<?php echo base_url('admin_parking_map'); ?>" ><i class="fa fa-map"></i>Parking Map</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-map-pin"></i>Parking Map List</a>
        </div>

        <h1>Parking Map List</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <?php if (empty($all_data)) { ?>
                        <?php if(in_array(161, $getRole)){ ?><button class="btn btn-success btn-cls" type="button" onclick="add();">Add Parking Map</button><?php } ?>
                        <?php } ?>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Description</th>
                                    <th>Map</th>
                                    <?php if(in_array(163, $getRole)){ ?><th>Status</th><?php } ?>
                                    <?php if(in_array(164, $getRole)){ ?><th>Action</th><?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                           <td class="custom-action-btn">
                                               <?php if(in_array(162, $getRole)){ ?> <u><a href="<?php echo base_url('admin_update_parking_map'.'/'.$this->uri->segment(2) . '/' . $data->id); ?>">
                                                <?php echo $sl++; ?>
                                                </a></u><?php }else{ echo $sl++; } ?>
                                            </td> 
                                            <td class="custom-action-btn">
                                                <?php echo $data->parking_description; ?>
                                            </td>
                                            <td><img style="height: 50px;width: 50px;" <?php if(!empty($data->map_image)){ ?> src="<?php echo base_url('assets/upload/parking_and_map'); ?>/<?php echo $data->map_image; ?>" <?php }else{ ?> src="<?php echo base_url('assets/upload/placeholder.png'); ?>" <?php } ?> ></td>
                                            <?php if(in_array(163, $getRole)){ ?><td class="custom-action-btn">
                                                <?php
                                                if ($data->is_active == "Y") {
                                                    echo '<i title="De-activate" class="fa fa-close" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                } else {
                                                    echo '<i title="Activate" class="fa fa-check" aria-hidden="false" onclick="change_status(' . $data->id . ');"></i>';
                                                }
                                                ?>
                                            </td><?php } ?>
                                            <?php if(in_array(164, $getRole)){ ?><td class="custom-action-btn">
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                            </td><?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add Event Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_parking_map').'/'.$this->uri->segment(2); ?>";
    }

    /**
     * Status Change Function
     *
     * @param   rec_id
     * @return  redirects and gives control to status handler
     */
    function change_status(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_status_parking_map'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to change status of this Parking Map?");
        }).modal("show");
    }

    /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_delete_parking_map'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this Parking Map?");
        }).modal("show");
    }

    
</script>
