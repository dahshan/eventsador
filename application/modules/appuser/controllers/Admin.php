<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH.'libraries/phpqrcode/qrlib.php');
/**
 * Admin Class for attendees [HMVC]. Handles all the datatypes and methodes required for attendees section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Appusermodel');
        $this->load->model('user/admin/Usermodel');
    }

    /**
     * Index Page for this attendees controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        // $all_data = $this->Appusermodel->get_detailed_result_data("event_access_request","appuser","user_id=appuser.id",array("event_id"=>$eid,"event_join"=>'1'));
        $all_data = $this->Appusermodel->get_result_data("user",array("register_status"=>'1',"role_id"=>'0','delete_flag' => 'N'));
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'appuser/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of attendees module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'attendees Title', 'trim|required');
            $this->form_validation->set_rules('details', 'attendees Overview', 'trim|required');
            $this->form_validation->set_rules('date', 'attendees Date', 'trim|required');
            $this->form_validation->set_rules('start_time', 'attendees Start Time', 'trim|required');
            $this->form_validation->set_rules('end_time', 'attendees End Time', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Appusermodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! attendees already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'attendees modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_agenda'));
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        if (!empty($id)) {
            $this->data['data_single'] = $this->Appusermodel->load_single_data($id);
            $this->data['all_room'] = $this->Appusermodel->get_result_data('agenda_location', ['event_id' => $this->data['data_single']->event_id]);
        }
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
                }
            }
        }
        else
        {
            $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
        }
        $this->data['all_speaker'] = $this->Appusermodel->get_result_data('speaker', ['is_active' => 'Y', 'delete_flag' => 'N']);
        $this->middle = 'attendees/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for organizer selected or not
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function organizers_check($organizers) {
        $organizers = $this->input->post('organizers');
        if (!isset($organizers) || count($organizers) == 0) {
            $this->form_validation->set_message('organizers_check', '{field} Cannotbe blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $event_id = $this->input->post('event_id');
            $event_data = $this->Appusermodel->get_row_data('attendees', ['id<>' => $event_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($event_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of attendees module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Appusermodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'attendees status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Delete function of attendees module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $user = $this->Usermodel->load_single_data($id);

        $flg = $this->Usermodel->delete($user);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'attendees deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }



}

/* End of file admin.php */
/* Location: ./application/modules/attendees/controllers/admin.php */
