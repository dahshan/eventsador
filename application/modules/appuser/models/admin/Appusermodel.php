<?php

/**
 * agenda Model Class. Handles all the datatypes and methodes required for handling agenda
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Appusermodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of agenda for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the agenda that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('id,event_name,attendees');
        $this->db->from(tablename('event'));
        $this->db->where('event.delete_flag', 'N');
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
            }
        }
        $this->db->order_by("event.id", "asc");

        $query = $this->db->get();
        $result = $query->result();
        if(!empty($result))
        {
            $fina_result=array();
            foreach($result as $val)
            {
                $val->pending_count=count($this->get_result_data("event_access_request", array("status"=>'not_approved',"event_id"=>$val->id)));
                if(!empty($val->attendees))
                {
                    $val->attendees_count=count(explode(',',$val->attendees));
                }
                else
                {
                    $val->attendees_count=0;
                }
                $fina_result[]=$val;
            }
            $result=$fina_result;
        }
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single agenda by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single agenda by id that has been added by current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('agenda'));
        if(!empty($id))
        {
            $this->db->where('id', $id);
        }

        $query = $this->db->get();
        $result = $query->row();
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1",$optional_where=NULL) {
        if(empty($optional_where))
        {
            $query = $this->db->get_where(tablename($table), $where);
        }
        else
        {
            $this->db->where_in('id', $optional_where);
            $query = $this->db->get(tablename($table), $where);
        }

        //echo $this->db->last_query(); exit;
        return $query->result();
    }


    public function get_my_events($table, $where =NULL) {
        
        $query = $this->db->get_where(tablename($table), $where);
        
        //echo $this->db->last_query(); exit;
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single agenda for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single agenda w.r.t. current admin [Table:  ets_event]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        $data['event_id'] = $this->input->post('event_id');
        $data['title'] = $this->input->post('title');
        $data['details'] = $this->input->post('details');
        $start_date_time=$this->input->post('date')." ".$this->input->post('start_time');
        $data['start_date_time'] = date("Y-m-d H:i:s",strtotime($start_date_time));
        $end_date_time=$this->input->post('date')." ".$this->input->post('end_time');
        $data['end_date_time'] = date("Y-m-d H:i:s",strtotime($end_date_time));
        if(!empty($this->input->post('room_id')))
        {
            $data['room_id']= $this->input->post('room_id');
            $data['location']=$this->get_row_data("agenda_location", array("id"=>$this->input->post('room_id')))->name;
        }
        if(!empty($this->input->post('speakers')))
        {
            $data['speakers'] = implode(',',$this->input->post('speakers'));
        }
        if (!empty($id)) 
        {
            $this->db->where('id', $id)->update(tablename('agenda'), $data);
            return $id;
        } 
        else 
        {
            $data['created_date'] = date("Y-m-d H:i:s");
            $this->db->insert(tablename('agenda'), $data);
            $last_id = $this->db->insert_id();
            if (!empty($last_id)) {
                return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of agenda for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current agenda status
     * and change it the the opposite [Table: pb_event]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('agenda'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('agenda'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    public function checked_in_status($id) {
        $this->db->select('*');
        $this->db->from(tablename('event_access_request'));
        $this->db->where('id', $id);
        // $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();
        // echo"<pre>";print_r($result);
        if (!empty($result)) {
            $checked_in = $result->checked_in;
            // echo  $checked_in;
            if ($checked_in == "0") {
                $new_is_active = "1";
            } else {
                $new_is_active = "0";
            }

            $update = array('checked_in' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('event_access_request'), $update)) {
                // echo $this->db->last_query();die;
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of agenda for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_event]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('agenda'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

    public function insert_data($table,$data1)
    {
        $this->db->insert(tablename($table),$data1);
        return $this->db->insert_id();
    }

    public function delete_data($table,$where)
    {
        $this->db->delete(tablename($table),$where);
        return $this->db->affected_rows();
    }

    public function update_data($table,$where,$data)
    {
        $this->db->where($where);
        $this->db->update($table, $data); 
        return $this->db->affected_rows();
    } 

    public function search_event($tag) 
    {
        $this->db->select('*');
        $this->db->from(tablename('agenda'));
        $this->db->or_like('event_name', $tag);
        $this->db->or_like('event_venue', $tag);
        $this->db->or_like('event_description', $tag);
        $this->db->order_by("entry_date", "desc");
        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) 
        {
            // echo $this->db->last_query();die;
            return $result;
        } 
        else 
        {
            return "";
        }
    }

    public function getchat($table,$where) 
    {
        $sql="select id,event_id,sender_id,receiver_id,message,created_date,DATE(created_date) as chat_date from ".$table." where event_id='".$where['event_id']."' and ((sender_id='".$where['sender_id']."' and receiver_id='".$where['receiver_id']."') or (sender_id='".$where['receiver_id']."' and receiver_id='".$where['sender_id']."')) order by created_date asc";
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $result = $query->result();
/*
        $chatArr = array();
        
         $i = 0;

          echo "<pre>";
        print_r($result);
        exit;

        foreach($result as $chat)
        {
            $chatArr[$chat->chat_date][$i]['id'] = $chat->id;
            // $chatArr[$chat->chat_date][$i]['event_id'] = $chat->event_id;
            // $chatArr[$chat->chat_date][$i]['receiver_id'] = $chat->receiver_id;
            // $chatArr[$chat->chat_date][$i]['message'] = $chat->message;
            // $chatArr[$chat->chat_date][$i]['flag'] = $chat->flag;
            // $chatArr[$chat->chat_date][$i]['created_date'] = $chat->created_date;
            $i++;
        }

        echo "<pre>";
        print_r($chatArr);
        exit;*/
        return $result;
    }



    public function getchatdates($table,$where) 
    {
        $sql="select DATE(created_date) as chat_date from ".$table." where event_id='".$where['event_id']."' and ((sender_id='".$where['sender_id']."' and receiver_id='".$where['receiver_id']."') or (sender_id='".$where['receiver_id']."' and receiver_id='".$where['sender_id']."')) group by DATE(created_date) ";
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $result = $query->result();

       
        return $result;
    }

    public function chat_messages($id) 
    {
        $sql="select cht_hd.*,event_name from ".tablename('chat_head')." as cht_hd,".tablename('agenda')." ev where cht_hd.event_id=ev.id and (sender_id='".$id."' or receiver_id='".$id."') order by created_date desc";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function my_agenda($where) 
    {
        $this->db->select('agenda.*');
        $this->db->from(tablename('my_agenda'));
        $this->db->join(tablename('agenda'),'my_agenda.agenda_id=agenda.id');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result();
        // echo $this->db->last_query();die;
        if (!empty($result)) 
        {
            return $result;
        } 
        else 
        {
            return "";
        }
    }

    public function agenda_available_dates($where) 
    {
        $sql="select date_format(start_date_time,'%Y-%m-%d') as agenda_date from ".tablename('agenda')." where (event_id='".$where['event_id']."' and user_id=0) or (event_id='".$where['event_id']."' and user_id='".$where['user_id']."' ) group by date_format(start_date_time,'%Y-%m-%d') order by date_format(start_date_time,'%Y-%m-%d') ASC";
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $result = $query->result();
        // print_r($result);die;
        return $result;
    }

    public function get_detailed_result_data($table,$table1,$join_condition,$where = "1=1",$tag=NULL)
    {
        $this->db->select('event_access_request.*,user.name,user.emailid,user.profile_image,user.phoneno,user.company,user.position_title');
        $this->db->join($table1,$join_condition);
        if(!empty($tag))
        {
            $this->db->like('user.name', $tag);
        }
        $query=$this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    public function runSQL($tablename,$datasql)
    {
        $sql="insert into ".tablename($tablename)." set ".$datasql;
        $query=$this->db->query($sql);
        $r=$this->db->insert_id();
        if(!empty($r))
        {
            return $r;
        }
        else
        {
            return;
        }
    }


}

/* End of file Eventmodel.php */
/* Location: ./application/modules/agenda/models/admin/Eventmodel.php */
