<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="<?php echo base_url('admin_attendees'); ?>" ><i class="fa fa-users"></i>Event Attendees</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-list"></i>Attendees List</a>
        </div>

        <h1>Attendees</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <button class="btn btn-success btn-cls" type="button" onclick="import_attendee();">Import Attendees</button>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Image</th>
                                    <th>Company</th>
                                    <th>Position</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <!-- <a href="<?php echo base_url('admin_update_agenda' . '/' . $data->id); ?>"> -->
                                                <?php echo $data->name; ?>
                                                <!-- </a> -->
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->emailid; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->phoneno; ?>
                                            </td>
                                            <td><img style="height: 50px;width: 50px;" <?php if(!empty($data->profile_image)){ ?> src="<?php echo base_url('assets/upload/appuser'); ?>/<?php echo $data->profile_image; ?>" <?php }else{ ?> src="<?php echo base_url('assets/upload/default_man.png'); ?>" <?php } ?> ></td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->company; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->position_title; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php if($data->checked_in=='1'){ ?>
                                                <img style="height: 20px;width: 20px" src="<?php echo base_url('assets/upload/checked.jpeg');?>" title="Checked in" onclick="checked_in(<?php echo $data->id; ?>);"></img>
                                                <?php }else{ ?>
                                                <img style="height: 20px;width: 20px" src="<?php echo base_url('assets/upload/unchecked.jpeg');?>" title="Check in" onclick="checked_in(<?php echo $data->id; ?>);"></i>
                                                <?php } ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <i title="Approve" class="fa fa-print" aria-hidden="false" onclick="print_badge(<?php echo $data->event_id; ?>,<?php echo $data->user_id; ?>);"></i>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add Event Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function import_attendee() {
        window.location.href = "<?php echo base_url('admin_import'); ?>/"+<?php echo $all_data[0]->event_id; ?>;
    }

   /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function print_badge(eid,uid) {
        // $("#myAlert").on('shown.bs.modal', function () {
        //     $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_print_badge'); ?>/"+eid+"/"+uid;
        //     });

        //     $("#notification_heading").html("Confirmation");
        //     $("#notification_body").html("Do you want to approve this request?");
        // }).modal("show");
    }

    function checked_in(eid,uid) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_checkedin_status'); ?>/"+eid+"/"+uid;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to change the checked in status?");
        }).modal("show");
    }

    
</script>
