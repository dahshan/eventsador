<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="<?php echo base_url('admin_attendees'); ?>" ><i class="fa fa-users"></i>Event Attendees</a>
            <a href="<?php echo base_url('admin_view_attendees').'/'.$this->uri->segment(2); ?>" ><i class="fa fa-list"></i>Attendees List</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-print"></i>Print Badge</a>
        </div>

        <h1>Print Badge</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>Choose Badge Format</h5>
                        <!-- <button class="btn btn-success btn-cls" type="button" onclick="add();">Add Agenda</button> -->
                    </div>
                    <div class="widget-content nopadding">
                        <div class="container-fluid">
                            <div class="row" style="margin: 0;">
                                <div class="span6">
                                    <!-- <div class="box" style="border: 2px solid #333; padding: 30px; margin: 30px 0; cursor: pointer;" id="first_design">
                                        <div class="row" style="margin: 0;">
                                            <div class="span3">
                                                <img src="http://diylogodesigns.com/blog/wp-content/uploads/2016/04/abc-events-logo-design.png" alt="" class="img-responsive">
                                            </div>
                                            <div class="span9">
                                                <h3 style="margin: 0 0 10px;">Event Full Name</h3>
                                                <h4 style="margin: 0;">Event Tag Line</h4>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row" style="margin: 0;">
                                            <div class="span4">
                                                <div class="thumbnail">
                                                  <img src="https://images.fastcompany.net/image/upload/w_596,c_limit,q_auto:best,f_auto,fl_lossy/fc/3000587-inline-inline-why-mark-zuckerberg-would-hate-corporate-incubators.jpg" alt="" class="img-responsive">
                                                </div>
                                            </div>
                                            <div class="span8">
                                                <h1 style="margin: 0 0 15px;"><b>Person Name</b></h1>
                                                <h3 style="margin: 0 0 10px;">Designation</h3>
                                                <h3 style="margin: 0;">Company Name</h3>    
                                            </div>
                                        </div>
                                        <div class="barcode" style="float: right;"><img src="https://2d6qxj3uqdaw38d6lk27l0ao-wpengine.netdna-ssl.com/wp-content/uploads/2015/10/apb-qr-code.png" alt="" width="100"></div>
                                        <div class="clearfix"></div>
                                        <div class="bottom-bar" style="background: blue; padding: 15px; margin: -30px; color: #fff;">
                                            Event Address &nbsp;|&nbsp; Event Date &nbsp;|&nbsp; Event Time
                                        </div>
                                    </div> -->
                                    <table id="first_design" style="width: 100%; margin: 50px auto; border: solid 2px #09156f;">
                                        <tr>
                                            <td>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="padding: 20px; vertical-align: middle; width: 30%; text-align: center;">
                                                            <img src="http://diylogodesigns.com/blog/wp-content/uploads/2016/04/abc-events-logo-design.png" alt="" class="img-responsive">
                                                        </td>
                                                        <td style="padding: 20px; vertical-align: middle; text-align: left; font-family: sans-serif; font-size: 20px; color: #444;">
                                                            <h3 style="margin: 10px 0 10px; font-family: sans-serif; font-size: 26px; font-weight: 600; color: #444;">Event Full Name</h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 0 20px; width: 30%; vertical-align: top; text-align: center;">
                                                            <img src="https://images.fastcompany.net/image/upload/w_596,c_limit,q_auto:best,f_auto,fl_lossy/fc/3000587-inline-inline-why-mark-zuckerberg-would-hate-corporate-incubators.jpg" alt="" class="img-responsive">
                                                        </td>
                                                        <td style="padding: 0 20px; vertical-align: top; text-align: left; font-family: sans-serif; font-size: 20px; color: #444;">
                                                            <h1 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 32px; font-weight: bold; color: #000;">
                                                                <b>Person Name</b>
                                                            </h1>
                                                            <h3 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 22px; color: #444;">
                                                                Designation
                                                            </h3>
                                                            <h3 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 18px; color: #444;">
                                                                Company Name
                                                            </h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="padding: 20px;"></td>
                                                    </tr>
                                                    <tr class="vendorListHeading">
                                                        <td colspan="2" style="padding: 0; vertical-align: middle; text-align: left; font-family: sans-serif; font-size: 14px; color: #fff; border-top: solid 2px #09156f;">
                                                            <table style="width: 100%; background: #09156f !important;">
                                                                <tr>
                                                                    <td style="padding: 20px; background: #09156f !important; vertical-align: middle; text-align: left; font-family: sans-serif; font-size: 14px; color: #fff !important; width: 60%;">
                                                                        Event Address &nbsp;|&nbsp; Event Date &nbsp;|&nbsp; Event Time
                                                                    </td>
                                                                    <td style="padding: 20px; background: #09156f !important; height: 50px; vertical-align: middle; text-align: right; font-family: sans-serif; font-size: 20px;">
                                                                        <img src="https://2d6qxj3uqdaw38d6lk27l0ao-wpengine.netdna-ssl.com/wp-content/uploads/2015/10/apb-qr-code.png" alt="" style="width:100px;">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="span6">
                                    <!-- <div class="box" style="border: 2px solid #333; padding: 30px; margin: 30px 0; cursor: pointer;" id="second_design">
                                        <div class="row" style="margin: 0;">
                                            <div class="span3">
                                                <img src="http://diylogodesigns.com/blog/wp-content/uploads/2016/04/abc-events-logo-design.png" alt="" class="img-responsive">
                                            </div>
                                            <div class="span6">
                                                <h3 style="margin: 0 0 10px;">Event Full Name</h3>
                                                <h4 style="margin: 0;">Event Tag Line</h4>
                                            </div>
                                            <div class="span3">
                                                <div class="barcode" style="float: right; margin: -10px 0 0;"><img src="https://2d6qxj3uqdaw38d6lk27l0ao-wpengine.netdna-ssl.com/wp-content/uploads/2015/10/apb-qr-code.png" alt="" width="100"></div>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row" style="margin: 0;">
                                            <div class="col-md-4">
                                                <div class="thumbnail">
                                                  <img src="https://images.fastcompany.net/image/upload/w_596,c_limit,q_auto:best,f_auto,fl_lossy/fc/3000587-inline-inline-why-mark-zuckerberg-would-hate-corporate-incubators.jpg" alt="" class="img-responsive">
                                                </div>
                                            </div>
                                            <div class="span12">
                                                <h1 style="margin: 0 0 15px;"><b>Person Name</b></h1>
                                                <h3 style="margin: 0 0 10px;">Designation</h3>
                                                <h3 style="margin: 0;">Company Name</h3>    
                                            </div>
                                        </div>
                                        
                                        <div class="clearfix"></div>
                                        <div class="bottom-bar" style="background: green; padding: 15px; margin: 30px -30px -30px; color: #fff;">
                                            Event Address &nbsp;|&nbsp; Event Date &nbsp;|&nbsp; Event Time
                                        </div>
                                    </div> -->
                                    <table id="second_design" style="width: 100%; margin: 50px auto; border: solid 2px #178c04;">
                                        <tr>
                                            <td>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="padding: 20px; vertical-align: middle; width: 30%; text-align: center;">
                                                            <img src="http://diylogodesigns.com/blog/wp-content/uploads/2016/04/abc-events-logo-design.png" alt="" class="img-responsive">
                                                        </td>
                                                        <td style="padding: 20px 0; vertical-align: middle; text-align: left; font-family: sans-serif; font-size: 20px; color: #444;">
                                                            <h3 style="margin: 10px 0 10px; font-family: sans-serif; font-size: 26px; font-weight: 600; color: #444;">
                                                                Event Full Name
                                                            </h3>
                                                        </td>
                                                        <td style="padding: 20px; vertical-align: middle; width: 30%; text-align: center;">
                                                            <img src="https://2d6qxj3uqdaw38d6lk27l0ao-wpengine.netdna-ssl.com/wp-content/uploads/2015/10/apb-qr-code.png" alt="" width="100" style="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="padding: 0 20px; vertical-align: top; text-align: left; font-family: sans-serif; font-size: 20px; color: #444;">
                                                            <h1 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 32px; font-weight: bold; color: #000;">
                                                                <b>Person Name</b>
                                                            </h1>
                                                            <h3 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 22px; color: #444;">
                                                                Designation
                                                            </h3>
                                                            <h3 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 18px; color: #444;">
                                                                Company Name
                                                            </h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="padding: 20px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="padding: 0; vertical-align: middle; text-align: left; font-family: sans-serif; font-size: 14px; color: #fff; border-top: solid 2px #178c04;">
                                                            <table style="width: 100%; background: #178c04 !important;">
                                                                <tr>
                                                                    <td style="padding: 20px; background: #178c04 !important; vertical-align: middle; text-align: left; font-family: sans-serif; font-size: 14px; color: #fff !important;">
                                                                        Event Address &nbsp;|&nbsp; Event Date &nbsp;|&nbsp; Event Time
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row" style="margin: 0;">
                                <div class="span6">
                                    <!-- <div class="box" style="border: 2px solid #333; padding: 30px; margin: 30px 0; cursor: pointer;" id="third_design">
                                        <div class="top-bar" style="background: orange; padding: 15px; margin: -30px -30px 30px; color: #fff;">
                                            <div class="row" style="margin: 0;">
                                                <div class="span3">
                                                    <img src="http://diylogodesigns.com/blog/wp-content/uploads/2016/04/abc-events-logo-design.png" alt="" class="img-responsive">
                                                </div>
                                                <div class="span5">
                                                    <h3 style="margin: 0 0 10px;">Event Full Name</h3>
                                                    <h4 style="margin: 0;">Event Tag Line</h4>
                                                </div>
                                                <div class="span4">
                                                    <p>Event Address</p> 
                                                    <p>Event Date</p> 
                                                    <p style="margin-bottom: 0;">Event Time</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 0;">
                                            <div class="span12">
                                                <h1 style="margin: 0 0 15px;"><b>Person Name</b></h1>
                                                <h3 style="margin: 0 0 10px;">Designation</h3>
                                                <h3 style="margin: 0;">Company Name</h3>    
                                            </div>
                                        </div>
                                        <div class="barcode" style="float: right;"><img src="https://2d6qxj3uqdaw38d6lk27l0ao-wpengine.netdna-ssl.com/wp-content/uploads/2015/10/apb-qr-code.png" alt="" width="100"></div>
                                        <div class="clearfix"></div>
                                    </div> -->
                                    <table id="third_design" style="width: 100%; margin: 50px auto; border: solid 2px #FFA500;">
                                        <tr>
                                            <td>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="padding: 20px; background-color: #FFA500 !important; color: #fff !important; vertical-align: middle; width: 30%; text-align: center;">
                                                            <img src="http://diylogodesigns.com/blog/wp-content/uploads/2016/04/abc-events-logo-design.png" alt="" class="img-responsive">
                                                        </td>
                                                        <td style="padding: 20px 0; background-color: #FFA500 !important; color: #fff !important; vertical-align: middle; text-align: left; font-family: sans-serif; font-size: 20px;">
                                                            <h3 style="margin: 10px 0 10px; font-family: sans-serif; font-size: 26px; font-weight: 600; color: #fff !important;">
                                                                Event Full Name
                                                            </h3>
                                                        </td>
                                                        <td style="padding: 20px; background-color: #FFA500 !important; color: #fff !important; vertical-align: middle; width: 30%; text-align: left; font-size: 14px;">
                                                            Event Address &nbsp;|&nbsp; Event Date &nbsp;|&nbsp; Event Time
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="padding: 20px 10px 30px 30px; vertical-align: top; text-align: left; font-family: sans-serif; font-size: 20px; color: #444; ">
                                                            <h1 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 32px; font-weight: bold; color: #000;">
                                                                <b>Person Name</b>
                                                            </h1>
                                                            <h3 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 22px; color: #444;">
                                                                Designation
                                                            </h3>
                                                            <h3 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 18px; color: #444;">
                                                                Company Name
                                                            </h3>
                                                        </td>
                                                        <td style="padding: 20px 30px 30px 10px; vertical-align: middle; text-align: center; font-family: sans-serif; font-size: 14px; color: #fff !important; ">
                                                            <img src="https://2d6qxj3uqdaw38d6lk27l0ao-wpengine.netdna-ssl.com/wp-content/uploads/2015/10/apb-qr-code.png" alt="" width="100" style="">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
<script type="text/javascript">
    /**
     * Add Event Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_agenda'); ?>";
    }

   /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function approve(id,eid,uid) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_approve_request'); ?>/"+id+"/"+eid+"/"+uid;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to approve this request?");
        }).modal("show");
    }

    $(document).on("click","#first_design",function(){
        var url='<?php echo base_url('attendees/admin/openpage/1').'/'.$event_data->id.'/'.$user_data->id;?>';
        window.open(url, "_blank");
    });
    $(document).on("click","#second_design",function(){
        var url='<?php echo base_url('attendees/admin/openpage/2').'/'.$event_data->id.'/'.$user_data->id;?>';
        window.open(url, "_blank");
    });
    $(document).on("click","#third_design",function(){
        var url='<?php echo base_url('attendees/admin/openpage/3').'/'.$event_data->id.'/'.$user_data->id;?>';
        window.open(url, "_blank");
    });

    
</script>
