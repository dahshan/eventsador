<script src="<?php echo base_url(); ?>assets/admin/js/dropzone.js"></script>

<style>
/*just bg and body style*/
.container{
background-color:#1E2832;
padding-bottom:20px;
margin-top:10px;
border-radius:5px;
}
.center{
text-align:center;  
}
#top{
margin-top:20px;  
}
.btn-container{
background:#F2F2F2;
border-radius:5px;
padding-bottom:20px;
margin-bottom:20px;
}
.white{
color:white;
}
.imgupload{
color:#1E2832;
padding-top:40px;
font-size:7em;
}
#namefile{
color:black;
}
h4>strong{
color:#ff3f3f
}
.btn-primary{
border-color: #ff3f3f !important;
color: #ffffff;
text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
background-color: #ff3f3f !important;
border-color: #ff3f3f !important;
}

/*these two are set to not display at start*/
.imgupload.ok{
display:none;
color:green;
}
.imgupload.stop{
display:none;
color:red;
}


/*this sets the actual file input to overlay our button*/ 
#fileup{
opacity: 0;
-moz-opacity: 0;
filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);
width:200px;
cursor: pointer;
position:absolute;
left: 50%;
transform: translateX(-50%);
bottom: 40px;
height: 50px;
}

/*switch between input and not active input*/
#submitbtn{
  padding:5px 50px;
  display:none;
}
#fakebtn{
  padding:5px 40px;
}


/*www.emilianocostanzo.com*/
#sign{
  color:#1E2832;
  position:fixed;
  right:10px;
  bottom:10px;
  text-shadow:0px 0px 0px #1E2832;
  transition:all.3s;
}
#sign:hover{
  color:#1E2832;
  text-shadow:0px 0px 5px #1E2832;
}
.image_upload .uploader{opacity: 0; position: absolute;}
.image_upload .uploader span.filename{display: none;}
.image_upload .uploader span.action{width: 100%;}
div.uploader{width: 380px;}
</style>

<div id="content">
    <div id="content-header">
        
      
    </div>
    <div class="container-fluid">
       <!--  <form action="<?php echo base_url('admin/importitems');?>"
      class="dropzone"
      id="my-awesome-dropzone"></form> -->

      <!--  <form action="<?php echo base_url('admin/importitems');?>" method="post" enctype="multipart/form-data" id="importFrm">
                <input type="file" name="file" />
                <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
        </form> -->

        
    </div>

    <div class="container-fluid">
        
                <div class="widget-box">

                <div class="widget-content image_upload">
                   <h2 class="">Import Attendees</h2>
      <p class=""><a download href="<?php echo base_url('assets/upload/sample.csv');?>"><u>Download template file</u></a> and fill in with the attendees information.</p>
      <form name="upload" method="post" action="<?php echo base_url('admin_import_process');?>" enctype="multipart/form-data" accept-charset="utf-8">
   
      <div class="center">
        <div class="btn-container">
          <!--the three icons: default, ok file (img), error file (not an img)-->
          <h1 class="imgupload"><i class="fa fa-file-image-o"></i></h1>
          <h1 class="imgupload ok"><i class="fa fa-check"></i></h1>
          <h1 class="imgupload stop"><i class="fa fa-times"></i></h1>
          <!--this field changes dinamically displaying the filename we are trying to upload-->
          <p id="namefile">Only csv allowed! (csv)</p>
          <input type="file" value="" name="csvfile" id="fileup" style="width: 380px;">
          <input type="hidden" name="eid" value="<?php echo $event_id;?>">
          <!--our custom btn which which stays under the actual one-->
          <button type="button" id="btnup" class="btn btn-orange btn-lg">Drag and drop your file here
or select it on the computer</button>
          <!--this is the actual file input, is set with opacity=0 beacause we wanna see our custom one-->
          
        </div>
      </div>
    
      <!--additional fields-->
    <div class="text-right">     
      
        <!--the defauld disabled btn and the actual one shown only if the three fields are valid-->
        <a class="btn btn-orange" href="<?php echo base_url('admin_view_attendees').'/'.$event_id;?>">Cancel</a>
        <input type="submit" value="Upload" name="importSubmit" class="btn btn-primary" id="submitbtn">
        <!-- <button type="button" class="btn btn-default" disabled="disabled" id="fakebtn">Submit! <i class="fa fa-minus-circle"></i></button> -->
     
    </div>
  </form>
</div>

                </div>
                </div>

        </div>

    


<script type="text/javascript">


   $('#fileup').change(function(){
//here we take the file extension and set an array of valid extensions
    var res=$('#fileup').val();
    var arr = res.split("\\");
    var filename=arr.slice(-1)[0];
    filextension=filename.split(".");
    filext="."+filextension.slice(-1)[0];
    valid=[".csv"];
//if file is not valid we show the error icon, the red alert, and hide the submit button
    if (valid.indexOf(filext.toLowerCase())==-1){
        $( ".imgupload" ).hide("slow");
        $( ".imgupload.ok" ).hide("slow");
        $( ".imgupload.stop" ).show("slow");
      
        $('#namefile').css({"color":"red","font-weight":700});
        $('#namefile').html("File "+filename+" is not  pic!");
        
        $( "#submitbtn" ).hide();
        $( "#fakebtn" ).show();
    }else{
        //if file is valid we show the green alert and show the valid submit
        $( ".imgupload" ).hide("slow");
        $( ".imgupload.stop" ).hide("slow");
        $( ".imgupload.ok" ).show("slow");
      
        $('#namefile').css({"color":"green","font-weight":700});
        $('#namefile').html(filename);
      
        $( "#submitbtn" ).show();
        $( "#fakebtn" ).hide();
    }
});


</script>


