<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_wifi_access'); ?>" ><i class="fa fa-wifi"></i>Wifi Access</a>
            <a href="<?php echo base_url('admin_view_wifi_access').'/'.$this->uri->segment(2); ?>" ><i class="fa fa-list"></i>Wifi Access List</a>
            <a href="javascript:void(0);" class="current">Wifi Access Management</a>
        </div>

        <h1>Wifi Access</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Wifi Access</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_wifi_access').'/'.$this->uri->segment(2) : base_url('admin_update_wifi_access').'/'.$this->uri->segment(2) . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Wifi Name" type="text" id="name" name="name" value="<?php echo (!empty(set_value('name'))) ? set_value('title') : ((!empty($data_single->name)) ? $data_single->name : ''); ?>" >
                                    <?php echo form_error('name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Wifi ID</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Wifi Id" type="text" id="wifi_id" name="wifi_id" value="<?php echo (!empty(set_value('wifi_id'))) ? set_value('wifi_id') : ((!empty($data_single->wifi_id)) ? $data_single->wifi_id : ''); ?>" >
                                    <?php echo form_error('wifi_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <!-- <div class="control-group">
                                <label class="control-label">Username</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Username" type="text" id="username" name="username" value="<?php echo (!empty(set_value('username'))) ? set_value('title') : ((!empty($data_single->username)) ? $data_single->username : ''); ?>" >
                                    <?php echo form_error('username', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div> -->
                            <div class="control-group">
                                <label class="control-label">Details</label>
                                <div class="controls">
                                    <textarea class="span11" id="wifi_details" maxlength="50" id="details" name="details"><?php echo (!empty(set_value('details'))) ? set_value('details') : ((!empty($data_single->details)) ? $data_single->details : ''); ?></textarea>
                                    <p id="wifi_details_count"></p>
                                    <?php echo form_error('details', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Password</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Password" type="text" id="password" name="password" value="<?php echo (!empty(set_value('password'))) ? set_value('title') : ((!empty($data_single->password)) ? $data_single->password : ''); ?>" >
                                    <?php echo form_error('password', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Logo</label>
                                <div class="controls">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                      <img  src="<?php if(!empty($data_single->logo)){ echo base_url('assets/upload/wifi_access')."/".$data_single->logo; } else {echo base_url('assets/upload/placeholder.png');}?>" data-src="<?php if(!empty($data_single->logo)){ echo base_url('assets/upload/wifi_access')."/".$data_single->logo; } else {echo base_url('assets/upload/placeholder.png');}?>" alt="...">
                                    </div>
                                    <div>
                                      <div class="controls custon_textarea" style="margin-left: 0;">
                                        <input type="file" name="logo" style="line-height:1px;">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <!-- <input type="hidden" name="eventid" id="eventid" value="<?php if(!empty($data_single->id)) echo $data_single->id; ?>"> -->
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

$("#wifi_details_count").text("Characters left: " + (50 - $("#wifi_details").val().length));
$("#wifi_details").keyup(function(){
  $("#wifi_details_count").text("Characters left: " + (50 - $(this).val().length));
});

</script>
