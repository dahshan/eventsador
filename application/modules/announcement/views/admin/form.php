<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_announcement'); ?>" ><i class="fa fa-microphone"></i>Announcement</a>
            <a href="<?php echo base_url('admin_view_announcement').'/'.$this->uri->segment(2); ?>" ><i class="fa fa-list"></i>Announcement List</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-microphone"></i>Announcement Management</a>
        </div>

        <h1>Announcement</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Announcement</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_announcement').'/'.$this->uri->segment(2) : base_url('admin_update_announcement').'/'.$this->uri->segment(2) . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <!-- <div class="control-group">
                                <label class="control-label">Event</label>
                                <div class="controls">
                                    <select name="event_id" id="event_id">
                                        <?php
                                        if (count($all_event) > 0) {
                                            foreach ($all_event as $all_event_val) {
                                                ?>
                                                <option value="<?php echo $all_event_val->id; ?>"
                                                        <?php if(!empty($data_single)){if($all_event_val->id==$data_single->event_id)echo 'selected'; }?>>
                                                            <?php echo (isset($all_event_val->event_name) && $all_event_val->event_name != '') ? $all_event_val->event_name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('event_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div> -->
                            <div class="control-group">
                                <label class="control-label">Title</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Announcement Title" type="text" id="title" name="title" value="<?php echo (!empty(set_value('title'))) ? set_value('title') : ((!empty($data_single->title)) ? $data_single->title : ''); ?>" >
                                    <?php echo form_error('title', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Description</label>
                                <div class="controls">
                                    <textarea class="span11" id="description" name="description"><?php echo (!empty(set_value('description'))) ? set_value('description') : ((!empty($data_single->description)) ? $data_single->description : ''); ?></textarea>
                                    <?php echo form_error('description', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <!-- <input type="hidden" name="eventid" id="eventid" value="<?php if(!empty($data_single->id)) echo $data_single->id; ?>"> -->
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

