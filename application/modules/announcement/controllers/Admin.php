<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for announcement [HMVC]. Handles all the datatypes and methodes required for announcement section of Optus
 *
 * @author  
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Announcementmodel');
        $this->load->model('accomodation/admin/Accomodationmodel');
    }

    /**
     * Index Page for this announcement controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Announcementmodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'announcement/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of announcement module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($eid,$id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Announcementmodel->modify($eid,$id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Announcement already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Announcement modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_view_announcement').'/'.$eid);
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        if (!empty($id)) {
            $this->data['data_single'] = $this->Announcementmodel->load_single_data($id);
        }
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
                }
            }
        }
        else
        {
            $this->data['all_event'] = $this->Announcementmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
        }
        $this->middle = 'announcement/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for organizer selected or not
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function organizers_check($organizers) {
        $organizers = $this->input->post('organizers');
        if (!isset($organizers) || count($organizers) == 0) {
            $this->form_validation->set_message('organizers_check', '{field} Cannotbe blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $event_id = $this->input->post('event_id');
            $event_data = $this->Announcementmodel->get_row_data('announcement', ['id<>' => $event_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($event_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of announcement module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Announcementmodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Announcement status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Delete function of announcement module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Announcementmodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Announcement deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function listannouncement($eid) 
    {
        $all_data = $this->Announcementmodel->get_result_data('announcement',array("delete_flag"=>'N',"event_id"=>$eid));
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;
        $this->middle = 'announcement/admin/listannouncement';
        $this->admin_layout();
    }

}

/* End of file admin.php */
/* Location: ./application/modules/announcement/controllers/admin.php */
