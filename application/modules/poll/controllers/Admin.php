<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for Media [HMVC]. Handles all the datatypes and methodes required for Media section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

      /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Pollmodel');
    }

    /**
     * Index Page for this Media controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {

        if (!empty($_POST['delete'])) {
            admin_authenticate();
            foreach ($_POST['delete'] as $key => $value) {
                $flg = $this->Pollmodel->delete_data($value);
            }
            $this->session->set_flashdata('successmessage', 'Polls deleted successfully');
        }

        $all_data = $this->Pollmodel->load_events();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'poll/admin/list';
        $this->admin_layout();
    }

    public function search() {
        $this->load->model('../models/admin/Adminauthmodel');
        $eventid = $_POST['event'];

        $all_data = [];
        if ($eventid) {
            $all_data = $this->Pollmodel->load_polls($eventid);
        }

        $data['all_data'] = $all_data;
        $this->load->view('poll/admin/ajax_polls',$data);
    }



    /**
     * Add/Edit function of Media module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {

        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('poll_question', 'Poll Question', 'trim|required');
            $this->form_validation->set_rules('event_id', 'Event', 'trim|required');
           
            if ($param == "A") {
               // $this->form_validation->set_rules('poll_answer[]', 'Poll Answer', 'trim|required');
            }

            $poll_answer = $this->input->post('poll_answer');

            if(empty($poll_answer[0]))
            {
                $this->session->set_flashdata('errormessage', 'Please Select Poll Answer');

                redirect(base_url('admin_poll'));

                /*if (!empty($id)) {
                 redirect(base_url('admin_update_survey' . '/' . $id));
                }
                else
                {
                    redirect(base_url('admin_add_survey'));
                }*/

            }


            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Pollmodel->modify($id);
                if (!empty($flg)) {
                    $this->session->set_flashdata('successmessage', 'Poll added successfully');
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_poll'));
            }
            // else
            // {
            //     // echo "notok";die;
            //     $flg = $this->Pollmodel->modify($id);
            //     if (!empty($flg)) {
            //         $this->session->set_flashdata('successmessage', 'Poll modified successfully');
            //     } else {
            //         $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
            //     }
            //     redirect(base_url('admin_poll'));
            // }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Pollmodel->load_single_data($id);

        $this->data['all_answers']  = $this->Pollmodel->fetch_all_answers($id);
    //   if($this->session->userdata('admin_role_type')!=1)
    //     {
    //         if($this->session->userdata('admin_role_type')==2)
    //         {
    //             // $this->data['all_event'] = $this->Pollmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_start_date >=' => date("Y-m-d")]);
    //             $this->data['all_event'] = $this->Pollmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N']);
    //         }
    //         else
    //         {
    //             if($this->session->userdata('admin_org_id')!=1)
    //             {
    //                 // $this->data['all_event'] = $this->Pollmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_start_date >=' => date("Y-m-d")]);

    //                 $this->data['all_event'] = $this->Pollmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N']);
    //             }
    //         }
    //     }
    //     else
    //     {
    //       // $this->data['all_event'] = $this->Pollmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_start_date >=' => date("Y-m-d")]);
    //         $this->data['all_event'] = $this->Pollmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N']);
    //     }
        $this->data['all_event'] = $this->Pollmodel->load_all_events();


       /* echo "<pre>";
        print_r($this->data['all_event']);
        exit;*/

        $this->middle = 'poll/admin/form';
        $this->admin_layout();
    }

   

    /**
     * Status Change function of Media module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Pollmodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Poll status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_poll'));
    }

    /**
     * Delete function of Media module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {

        admin_authenticate();
        $id=base64_decode(urldecode($id));

        $flg = $this->Pollmodel->delete_data($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Poll deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

         redirect(base_url('admin_poll'));
    }





}

/* End of file admin.php */
/* Location: ./application/modules/media/controllers/admin.php */
