<script>
$(document).ready(function () {

    $("button#add").click(function(){   
    //$(".abcd:last").clone().appendTo(".wrapper");
    $(".abcd:last").clone(true).find("input:text").val("").end().appendTo(".wrapper");  
});
$(".glyphicon-remove").click(function () {

       $(this).closest(".abcd").remove();
    });

});
</script>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_poll'); ?>" ><i class="fa fa-bar-chart"></i>Poll</a>
            <a href="javascript:void(0);" class="current">Poll Management</a>
        </div>

        <h1>Poll</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Poll</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_poll') : base_url('admin_update_poll') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Select Event</label>
                                <div class="controls">
                                    <select name="event_id" >
                                        <?php
                                        if (count($all_event) > 0) {
                                            foreach ($all_event AS $event) {
                                                $users = explode(',',$event->users);
                                                if(($this->session->userdata('admin_role_type')==1) || (in_array($this->session->userdata('admin_uid'),$users)) || ($this->session->userdata('admin_role_type')==2) ){
                                                ?>
                                                <option value="<?php echo (isset($event->id) && $event->id != '') ? $event->id : ''; ?>"
                                                        <?php if(!empty($data_single)){ if($event->id==$data_single->event_id){ ?> selected <?php } }?>>
                                                            <?php echo (isset($event->event_name) && $event->event_name != '') ? $event->event_name : ''; ?> 
                                                </option>
                                                <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('event_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Poll Question</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Poll Question" type="text" id="poll_question" name="poll_question" value="<?php echo (!empty(set_value('poll_question'))) ? set_value('poll_question') : ((!empty($data_single->poll_question)) ? $data_single->poll_question : ''); ?>" >
                                    <?php echo form_error('poll_question', '<div style="color:red;">', '</div>'); ?>
                                </div>
                               
                            </div>


                            <?php


                              if(!empty($all_answers))
                              {
                                for($i=0;$i<count($all_answers);$i++){
                                ?>

                                  <div class="control-group option-group">
                                      
                                      <div class="controls">


                                      <div class="form-group abcd" id="abcde">      
                                          <div class="col-sm-12" >
                                            <div class="btn-group week">
                                               <input  placeholder="Poll Answer" type="text" id="poll_answer" name="poll_answer[]" value="<?php echo (!empty($all_answers[$i]->poll_answer)) ? $all_answers[$i]->poll_answer : ''; ?>">
                                                                 
                                                                   
                                           </div> 
                                         
                                              <span class="glyphicon glyphicon-remove"><i class="icon-remove"></i></span>
                                        </div>
                                     </div>
                                              
                                          <!-- <label class="remove">Delete</label> -->
                                      </div>
                                  </div>

                              <?php
                              }
                             }
                             ?>


                            <div class="control-group option-group">
                                      
                                  <div class="controls wrapper">
                                        
                                   
                                    <div class="form-group abcd" id="abcde">      
                                      <div class="col-sm-12" >
                                        <div class="btn-group week">
                                          <input  placeholder="Poll Answer" type="text" id="poll_answer" name="poll_answer[]" value="<?php echo (!empty($all_answers->poll_answer)) ? $all_answers->poll_answer : ''; ?>">          
                                        <?php echo form_error('poll_answer[]', '<div style="color:red;">', '</div>'); ?>                       
                                       </div> 
                                      
                                          <span class="glyphicon glyphicon-remove"><i class="icon-remove"></i></span>
                                    
                                    </div>
                                    </div>

                                </div>

                            </div>

                               <div class="control-group">
                                <div class="widget-content">
                                    <button type="button" id="add" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus" aria-hidden="true"></i> Add Poll Answer</button>
                                    
                                    <!-- <div class="fa fa-plus option-show" id="option-show">Add option</div> -->
                                    <!-- <input type="button" value="ADD OPTION" id="add_phone_number"> -->
                                </div>
                                </div>



                           <!--  <div class="control-group">
                                <label class="control-label">Poll Answer</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Poll Answer" type="text" id="poll_answer" name="poll_answer[]" value="<?php echo (!empty(set_value('poll_answer'))) ? set_value('poll_answer') : ((!empty($data_single->poll_answer)) ? $data_single->poll_answer : ''); ?>" ><br>
                                    <input class="span11" placeholder="Enter Poll Answer" type="text" id="poll_answer" name="poll_answer[]" value="<?php echo (!empty(set_value('poll_answer'))) ? set_value('poll_answer') : ((!empty($data_single->poll_answer)) ? $data_single->poll_answer : ''); ?>" ><br>
                                    <input class="span11" placeholder="Enter Poll Answer" type="text" id="poll_answer" name="poll_answer[]" value="<?php echo (!empty(set_value('poll_answer'))) ? set_value('poll_answer') : ((!empty($data_single->poll_answer)) ? $data_single->poll_answer : ''); ?>" ><br>
                                    <input class="span11" placeholder="Enter Poll Answer" type="text" id="poll_answer" name="poll_answer[]" value="<?php echo (!empty(set_value('poll_answer'))) ? set_value('poll_answer') : ((!empty($data_single->poll_answer)) ? $data_single->poll_answer : ''); ?>" ><br>
                                    <?php echo form_error('poll_answer', '<div style="color:red;">', '</div>'); ?>



                                </div>
                               
                            </div> -->
                           
                            <input type="hidden" name="media_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

