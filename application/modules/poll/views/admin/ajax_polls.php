<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>

                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>                       
                    </div>
                    <div class="widget-content nopadding">
                        <form id="delete" method="post" action="<?php echo base_url('admin_poll'); ?>">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Poll Question</th>
                                    <th>Poll Create Date</th>
                                   <!--  <th>Poll End Date</th> -->
                                     <?php if(in_array(120, $getRole)){ ?> <th>Action</th><?php } ?>
                                     <?php if(in_array(120, $getRole)){ ?> <th>Select</th><?php } ?>
                                   
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <?php if(in_array(119, $getRole)){ ?>  <u><a href="<?php echo base_url('admin_update_poll' . '/' . $data->id); ?>">
                                                    <?php echo $data->poll_question; ?>
                                                </a></u> <?php }else{ echo $data->poll_question; } ?> 
                                                
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo date('d-m-Y',strtotime($data->poll_create_date)); ?>
                                            </td>
                                            <!-- <td class="custom-action-btn">
                                                <?php echo date('d-m-Y',strtotime($data->poll_end_date)); ?>
                                            </td> -->
                                           <?php if(in_array(120, $getRole)){ ?> <td class="custom-action-btn">

                                                
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_poll('<?php echo urlencode(base64_encode($data->id)); ?>');"></i>
                                            </td> <?php } ?> 
                                            <?php if(in_array(120, $getRole)){ ?> 
                                                <td class="custom-action-btn">
                                                    <input type='checkbox' name='delete[]' value='<?php echo $data->id; ?>' >
                                                </td>
                                            <?php } ?> 
                                           
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php if(in_array(120, $getRole)){ ?><button class="btn btn-danger btn-cls" type="button" id="deletepollsbutton" style="margin-top: 20px;" onclick="delete_polls();">Delete Polls</button><?php } ?>
                        </form>


                    </div>