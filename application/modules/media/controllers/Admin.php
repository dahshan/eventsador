<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for Media [HMVC]. Handles all the datatypes and methodes required for Media section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller
{

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct()
    {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Mediamodel');
        $this->load->model('point/admin/pointmodel');
    }

    /**
     * Index Page for this Media controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index()
    {
        $all_data = $this->Mediamodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'media/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of Media module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL)
    {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            if ($param == "A") {
                $this->form_validation->set_rules('video_file', 'Video', 'callback_video_check');
                $this->form_validation->set_rules('img_file', 'Image', 'callback_img_check');
            }

            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Mediamodel->modify($id);
                if (!empty($flg)) {
                    $this->session->set_flashdata('successmessage', 'Media modified successfully');
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_media'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Mediamodel->load_single_data($id);
        // $this->data['all_event'] = $this->Mediamodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
        if ($this->session->userdata('admin_role_type') != 1) {
            if ($this->session->userdata('admin_role_type') == 2) {
                $this->data['all_event'] = $this->Mediamodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'), 'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
            } else {
                if ($this->session->userdata('admin_org_id') != 1) {
                    $this->data['all_event'] = $this->Mediamodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'), 'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
                }
            }
        } else {
            $this->data['all_event'] = $this->Mediamodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
        }
        $this->middle = 'media/admin/form';
        $this->admin_layout();
    }

    public function formimage($id)
    {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('img_file', 'Image', 'callback_img_check');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Mediamodel->modify_image($id);
                if (!empty($flg)) {
                    $this->session->set_flashdata('successmessage', 'Image added successfully');
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_view_media_image') . '/' . $id);
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        $this->middle = 'media/admin/formimage';
        $this->admin_layout();
    }

    public function formvideo($id)
    {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('video_file', 'Video', 'callback_video_check');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Mediamodel->modify_video($id);
                if (!empty($flg)) {
                    $this->session->set_flashdata('successmessage', 'Video added successfully');
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_view_media_video') . '/' . $id);
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        $this->middle = 'media/admin/formvideo';
        $this->admin_layout();
    }

    public function formdocument($id, $mid = NULL)
    {
        if (isset($_POST['submit'])) {
            if (empty($mid)) {
                $this->form_validation->set_rules('doc_file', 'Document', 'callback_doc_check');
            }
            $this->form_validation->set_rules('document_text', 'Document Text', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Mediamodel->modify_document($id, $mid);
                if (!empty($flg)) {
                    $this->session->set_flashdata('successmessage', 'Document added successfully');
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_view_media_document') . '/' . $id);
            }
        }
        $this->data = array();
        if (!empty($mid)) {
            $this->data['data_single'] = $this->Mediamodel->get_row_data("media_files", array("id" => $mid));
        }

        $this->data['id'] = $id;
        // $this->data['id'] = $mid;
        $this->middle = 'media/admin/formdocument';
        $this->admin_layout();
    }

    /**
     * Used for video validation
     *
     * @access    public
     * @param    none
     * @return    boolean
     */
    public function video_check($email)
    {
        $file = $_FILES['video_file'];
        if (count($file['name']) == 1 && empty($file['name'][0])) {
            $video_error = 1;
            $this->form_validation->set_message('video_check', '{field} cannot blank');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Used for image validation
     *
     * @access    public
     * @param    none
     * @return    boolean
     */
    public function img_check($email)
    {
        $file = $_FILES['img_file'];
        if (count($file['name']) == 1 && empty($file['name'][0])) {
            $img_error = 1;
            $this->form_validation->set_message('img_check', '{field} cannot blank');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function doc_check($email)
    {
        $file = $_FILES['doc_file'];
        if (count($file['name']) == 1 && empty($file['name'][0])) {
            $img_error = 1;
            $this->form_validation->set_message('doc_check', '{field} cannot blank');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function listviewimage($eid)
    {
        $all_data = $this->Mediamodel->get_result_data('media_files', array("delete_flag" => 'N', "type" => 'P', "event_id" => $eid));
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;

        $this->middle = 'media/admin/listimage';
        $this->admin_layout();
    }

    public function listviewimage_pending($eid)
    {
        $all_data = $this->Mediamodel->get_result_data('media_files', array("delete_flag" => 'N', "type" => 'P', "event_id" => $eid, "approval_status" => 0));
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;

        $this->middle = 'media/admin/listimage_pending';
        $this->admin_layout();
    }

    public function listviewvideo($eid)
    {
        $all_data = $this->Mediamodel->get_result_data('media_files', array("delete_flag" => 'N', "type" => 'V', "event_id" => $eid));
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;
        $this->middle = 'media/admin/listvideo';
        $this->admin_layout();
    }

    public function listviewvideo_pending($eid)
    {
        $all_data = $this->Mediamodel->get_result_data('media_files', array("delete_flag" => 'N', "type" => 'V', "event_id" => $eid, "approval_status" => 0));
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;
        $this->middle = 'media/admin/listvideo_pending';
        $this->admin_layout();
    }

    public function listviewdocument($eid)
    {
        $all_data = $this->Mediamodel->get_result_data('media_files', array("delete_flag" => 'N', "type" => 'D', "event_id" => $eid));
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;
        $this->middle = 'media/admin/listdocument';
        $this->admin_layout();
    }

    /**
     * Status Change function of Media module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id)
    {
        $flg = $this->Mediamodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Media status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_media'));
    }

    /**
     * Delete function of Media module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id)
    {
        $flg = $this->Mediamodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Media deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Delete function of Media file module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete_file($id, $is_pending = 0)
    {
        $file = $this->Mediamodel->get_row_data('media_files', ['id' => $id]);
        $flg = $this->Mediamodel->delete_file($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Media file deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }


        redirect(base_url('admin_update_media/') . $file->event_id);
    }


    public function media_approve_status($id, $event_id, $is_pending = 0)
    {
        admin_authenticate();
        $id = base64_decode(urldecode($id));
        //$event_id=$event_id;
        $status = $this->Mediamodel->media_approve_status($id);

        // echo 'test ? disapprove?'; exit;

        if (!empty($status)) {
            $info = $this->pointmodel->get_row_data("media_files", array("id" => $id));
            $data['user_id'] = $info->uploaded_by_id;
            $data['media_id'] = $id;
            $data['point_type'] = "Picture";
            $data['point'] = 2;
            $data['event_id'] = $info->event_id;
            $data['created_date'] = date("Y-m-d H:i:s");
            $this->pointmodel->insert_data("point", $data);

            $media = $this->Mediamodel->get_row_data('media_files', ['id' => $id]);
            $sender = $this->Mediamodel->get_row_data('user', ['id' => $this->session->userdata('admin_uid')]);
            $receiver = $this->Mediamodel->get_row_data('user', ['id' => $media->uploaded_by_id]);
            $title = "Photo Approval";
            $message = "Congratulations, your photo has been published";
            $payload = ['title' => $title, 'message' => $message,
                'sender_id' => $sender->id, 'receiver_id' => $receiver->id,
                'event_id' => $event_id, 'callback' => 'ANNOUNCE']; //echo json_encode($payload); exit;
            $this->Mediamodel->processApprove($payload);
            $this->notification($receiver->fcm_reg_token, $title, $message, $payload, $receiver->device_type);//exit;
            $this->session->set_flashdata('successmessage', 'Media Approved');
        } else {
            $where['media_id'] = $id;
            $where['point_type'] = "Picture";
            $this->pointmodel->delete_data("point", $where);
            $this->session->set_flashdata('successmessage', 'Media Not Approved');
        }

        //redirect(base_url('admin_view_media_image').'/'.$id);
        $url = $is_pending ? base_url('admin_view_media_image_pending') : base_url('admin_view_media_image');
        $redirect = $url . '/' . $event_id;
        redirect($redirect);
    }


    // When approve
    public function media_video_approve_status($id, $event_id, $is_pending = 0)
    {
        admin_authenticate();
        $id = base64_decode(urldecode($id));
        //$event_id=$event_id;
        $status = $this->Mediamodel->media_approve_status($id);

        // echo 'test ? approve?'; exit;

        if (!empty($status)) {
            $info = $this->pointmodel->get_row_data("media_files", array("id" => $id));
            $data['user_id'] = $info->uploaded_by_id;
            $data['media_id'] = $id;
            $data['point_type'] = "Video";
            $data['point'] = 2;
            $data['created_date'] = date("Y-m-d H:i:s");
            $this->pointmodel->insert_data("point", $data);

            $media = $this->Mediamodel->get_row_data('media_files', ['id' => $id]);
            $sender = $this->Mediamodel->get_row_data('user', ['id' => $this->session->userdata('admin_uid')]);
            $receiver = $this->Mediamodel->get_row_data('user', ['id' => $media->uploaded_by_id]);
            $title = "Photo Approval";
            $message = "Congratulations, your video has been published";
            $payload = ['title' => $title, 'message' => $message,
                'sender_id' => $sender->id, 'receiver_id' => $receiver->id,
                'event_id' => $event_id, 'callback' => 'ANNOUNCE']; //echo json_encode($payload); exit;
            $this->Mediamodel->processApprove($payload);
            $this->notification($receiver->fcm_reg_token, $title, $message, $payload, $receiver->device_type);//exit;
            $this->session->set_flashdata('successmessage', 'Media Approved');
        } else {
            $where['media_id'] = $id;
            $where['point_type'] = "Video";
            $this->pointmodel->delete_data("point", $where);
            $this->session->set_flashdata('successmessage', 'Media Not Approved');
        }

        $url = $is_pending ? base_url('admin_view_media_video_pending') : base_url('admin_view_media_video');
        //redirect(base_url('admin_view_media_image').'/'.$id);
        $redirect = $url . '/' . $event_id;
        redirect($redirect);
    }

    public function notification_old($fcm, $text, $title, $data = false)
    {
        $msg = $title;
        $msgg = $text;
        $this->load->library('gcmpushmessage', 'AAAAQQY2SPA:APA91bHXm3KVLxckAVs2-hYNfbif6fxdN-eXTEHNUnBG3efje1vvv2aYNTJs2usgZNmDFeHy5EH0wwynQ4vtG6t8DYb-MuDfJC4KOd_Y1kgOowCJOh1pe9ZF8avPpI80oCM_ffWGl6Mu');
        // $gcmregtoken = "ckpnfYyp5jc:APA91bFd8ZVoSHFd9URKne44NqgOMOZZk6Ix1aBTmfY8Y2bUloY3Sgl4tHjJ5uJgoLmUUuMVCBSC6vggK4ZbhtoFPfjLFn1lOmuISGwIETV-hw2UT_rKw57em5-XLGTdsLbP9GvNBBly";
        $gcmregtoken = $fcm;
        $this->gcmpushmessage->setDevices($gcmregtoken);
        $content = ['title' => $title, 'body' => "Eventsador"];
        if ($data) $content['payload'] = $data;
        $r = $this->gcmpushmessage->send($text, $content);
        // print_r($r);die;
        return $r;
    }

    public function notification($fcm, $text, $title, $data = false, $type = 'android')
    {
        $this->load->library('PushNotification');
        $r = false;
        if ($type == 'android') $r = $this->pushnotification->sendNotificationAndroid($fcm, $title, $text, $data);
        else if ($type == 'ios') $r = $this->pushnotification->sendNotificationIos($fcm, $title, $text, $data);
        else $r = false;
        return $r;
    }
}

/* End of file admin.php */
/* Location: ./application/modules/media/controllers/admin.php */
