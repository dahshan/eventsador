<?php

/**
 * Media Model Class. Handles all the datatypes and methodes required for handling Media
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Mediamodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of Media for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the Media that has been added by current admin [Table:  ets_media]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('id,event_name');
        $this->db->from(tablename('event'));
        $this->db->where('event.delete_flag', 'N');
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->db->where('event.organizers', $this->session->userdata('admin_uid'));
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->db->where('event.organizers', $this->session->userdata('admin_org_id'));
                }
                $this->db->where("FIND_IN_SET(".$this->session->userdata('admin_uid').", users) !=", 0);
            }

            
        }
        $this->db->order_by("event.id", "asc");

        $query = $this->db->get();
        $result = $query->result();

        if(!empty($result))
        {
            $fina_result=array();
            foreach($result as $val)
            {
                $val->picture_count=count($this->get_result_data("media_files", array("delete_flag"=>'N',"type"=>'P',"event_id"=>$val->id)));
                $val->picture_count_pending = count($this->get_result_data("media_files", array("delete_flag"=>'N',"type"=>'P',"event_id"=>$val->id, "approval_status"=> 0)));
                $val->video_count=count($this->get_result_data("media_files", array("delete_flag"=>'N',"type"=>'V',"event_id"=>$val->id)));
                $val->video_count_pending=count($this->get_result_data("media_files", array("delete_flag"=>'N',"type"=>'V',"event_id"=>$val->id, "approval_status"=> 0)));
                $val->document_count=count($this->get_result_data("media_files", array("delete_flag"=>'N',"type"=>'D',"event_id"=>$val->id)));
                $fina_result[]=$val;
            }
            $result=$fina_result;
        }
        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single Media by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single Media by id that has been added by current admin [Table:  ets_media]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('media'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1") {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->result();
    }

    /**
     * Used for Save(Insert/Update) functionality of single Media for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single Media w.r.t. current admin [Table:  ets_media]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        $event_id = $this->input->post('event_id');
        // Image Upload Validation
        $file = $_FILES['img_file'];
        if (isset($file['name']) && $file['name'] != '' && count($file['name'])) {
            foreach ($file['name'] AS $file_name_key => $file_name) {
                if (!empty($file['name'][$file_name_key])) {
                    $info = pathinfo($file['name'][$file_name_key]);
                    $ext = $info['extension']; // get the extension of the file
                    $newname = "media_" . time() . $file_name_key . "." . $ext;
                    $target = __DIR__ . '/../../../../../assets/upload/media_files/' . $newname;
                    move_uploaded_file($_FILES['img_file']['tmp_name'][$file_name_key], $target);
                    $data = array(
                        'event_id'      => $event_id,
                        'file_name'     => $newname,
                        'type'          => 'P',
                        'delete_flag'   => 'N',
                        'entry_date'    => date('Y-m-d H:i:s'),
                        'uploaded_by_id' => '1',
                        'uploaded_by'   => 'admin',
                        'approval_status' => '0'
                    );
                    $this->db->insert(tablename('media_files'), $data);
                }
            }
        }
        // Video Upload Validation
        $file = $_FILES['video_file'];
        if (isset($file['name']) && $file['name'] != '' && count($file['name'])) {
            foreach ($file['name'] AS $file_name_key => $file_name) {
                if (!empty($file['name'][$file_name_key])) {
                    $info = pathinfo($file['name'][$file_name_key]);
                    $ext = $info['extension']; // get the extension of the file
                    $newname = "media_" . time() . $file_name_key . "." . $ext;
                    $target = __DIR__ . '/../../../../../assets/upload/media_files/' . $newname;
                    move_uploaded_file($_FILES['video_file']['tmp_name'][$file_name_key], $target);
                    $data = array(
                        'event_id' => $event_id,
                        'file_name' => $newname,
                        'type' => 'V',
                        'delete_flag' => 'N',
                        'entry_date' => date('Y-m-d H:i:s'),
                        'uploaded_by_id' => '1',
                        'uploaded_by'   => 'admin',
                        'approval_status' => '0'
                    );
                    $this->db->insert(tablename('media_files'), $data);
                }
            }
        }
        // Video Document Validation
        $file = $_FILES['doc_file'];
        if (isset($file['name']) && $file['name'] != '' && count($file['name'])) {
            foreach ($file['name'] AS $file_name_key => $file_name) {
                if (!empty($file['name'][$file_name_key])) {
                    $info = pathinfo($file['name'][$file_name_key]);
                    $ext = $info['extension']; // get the extension of the file
                    $newname = "media_" . time() . $file_name_key . "." . $ext;
                    $target = __DIR__ . '/../../../../../assets/upload/media_files/' . $newname;
                    move_uploaded_file($_FILES['doc_file']['tmp_name'][$file_name_key], $target);
                    $data = array(
                        'event_id' => $event_id,
                        'file_name' => $newname,
                        'type' => 'D',
                        'delete_flag' => 'N',
                        'entry_date' => date('Y-m-d H:i:s'),
                        'uploaded_by_id' => '1',
                        'uploaded_by'   => 'admin',
                        'approval_status' => '0'
                    );
                    $this->db->insert(tablename('media_files'), $data);
                }
            }
        }
        return true;
    }

    public function modify_image($id) {
        // Image Upload Validation
        $file = $_FILES['img_file'];
        if (isset($file['name']) && $file['name'] != '' && count($file['name'])) {
            foreach ($file['name'] AS $file_name_key => $file_name) {
                if (!empty($file['name'][$file_name_key])) {
                    $info = pathinfo($file['name'][$file_name_key]);
                    $ext = $info['extension']; // get the extension of the file
                    $newname = "media_" . time() . $file_name_key . "." . $ext;
                    $target = __DIR__ . '/../../../../../assets/upload/media_files/' . $newname;
                    move_uploaded_file($_FILES['img_file']['tmp_name'][$file_name_key], $target);
                    $data = array(
                        'event_id' => $id,
                        'file_name' => $newname,
                        'type' => 'P',
                        'delete_flag' => 'N',
                        'entry_date' => date('Y-m-d H:i:s'),
                        'uploaded_by_id' => '1',
                        'uploaded_by'   => 'admin',
                        'approval_status' => '0',
                        'tag' => $_POST['tag'],
                    );
                    $this->db->insert(tablename('media_files'), $data);
                }
            }
        }
        return true;
    }

    public function modify_video($id) {
        // Video Upload Validation
        $file = $_FILES['video_file'];
        if (isset($file['name']) && $file['name'] != '' && count($file['name'])) {
            foreach ($file['name'] AS $file_name_key => $file_name) {
                if (!empty($file['name'][$file_name_key])) {
                    $info = pathinfo($file['name'][$file_name_key]);
                    $ext = $info['extension']; // get the extension of the file
                    $newname = "media_" . time() . $file_name_key . "." . $ext;
                    $target = __DIR__ . '/../../../../../assets/upload/media_files/' . $newname;
                    move_uploaded_file($_FILES['video_file']['tmp_name'][$file_name_key], $target);
                    $data = array(
                        'event_id' => $id,
                        'file_name' => $newname,
                        'type' => 'V',
                        'delete_flag' => 'N',
                        'entry_date' => date('Y-m-d H:i:s'),
                        'uploaded_by_id' => '1',
                        'uploaded_by'   => 'admin',
                        'approval_status' => '0'
                    );
                    $this->db->insert(tablename('media_files'), $data);
                }
            }
        }
        return true;
    }

    public function modify_document($id,$mid=NULL) {
        // Video Upload Validation
        $file = $_FILES['doc_file'];
        if (isset($file['name']) && $file['name'] != '' && count($file['name'])) {
            foreach ($file['name'] AS $file_name_key => $file_name) {
                if (!empty($file['name'][$file_name_key])) {
                    $info = pathinfo($file['name'][$file_name_key]);
                    $ext = $info['extension']; // get the extension of the file
                    $newname = "media_" . time() . $file_name_key . "." . $ext;
                    $data['file_name'] = $newname;
                    $target = __DIR__ . '/../../../../../assets/upload/media_files/' . $newname;
                    move_uploaded_file($_FILES['doc_file']['tmp_name'][$file_name_key], $target);
                }
            }
            $data['event_id'] = $id;
            $data['document_text'] = $this->input->post('document_text');
            if(empty($mid))
            {
                $data['type'] = 'D';
                $data['delete_flag'] = 'N';
                $data['uploaded_by_id'] = '1';
                $data['uploaded_by']   = 'admin';
                $data['entry_date'] = date('Y-m-d H:i:s');
                $data['approval_status'] = '1';
                $this->db->insert(tablename('media_files'), $data);
            }
            else
            {
                $this->db->update(tablename('media_files'),$data, array("id"=>$mid));
            }
        }
        return true;
    }

    /**
     * Used for change status functionality of Media for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current Media status
     * and change it the the opposite [Table: pb_media]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('media'));
        $this->db->where('id', $id);
        $this->db->where('delete_flag', 'N');

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('media'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    /**
     * Used for delete functionality of Media for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_media]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('media_files'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    public function media_approve_status($id)
    {
        $sql="select approval_status from ".tablename('media_files')."  where id='".$id."' ";
        $query=$this->db->query($sql);
        $adminstatus=$query->row();
        if($adminstatus->approval_status==0)
        {
            $approval_status=1;
        }
        else
        {
            $approval_status=0;
        }
        $sq="update ".tablename('media_files')."  set approval_status='".$approval_status."' where id='".$id."'";
        $qq=$this->db->query($sq);
        if(!empty($qq))
        {
            return $approval_status;
        }
        else
        {
            return $approval_status;
        }
    }

    /**
     * Used for delete functionality of Media file for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, and deletes it [Table: pb_media]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function delete_file($id) {
        $delete_faq = array('delete_flag' => 'Y');
        $this->db->where('id', $id);

        if ($this->db->update(tablename('media_files'), $delete_faq, ['id' => $id])) {
            return 1;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }


    public function processApprove($payload){
        $noti_data = [
            'event_id'=> $payload['event_id'],
            'notification_title' => $payload['title'],
            'notification_text' => $payload['message'],
            'receivers'=> 0,
            'created_by'=> $payload['sender_id'],
            'option_noti' => 'app',
            'created_date' => date('Y-m-d H:i:s')
            ];
        $this->db->insert('notification', $noti_data);
        $noti_id = $this->db->insert_id();
        
        $touser_data = ['noti_id'=> $noti_id, 'user_id'=> $payload['receiver_id']];
        $this->db->insert('notification_touser', $touser_data);
    }


    public function processNotification($payload){
        $sender = $payload['sender_id'];
        $receiver = $payload['receiver_id'];
        // $whr = "((receiver_id=$receiver AND sender_id=$sender) OR (sender_id=$receiver AND receiver_id=$sender))";
        // $chat_head = $this->db->select('*')->where($whr)->where('event_id', $payload['event_id'])->get('chat_head')->row();

        // if ($payload['sender_id'] == $payload['receiver_id']) return;
        // $chat_data = ['event_id'=> $payload['event_id'], 'sender_id'=> $payload['sender_id'], 'receiver_id'=> $payload['receiver_id'],
        //                 'message'=> '<'.$payload['title'].'>'.$payload['message'], 'say_hi'=> '0', 'flag'=> '0', 'created_date'=> date('Y-m-d H:i:s')];
        // if (!$chat_head){
        //     $chat_head_data = ['event_id'=> $payload['event_id'], 'sender_id'=> $payload['sender_id'], 'receiver_id'=> $payload['receiver_id']
        //             , 'message'=> '<'.$payload['title'].'> '.$payload['message'], 'say_hi'=> '1', 'created_date'=> date('Y-m-d H:i:s')];
        //     $this->db->insert('chat_head', $chat_head_data);
            
        //     $chat_data['flag'] = '0'; $chat_data['say_hi'] = '1';
        // }
        // $this->db->insert('attendees_chat', $chat_data);
    }

}

/* End of file Mediamodel.php */
/* Location: ./application/modules/media/models/admin/Mediamodel.php */
