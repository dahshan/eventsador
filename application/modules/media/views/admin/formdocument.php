<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_media'); ?>"><i class="fa fa-medium"></i>Media</a>
            <a href="<?php echo base_url('admin_view_media_document').'/'.$this->uri->segment(2); ?>" class="current"><i class="fa fa-file-pdf-o"></i>Media Documents</a>
            <a href="javascript:void(0);" class="current">Media Document Management</a>
        </div>

        <h1>Media</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Media</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo (empty($data_single)) ? base_url('admin_add_media_document'). '/' . $id : base_url('admin_update_media_document') . '/' . $data_single->event_id. '/' . $data_single->id; ?>"  name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Document</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Document" type="text" id="document_text" name="document_text" value="<?php echo (!empty(set_value('document_text'))) ? set_value('title') : ((!empty($data_single->document_text)) ? $data_single->document_text : ''); ?>" >
                                    <?php echo form_error('document_text', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Choose Document</label>
                                <div class="controls">
                                    <input value="dfgdfg" type="file" name="doc_file[]" id="doc_file" accept="application/pdf" >
                                    <span class="filename"><?php echo (!empty(set_value('file_name'))) ? set_value('file_name') : ((!empty($data_single->file_name)) ? $data_single->file_name : ''); ?></span>
                                    <?php echo form_error('doc_file', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <input type="hidden" name="media_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

