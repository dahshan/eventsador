<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_media'); ?>"><i class="fa fa-medium"></i>Media</a>
            <a href="<?php echo base_url('admin_view_media_image').'/'.$this->uri->segment(2); ?>"><i class="fa fa-image"></i>Media Images</a>
            <a href="javascript:void(0);" class="current">Media Image Management</a>
        </div>

        <h1>Media</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Media</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('admin_add_media_image') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Choose Photo</label>
                                <div class="controls">
                                    <input type="file" name="img_file[]" id="img_file" accept="image/*" multiple>
                                    <?php echo form_error('img_file', '<div style="color:red;">', '</div>'); ?>
                                </div>
                                <div class="controls">
                                    <?php
                                    if (!empty($data_single->id)) {
                                        $all_images = $this->Mediamodel->get_result_data('media_files', ['event_id' => $data_single->id, 'delete_flag' => 'N', 'type' => 'P']);
                                        if (count($all_images) > 0) {
                                            foreach ($all_images AS $all_images_val) {
                                                ?>
                                                <div style="position: relative; display: inline-block;">
                                                    <span style="position: absolute; z-index: 9; top: 5px; right: 10px;">
                                                        <a href="javascript:void(0);" onclick="delete_file('<?php echo $all_images_val->id; ?>')"><i class="fa fa-times"></i></a>
                                                    </span>
                                                    <img src="<?php echo base_url(); ?>assets/upload/media_files/<?php echo $all_images_val->file_name; ?>" height="100" width="150">
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Tag</label>
                                <div class="controls">
                                    <textarea class="span11" placeholder="Tag" type="text" id="tag" name="tag"><?php echo (!empty(set_value('tag'))) ? set_value('tag') : ((!empty($data_single->document_text)) ? $data_single->document_text : ''); ?></textarea>
                                    <?php echo form_error('tag', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>


                            <input type="hidden" name="media_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
        $('.timepicker').timepicker({defaultTime: false});
    })

</script>