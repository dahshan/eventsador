<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-medium"></i>Media</a>
        </div>

        <h1>Media</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <?php if(in_array(69, $getRole)){ ?><button class="btn btn-success btn-cls" type="button" onclick="add();">Add Media</button><?php } ?>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Event Name</th>
                                    <?php if(in_array(142, $getRole)){ ?><th>Total Images</th><?php } ?>
                                    <?php if(in_array(142, $getRole)){ ?><th>Pending Images</th><?php } ?>
                                    <?php if(in_array(143, $getRole)){ ?><th>Total Videos</th><?php } ?>
                                    <?php if(in_array(143, $getRole)){ ?><th>Pending Videos</th><?php } ?>
                                    <?php if(in_array(144, $getRole)){ ?><th>Total Documents</th><?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->event_name; ?>
                                            </td>
                                            <?php if(in_array(142, $getRole)){ ?><td class="custom-action-btn"><u><a href="<?php echo base_url('admin_view_media_image' . '/' . $data->id); ?>"><?php echo $data->picture_count; ?></a></u></td><?php } ?>
                                            <?php if(in_array(142, $getRole)){ ?><td class="custom-action-btn"><u><a href="<?php echo base_url('admin_view_media_image_pending' . '/' . $data->id); ?>"><?php echo $data->picture_count_pending; ?></a></u></td><?php } ?>
                                            <?php if(in_array(143, $getRole)){ ?><td class="custom-action-btn"><u><a href="<?php echo base_url('admin_view_media_video' . '/' . $data->id); ?>"><?php echo $data->video_count; ?></a></u></td><?php } ?>
                                            <?php if(in_array(143, $getRole)){ ?><td class="custom-action-btn"><u><a href="<?php echo base_url('admin_view_media_video_pending' . '/' . $data->id); ?>"><?php echo $data->video_count_pending; ?></a></u></td><?php } ?>
                                            <?php if(in_array(144, $getRole)){ ?><td class="custom-action-btn"><u><a href="<?php echo base_url('admin_view_media_document' . '/' . $data->id); ?>"><?php echo $data->document_count; ?></a></u></td><?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /**
     * Add Media Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_media'); ?>";
    }
</script>
