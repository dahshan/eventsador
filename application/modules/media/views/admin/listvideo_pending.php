<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<style>

.controls {
  float: right;
}

</style>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_media'); ?>"><i class="fa fa-medium"></i>Media</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-video-camera"></i>Media Videos</a>
        </div>

        <h1>Videos</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                       <?php if(in_array(148, $getRole)){ ?> <button class="btn btn-success btn-cls" type="button" onclick="add();">Add Video</button><?php } ?>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Video</th>
                                    <th>Uploaded By</th>
                                    <?php if(in_array(149, $getRole)){ ?><th>Approve</th><?php } ?>
                                    <?php if(in_array(150, $getRole)){ ?><th>Action</th><?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <video width="400" height="200" controls>
                                                    <source src="<?php echo base_url('assets/upload/media_files'); ?>/<?php echo $data->file_name; ?>" >
                                                </video>
                                            </td>
                                            <td><?php echo $data->uploaded_by; ?></td>
                                            <?php if(in_array(149, $getRole)){ ?><td>
                                                <div class="span6">
                                                <div class="controls">
                                                    <input type="hidden" name="event_id" id="event_id" value="<?php echo $id; ?>" />
                                                  <input id="approval_status" type="checkbox" name="approval_status" <?php if($data->uploaded_by_id==1 && $data->uploaded_by=="admin"){ echo 'checked disabled'; }else{ if(!empty($data) &&  $data->approval_status == '1') echo 'checked'; } ?> value="" class="lcs_check toggle-switch" data-did="<?php echo urlencode(base64_encode($data->id));?>" autocomplete="off" />
                                                </div>
                                              </div>
                                               
                                            </td><?php } ?>

                                            <?php if(in_array(150, $getRole)){ ?><td class="custom-action-btn">
                                                <i title="Delete" class="fa fa-trash" aria-hidden="false" onclick="delete_record(<?php echo $data->id; ?>);"></i>
                                            </td><?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>

<script type="text/javascript">
    /**
     * Add Media Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_media_video').'/'.$id; ?>";
    }

    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_delete_media'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this Video?");
        }).modal("show");
    }
</script>

<script>

$(document).ready(function(){

    $('.toggle-switch').lc_switch('YES', 'NO');

    // triggered each time a field is checked
    $('body').delegate('.lcs_check', 'lcs-on', function() {
        if($(this).is(":checked")) {
          var id=$(this).data("did");
          var event_id = $("#event_id").val();
          //alert(event_id);
          window.location.href = "<?php echo base_url('admin_media_video_approve_status'); ?>/" + id +"/"+event_id + "/1";
        }
    });
    
    // triggered each time a is unchecked
    $('body').delegate('.lcs_check', 'lcs-off', function() {
        if($(this).attr('id') == 'approval_status')
        {    
          var id=$(this).data("did");
          var event_id = $("#event_id").val();
          window.location.href = "<?php echo base_url('admin_media_video_approve_status'); ?>/" + id+"/"+event_id + "/1";
        }
    });
});

$('.show_delete_button').live('click', function() {
    if($(this).prop('checked') == true){
        $('#delete_all').css('display','block');
    }
    else
    {
        $('#delete_all').css('display','none');
    }
});

</script>

