<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for Sponsor [HMVC]. Handles all the datatypes and methodes required for Sponsor section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Sponsormodel');
        $this->load->helper('common');
    }

    /**
     * Index Page for this Sponsor controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Sponsormodel->load_events();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'sponsor/admin/list';
        $this->admin_layout();
    }

    public function search() {
        $this->load->model('../models/admin/Adminauthmodel');
        $eventid = $_POST['event'];

        $event = $this->Sponsormodel->load_event($eventid);
        $all_data = [];
        if ($event->sponsors) {
            $all_data = $this->Sponsormodel->load_sponsors($event->sponsors);
        }

        $data['all_data'] = $all_data;
        $this->load->view('sponsor/admin/ajax_sponsors',$data);
    }

    
    public function rearrange() {
        if (isset($_POST['arrange'])) {
            $_POST['arrange'] = array_reverse($_POST['arrange']);
            foreach ($_POST['arrange'] as $arrange => $id) {
                if (is_numeric($arrange) && is_numeric($id)) {
                    $this->Sponsormodel->rearrange($id, $arrange);
                }
            }
        }
    }


    /**
     * Index Page for this Organizer controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function sponsors_by_event() {
        if (is_numeric($this->uri->segment(2))) {
            $event = $this->Sponsormodel->load_event_data($this->uri->segment(2));
            $all_data = $this->Sponsormodel->load_all_data_in($event->sponsors);
            $uri = $this->uri->segment(1);
            $this->data = array();
            $this->data['uri'] = $uri;
            $this->data['all_data'] = $all_data;
            $this->middle = 'sponsor/admin/list';
            $this->admin_layout();
        }
    }

    /**
     * Add/Edit function of Sponsor module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            if ($param == 'A') {
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_new_email_check');
            } else {
                $this->form_validation->set_rules('email', 'Email', 'trim|required|callback_old_email_check');
            }

            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Sponsormodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Sponsor already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Sponsor modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_sponsor'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->Sponsormodel->load_single_data($id);
        $this->middle = 'sponsor/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function new_email_check($email) {
        if (isset($email) && $email != '') {
            $sponsor_data = $this->Sponsormodel->get_row_data('user', ['emailid' => $email, 'delete_flag' => 'N']);
            if (count($sponsor_data) > 0) {
                $this->form_validation->set_message('new_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $sponsor_id = $this->input->post('sponsor_id');
            $sponsor_data = $this->Sponsormodel->get_row_data('user', ['id<>' => $sponsor_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (!empty($sponsor_data) && count($sponsor_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of Sponsor module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Sponsormodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Sponsor status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_sponsor'));
    }

    /**
     * Delete function of Sponsor module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $user = $this->Sponsormodel->load_single_data($id);
        $flg = $this->Sponsormodel->delete($user);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Sponsor deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_sponsor'));
    }

}

/* End of file admin.php */
/* Location: ./application/modules/sponsor/controllers/admin.php */
