<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php echo base_url('admin_sponsor'); ?>"><i class="fa fa-money"></i>Sponsor</a>
            <a href="javascript:void(0);" class="current"></i>Sponsor Management</a>
        </div>

        <h1>Sponsor</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Sponsor</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_sponsor') : base_url('admin_update_sponsor') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Name</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Name" type="text" id="name" name="name" value="<?php echo (!empty(set_value('name'))) ? set_value('name') : ((!empty($data_single->name)) ? $data_single->name : ''); ?>" >
                                    <?php echo form_error('name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Email</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Email" type="text" id="email" name="email" value="<?php echo (!empty(set_value('email'))) ? set_value('email') : ((!empty($data_single->emailid)) ? $data_single->emailid : ''); ?>">
                                    <?php echo form_error('email', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Phone</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Phone" type="text" id="phoneno" name="phone" value="<?php echo (!empty(set_value('phone'))) ? set_value('phone') : ((!empty($data_single->phoneno)) ? $data_single->phoneno : ''); ?>" >
                                    <?php echo form_error('phone', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Location</label>
                                <div class="controls">
                                    <textarea class="span11" id="location" name="location"><?php echo (!empty(set_value('location'))) ? set_value('location') : ((!empty($data_single->location)) ? $data_single->location : ''); ?></textarea>
                                    <?php echo form_error('location', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Company</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Company" type="text" id="company" name="company" value="<?php echo (!empty(set_value('company'))) ? set_value('company') : ((!empty($data_single->company)) ? $data_single->company : ''); ?>" >
                                    <?php echo form_error('company', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                             <div class="control-group">
                                <label class="control-label">Website URL</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Website" type="url" id="website" name="website" value="<?php echo (!empty(set_value('website'))) ? set_value('website') : ((!empty($data_single->website)) ? $data_single->website : ''); ?>" >
                                    <?php echo form_error('website', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Position Title</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Position Title" type="text" id="position_title" name="position_title" value="<?php echo (!empty(set_value('position_title'))) ? set_value('position_title') : ((!empty($data_single->position_title)) ? $data_single->position_title : ''); ?>" >
                                    <?php echo form_error('position_title', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Image</label>
                                <div class="controls">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                      <img  src="<?php if(!empty($data_single->profile_image)){ echo base_url('assets/upload/appuser')."/".$data_single->profile_image; } else {echo base_url('assets/upload/default_man.png');}?>" data-src="<?php if(!empty($data_single->profile_image)){ echo base_url('assets/upload/appuser')."/".$data_single->profile_image; } else {echo base_url('assets/upload/default_man.png');}?>" alt="...">
                                    </div>
                                    <div>
                                      <div class="controls custon_textarea" style="margin-left: 0;">
                                        <input type="file" name="image" style="line-height:1px;">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Supported By</label>
                                <div class="controls">
                                    <input type="checkbox" <?php if(!empty($data_single->sponsor_supported_by) && $data_single->sponsor_supported_by == 'Y'){ echo "checked"; }?> value="1" name="sponsor_supported_by" id="sponsor_supported_by">
                                </div>
                            </div>

                            <input type="hidden" name="sponsor_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
