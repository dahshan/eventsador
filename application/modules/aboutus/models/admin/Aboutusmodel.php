<?php

/**
 * role Model Class. Handles all the datatypes and methodes required for handling role
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since Version 0.0.1
 */
class Aboutusmodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of role for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the role that has been added by current admin [Table:  ets_role]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
   
    public function content_get($where)
    {
        return $this->db->where($where)->get(tablename('aboutus'))->row();
    }

    public function fetch_details() {
        $this->db->select('*');
        $this->db->from(tablename('aboutus'));

        $query = $this->db->get();
        $result = $query->row();

        // echo "<pre>";print_r($result);exit;

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

}

/* End of file rolemodel.php */
/* Location: ./application/modules/role/models/admin/rolemodel.php */
