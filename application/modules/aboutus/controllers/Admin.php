<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for User [HMVC]. Handles all the datatypes and methodes required for User section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Aboutusmodel');
        $this->load->model('role/admin/Rolemodel');
    }

    /**
     * Index Page for this role controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Rolemodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'role/admin/list';
        $this->admin_layout();
    }

    public function aboutus() {

   
        $this->data = array();
        $where['id']='1';
        $data['result']=$this->aboutusmodel->content_get($where);
        $this->data['all_module'] = $this->Rolemodel->get_result_data('modules', ['url_slag!='=>'admin_aboutus','is_active' => 'Y', 'delete_flag' => 'N']);
        $this->middle = 'aboutus/admin/aboutus';
        $this->admin_layout();

    }
   

}

/* End of file admin.php */
/* Location: ./application/modules/user/controllers/admin.php */
