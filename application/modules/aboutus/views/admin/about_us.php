<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo site_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="<?php echo site_url('admin/about-us'); ?>" class="current">About Us</a> </div>
        <h1>Manage About-us</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Add About-us</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo (empty($result->id)) ? base_url('admin/formcontent') : base_url('admin/formcontent') . '/' . urlencode(base64_encode($result->id)); ?>" name="banner_frm" id="banner_frm" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">

                            <label class="control-label">Content Image</label>
                              <div class="controls">
                                <input class="span4 nopadding" type="file" name="content_image" />
                                  <?php if(!empty($result)) { ?>

                                 <img src="<?php echo base_url().'assets/uploads/aboutus/'.$result->content_image; ?>" alt="" style="width:100px;height:120px;border: 2px solid #a1a1a1;">

                                 <?php } ?>
                                 <p style="color: red;">*Image size must be 650*435</p>
                                </div>
                               
                            </div>

                            <div class="control-group">
                                <label class="control-label">Content Description</label>
                                <div class="controls">
                                   <!--  <input class="span11" placeholder="Description" type="text" id="content_description" name="content_description" value="<?php echo (!empty($result->content_description)) ? $result->content_description : ''; ?>"> -->

                                     <textarea class="span11 ckeditor" placeholder="Description" id="content_description" name="content_description" ><?php echo (!empty($result->content_description)) ? $result->content_description : ''; ?></textarea>

                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Privacy Statement</label>
                                <div class="controls">
                                   <textarea class="span11 ckeditor" placeholder="Privacy Statement" id="privacy_statement" name="privacy_statement" ><?php echo (!empty($result->privacy_statement)) ? $result->privacy_statement : ''; ?></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Terms Of Use</label>
                                <div class="controls">
                                   <textarea class="span11 ckeditor" placeholder="Terms Of Use" id="terms_of_use" name="terms_of_use" ><?php echo (!empty($result->terms_of_use)) ? $result->terms_of_use : ''; ?></textarea>
                                </div>
                            </div>

                             <div class="form-actions">
                            <input type="hidden" name="id" <?php if(!empty($result)){ ?> value="<?php echo $result->id; ?>" <?php } ?>>
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){


  $("#banner_frm").validate({
      rules:{
        content_name:{
          "required":true
        },
        content_type:{
          "required":true
        }
      },
      messages:{
        content_name:{
          "required":"Please Enter Content Name..!!"
        },
        content_type:{
          "required":"Please Enter Content Type..!!"
        }
      }
      
  });

 

});
</script>
<style>
.error{
  color: #dd0000 !important;
}


</style>