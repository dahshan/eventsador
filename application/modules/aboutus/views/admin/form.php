<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-user"></i>Role Management</a>
        </div>

        <h1>Role</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Role</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_role') : base_url('admin_update_role') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Designation</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Enter Designation" type="text" id="f_name" name="name" value="<?php echo (!empty(set_value('name'))) ? set_value('name') : ((!empty($data_single->name)) ? $data_single->name : ''); ?>" >
                                    <?php echo form_error('name', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Choose Modules to give permission:</label>
                                <div class="controls">
                                    <?php
                                    if (count($all_module) > 0) {
                                        foreach ($all_module AS $all_module_val) {
                                            if (isset($data_single->permited_module) && $data_single->permited_module != '') {
                                                $permited_module = explode(',', $data_single->permited_module);
                                            } else {
                                                $permited_module = [];
                                            }
                                            ?>
                                            <input class="span11" type="checkbox" name="modules[]" value="<?php echo (isset($all_module_val->id) && $all_module_val->id != '') ? $all_module_val->id : ''; ?>" <?php echo (in_array($all_module_val->id, $permited_module) ? 'checked' : ''); ?>><?php echo (isset($all_module_val->module_name) && $all_module_val->module_name != '') ? $all_module_val->module_name : ''; ?>
                                            <br>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <?php echo form_error('modules[]', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <input type="hidden" name="role_id" value="<?php echo $id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
