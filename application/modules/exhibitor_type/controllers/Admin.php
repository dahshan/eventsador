<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class for Exhibitor Type [HMVC]. Handles all the datatypes and methodes required for exhibitor section of eventsador
 *
 * @author  
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/ExhibitorTypemodel');
    }

    /**
     * Index Page for this exhibitor controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->ExhibitorTypemodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'exhibitor_type/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of exhibitor module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {

            //echo "<pre>"; print_r($_POST); die;


            $this->form_validation->set_rules('name', 'Name', 'trim|required|callback_check_name');
           

           

            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->ExhibitorTypemodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! Exhibitor type already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'Exhibitor type modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_exhibitor_type'));
            }
        }

        $this->data = array();
        $this->data['id'] = $id;
        $this->data['data_single'] = $this->ExhibitorTypemodel->load_single_data($id);
        $this->middle = 'exhibitor_type/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function check_name($name) {
        if (isset($name) && $name != '') {
            $exhibitor_data = $this->ExhibitorTypemodel->get_row_data('exhibitor_type', ['name' => $name]);
            if (!empty($exhibitor_data) && count($exhibitor_data) > 0) {
                $this->form_validation->set_message('check_name', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    

    /**
     * Status Change function of exhibitor module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->ExhibitorTypemodel->status($id);
        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'exhibitor type status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect(base_url('admin_exhibitor_type'));
    }


    public function delete($id) {
        $flg = $this->ExhibitorTypemodel->delete_data('exhibitor_type', ['id' => $id]);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Exhibitor Typemodel deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_exhibitor_type'));
    }

    

}

/* End of file admin.php */
/* Location: ./application/modules/Exhibitor_Type/controllers/admin.php */
