<?php

/**
 * exhibitor type Model Class. Handles all the datatypes and methodes required for handling Exhibitor Type
 *
 * @author  
 * @access      public
 * @since Version 0.0.1
 */
class ExhibitorTypemodel extends CI_Model {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
        $this->load->helper('string');
    }

    /**
     * Used for loading functionality of exhibitor for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads all the exhibitor that has been added by current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_all_data() {
        $this->db->select('*');
        $this->db->from(tablename('exhibitor_type'));
        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);exit;
        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for loading functionality of single exhibitor by id for an admin
     *
     * <p>Description</p>
     *
     * <p>This function loads a single exhibitor by id that has been added by current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  array
     */
    public function load_single_data($id = "") {
        $this->db->select('*');
        $this->db->from(tablename('exhibitor_type'));
        $this->db->where('id', $id);

        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            return $result;
        } else {
            return "";
        }
    }

    /**
     * Used for fetching rows from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches rows from any table depending upon condition</p>
     *
     * @access  public
     * @param   {string} table - the table name whose data will be fetched
     * @param   {array[]} where - the where clause parameter for the sql
     * @return array
     */
    public function get_result_data($table, $where = "1=1") {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->result();
    }

     

    /**
     * Used for Save(Insert/Update) functionality of single exhibitor for an admin
     *
     * <p>Description</p>
     *
     * <p>This function saves a single exhibitor w.r.t. current admin [Table:  ets_organizer]</p>
     *
     * @access  public
     * @param none
     * @return Success or Failure of Save
     */
    public function modify($id = '') {
        $name = $this->input->post('name');
        if (!empty($id)) {
            $data = array(
                    'name' => $name
                    );
            $this->db->where('id', $id)->update(tablename('exhibitor_type'), $data);
            return $id;
        } else {
            // Check for Duplicate
            $data = array(
                'name' => $name
            );
            $this->db->insert(tablename('exhibitor_type'), $data);
            $last_id = $this->db->insert_id();
            if (!empty($last_id)) {
               return $last_id;
            } else {
                return "";
            }
        }
    }

    /**
     * Used for change status functionality of exhibitor for an admin
     *
     * <p>Description</p>
     *
     * <p>This function takes id as input, check the current exhibitor status
     * and change it the the opposite [Table: pb_organizer]</p>
     *
     * @access  public
     * @param none
     * @return  int
     */
    public function status($id) {
        $this->db->select('*');
        $this->db->from(tablename('exhibitor_type'));
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();

        if (!empty($result)) {
            $is_active = $result->is_active;

            if ($is_active == "N") {
                $new_is_active = "Y";
            } else {
                $new_is_active = "N";
            }

            $update = array('is_active' => $new_is_active);
            $this->db->where('id', $id);

            if ($this->db->update(tablename('exhibitor_type'), $update)) {
                return 1;
            } else {
                return;
            }
        } else {
            return;
        }
    }

   

    /**
     * Used for fetching one row from a table
     *
     * <p>Description</p>
     *
     * <p>This function fetches one row from any table depending upon condition</p>
     *
     * @access  public
     * @param none
     * @return array
     */
    public function get_row_data($table, $where) {
        $query = $this->db->get_where(tablename($table), $where);
        return $query->row();
    }

    public function delete_data($table,$where)
    {
        $this->db->delete(tablename($table),$where);
        return $this->db->affected_rows();
    }


}

/* End of file Organizermodel.php */
/* Location: ./application/modules/exhibitor_type/models/admin/ExhibitorTypemodel.php */
