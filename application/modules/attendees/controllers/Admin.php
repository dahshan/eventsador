<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH.'libraries/phpqrcode/qrlib.php');
/**
 * Admin Class for attendees [HMVC]. Handles all the datatypes and methodes required for attendees section of Optus
 *
 * @author  pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since   Version 0.0.1
 */
class Admin extends MX_Controller {

    /**
     * The constructor method of this class.
     *
     * @access  public
     * @param   none
     * @return  Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        admin_authenticate();
        $this->load->model('admin/Attendeesmodel');
    }

    /**
     * Index Page for this attendees controller.
     *
     * @access  public
     * @param   none
     * @return  string
     */
    public function index() {
        $all_data = $this->Attendeesmodel->load_all_data();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['all_data'] = $all_data;
        $this->middle = 'attendees/admin/list';
        $this->admin_layout();
    }

    /**
     * Add/Edit function of attendees module
     *
     * @access  public
     * @param   id
     * @return  string
     */
    public function form($id = NULL) {
        if (!empty($id)) {
            $param = 'E';
        } else {
            $param = 'A';
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'attendees Title', 'trim|required');
            $this->form_validation->set_rules('details', 'attendees Overview', 'trim|required');
            $this->form_validation->set_rules('date', 'attendees Date', 'trim|required');
            $this->form_validation->set_rules('start_time', 'attendees Start Time', 'trim|required');
            $this->form_validation->set_rules('end_time', 'attendees End Time', 'trim|required');
            if ($this->form_validation->run($this) == TRUE) {
                $flg = $this->Attendeesmodel->modify($id);
                if (!empty($flg)) {
                    if ($flg == "E") {
                        $this->session->set_flashdata('errormessage', 'Oops! attendees already exists');
                    } else {
                        $this->session->set_flashdata('successmessage', 'attendees modified successfully');
                    }
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }
                redirect(base_url('admin_agenda'));
            }
        }
        $this->data = array();
        $this->data['id'] = $id;
        if (!empty($id)) {
            $this->data['data_single'] = $this->Attendeesmodel->load_single_data($id);
            $this->data['all_room'] = $this->Attendeesmodel->get_result_data('agenda_location', ['event_id' => $this->data['data_single']->event_id]);
        }
        if($this->session->userdata('admin_role_type')!=1)
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_uid'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
            }
            else
            {
                if($this->session->userdata('admin_org_id')!=1)
                {
                    $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['organizers' => $this->session->userdata('admin_org_id'),'is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
                }
            }
        }
        else
        {
            $this->data['all_event'] = $this->Accomodationmodel->get_result_data('event', ['is_active' => 'Y', 'delete_flag' => 'N', 'event_date >=' => date("Y-m-d")]);
        }
        $this->data['all_speaker'] = $this->Attendeesmodel->get_result_data('speaker', ['is_active' => 'Y', 'delete_flag' => 'N']);
        $this->middle = 'attendees/admin/form';
        $this->admin_layout();
    }

    /**
     * Used for organizer selected or not
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function organizers_check($organizers) {
        $organizers = $this->input->post('organizers');
        if (!isset($organizers) || count($organizers) == 0) {
            $this->form_validation->set_message('organizers_check', '{field} Cannotbe blank!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Used for new email available check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_email_check($email) {
        if (isset($email) && $email != '') {
            $event_id = $this->input->post('event_id');
            $event_data = $this->Attendeesmodel->get_row_data('attendees', ['id<>' => $event_id, 'emailid' => $email, 'delete_flag' => 'N']);
            if (count($event_data) > 0) {
                $this->form_validation->set_message('old_email_check', '{field} allready exist!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    /**
     * Status Change function of attendees module
     *
     * @access  public
     * @param   id
     * @return  success of failure of status change
     */
    public function status($id) {
        $flg = $this->Attendeesmodel->status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'attendees status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Delete function of attendees module
     *
     * @access  public
     * @param   id
     * @return  success of failure of delete
     */
    public function delete($id) {
        $flg = $this->Attendeesmodel->delete($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'attendees deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function room() 
    {
        $data = $this->Attendeesmodel->get_result_data("agenda_location",array("event_id"=>$this->input->post('eid')));
        $option="";
        if (!empty($data)) 
        {
            foreach($data as $val)
            {
                $option.='<option value="'.$val->id.'">'.$val->name.'</option>';
            }
        }
        echo $option;
    }

    public function listrequest($eid) 
    {
        // $all_data = $this->Attendeesmodel->get_detailed_result_data("event_access_request","user","user_id=user.id",array("event_id"=>$eid,"status"=>"not_approved"));
        $all_data = $this->Attendeesmodel->listrequest($eid);
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['all_data'] = $all_data;
        $this->middle = 'attendees/admin/listrequest';
        $this->admin_layout();
    }

    public function listattendees($eid, $type="all types") 
    {
        $type = urldecode($type);
        if (!empty($_POST['select'])) {
            // echo "<pre>";
            // print_r($_POST);
            // exit;
            admin_authenticate();
            if ($_POST['print'] == 0) {
                foreach ($_POST['select'] as $key => $value) {
                    $flg = $this->Attendeesmodel->deleteAttee($value);
                }
                $this->session->set_flashdata('successmessage', 'Attendees deleted successfully');                
            } else {
                $this->print_badge_multi($eid);
                return;
            }
            
        }
        $types = [];
        $types [] = 'all types';
        $all_data = $this->Attendeesmodel->get_detailed_result_data("event_access_request","user","user_id=user.id",array("event_id"=>$eid,"event_join"=>'1', 'user.role_id'=> 0));
        foreach ($all_data as $one) {
            $types[] = $one->user_type;
        }
        $types = array_unique($types);
        // echo "<pre>";
        // print_r($types);
        // exit;
        if ($type != "all types") {
            $all_data = $this->Attendeesmodel->get_detailed_result_data("event_access_request","user","user_id=user.id",array("event_id"=>$eid,"event_join"=>'1', 'user.role_id'=> 0, 'event_access_request.user_type' => $type));
        }
        
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['id'] = $eid;
        $this->data['currtype'] = $type;
        $this->data['types'] = $types;
        $this->data['all_data'] = $all_data;
        $this->middle = 'attendees/admin/listattendees';
        $this->admin_layout();
    }

    public function emailattendees($eid) 
    {
        $all_data = $this->Attendeesmodel->get_detailed_result_data_array("event_access_request","user","user_id=user.id",array("event_id"=>$eid,"event_join"=>'1', 'user.role_id'=> 0));
        if (count($all_data) == 0) {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
            redirect($_SERVER['HTTP_REFERER']);
        }
        $file = fopen(APPPATH . '../uploads/attendees_' . $eid . '_' . $this->session->userdata('admin_uid') .'.csv', 'wb');
        fputcsv($file, array_keys($all_data[0]));
        foreach ($all_data as $data_array) {
            fputcsv($file, $data_array);
        }

        $event = $this->Attendeesmodel->load_event($eid);
        $user = $this->Attendeesmodel->load_user($this->session->userdata('admin_uid'));



        $subject = "Eventsador - " . $event->event_name;
        $message = '<p>Greetings ' . $user->name . ",</p>";
        $message .= '<p>list attendees [' . $event->event_name . '] attached..</p>';
        $message .= '<p>Thank you,</p>';
        $message .= '<p>Eventsador Admin</p>';

        $mail_data = [
            'name' => $user->name,
            'body' => $message,
        ];

        $this->load->helper('email');
        send_email_file($user->emailid, $subject, $mail_data, '/uploads/attendees_' . $eid . '_' . $this->session->userdata('admin_uid') .'.csv');
        $this->session->set_flashdata('successmessage', 'Email Sent Successfully');
        redirect($_SERVER['HTTP_REFERER']);

    }

    public function download_attendees_csv($eid) 
    {
        $all_data = $this->Attendeesmodel->get_detailed_result_data_array("event_access_request","user","user_id=user.id",array("event_id"=>$eid,"event_join"=>'1', 'user.role_id'=> 0));
        if (count($all_data) == 0) {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
            redirect($_SERVER['HTTP_REFERER']);
        }
        $file = fopen(APPPATH . '../uploads/attendees_' . $eid . '_' . $this->session->userdata('admin_uid') .'.csv', 'wb');
        fputcsv($file, array_keys($all_data[0]));
        foreach ($all_data as $data_array) {
            fputcsv($file, $data_array);
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=example.csv');
        header('Pragma: no-cache');
        readfile(base_url('/uploads/attendees_' . $eid . '_' . $this->session->userdata('admin_uid') .'.csv'));

    }


    public function approve_request($id,$eid,$uid) 
    {
        //echo $uid; die;
        $user_data = $this->Attendeesmodel->get_row_data('user', ['id' => $uid]);
        //echo "<pre>"; print_r($user_data); die;
        $event_data = $this->Attendeesmodel->get_row_data('event', ['id' => $eid]);
        $code=rand();
        $all_data = $this->Attendeesmodel->update_data("event_access_request",array("id"=>$id),array("status"=>"approved","invitation_code"=>$code));
        if(!empty($all_data))
        {
            $subject = "Eventsador - Request Approved";
            $message = '<p>Greetings ' . $user_data->name . ",</p>";
            $message .= '<p>Your join request for the event '.$event_data->event_name.' has been accepted. Please use the following code to join the event.</p>';
            $message .= '<p><strong>Code: </strong>' . $code;
            $message .= '<p>Thank you,</p>';
            $message .= '<p>Eventsador Admin</p>';

            $mail_data = [
               'name' => $user_data->name,
               'body' => $message,
            ];

           $this->load->helper('email');
           send_email($user_data->emailid, $subject, $mail_data);
           $this->session->set_flashdata('successmessage', 'Request Approved Successfully');
        } 
        else 
        {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function print_badge_multi($eid) 
    {
        if (!empty($_POST['select'])) {
            admin_authenticate();
            if ($_POST['print'] == 1) {

                $attee = [];

                foreach ($_POST['select'] as $key => $value) {
                    $attee[] = $value;
                }

                $attee = implode(',', $attee);
                $attee_data = $this->Attendeesmodel->get_result_data('event_access_request', null, $attee);
                $users = [];

                foreach ($attee_data as $attee_one) {
                    $users[] = $attee_one->user_id;
                }

                $users = implode(',', $users);

                $data['users_data'] = $this->Attendeesmodel->get_result_data('user', null, $users);
                // echo "<pre>";
                // print_r($users_data);
                // exit;
                $data['event_data'] = $this->Attendeesmodel->get_row_data('event', ['id' => $eid]);
                $this->load->view('attendees/admin/openpage',$data);
            }
            
        }
    }


    public function print_badge($eid,$uid) 
    {
        $user_data = $this->Attendeesmodel->get_row_data('user', ['id' => $uid]);
        $event_data = $this->Attendeesmodel->get_row_data('event', ['id' => $eid]);
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['user_data'] = $user_data;
        $this->data['event_data'] = $event_data;
        $this->middle = 'attendees/admin/printbadge';
        $this->admin_layout();
    }

    public function openpage($template_no,$eid,$uid) 
    {
        $data['users_data'][] = $this->Attendeesmodel->get_row_data('user', ['id' => $uid]);
        $data['event_data'] = $this->Attendeesmodel->get_row_data('event', ['id' => $eid]);
        if($template_no==1)
        {
            $this->load->view('attendees/admin/openpage',$data);
        }
        elseif($template_no==2)
        {
            $this->load->view('attendees/admin/openpage1',$data);
        }
        elseif($template_no==3)
        {
            $this->load->view('attendees/admin/openpage2',$data);
        }
    }

    public function import_attendee($eid) 
    {
        // $user_data = $this->Attendeesmodel->get_row_data('user', ['id' => $uid]);
        // $event_data = $this->Attendeesmodel->get_row_data('event', ['id' => $eid]);
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        // $this->data['user_data'] = $user_data;
        $this->data['event_id'] = $eid;
        $this->middle = 'attendees/admin/import';
        $this->admin_layout();
    }

    public function csvimport()
    {
        $restablecol[0] = "name";
        $restablecol[1] = "emailid";
        $restablecol[2] = "phoneno";
        $restablecol[3] = "company";
        $restablecol[4] = "position_title";
        $restablecol[5] = "address";
        
        $header = array();  
        $handle = fopen($_FILES['csvfile']['tmp_name'], "r"); 
        $i=0; $j=0;
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
        {  
            $i++;
            if($i==1)
            {
                $header[] = $data; 
            }
            else
            {
                if(count(array_diff( $restablecol, $header[0])) ==0 )
                {
                    $datasql="";
                    for($i=0;$i<count($data);$i++)
                    {
                        if(!empty($data[$i]))
                        {
                            // if($i==(count($data)-1))
                            //   $datasql.=$restablecol[$i]."='".$data[$i]."'";
                            // else
                            $datasql.=$restablecol[$i]."='".$data[$i]."',";
                        }
                    }
                    if(!empty($datasql))
                    {
                        $datasql.="register_type='normal',password='".sha1(123456)."',register_status='1',entry_date='".date("Y-m-d H:i:s")."'";
                        $arrsql=$this->Attendeesmodel->runSQL("user",$datasql);
                        if(!empty($arrsql))
                        {
                            $datasql1="event_id='".$this->input->post('eid')."',user_id='".$arrsql."',status='approved',event_join='1',is_request='0',requested_by='Admin',created_date='".date("Y-m-d H:i:s")."'";
                            $arrsql1=$this->Attendeesmodel->runSQL("event_access_request",$datasql1);
                            $j++;
                        } 
                    }
                }
            }
        }
        if($j>0)
        {
            $this->session->set_flashdata('successmessage','CSV Uploaded successfully');
        }
        else
        {
            $this->session->set_flashdata('errormessage','Oops! something went wrong. Please try again');
        }
        $redirect = base_url('admin_view_attendees').'/'.$this->input->post('eid');
        redirect($redirect);
    }

    public function checked_in_status($id) {
        $flg = $this->Attendeesmodel->checked_in_status($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Attendee checked in status changed successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function qr_code() 
    {
        $tempDir = FCPATH."assets/upload/qrcode/";
        // $data1['user_number']="ES-".substr(uniqid(),0,5);
        $data1['user_number']="ES-".uniqid();
        $codeContents = "User Number:-".$data1['user_number'];
        $fileName = 'qr_file_'.date("YmdHis").'.png';
        
        $pngAbsoluteFilePath = $tempDir.$fileName;
        $urlRelativeFilePath = FCPATH."assets/upload/qrcode/".$fileName;
        
        // generating
        if (!file_exists($pngAbsoluteFilePath)) 
        {
            QRcode::png($codeContents, $pngAbsoluteFilePath);
        } 
        echo "ok";die;
    }

     public function deleteAttee($id,$eid) {
        $flg = $this->Attendeesmodel->deleteAttee($id);

        if (!empty($flg)) {
            $this->session->set_flashdata('successmessage', 'Attendee deleted successfully');
        } else {
            $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
        }

        redirect(base_url('admin_view_attendees/'.$eid));
    }

}

/* End of file admin.php */
/* Location: ./application/modules/attendees/controllers/admin.php */
