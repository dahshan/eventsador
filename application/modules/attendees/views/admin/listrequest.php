<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="<?php echo base_url('admin_attendees'); ?>" ><i class="fa fa-users"></i>Attendees</a>
            <a href="javascript:void(0);" class="current">Attendees Request</a>
        </div>

        <h1>Attendees Request</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <!-- <button class="btn btn-success btn-cls" type="button" onclick="add();">Add Agenda</button> -->
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Mobile</th>
                                    <th>Image</th>
                                    <th>Company</th>
                                    <th>Position</th>
                                    <th>Requested Date</th>
                                    <?php if(in_array(154, $getRole)){ ?> <th>Action</th><?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <!-- <a href="<?php echo base_url('admin_update_agenda' . '/' . $data->id); ?>"> -->
                                                <?php echo $data->name; ?>
                                                <!-- </a> -->
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->emailid; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->phoneno; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->mobile_no; ?>
                                            </td>
                                            <td><img style="height: 50px;width: 50px;" <?php if(!empty($data->profile_image)){ ?> src="<?php echo base_url('assets/upload/appuser'); ?>/<?php echo $data->profile_image; ?>" <?php }else{ ?> src="<?php echo base_url('assets/upload/default_man.png'); ?>" <?php } ?> ></td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->company; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->position_title; ?>
                                            </td>
                                            <td class="custom-action-btn">
                                                <?php echo date("M,d Y",strtotime($data->created_date)); ?>
                                            </td> <?php if(in_array(154, $getRole)){ ?> 
                                            <td class="custom-action-btn">
                                                <?php if($data->status=='not_approved'){ ?> 
                                                <i title="Approve" class="fa fa-lock" aria-hidden="false" onclick="approve(<?php echo $data->id; ?>,<?php echo $data->event_id; ?>,<?php echo $data->user_id; ?>);"></i>
                                                <?php }else{ ?> 

                                                <a title="Approve again" onclick="approve(<?php echo $data->id; ?>,<?php echo $data->event_id; ?>,<?php echo $data->user_id; ?>);">Approve again</a>
                                                <?php } ?> 

                                            </td><?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add Event Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_agenda'); ?>";
    }

   /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function approve(id,eid,uid) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_approve_request'); ?>/"+id+"/"+eid+"/"+uid;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to approve this request?");
        }).modal("show");
    }

    
</script>
