<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>Badge Format</title>
  </head>
  <body>
  	<div class="container">
  		<div class="row">
  			<div class="col-md-6 col-md-offset-3">

            <?php foreach ($users_data as $user_data): ?>
                <table style="width: 600px; margin: 50px auto; border: solid 2px #FFA500;">
                    <tr>
                        <td>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="padding: 20px; background-color: #FFA500 !important; color: #fff !important; vertical-align: middle; width: 200px; text-align: center;">
                                        <img src="<?php echo base_url('assets/upload/event').'/'.$event_data->event_logo;?>" alt="" style="margin: 0 auto; width:  100px;">
                                    </td>
                                    <td style="padding: 20px 0; background-color: #FFA500 !important; color: #fff !important; vertical-align: middle; text-align: left; font-family: sans-serif; font-size: 20px;">
                                        <h3 style="margin: 10px 0 10px; font-family: sans-serif; font-size: 26px; font-weight: 600; color: #fff !important;">
                                            <?php echo $event_data->event_name; ?>
                                        </h3>
                                    </td>
                                    <!--<td style="padding: 20px; background-color: #FFA500 !important; color: #fff !important; vertical-align: middle; width: 200px; text-align: left; font-size: 14px;">-->
                                    <!--    <?php echo $event_data->event_venue; ?> &nbsp;|&nbsp; <?php echo date("M d,Y",strtotime($event_data->event_date)); ?> &nbsp;|&nbsp; <?php echo $event_data->event_time; ?>-->
                                    <!--</td>-->
                                    <td style="padding: 20px; background: #FFA500 !important; vertical-align: middle; text-align: left; font-family: sans-serif; font-size: 14px; color: #fff !important; width: 400px;">
                                        <?= $event_data->city;?>, <?=$event_data->country?> &nbsp;|&nbsp; <?php echo date("M d,Y",strtotime($event_data->event_date)); ?> &nbsp;|&nbsp; <?php echo $event_data->event_start_time; ?> - <?=$event_data->event_end_time?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding: 20px 10px 30px 30px; vertical-align: top; text-align: left; font-family: sans-serif; font-size: 20px; color: #444; ">
                                        <h1 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 32px; font-weight: bold; color: #000;">
                                            <b><?php echo $user_data->name;?></b>
                                        </h1>
                                        <h3 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 22px; color: #444;">
                                            <?php echo $user_data->position_title;?>
                                        </h3>
                                        <h3 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 18px; color: #444;">
                                            <?php echo $user_data->company;?>
                                        </h3>
                                    </td>
                                    <td style="padding: 20px 30px 30px 10px; vertical-align: middle; text-align: center; font-family: sans-serif; font-size: 14px; color: #fff !important; ">
                                        <img <?php if(!empty($user_data->qr_code)){ ?> src="<?php echo base_url('assets/upload/qrcode').'/'.$user_data->qr_code;?>" <?php }else{ ?> src="https://2d6qxj3uqdaw38d6lk27l0ao-wpengine.netdna-ssl.com/wp-content/uploads/2015/10/apb-qr-code.png" <?php } ?> alt="" width="100" style="">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            <?php endforeach ?>

    		</div>
    	</div>
        <div align="center">
        <a href="javascript:void(0);" class="btn btn-primary" id="print">Print</a>
        </div>
	</div>
    <!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).on("click","#print",function(){
        window.print();
    })
</script>