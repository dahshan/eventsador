<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-calendar"></i>Agenda</a>
        </div>

        <h1>Agenda</h1>
    </div>

    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Agenda</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" action="<?php echo ($id == '') ? base_url('admin_add_agenda') : base_url('admin_update_agenda') . '/' . $id; ?>" name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label class="control-label">Event</label>
                                <div class="controls">
                                    <select name="event_id" id="event_id">
                                        <?php
                                        if (count($all_event) > 0) {
                                            foreach ($all_event as $all_event_val) {
                                                ?>
                                                <option value="<?php echo $all_event_val->id; ?>"
                                                        <?php if(!empty($data_single)){if($all_event_val->id==$data_single->event_id)echo 'selected'; }?>>
                                                            <?php echo (isset($all_event_val->event_name) && $all_event_val->event_name != '') ? $all_event_val->event_name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('event_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Title</label>
                                <div class="controls">
                                    <input class="span11" placeholder="Agenda Title" type="text" id="title" name="title" value="<?php echo (!empty(set_value('title'))) ? set_value('title') : ((!empty($data_single->title)) ? $data_single->title : ''); ?>" >
                                    <?php echo form_error('title', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Overview</label>
                                <div class="controls">
                                    <textarea class="span11" id="details" name="details"><?php echo (!empty(set_value('details'))) ? set_value('details') : ((!empty($data_single->details)) ? $data_single->details : ''); ?></textarea>
                                    <?php echo form_error('details', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Date</label>
                                <div class="controls">
                                    <input onkeypress="return false" class="span11 datepicker" placeholder="Select Date" type="text" id="date" name="date" value="<?php echo (!empty(set_value('date'))) ? set_value(date('m/d/Y',strtotime($data_single->start_date_time))) : ((!empty($data_single->start_date_time)) ? date('m/d/Y',strtotime($data_single->start_date_time)) : ''); ?>" >
                                    <?php echo form_error('date', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="span5">
                                    <label class="control-label">Start Time</label>
                                    <div class="controls">
                                        <input onkeypress="return false" class="span11 timepicker" placeholder="Start Time" type="text" id="start_time" name="start_time" value="<?php echo (!empty(set_value('start_time'))) ? set_value('start_time') : ((!empty($data_single->start_date_time)) ? date('H:i a',strtotime($data_single->start_date_time)) : ''); ?>" >
                                        <?php echo form_error('start_time', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="span5">
                                    <label class="control-label">End Time</label>
                                    <div class="controls">
                                        <input onkeypress="return false" class="span11 timepicker" placeholder="End Time" type="text" id="end_time" name="end_time" value="<?php echo (!empty(set_value('end_time'))) ? set_value('end_time') : ((!empty($data_single->end_date_time)) ? date('H:i a',strtotime($data_single->end_date_time)) : ''); ?>" >
                                        <?php echo form_error('end_time', '<div style="color:red;">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Speaker</label>
                                <div class="controls">
                                    <select name="speakers[]" multiple >
                                        <?php
                                        if (count($all_speaker) > 0) {
                                            if (isset($data_single->speakers) && $data_single->speakers != '') {
                                                $speakers_selected = explode(',',$data_single->speakers);
                                            } else {
                                                $speakers_selected = [];
                                            }
                                            foreach ($all_speaker AS $all_speaker_val) {
                                                ?>
                                                <option value="<?php echo (isset($all_speaker_val->id) && $all_speaker_val->id != '') ? $all_speaker_val->id : ''; ?>"
                                                        <?php echo in_array($all_speaker_val->id, $speakers_selected)?'selected':''; ?>>
                                                            <?php echo (isset($all_speaker_val->name) && $all_speaker_val->name != '') ? $all_speaker_val->name : ''; ?> 
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('speaker_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Location</label>
                                <div class="controls">
                                    <select name="room_id" id="room_id">
                                        <?php if(!empty($all_room)){
                                            foreach($all_room as $val)
                                            {
                                        ?>
                                            <option value="<?php echo $val->id;?>" <?php if($val->id==$data_single->room_id) echo 'selected'; ?>><?php echo $val->name;?></option>
                                        <?php
                                            }
                                        }
                                        ?>  
                                    </select>
                                    <?php echo form_error('room_id', '<div style="color:red;">', '</div>'); ?>
                                </div>
                            </div>
                            <input type="hidden" name="eventid" id="eventid" value="<?php if(!empty($data_single->id)) echo $data_single->id; ?>">
                            <div class="form-actions">
                                <input type="submit" value="Submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker();
        $('.timepicker').timepicker({defaultTime: false});
        $(document).on("change","#event_id",function(){
            $("#s2id_room_id a").text("");
            var saveData = $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url('agenda/admin/room'); ?>",
                    data: {eid:$(this).val()},
                    dataType: "text",
                    success: function(resultData) { 
                        $("#room_id").html(resultData);
                    }
            });
        });
    })
</script>
