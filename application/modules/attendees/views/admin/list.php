<?php $getRole = getRole();    //echo "<pre>"; print_r($getRole); die; ?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url('admin_dashboard'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="javascript:void(0);" class="current"><i class="fa fa-users"></i>Event Attendees</a>
        </div>

        <h1>Attendees</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>List</h5>
                        <!-- <button class="btn btn-success btn-cls" type="button" onclick="add();">Add Agenda</button> -->
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Event Name</th>
                                    <?php if(in_array(76, $getRole)){ ?>
                                    <th>Total Pending Request</th>
                                    <?php } ?>
                                    <?php if(in_array(141, $getRole)){ ?>
                                    <th>Total Attendees</th>
                                    <?php } ?>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 1;
                                if (!empty($all_data)) {
                                    foreach ($all_data as $data) {
                                        ?>
                                        <tr class="gradeX">
                                            <td class="custom-action-btn"><?php echo $sl++; ?></td>
                                            <td class="custom-action-btn">
                                                <?php echo $data->event_name; ?>
                                            </td>
                                            <?php if(in_array(76, $getRole)){ ?>
                                            <td class="custom-action-btn"><u><a href="<?php echo base_url('admin_view_request' . '/' . $data->id); ?>"><?php echo $data->pending_count; ?></a></u></td>
                                            <?php } ?>
                                            <?php if(in_array(141, $getRole)){ ?>
                                            <td class="custom-action-btn"><u><a href="<?php echo base_url('admin_view_attendees' . '/' . $data->id); ?>"><?php echo $data->attendees_count; ?></a></u></td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
  Notification DIV
  This div acts as the notification before performing any action
-->
<div id="myAlert" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="notification_heading">Alert modal</h3>
    </div>

    <div class="modal-body">
        <p id="notification_body"></p>
    </div>

    <div class="modal-footer">
        <a data-dismiss="modal" id="modal_confirm" class="btn btn-primary" href="#">Confirm</a>
        <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- [end] Notification DIV -->

<script type="text/javascript">
    /**
     * Add Event Function
     *
     * @param   none
     * @return  redirects and gives control to add faq handler
     */
    function add() {
        window.location.href = "<?php echo base_url('admin_add_agenda'); ?>";
    }

    

    /**
     * Delete Function
     *
     * @param   rec_id
     * @return  redirects and gives control to delete handler
     */
    function delete_record(rec_id) {
        $("#myAlert").on('shown.bs.modal', function () {
            $("#modal_confirm").click(function () {
                window.location.href = "<?php echo base_url('admin_delete_event'); ?>/" + rec_id;
            });

            $("#notification_heading").html("Confirmation");
            $("#notification_body").html("Do you want to delete this Event?");
        }).modal("show");
    }
</script>
