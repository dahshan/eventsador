<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'libraries/REST_Controller.php');
require_once(APPPATH.'libraries/phpqrcode/qrlib.php');
class Api extends REST_Controller {

function __construct() {
  // Construct the parent class
  parent::__construct();
  $this->load->model("user/admin/usermodel");
  $this->load->model("event/admin/eventmodel");
  $this->load->model("aboutus/admin/aboutusmodel");
  $this->load->model("exhibitor/admin/exhibitormodel");
  $this->load->model("logistics/admin/logisticsmodel");
  $this->load->model("bulletin/admin/bulletinmodel");
  $this->load->model("host/admin/hostmodel");
  $this->load->model("accomodation/admin/accomodationmodel");
  $this->load->model("transportation/admin/transportationmodel");
  $this->load->model("wifi_access/admin/wifiaccessmodel");
  $this->load->model("floor_map/admin/floormapmodel");
  $this->load->model("local_attraction/admin/localattractionmodel");
  $this->load->model("lead/admin/leadmodel");
  $this->load->model("attendees/admin/attendeesmodel");
  $this->load->model("agenda/admin/agendamodel");
  $this->load->model("notification/admin/notificationmodel");
  $this->load->model("faq/admin/faqmodel");
  $this->load->model("point/admin/pointmodel");
  $this->load->helper('email');
}

/*******USER REGISTRATION**************/

public function registration_post()
{
   $data=json_decode( file_get_contents('php://input') );
    //echo "<pre>"; print_r($data); die;
    if((!empty($data->facebook_id) || !empty($data->linkedin_id) || (!empty($data->email) && !empty($data->password) )) && !empty($data->register_type) && !empty($data->device_type))
    {
      $where = array();
      if($data->register_type=="normal")
      {
        $where['emailid']=$data->email;
        $chk=$this->usermodel->appuser_get($where);
      }
      if($data->register_type=="facebook")
      {
        $where['facebook_id']=$data->facebook_id;
        $chk=$this->usermodel->appuser_get($where);
      }
      if($data->register_type=="linkedin")
      {
        $where['linkedin_id']=$data->linkedin_id;
        $chk=$this->usermodel->appuser_get($where);
      }
      //echo "<pre>"; print_r($chk); die;
      if(empty($chk))
      {
        if($data->register_type=="normal")
        {          
          $data1['emailid']=$data->email; 
          $data1['password']=(md5($data->password));
          $data1['register_status']='1';
        }
        if($data->register_type=="facebook")
        {
          $data1['facebook_id']=$data->facebook_id;
          $data1['register_status']='1';
        }
        if($data->register_type=="linkedin")
        {
          $data1['linkedin_id']=$data->linkedin_id;
          $data1['register_status']='1';
        }
        $data1['register_type']=$data->register_type;
        if(!empty($data->name))
        {
        	$data1['name']=$data->name;
        }
        $data1['device_type']=$data->device_type;
        if($data->device_type=='android')
        {
          if(!empty($data->fcm_reg_token) && !empty($data->device_id))
          {
            $data1['fcm_reg_token']=$data->fcm_reg_token;
            $data1['device_id']=$data->device_id;
          }
          else
          {
            $response['status']=0;
            $response['message']="Something went Wrong, please try again";
            $this->response($response);
          }
        }
        else
        {
          if(!empty($data->deviceid))
          {
            $data1['deviceid']=$data->deviceid;
          }
          else
          {
            $response['status']=0;
            $response['message']="Something went Wrong, please try again";
            $this->response($response);
          }
        }
        $data1['entry_date']=date('Y-m-d H:i:s');
        if($data->register_type=="normal")
        {
          $this->load->helper('string');
          $code=random_string('alnum', 6);
          $data1['otp']=$code;
        }
        // $data1['user_number']="ES-".substr(uniqid(),0,5);
        $data1['user_number']="ES-".uniqid();
        //////////////QRcode///////////////
          $tempDir = FCPATH."assets/upload/qrcode/";
          $codeContents = "User Number:-".$data1['user_number'];
          $fileName = 'qr_file_'.date("YmdHis").'.png';
          
          $pngAbsoluteFilePath = $tempDir.$fileName;
          $urlRelativeFilePath = FCPATH."assets/upload/qrcode/".$fileName;
          $data1['invitation_code']='';
          $data1['qr_code']=$fileName;
          // generating
          if (!file_exists($pngAbsoluteFilePath)) 
          {
              QRcode::png($codeContents, $pngAbsoluteFilePath);
          } 
        //////////////QRcode///////////////
          $data1['role_id']='0';
          $data1['type_id']='1';

         // echo "<pre>"; print_r($data1); die;
        $register=$this->usermodel->appuser_insert($data1);

        if(!empty($register))
        {
        	$ww['id']=$register;

          // set user notification by default true          
            $notification_data = array('user_id' => $register,
                'event_notify' => 'Y',
                'agenda_notify' => 'Y'
            );
            $this->db->insert(tablename('user_notification'), $notification_data);
            // end set user notification by default true

        	$response['user_info']=$this->usermodel->appuser_get($ww);

          $response['user_info']->event_notify = 'Y';
          $response['user_info']->agenda_notify = 'Y';

          $response['user_info']->email = $response['user_info']->emailid;
          	if($data->register_type=="normal")
          	{
              $msg = '
              <html>
              <head>
                <title>Registration Verification</title>
              </head>
              <body>
                 <p><b>Greetings from Eventsador!</b></p>
                 <p>You are successfully registered.</p>
              </body>
              </html>
              ';

	            $dataa['body']=$msg;
	            send_email($data->email,"Eventsador",$dataa);
	            $response['status']=1;
	            $response['message']="Registration Mail Sent To Your Email";
	            // $response['OTP']=$code;
          	}
          	else
          	{
	            $response['status']=1;
	            $response['message']="Registration Successful";              
          	}
        }
        else
        {
          $response['status']=2;
          $response['message']="Registration Failed";
        }
      }
      else
      {
        if($data->register_type=="normal")
        {
          if(md5($data->password)!=$chk->password)
          {
            $response['status']=4;
            $response['message']="Email id or Password incorrect.";//Login Failed
            $this->response($response);
          }

          if(($data->email)!=$chk->emailid)
          {
            $response['status']=4;
            $response['message']="Email id or Password incorrect.";//Login Failed
            $this->response($response);
          }
        }


        if($data->register_type=="facebook")
        {
          if($data->facebook_id!=$chk->facebook_id)
          {
            $response['status']=4;
            $response['message']="Login Failed";
            $this->response($response);
          }
        }



        if($data->device_type=='android')
        {
          if(!empty($data->fcm_reg_token) && !empty($data->device_id))
          {
            $data1['fcm_reg_token']=$data->fcm_reg_token;
            $data1['device_id']=$data->device_id;
          }
          else
          {
            $response['status']=0;
            $response['message']="Something went Wrong, please try again";
            $this->response($response);
          }
        }else if($data->device_type=='web')
        {
          if(!empty($data->fcm_reg_token) && !empty($data->device_id))
          {
            $data1['fcm_reg_token']=$data->fcm_reg_token;
            $data1['device_id']=$data->device_id;
          }
          else
          {
            $response['status']=0;
            $response['message']="Something went Wrong, please try again";
            $this->response($response);
          }
        }
        else
        {
          if(!empty($data->device_id))
          {
            $data1['device_id']=$data->device_id;
          }
          else
          {
            $response['status']=0;
            $response['message']="Something went Wrong, please try again";
            $this->response($response);
          }
        }
        $this->usermodel->update_data("user",array("id"=>$chk->id),$data1);
        $chk1=$this->usermodel->get_row_data("user",array("id"=>$chk->id));
        if(!empty($chk1->profile_image))
        {
          $chk1->image=base_url('assets/upload/appuser')."/".$chk1->profile_image;
        }
        else
        {
          $chk1->image="";
        }
        $chk1->email=$chk1->emailid;

        if($chk1->role_id==0 && $chk1->type_id==1){
          $chk1->role_type = 'appuser';
        }else if($chk1->role_id==0 && $chk1->type_id==3){
          $chk1->role_type = 'speaker';
        }else if($chk1->role_id==0 && $chk1->type_id==4){
          $chk1->role_type = 'host';
        }else if($chk1->role_id==0 && $chk1->type_id==5){
          $chk1->role_type = 'exhibitor';
        }else if($chk1->role_id==0 && $chk1->type_id==6){
          $chk1->role_type = 'sponsor';
        }else if($chk1->role_id==2 && $chk1->type_id==2){
          $chk1->role_type = 'organizer';
        }else{
          $chk_role=$this->usermodel->get_row_data("role",array("id"=>$chk->role_id));
          $chk1->role_type = strtolower($chk_role->name);
        }

        $response['user_info']=$chk1;
        $response['status']=3;
        $response['message']="Login Successful";
      } 
    }
    else
    {
      $response['status']=0;
      $response['message']="No Data Found";
    }
    $this->response($response);
}

public function check_email_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->email))
    {
      $where = array();
      $where['emailid']=$data->email;
      $where['is_active']='Y';
      $where['delete_flag']='N';
      $chk=$this->usermodel->appuser_get($where);
      if(empty($chk))
      {
        $response['status']=1;
        $response['message']="Email Not Exists";
      }
      else
      {
        $response['status']=2;
        $response['message']="Email Already Exists";
      } 
    }
    else
    {
      $response['status']=0;
      $response['message']="No Data Found";
    }
    $this->response($response);
}

public function event_list_post() 
{
    $where['is_active']='Y';
    $where['delete_flag']='N';
    $ev = $this->eventmodel->get_result_data("event",$where);
    if (!empty($ev)) 
    {
        $final = array();
        foreach ($ev as $e) 
        {
            $data1['id'] = $e->id;
            $data1['event_name'] = $e->event_name;
            $data1['event_venue'] = $e->city.', '.$e->country;
            $data1['event_description'] = $e->event_description;
            $data1['event_date'] = date("d F, Y ", strtotime($e->event_start_date));
            $data1['event_time'] = $e->event_start_time;
            if(!empty($e->event_logo))
            {
            	$data1['logo'] = base_url('assets/upload/event') . '/' . $e->event_logo;
            }
            else
            {
            	$data1['logo'] = "";
            }
            $organizers = explode(',', $e->organizers);
            $data1['organizers'] = $this->eventmodel->get_result_data('user','',$organizers);
            $sponsors = explode(',', $e->sponsors);
            $data1['sponsors'] = $this->eventmodel->get_result_data('user','',$sponsors);
            $media = $this->eventmodel->get_result_data('media_files',array("delete_flag"=>'N',"event_id"=>$e->id));
            if(!empty($media))
            {
                $final_media=array();
                foreach($media as $val)
                {
                    $val->file_name=base_url('assets/upload/media_files').'/'.$val->file_name;
                    $final_media[]=$val;
                }
                $media=$final_media;
            }
            $data1['media'] =$media;
            $final[] = $data1;
        }
        $response['event_info'] = $final;
        $response['status'] = 1;
        $response['message'] = "Event List";
    }
    else 
    {
        $response['status'] = 2;
        $response['message'] = "Events Not Available";
    }
    $this->response($response);
}


/////////////////////////////////// MY EVENT LIST //////////////////////////////////////

public function my_event_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->id))
    {
      
      $where        = '';
      $where        = " delete_flag = 'N' AND attendees LIKE '%".$data->id."%'";
      $result_list    = $this->eventmodel->get_my_events('event',$where);

     /* echo "<pre>";
      print_r($result_list);
      exit;*/

      if(!empty($result_list))
      {
        $final = array();
        foreach ($result_list as $e) 
        {
            $data1['id'] = $e->id;
            $data1['event_name'] = $e->event_name;
            $data1['event_venue'] = $e->city.', '.$e->country;
            $data1['event_description'] = $e->event_description;
            $data1['event_date'] = date("d F, Y ", strtotime($e->event_start_date));
            $data1['event_time'] = $e->event_start_time;
            if(!empty($e->event_logo))
            {
              $data1['logo'] = base_url('assets/upload/event') . '/' . $e->event_logo;
            }
            else
            {
              $data1['logo'] = "";
            }

            $attendees = explode(',', $e->attendees);
            $data1['attendees'] = $this->eventmodel->get_result_data('user','',$attendees);

            $organizers = explode(',', $e->organizers);
            $data1['organizers'] = $this->eventmodel->get_result_data('user','',$organizers);
            $sponsors = explode(',', $e->sponsors);
            $data1['sponsors'] = $this->eventmodel->get_result_data('user','',$sponsors);
            $media = $this->eventmodel->get_result_data('media_files',array("delete_flag"=>'N',"event_id"=>$e->id));
            if(!empty($media))
            {
                $final_media=array();
                foreach($media as $val)
                {
                    $val->file_name=base_url('assets/upload/media_files').'/'.$val->file_name;
                    $final_media[]=$val;
                }
                $media=$final_media;
            }
            $data1['media'] =$media;
            $final[] = $data1;
        }
        $response['event_info'] = $final;
        $response['status'] = 1;
        $response['message'] = "My Event List";
      }
      else
      {
        $response['status']=2;
        $response['message']="No Events Found";
      } 
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// UPCOMING EVENTS //////////////////////////////////////

public function upcoming_event_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->id))
    {
      
      $where        = '';
      $where        = " is_active = 'Y' AND delete_flag = 'N' AND event_start_date > CURDATE()";
      $result_list    = $this->eventmodel->get_my_events('event',$where);

      // echo "<pre>";
      // print_r($result_list);
      // exit;

      if(!empty($result_list))
      {
        $final = array();
        foreach ($result_list as $e) 
        {
            $data1['id'] = $e->id;
            $data1['event_name'] = $e->event_name;
            // $data1['event_venue'] = $e->event_venue;
            $data1['event_venue'] = $e->city.', '.$e->country;
            $data1['event_description'] = $e->event_description;
            // $data1['route']=$e->route;
            // $data1['street_no'] = $e->street_no;
            // $data1['street_name'] = $e->street_name;
            // $data1['state'] = $e->state;
            // $data1['city'] = $e->city;
            // $data1['country']=$e->country;
            // $data1['zip_code'] = $e->zip_code;
            // $data1['lat'] = $e->lat;
            // $data1['lng'] = $e->lng;
            $data1['event_date'] = date("d F, Y ", strtotime($e->event_start_date));
            $data1['event_time'] = $e->event_start_time;
            if(!empty($e->event_logo))
            {
              $data1['logo'] = base_url('assets/upload/event') . '/' . $e->event_logo;
            }
            else
            {
              $data1['logo'] = "";
            }

            $attendees = explode(',', $e->attendees);
            $data1['attendees'] = $this->eventmodel->get_result_data('user','',$attendees);

            $organizers = explode(',', $e->organizers);
            $data1['organizers'] = $this->eventmodel->get_result_data('user','',$organizers);
            $sponsors = explode(',', $e->sponsors);
            $data1['sponsors'] = $this->eventmodel->get_result_data('user','',$sponsors);
            $media = $this->eventmodel->get_result_data('media_files',array("delete_flag"=>'N',"event_id"=>$e->id));
            if(!empty($media))
            {
                $final_media=array();
                foreach($media as $val)
                {
                    $val->file_name=base_url('assets/upload/media_files').'/'.$val->file_name;
                    $final_media[]=$val;
                }
                $media=$final_media;
            }
            $data1['media'] =$media;
            $final[] = $data1;
        }
        $response['event_info'] = $final;
        $response['status'] = 1;
        $response['message'] = "Event List";
      }
      else
      {
        $response['status']=2;
        $response['message']="No Events Found";
      } 
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// PAST EVENTS //////////////////////////////////////

public function past_event_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->id))
    {
      
      $where        = '';
      $where        = " is_active = 'Y' AND delete_flag = 'N' AND event_date < CURDATE()";
      $result_list    = $this->eventmodel->get_my_events('event',$where);

      if(!empty($result_list))
      {
        $final = array();
        foreach ($result_list as $e) 
        {
            $data1['id'] = $e->id;
            $data1['event_name'] = $e->event_name;
            // $data1['event_venue'] = $e->event_venue;
            $data1['event_venue'] = $e->city.', '.$e->country;
            $data1['event_description'] = $e->event_description;
            // $data1['route']=$e->route;
            // $data1['street_no'] = $e->street_no;
            // $data1['street_name'] = $e->street_name;
            // $data1['state'] = $e->state;
            // $data1['city'] = $e->city;
            // $data1['country']=$e->country;
            // $data1['zip_code'] = $e->zip_code;
            // $data1['lat'] = $e->lat;
            // $data1['lng'] = $e->lng;
            $data1['event_date'] = date("d F, Y ", strtotime($e->event_start_date));
            $data1['event_time'] = $e->event_start_time;
            if(!empty($e->event_logo))
            {
              $data1['logo'] = base_url('assets/upload/event') . '/' . $e->event_logo;
            }
            else
            {
              $data1['logo'] = "";
            }

            $attendees = explode(',', $e->attendees);
            $data1['attendees'] = $this->eventmodel->get_result_data('user','',$attendees);

            $organizers = explode(',', $e->organizers);
            $data1['organizers'] = $this->eventmodel->get_result_data('user','',$organizers);
            $sponsors = explode(',', $e->sponsors);
            $data1['sponsors'] = $this->eventmodel->get_result_data('user','',$sponsors);
            $media = $this->eventmodel->get_result_data('media_files',array("delete_flag"=>'N',"event_id"=>$e->id));
            if(!empty($media))
            {
                $final_media=array();
                foreach($media as $val)
                {
                    $val->file_name=base_url('assets/upload/media_files').'/'.$val->file_name;
                    $final_media[]=$val;
                }
                $media=$final_media;
            }
            $data1['media'] =$media;
            $final[] = $data1;
        }
        $response['event_info'] = $final;
        $response['status'] = 1;
        $response['message'] = "My Event List";
      }
      else
      {
        $response['status']=2;
        $response['message']="No Events Found";
      } 
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////



/////////////////////////////////// EVENT DETAILS //////////////////////////////////////

public function event_details_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->event_id))
    {
      
      $result_list  = $this->eventmodel->get_result_data('event',array("id"=>$data->event_id));

      if(!empty($result_list))
      {
        $final = array();
        foreach ($result_list as $e) 
        {
            $data1['id'] = $e->id;
            $data1['event_name'] = $e->event_name;
            $data1['event_venue'] = $e->event_venue;
            //$data1['event_venue'] = $e->city.', '.$e->country;
            $data1['event_description'] = $e->event_description;
            $data1['street_no'] = $e->street_no;
            $data1['street_name'] = $e->street_name;
            $data1['state'] = $e->state;
            $data1['city'] = $e->city;
            $data1['country']=$e->country;
            $data1['zip_code'] = $e->zip_code;
            $data1['lat'] = $e->lat;
            $data1['lng'] = $e->lng;
            $data1['event_date'] = date("d F, Y ", strtotime($e->event_start_date));
            $data1['event_time'] = $e->event_start_time;
            if(!empty($e->event_logo))
            {
              $data1['logo'] = base_url('assets/upload/event') . '/' . $e->event_logo;
            }
            else
            {
              $data1['logo'] = "";
            }

            $attendees = explode(',', $e->attendees);
            $data1['attendees'] = $this->eventmodel->get_result_data('user','',$attendees);

            $organizers = explode(',', $e->organizers);
            $data1['organizers'] = $this->eventmodel->get_result_data('user','',$organizers);
            $sponsors = explode(',', $e->sponsors);
            $data1['sponsors'] = $this->eventmodel->get_result_data('user','',$sponsors);
            $media = $this->eventmodel->get_result_data('media_files',array("delete_flag"=>'N',"event_id"=>$e->id));
            if(!empty($media))
            {
                $final_media=array();
                foreach($media as $val)
                {
                    $val->file_name=base_url('assets/upload/media_files').'/'.$val->file_name;
                    $final_media[]=$val;
                }
                $media=$final_media;
            }
            $data1['media'] =$media;

            /*****/
            if($e->event_start_date > date('Y-m-d')){
               $data1['event_type'] = 'Upcoming';
            }elseif($e->event_end_date < date('Y-m-d')){
               $data1['event_type'] = 'Past';
            }else{
              $data1['event_type'] = '';
            }

            $final[] = $data1;
        }
        $response['event_info'] = $final;
        $response['status'] = 1;
        $response['message'] = "Event Details";
      }
      else
      {
        $response['status']=2;
        $response['message']="No Events Found";
      } 
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// ORGANIZER DETAILS //////////////////////////////////////

public function event_organizer_details_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->event_id))
    {
      
      $result_list  = $this->eventmodel->get_result_data('event',array("id"=>$data->event_id));


      if(!empty($result_list))
      {

        if(!empty($result_list[0]->organizers))
        {

          $organizers = explode(',', $result_list[0]->organizers);

          $organizer_list = $this->eventmodel->get_result_data('user','',$organizers);
          $final_array=array();
          if(!empty($organizer_list))
          {
          	foreach($organizer_list as $val)
          	{
          		if(!empty($val->profile_image))
          		{
          			$val->image=base_url('assets/upload/profimg')."/".$val->profile_image;
          		}
          		$final_array[]=$val;
          	}
          }
          $response['status']=1;
          $response['event_organizer_info'] = $final_array;
          $response['message']="Event Organizer List";

        }
        else
        {
            $response['status']=3;
            $response['message']="Event Has No Organizers";
        }
      }
      else
      {
        $response['status']=2;
        $response['message']="No Event Found";
      } 
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// ATTENDEES DETAILS //////////////////////////////////////

public function event_attendees_details_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->event_id) && !empty($data->attendies_id))
    {
      
      $result_list  = $this->eventmodel->get_result_data('event',array("id"=>$data->event_id));


      if(!empty($result_list))
      {

          if(!empty($result_list[0]->attendees))
          {
            $attendees = explode(',', $result_list[0]->attendees);

            $attendees_list = $this->eventmodel->get_result_data('user','',$attendees);

            //echo "<pre>"; print_r($attendees_list); die;

            $final_array=array();
            if(!empty($attendees_list))
            {
              foreach($attendees_list as $key=>$val)
              {
                //echo "<pre>"; print_r($val); die;
                // check this user is bookmark or not
                $friend=$this->db->where('user_id',$data->attendies_id)->where('friend_id',$val->id)->get('bookmark')->num_rows();
                $val->bookmark = $friend;


                if($val->id==$data->attendies_id)
                {
                  unset($attendees_list[$key]);
                  continue;
                }
                if(!empty($val->profile_image))
                {
                  $val->profile_image=base_url('assets/upload/appuser')."/".$val->profile_image;
                }   else{
                  $val->profile_image='';
                }   

                if(!empty($val->country))
                {
                  $country = $this->db->where('id',$val->country)->get('countries')->row();
                  $val->country=$country->name;
                }
                 if(!empty($val->city))
                {
                  $city = $this->db->where('id',$val->city)->get('cities')->row();
                  $val->city=$city->name;
                }

                $final_array[]=$val;
              }
            }
            $response['status']=1;
            $response['event_attendees_info'] = $final_array;
            $response['message']="Event Attendees List";
          }
          else
          {
            $response['status']=3;
            $response['message']="Event Has No Attendees";
          }
      }
      else
      {
        $response['status']=2;
        $response['message']="No Event Found";
      } 
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// DRESS CODE DETAILS //////////////////////////////////////

public function event_dress_code_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->event_id))
    {
      
      $result_list  = $this->eventmodel->get_result_data('event',array("id"=>$data->event_id));

      if(!empty($result_list))
      {

        if(!empty($result_list[0]->dress_code))
        {

          $response['status']=1;
          $event_dress_code_info['event_dress_code'] = $result_list[0]->dress_code;
          if(!empty($result_list[0]->dress_code_image))
          {
          	$event_dress_code_info['event_dress_code_image'] = base_url('assets/upload/event/dress_code')."/".$result_list[0]->dress_code_image;
          }
          else
          {
          	$event_dress_code_info['event_dress_code_image'] = "";
          }
          $response['event_dress_code_info'] = $event_dress_code_info;
          $response['message']="Event Dress Code";
        }
        else
        {
          $response['status']=3;
          $response['message']="Dress Code Not Found";
        } 
      }
      else
      {
        $response['status']=2;
        $response['message']="No Event Found";
      }
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// EVENT AGENDA DETAILS //////////////////////////////////////

public function event_agenda_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->event_id))
    {
      
      $result_list  = $this->eventmodel->get_result_data('event',array("id"=>$data->event_id));

      if(!empty($result_list))
      {

        $agenda_list  = $this->eventmodel->get_result_data('agenda',array("event_id"=>$data->event_id,"agenda_owner_type"=>"admin","is_active"=>"Y","delete_flag"=>"N"));


        if(!empty($agenda_list))
        {

          $response['status']=1;
          $response['event_agenda_info'] = $agenda_list;
          $response['message']="Event Agenda List";

        }
        else
        {
          $response['status']=3;
          $response['message']="Agenda Not Found";
        } 

      }
      else
      {
        $response['status']=2;
        $response['message']="No Event Found";
      }
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// SPEAKERS DETAILS //////////////////////////////////////

public function event_speakers_details_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->event_id))
    {
      
      $result_list  = $this->eventmodel->get_result_data('event',array("id"=>$data->event_id));


      if(!empty($result_list))
      {

        $speakers = explode(',', $result_list[0]->speakers);
        $speaker_list = $this->eventmodel->get_result_data('user','',$speakers);
        $final_array=array();
        if(!empty($speaker_list))
        {
          	foreach($speaker_list as $val)
          	{
          		if(!empty($val->profile_image))
          		{
          			$val->image=base_url('assets/upload/appuser')."/".$val->profile_image;
          		}
          		$final_array[]=$val;
          	}
        }
        $response['status']=1;
        $response['event_speaker_info'] = $final_array;
        $response['message']="Event Speakers List";
      }
      else
      {
        $response['status']=2;
        $response['message']="No Event Found";
      } 
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// SPEAKER LIKE //////////////////////////////////////

public function speaker_like_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->event_id) && !empty($data->speaker_id) && !empty($data->attendee_id) && !empty($data->like))
    {

      $result_list  = $this->eventmodel->get_result_data('event',array("id"=>$data->event_id));


      if(!empty($result_list))
      {
      
          $data1                = array();
          $data1['event_id']    = $data->event_id;
          $data1['speaker_id']  = $data->speaker_id;
          $data1['attendee_id'] = $data->attendee_id;
          $data1['is_like']     = $data->like;

          $insert_like=$this->eventmodel->insert_data('speaker_like',$data1);


          if(!empty($insert_like))
          {

            $response['status']=1;
            $response['message']="Speaker Liked";

          }
          
      }
      else
      {
        $response['status']=2;
        $response['message']="No Event Found";
      } 
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// SPEAKER COMMENT //////////////////////////////////////

public function speaker_comment_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->event_id) && !empty($data->speaker_id) && !empty($data->attendee_id) && !empty($data->comment))
    {

      $result_list  = $this->eventmodel->get_result_data('event',array("id"=>$data->event_id));


      if(!empty($result_list))
      {
      
          $data1                = array();
          $data1['event_id']    = $data->event_id;
          $data1['speaker_id']  = $data->speaker_id;
          $data1['attendee_id'] = $data->attendee_id;
          $data1['comment']     = $data->comment;

          $insert_comment=$this->eventmodel->insert_data('speaker_comment',$data1);


          if(!empty($insert_comment))
          {
            $dat['event_id']=$data->event_id;
            $dat['user_id']=$data->attendee_id;
            $dat['comment_id']=$insert_comment;
            $dat['point_type']="Comment/Speaker";
            $dat['point']=3;
            $dat['created_date']=date("Y-m-d H:i:s");
            $this->pointmodel->insert_data("point",$dat);

            $response['status']=1;
            $response['message']="Comment Successful";

          }
         

      }
      else
      {
        $response['status']=2;
        $response['message']="No Event Found";
      } 
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// ATTENDEES CHAT //////////////////////////////////////

public function attendees_chatbk_post()
{
    $data=json_decode( file_get_contents('php://input') );
   // echo "<pre>"; print_r($data); die;
    if(!empty($data->event_id) && !empty($data->sender_id) && !empty($data->receiver_id) && !empty($data->message))
    {

      $result_list  = $this->eventmodel->get_result_data('event',array("id"=>$data->event_id));


      if(!empty($result_list))
      {
      	$data1                = array();
        $data1['event_id']    = $data->event_id;
        $data1['sender_id']   = $data->sender_id;
        $data1['receiver_id'] = $data->receiver_id;
        $data1['message']     = $data->message;
        $data1['created_date']= date("Y-m-d H:i:s");

      	$chat_head_info  = $this->eventmodel->getchat('ets_chat_head',array("event_id"=>$data->event_id,"sender_id"=>$data->sender_id,"receiver_id"=>$data->receiver_id));
      	if(empty($chat_head_info))
      	{
      		$insert_chat_head=$this->eventmodel->insert_data('chat_head',$data1);
      	}
      	else
      	{
      		$update_chat_head=$this->eventmodel->update_data('chat_head',array("id"=>$chat_head_info[0]->id),$data1);
      	}

        //echo "<pre>"; print_r($data1); die;
        $insert_chat=$this->eventmodel->insert_data('attendees_chat',$data1);


        if(!empty($insert_chat))
        {

            $response['status']=1;
            $response['message']="Chat Successful";

        }
        

      }
      else
      {
        $response['status']=2;
        $response['message']="No Event Found";
      } 
    
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}



public function attendees_chat_post()
{
    $data=json_decode( file_get_contents('php://input') );
    //echo "<pre>"; print_r($data); die;
    if(!empty($data->event_id) && !empty($data->sender_id) && !empty($data->receiver_id) && !empty($data->message))
    {

      $result_list  = $this->eventmodel->get_result_data('event',array("id"=>$data->event_id));
      //echo "<pre>"; print_r($result_list); die;
      if(!empty($result_list))
      {

        if(!empty($data->say_hi) && $data->say_hi == '1')
        {
          $where = array();
          $where['event_id']=$data->event_id;
          $where['sender_id']=$data->sender_id;
          $where['receiver_id']=$data->receiver_id;
          $where['say_hi']='1';
          $hi_exist = $this->eventmodel->get_row_data("attendees_chat", $where);
          if($hi_exist)
          {
             $response['status']  = 3;
            $response['message'] = "User has already waved";
            $this->response($response);
          }
         
        }

        $data1                = array();
        $data1['event_id']    = $data->event_id;
        $data1['sender_id']   = $data->sender_id;
        $data1['receiver_id'] = $data->receiver_id;
        $data1['message']     = $data->message;
        if(!empty($data->say_hi))
        {
          $data1['say_hi']      = $data->say_hi;
        }
        $data1['created_date']= date("Y-m-d H:i:s");

        $chat_head_info  = $this->eventmodel->getchat('ets_chat_head',array("event_id"=>$data->event_id,"sender_id"=>$data->sender_id,"receiver_id"=>$data->receiver_id));
        if(empty($chat_head_info))
        {
          $insert_chat_head=$this->eventmodel->insert_data('chat_head',$data1);
        }
        else
        {
          $update_chat_head=$this->eventmodel->update_data('chat_head',array("id"=>$chat_head_info[0]->id),$data1);
        }


        //echo "<pre>"; print_r($data1); die;
        $insert_chat=$this->eventmodel->insert_data('attendees_chat',$data1);


        if(!empty($insert_chat))
        {
          // notification to receiver
            $where3['id'] = $data->receiver_id;
            $where4['id'] = $data->sender_id;
            $receiver_details = $this->eventmodel->get_row_data("user", $where3);
            $sender_details = $this->eventmodel->get_row_data("user", $where4);
            $this->notification($receiver_details->fcm_reg_token,$data->message,$sender_details->name.' send you messange');

            // end notification to receiver

            $response['status']=1;
            $response['message']="Chat Successful";
        }
        

      }
      else
      {
        $response['status']=2;
        $response['message']="No Event Found";
      } 
    
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

// public function privacy_policy_get()
// {

//     $result=$this->aboutusmodel->fetch_details();

//     if(!empty($result))
//     {
//         $response['status']=1;
//         $response['result']=$result->privacy_statement;
//         $response['message']="Privacy Statement";
//     }
//     else
//     {
//         $response['status']=0;
//         $response['message']="No Data Found";
//     } 
   
//     $this->response($response);
// }

public function privacy_policy_post()
{

    $result=$this->aboutusmodel->fetch_details();

    if(!empty($result))
    {
        $response['status']=1;
        //$response['result']=$result->privacy_statement;
        $result= strip_tags(html_entity_decode($result->privacy_statement));
        $result= str_replace("\r\n",'', $result);
        $response['result']= str_replace("\t",'', $result); 



        $response['message']="Privacy Statement";
    }
    else
    {
        $response['status']=0;
        $response['message']="No Data Found";
    } 
   
    $this->response($response);
}


// public function terms_of_use_get()
// {

//     $result=$this->aboutusmodel->fetch_details();

//     if(!empty($result))
//     {
//         $response['status']=1;
//         $response['result']=$result->terms_of_use;
//         $response['message']="Terms Of Use";
//     }
//     else
//     {
//         $response['status']=0;
//         $response['message']="No Data Found";
//     } 
   
//     $this->response($response);
// }

public function terms_of_use_post()
{

    $result=$this->aboutusmodel->fetch_details();

    if(!empty($result))
    {
        $response['status']=1;
        // $response['result']= $result->terms_of_use;
        $result= strip_tags(html_entity_decode($result->terms_of_use));
        $result= str_replace("\r\n",'', $result);
        $response['result']= str_replace("\t",'', $result); 
        $response['message']="Terms Of Use";
    }
    else
    {
        $response['status']=0;
        $response['message']="No Data Found";
    } 
   
    $this->response($response);
}

/////////////////////////////////// Like Event //////////////////////////////////////

public function like_event_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->attendies_id))
  {
    $where['event_id']=$data->event_id;
    $where['attendies_id']=$data->attendies_id;
    $like_search = $this->eventmodel->get_row_data("event_like", $where);
    if(!empty($like_search))
    {
      $like_del=$this->eventmodel->delete_data("event_like",$where);
      if(!empty($like_del))
      {
        $response['status']=3;
        $response['message']="Like Removed";
      }
      else
      {
        $response['status']=4;
        $response['message']="Like Not Removed";
      }
    }
    else
    {
      $data1['event_id']=$data->event_id;
      $data1['attendies_id']=$data->attendies_id;
      $data1['created_date']=date("Y-m-d H:i:s");
      $like_insert = $this->eventmodel->insert_data("event_like",$data1);
      if(!empty($like_insert))
      {
        $response['status']=1;
        $response['message']="Liked";
      }
      else
      {
        $response['status']=2;
        $response['message']="Error in Like Process";
      }
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Comment Event //////////////////////////////////////

public function comment_event_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->attendies_id) && !empty($data->comment))
  {
    $data1['event_id']=$data->event_id;
    $data1['attendies_id']=$data->attendies_id;
    $data1['comment']=$data->comment;
    $data1['created_date']=date("Y-m-d H:i:s");
    $comment_insert = $this->eventmodel->insert_data("event_comment",$data1);
    if(!empty($comment_insert))
    {
      $response['status']=1;
      $response['message']="Comment Posted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Comment Not Posted";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Get Comments of Event //////////////////////////////////////

public function get_event_comment_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $where['event_id']=$data->event_id;
    $event_info = $this->eventmodel->get_result_data("event_comment", $where);
    if(!empty($event_info))
    {
      $eventarr=array();
      foreach($event_info as $val)
      {
        $where1['id']=$val->attendies_id;
        $user_info = $this->eventmodel->get_row_data("user", $where1);
        $val->user_info=$user_info;
        $eventarr[]=$val;
      }
      $response['status']=1;
      $response['message']="Comment List";
      $response['comment_info']=$eventarr;
    }
    else
    {
      $response['status']=2;
      $response['message']="No Comment Found";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Event Search //////////////////////////////////////

public function search_event_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->tag))
  {
    $event_info = $this->eventmodel->search_event($data->tag);
    if(!empty($event_info))
    {
      $final=array();
      foreach ($event_info as $e) 
      {
        $e->event_dat = date("d F, Y ", strtotime($e->event_start_date));
        $e->event_date = $e->event_start_time;
        if(!empty($e->event_logo))
        {
          $e->logo = base_url('assets/upload/event') . '/' . $e->event_logo;
        }
        else
        {
          $e->logo = "";
        }
        $final[] = $e;
      }
      $response['status']=1;
      $response['message']="Event List";
      $response['event_info']=$final;
    }
    else
    {
      $response['status']=2;
      $response['message']="No Event Found";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Check User Registered Event //////////////////////////////////////

public function check_user_registered_event_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->user_id) && !empty($data->event_id))
  {
    $where['id']=$data->event_id;
    $event_info = $this->eventmodel->get_row_data("event", $where);
    if(!empty($event_info))
    {
      $attendees = explode(',', $event_info->attendees);
      if(!empty($attendees))
      {
        $flag=0;
        foreach($attendees as $val)
        {
          if($val==$data->user_id)
          {
            $flag=1;
            break;
          }
        }
        if($flag==1)
        {
          $response['status']=1;
          $response['message']="Attendee";
        }
        else
        {
          $response['status']=2;
          $response['message']="Not an Attendee";
        }
      }
      else
      {
        $response['status']=3;
        $response['message']="No Attendee Found";
      }
    }
    else
    {
      $response['status']=4;
      $response['message']="Invalid Event";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Rate Event //////////////////////////////////////

public function rate_event_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->attendies_id) && !empty($data->rate))
  {
    $where['event_id']=$data->event_id;
    $where['attendies_id']=$data->attendies_id;
    $rate_search = $this->eventmodel->get_row_data("event_rate", $where);
    if(empty($rate_search))
    {
      $data1['event_id']=$data->event_id;
      $data1['attendies_id']=$data->attendies_id;
      $data1['rate']=$data->rate;
      $data1['created_date']=date("Y-m-d H:i:s");
      $rate_insert = $this->eventmodel->insert_data("event_rate",$data1);
      if(!empty($rate_insert))
      {
        $response['status']=1;
        $response['message']="Rating Done";
      }
      else
      {
        $response['status']=2;
        $response['message']="Rating Not Done";
      }
    }
    else
    {
      $response['status']=3;
      $response['message']="You have already post your rating";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Request Access Event //////////////////////////////////////

public function request_access_event_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->user_id))
  {
    $where['event_id']=$data->event_id;
    $where['user_id']=$data->user_id;
    $request_access = $this->eventmodel->get_row_data("event_access_request", $where);
    if(empty($request_access))
    {
      $data1['event_id']=$data->event_id;
      $data1['user_id']=$data->user_id;
      $data1['created_date']=date("Y-m-d H:i:s");
      $request_insert = $this->eventmodel->insert_data("event_access_request",$data1);
      if(!empty($request_insert))
      {
        //// mail to admin
        $where1['id']=$data->event_id;
        $where2['id']=$data->user_id;
        $event_details = $this->eventmodel->get_row_data("event", $where1);
        //echo "<pre>"; print_r($event_details); die;
        $user_details = $this->eventmodel->get_row_data("user", $where2);

        // mail to user
        $subject1 = "Greetings from Eventsador";
        $message1 = '<p>Hello '.$user_details->name.',</p>';
        $message1 .= '<p> To join the event a Confirmation code would be send to you when approved by the organizer.</p>';
        $message1 .= '<p>Thank you,</p>';
        $message1 .= '<p>Eventsador</p>';

        $mail_data1 = [
             'name' => $user_details->name,
             'body' => $message1,
        ];
        send_email($user_details->emailid, $subject1, $mail_data1);

        $subject = "Eventsador - User Requested for Event";
        $message = '<p>Hello ,</p>';
        $message .= '<p> A User Requested for Event :'.$event_details->event_name.'</p>';

        $message .= '<br><br><p> <b>User Details : - </b></p>';
        $message .= '<p> User Name :'.$user_details->name.'</p>';
        $message .= '<p> User Email :'.$user_details->emailid.'</p>';

        $message .= '<p>Thank you,</p>';
        $message .= '<p>Eventsador</p>';

        $mail_data = [
             'name' => $user_details->name,
             'body' => $message,
        ];
        $this->load->helper('email');

        //send_email('app@eventsador.com', $subject, $mail_data);

        // send mail to event admin
        $event_admin = explode(',', $event_details->users);
        if(!empty($event_admin)){
          foreach ($event_admin as $val) {
            $where44['id']=$val;
            $user_details = $this->eventmodel->get_row_data("user", $where44);
            send_email($user_details->emailid, $subject, $mail_data);
            if($user_details->fcm_reg_token!=''){
              $this->notification($user_details->fcm_reg_token,$message,$subject);
            }
          }
        }


        // send mail to organizers
        $event_organizers = explode(',', $event_details->organizers);
        if(!empty($event_organizers)){
          foreach ($event_organizers as $valOrg) {
            $where48['id']=$valOrg;
            $user_org = $this->eventmodel->get_row_data("user", $where48);
            send_email($user_org->emailid, $subject, $mail_data);
            //echo $user_details->fcm_reg_token;
            if($user_details->fcm_reg_token!=''){

              $this->notification($user_org->fcm_reg_token,$message,$subject);
            }
          }
        }




        $response['status']=1;
        $response['message']="Request Sent";
      }
      else
      {
        $response['status']=2;
        $response['message']="Request Not Sent";
      }
    }
    else
    {
        //// mail to admin
        $where1['id']=$data->event_id;
        $where2['id']=$data->user_id;
        $event_details = $this->eventmodel->get_row_data("event", $where1);
        //echo "<pre>"; print_r($event_details); die;
        $user_details = $this->eventmodel->get_row_data("user", $where2);

        // mail to user
        $subject1 = "Greetings from Eventsador";
        $message1 = '<p>Hello '.$user_details->name.',</p>';
        $message1 .= '<p> To join the event a Confirmation code would be send to you when approved by the organizer.</p>';
        $message1 .= '<p>Thank you,</p>';
        $message1 .= '<p>Eventsador</p>';

        $mail_data1 = [
             'name' => $user_details->name,
             'body' => $message1,
        ];
        send_email($user_details->emailid, $subject1, $mail_data1);


        $subject = "Eventsador - User Requested for Event";
        $message = '<p>Hello ,</p>';
        $message .= '<p> A User Requested for Event :'.$event_details->event_name.'</p>';

        $message .= '<br><br><p> <b>User Details : - </b></p>';
        $message .= '<p> User Name :'.$user_details->name.'</p>';
        $message .= '<p> User Email :'.$user_details->emailid.'</p>';

        $message .= '<p>Thank you,</p>';
        $message .= '<p>Eventsador</p>';

        $mail_data = [
             'name' => $user_details->name,
             'body' => $message,
        ];
        $this->load->helper('email');

        //send_email('app@eventsador.com', $subject, $mail_data);

        // send mail to event admin
        $event_admin = explode(',', $event_details->users);
        if(!empty($event_admin)){
          foreach ($event_admin as $val) {
            $where44['id']=$val;
            $user_details = $this->eventmodel->get_row_data("user", $where44);
            send_email($user_details->emailid, $subject, $mail_data);
            if($user_details->fcm_reg_token!=''){
              $this->notification($user_details->fcm_reg_token,$message,$subject);
            }
          }
        }

        // send mail to organizers
        $event_organizers = explode(',', $event_details->organizers);
        if(!empty($event_organizers)){
          foreach ($event_organizers as $valOrg) {
            $where48['id']=$valOrg;
            $user_org = $this->eventmodel->get_row_data("user", $where48);
            send_email($user_org->emailid, $subject, $mail_data);
            //echo $user_details->fcm_reg_token;
            if($user_details->fcm_reg_token!=''){

              $this->notification($user_org->fcm_reg_token,$message,$subject);
            }
          }
        }     


      $response['status']=3;
      // $response['message']="You have already sent request";
      $response['message']="Request sent again";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Join Event //////////////////////////////////////

public function join_event_post()
{
 	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->event_id) && !empty($data->user_id) && !empty($data->invitation_code))
  	{
	    $where['event_id']=$data->event_id;
	    $where['user_id']=$data->user_id;
	    $request_access = $this->eventmodel->get_row_data("event_access_request", $where);
	    if(!empty($request_access))
	    {
	    	if($request_access->event_join=="1")
	      	{
		        $response['status']=4;
		        $response['message']="Already joined";
	      	}
	      	else
	      	{
		        if($request_access->invitation_code==$data->invitation_code)
		        {
		        	$data1['event_join']='1';
              $up_request_access = $this->eventmodel->update_data("event_access_request",$where,$data1);
		        	if(!empty($up_request_access))
		        	{
		        		$where1['id']=$data->event_id;
		        		$event_info = $this->eventmodel->get_row_data("event", $where1);
		        		if(!empty($event_info->attendees))
		        		{
		        			$attendees = explode(',', $event_info->attendees);
		      				$array_index=count($attendees)+1;	
		      				$attendees[$array_index]=$data->user_id;
		      				$attendees = implode(',', $attendees);
		      			}
		        		else
		  				  {
		  					  $attendees=$data->user_id;
		      			}
		      			$up_event = $this->eventmodel->update_data("event",$where1,array("attendees"=>$attendees));
	      				if(!empty($up_event))
	      				{
                  $response['status']=1;
	          			$response['message']="Joined";
	      				}
	      				else
	      				{
	      					$response['status']=2;
	          				$response['message']="Error in join process";
	      				}
		        	}
		        	else
					{
						$response['status']=2;
						$response['message']="Error in join process";
					}
		        }
		        else
		        {
		          $response['status']=3;
		          $response['message']="Invalid invitation code";
		        }
	      	}
	    }
	    else
	    {
	    	$response['status']=5;
	      	$response['message']="No request found";
	    }
  	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// ATTENDEES CHAT List//////////////////////////////////////

public function get_chat_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->sender_id) && !empty($data->receiver_id))
  {
  	$where['event_id']=$data->event_id;
  	$where['sender_id']=$data->sender_id;
  	$where['receiver_id']=$data->receiver_id;
    $this->eventmodel->update_chat(tablename("attendees_chat"),$where);
  	$chat_list  = $this->eventmodel->getchat(tablename("attendees_chat"),$where);

    $get_chat_dates  = $this->eventmodel->getchatdates(tablename("attendees_chat"),$where);


    $final_chat_list = array();
    $i = 0;
    foreach($get_chat_dates as $dates)
    {
      $new_chat_list = array();
      foreach($chat_list  as $val)
      {
        if($dates->chat_date==$val->chat_date)
        {
          $new_chat_list['date']=$dates->chat_date;
          $new_chat_list['info'][]=$val;
        }
      }
      $final_chat_list[]=$new_chat_list;
      // $new_chat_list[$i]['date'] = $dates->chat_date;
      // $new_chat_list[$i]['chat_data'] =  (object)$chat_list[$dates->chat_date];
      // $i++;
    }
    // print_r($final_chat_list);die;

   
  	if(!empty($final_chat_list))
    {
      $response['status']=1;
      $response['message']="Chat List";
      $response['chat_list']=$final_chat_list;
    }
    else
    {
      $response['status']=2;
      $response['message']="No Chat Found";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// CHAT Messages //////////////////////////////////////

public function chat_messages_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->attendies_id))
    {
    	$chat_messages  = $this->eventmodel->chat_messages($data->attendies_id);
     // echo $this->db->last_query(); die;
    	if(!empty($chat_messages))
	    {
        $final_array=array();
        foreach($chat_messages as $val)
        {
        	$whr['event_id']=$val->event_id;
        	$whr['sender_id']=$val->sender_id;
        	$whr['receiver_id']=$val->receiver_id;
        	$val->total_unread=$this->eventmodel->chat_count($whr);
        	if($val->sender_id==$data->attendies_id)
        	{
        		$where['id']=$val->receiver_id;
        	}
        	else
        	{
        		$where['id']=$val->sender_id;
        	}
          	$info=$this->usermodel->appuser_get($where);
          	if(!empty($info->profile_image))
          	{
          		$val->image=base_url('assets/upload/appuser')."/".$info->profile_image;
          	}
          	else
          	{
          		$val->image="";
          	}
          	$val->friend_name=@$info->name;
          	$final_array[]=$val;
        }
	    	$response['status']=1;
	      	$response['message']="Chat Message List";
	      	$response['chat_message_list']=$final_array;
	    }
	    else
	    {
	        $response['status']=2;
	        $response['message']="No Chat Message Found";
	    } 
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Update Profile //////////////////////////////////////

public function updateprofile_post()
{ 
    $data=json_decode( file_get_contents('php://input'));
    if(!empty($data->user_id))
    {
    	$data1=array();
    	if(!empty($data->phone_no))
      	{
        	$data1['phoneno']=$data->phone_no;
      	}
      	if(!empty($data->mobile_no))
      	{
        	$data1['mobile_no']=$data->mobile_no;
      	}
      	if(!empty($data->address))
      	{
        	$data1['address']=$data->address;
      	}
      	if(!empty($data->email))
      	{
        	$data1['contact_email']=$data->email;
      	}
      	if(!empty($data->skype_id))
      	{
        	$data1['skype_id']=$data->skype_id;
      	}
      	if(!empty($data->wechat_id))
      	{
        	$data1['wechat_id']=$data->wechat_id;
      	}
      	if(!empty($data->fax_no))
      	{
        	$data1['fax_no']=$data->fax_no;
      	}
      	if(!empty($data->biography))
      	{
        	$data1['biography']=$data->biography;
      	}
      	if(!empty($data->position_title))
      	{
        	$data1['position_title']=$data->position_title;
      	}
      	if(!empty($data->name))
      	{
        	$data1['name']=$data->name;
      	}
      	if(!empty($data->location))
      	{
        	$data1['location']=$data->location;
      	}
      	if(!empty($data->company))
      	{
        	$data1['company']=$data->company;
      	}

        if(!empty($data->country))
        {
          $data1['country']=$data->country;
        }
        if(!empty($data->city))
        {
          $data1['city']=$data->city;
        }


      	$where['id']=$data->user_id;
      	$up=$this->usermodel->update_data("user",$where,$data1);
      	$response['status']=1;
        $response['message']="Edit Successful";
    }
    else
    {
        $response['status']=0;
        $response['message']="No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Send Contact Request //////////////////////////////////////

public function send_contact_request_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->user_id) && !empty($data->friend_id))
  	{
	    $where['user_id']=$data->user_id;
	    $where['friend_id']=$data->friend_id;
    	$contact_request = $this->eventmodel->getcontact(tablename("contact"), $where);
	    if(!empty($contact_request))
	    {
	    	if($contact_request[0]->user_id==$data->user_id)
	    	{
	    		$response['status']=3;
	        	$response['message']="Request Already Sent";
	    	}
	    	else
	    	{
	    		$data1['user_id']=$data->user_id;
		        $data1['friend_id']=$data->friend_id;
		        $data1['created_date']=date("Y-m-d H:i:s");
		        $contact_update = $this->eventmodel->update_data("contact",array("id"=>$contact_request[0]->id),$data1);
		       	if(!empty($contact_update))
			    {
			        $response['status']=1;
			        $response['message']="Request Sent";
			    }
			    else
			    {
			        $response['status']=2;
			        $response['message']="Request Not Sent";
			    } 
	    	}
	    }
	    else
	    {
	      $data1['user_id']=$data->user_id;
	      $data1['friend_id']=$data->friend_id;
	      $data1['created_date']=date("Y-m-d H:i:s");
	      $contact_insert = $this->eventmodel->insert_data("contact",$data1);
	      if(!empty($contact_insert))
	      {
	        $response['status']=1;
	        $response['message']="Request Sent";
	      }
	      else
	      {
	        $response['status']=2;
	        $response['message']="Request Not Sent";
	      }
	    } 
  	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Contact Request List //////////////////////////////////////

public function list_contact_request_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->user_id))
  	{
	    $where['friend_id']=$data->user_id;
	    $where['status']="pending";
    	$contact_list = $this->eventmodel->get_result_data("contact", $where);
	    if(!empty($contact_list))
	    {
        $contact_info=array();
        foreach($contact_list as $val)
        {
          if($data->user_id==$val->user_id)
          {
            $id=$val->friend_id;
          }
          else
          {
            $id=$val->user_id;
          }
          $user_info = $this->eventmodel->get_row_data("user", array("id"=>$id));
          if(!empty($user_info))
          {
            $final_data['name']=$user_info->name;
            $final_data['biography']=$user_info->biography;
            $final_data['position_title']=$user_info->position_title;
            $final_data['company']=$user_info->company;
            $final_data['location']=$user_info->location;
            if(!empty($user_info->profile_image))
            {
              $final_data['image']=base_url('assets/upload/appuser').'/'.$user_info->profile_image;
            }
            else
            {
              $final_data['image']="";
            }
            $val->friend_info=$final_data;
          }
          $contact_info[]=$val;
        }
        $contact_list=$contact_info;
	    	$response['status']=1;
  			$response['message']="Contact Request List";
  			$response['contact_request_info']=$contact_list;
		  }
	    else
	    {
	    	$response['status']=2;
	      $response['message']="No Contact Request";
	    } 
  	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Accept Contact Request //////////////////////////////////////

public function accept_contact_request_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->user_id) && !empty($data->friend_id))
  	{
  		$where['user_id']=$data->friend_id;
	    $where['friend_id']=$data->user_id;
	    $where['status']="pending";
    	$contact_request = $this->eventmodel->get_row_data("contact", $where);
	    if(!empty($contact_request))
	    {
	    	$data1['status']="accepted";
		    $contact_update = $this->eventmodel->update_data("contact",array("id"=>$contact_request->id),$data1);
	       	if(!empty($contact_update))
		    {
	        $response['status']=1;
	        $response['message']="Contact Request Accepted";
		    }
		    else
		    {
		        $response['status']=2;
		        $response['message']="Request Not Accepted";
		    } 
		}
	    else
	    {
	    	$response['status']=3;
	      	$response['message']="Invalid Contact Request";
	    } 
  	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Contact List //////////////////////////////////////

public function contact_list_post()
{
	$data=json_decode( file_get_contents('php://input') );
	if(!empty($data->user_id))
	{
    $contact_list=array();
		//$contact_list = $this->eventmodel->contact_list($data->user_id);
    $contact_list = $this->eventmodel->contact_list_order_by_name_1($data->user_id);
    if(!empty($contact_list))
    {
      $contact_info=array();
      foreach($contact_list as $val)
      {
        if($data->user_id==$val->user_id)
        {
          $id=$val->friend_id;
        }
        else
        {
          $id=$val->user_id;
        }
        $user_info = $this->eventmodel->get_row_data("user", array("id"=>$id));
        if(!empty($user_info))
        {
          $final_data['name']=$user_info->name;
          $final_data['biography']=$user_info->biography;
          $final_data['position_title']=$user_info->position_title;
          $final_data['company']=$user_info->company;
          $final_data['location']=$user_info->location;
          if(!empty($user_info->profile_image))
          {
            $final_data['image']=base_url('assets/upload/appuser').'/'.$user_info->profile_image;
          }
          else
          {
            $final_data['image']="";
          }
          $val->friend_info=$final_data;
        }
        $contact_info[]=$val;
      }
      $response['request_contact_list']=$contact_info;
    }
    else
    {
      $response['request_contact_list']=array();
    }

  //  $contact=$this->eventmodel->get_result_data("contact",array("user_id"=>$data->user_id,"contact_type!="=>'request'));
    $contact = $this->eventmodel->contact_list_order_by_name($data->user_id);
    if(!empty($contact))
    {
      $con=array();
      foreach($contact as $val)
      {
        if(!empty($val->contact_image))
        {
          $val->contact_image=base_url('assets/upload/contact').'/'.$val->contact_image;
        }
        $con[]=$val;
      }
      $contact=$con;
      $response['contact_list']=$contact;
    }
    else
    {
      $response['contact_list']=array();
    }
    $response['status']=1;
    $response['message']="Contact List";
  }
	else
	{
    $response['status']  = 0;
    $response['message'] = "No Data Found";
	}
	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Add Contact //////////////////////////////////////

public function add_contact_post()
{
 // echo "<pre>"; print_r($_POST); die;
  if(!empty($this->post('user_id')) && !empty($this->post('contact_type')))
  {
    $data['user_id']=$this->post('user_id');
    $data['contact_type']=$this->post('contact_type');
    $data['created_date']=date("Y-m-d H:i:s");
    if($this->post('contact_type')=='picture')
    {
      if (!empty($_FILES['contact_image']['name'])) 
      {
        $this->load->library('upload');
        $config['upload_path'] = './assets/upload/contact/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
        $config['file_name'] = time();

        $this->upload->initialize($config);
        if($_FILES['contact_image']['error']==0)
        {
          $profilepic=$_FILES['contact_image']['name'];
          $destination="./assets/upload/contact/".$profilepic;
          if (move_uploaded_file($_FILES['contact_image']['tmp_name'] ,$destination))
          {
            $data['contact_image']=$profilepic;
          }
        }
        $data['status']='accepted';
        $insert_info = $this->eventmodel->insert_data("contact",$data);
        if(!empty($insert_info))
        {
          $response['status']  = 1;
          $response['message'] = "Contact added successfully";
        }
        else
        {
          $response['status']  = 2;
          $response['message'] = "Contact not added";
        }
      } 
      else
      {
        $response['status']  = 0;
        $response['message'] = "No Data Found";
      } 
    }
    else
    {
      //echo "<pre>"; print_r($_FILES); die;
      // if(!empty($this->post('name')) || !empty($this->post('email')) || !empty($this->post('phone')) || !empty($this->post('title')) || !empty($this->post('position')) || !empty($this->post('company')) || !empty($this->post('location')) || !empty($this->post('skype_id')) || !empty($this->post('wechat_id')) || !empty($this->post('fax_no')))
      // {
        if(!empty($this->post('name')))
        {
          $data['name']=$this->post('name');
        }
        if(!empty($this->post('email')))
        {
          $data['email']=$this->post('email');
        }
        if(!empty($this->post('phone')))
        {
          $data['phone']=$this->post('phone');
        }
        if(!empty($this->post('title')))
        {
          $data['title']=$this->post('title');
        }
        if(!empty($this->post('position')))
        {
          $data['position']=$this->post('position');
        }
        if(!empty($this->post('company')))
        {
          $data['company']=$this->post('company');
        }
        if(!empty($this->post('location')))
        {
          $data['location']=$this->post('location');
        }
        if(!empty($this->post('skype_id')))
        {
          $data['skype_id']=$this->post('skype_id');
        }
        if(!empty($this->post('wechat_id')))
        {
          $data['wechat_id']=$this->post('wechat_id');
        }
        if(!empty($this->post('fax_no')))
        {
          $data['fax_no']=$this->post('fax_no');
        }
        if(!empty($this->post('city')))
        {
          $data['city']=$this->post('city');
        }
        if(!empty($this->post('country')))
        {
          $data['country']=$this->post('country');
        }
     
      if (!empty($_FILES['contact_image']['name'])) 
      {
        $this->load->library('upload');
        $config['upload_path'] = './assets/upload/contact/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
        $config['file_name'] = time();

        $this->upload->initialize($config);
        if($_FILES['contact_image']['error']==0)
        {
          $profilepic=$_FILES['contact_image']['name'];
          $destination="./assets/upload/contact/".$profilepic;
          if (move_uploaded_file($_FILES['contact_image']['tmp_name'] ,$destination))
          {
            $data['contact_image']=$profilepic;
          }
        }
        
      } 

      //echo "<pre>"; print_r($data); die;
      $data['status']='accepted';
        $insert_info = $this->eventmodel->insert_data("contact",$data);
        if(!empty($insert_info))
        {
          $response['status']  = 1;
          $response['message'] = "Contact added successfully";
        }
        else
        {
          $response['status']  = 2;
          $response['message'] = "Contact not added";
        }
      // }
      // else
      // {
      //   $response['status']  = 0;
      //   $response['message'] = "No Data Found";
      // }
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Edit Contact //////////////////////////////////////

public function edit_contact_post()
{
  if(!empty($this->post('contact_id')))
  {
    if (!empty($_FILES['contact_image']['name'])) 
    {
      $this->load->library('upload');
      $config['upload_path'] = './assets/upload/contact/';
      $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
      $config['file_name'] = time();

      $this->upload->initialize($config);
      if($_FILES['contact_image']['error']==0)
      {
        $profilepic=$_FILES['contact_image']['name'];
        $destination="./assets/upload/contact/".$profilepic;
        if (move_uploaded_file($_FILES['contact_image']['tmp_name'] ,$destination))
        {
          $data['contact_image']=$profilepic;
        }
      }
    }
    if(!empty($this->post('name')))
    {
      $data['name']=$this->post('name');
    }
    if(!empty($this->post('email')))
    {
      $data['email']=$this->post('email');
    }
    if(!empty($this->post('phone')))
    {
      $data['phone']=$this->post('phone');
    }
    if(!empty($this->post('title')))
    {
      $data['title']=$this->post('title');
    }
    if(!empty($this->post('position')))
    {
      $data['position']=$this->post('position');
    }
    if(!empty($this->post('company')))
    {
      $data['company']=$this->post('company');
    }
    if(!empty($this->post('location')))
    {
      $data['location']=$this->post('location');
    }
    if(!empty($this->post('skype_id')))
    {
      $data['skype_id']=$this->post('skype_id');
    }
    if(!empty($this->post('wechat_id')))
    {
      $data['wechat_id']=$this->post('wechat_id');
    }
    if(!empty($this->post('fax_no')))
    {
      $data['fax_no']=$this->post('fax_no');
    }

    if(!empty($this->post('city')))
        {
          $data['city']=$this->post('city');
        }
        if(!empty($this->post('country')))
        {
          $data['country']=$this->post('country');
        }
   // echo "<pre>"; print_r($data); die;
    $update_info = $this->eventmodel->update_data("contact",array("id"=>$this->post('contact_id')),$data);
    if(!empty($update_info))
    {
      $response['status']  = 1;
      $response['message'] = "Contact updated successfully";
    }
    else
    {
      $response['status']  = 2;
      $response['message'] = "Contact not updated";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Delete Contact //////////////////////////////////////

public function delete_contact_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->contact_id))
  {
    $del_info = $this->eventmodel->delete_data("contact",array("id"=>$data->contact_id));
    if(!empty($del_info))
    {
      $response['status']  = 1;
      $response['message'] = "Contact deleted successfully";
    }
    else
    {
      $response['status']  = 2;
      $response['message'] = "Contact not deleted";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Bookmark //////////////////////////////////////

public function bookmark_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->user_id) && !empty($data->friend_id))
  {
    $where['user_id']=$data->user_id;
    $where['friend_id']=$data->friend_id;
    $bookmark_search = $this->eventmodel->get_row_data("bookmark", $where);
    if(!empty($bookmark_search))
    {
      $bookmark_del=$this->eventmodel->delete_data("bookmark",$where);
      if(!empty($bookmark_del))
      {
        $response['status']=3;
        $response['message']="Bookmark Removed";
      }
      else
      {
        $response['status']=4;
        $response['message']="Bookmark Not Removed";
      }
    }
    else
    {
      $data1['user_id']=$data->user_id;
      $data1['friend_id']=$data->friend_id;
      $data1['created_date']=date("Y-m-d H:i:s");
      $bookmark_insert = $this->eventmodel->insert_data("bookmark",$data1);
      if(!empty($bookmark_insert))
      {
        $response['status']=1;
        $response['message']="Bookmarked";
      }
      else
      {
        $response['status']=2;
        $response['message']="Error in Bookmark Process";
      }
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Upload Profilepic //////////////////////////////////////

public function upload_profilepic_post()
{
	$user_id=$this->post('user_id');
    if(!empty($user_id))
    {
        if (!empty($_FILES['image']['name'])) 
        {
          $this->load->library('upload');
          $config['upload_path'] = './assets/upload/appuser/';
          $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
          $config['file_name'] = time();

          $this->upload->initialize($config);
          if($_FILES['image']['error']==0)
          {
            $profilepic=$_FILES['image']['name'];
            $destination="./assets/upload/appuser/".$profilepic;
            if (move_uploaded_file($_FILES['image']['tmp_name'] ,$destination))
            {
              $data1['profile_image']=$profilepic;
            }
          }
        } 
        else
       	{
          // if(!empty($id))
          // {
          //   $data1['image'] = $this->input->post('image');
          // }
          // else
          // {
          //   $data1['image'] = "";
          // }
       		$response['status']=0;
	        $response['message']="No Data Found";
	        $this->response($response);
        }
        $where['id']=$user_id;
        $det=$this->usermodel->update_data("user",$where,$data1);
        if(!empty($det))
        {
          $pic=$this->usermodel->appuser_get($where);
          if(!empty($pic->profile_image))
          {
            $image=base_url('assets/upload/appuser').'/'.$pic->profile_image;
          }
          else
          {
            $image="";
          }
          $response['status']=1;
          $response['message']="Picture Uploaded";
          $response['Image']=$image;
        }
        else
        {
            $response['status']=2;
            $response['message']="Picture Not Uploaded";
        }
    }
    else
    {
        $response['status']=0;
        $response['message']="No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Add Link //////////////////////////////////////

public function add_link_post()
{
  $data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->user_id) && !empty($data->url) && !empty($data->link_type))
  	{
	    $data1['user_id']=$data->user_id;
	    $data1['url']=$data->url;
	    $data1['link_type']=$data->link_type;
	    $data1['created_date']=date("Y-m-d H:i:s");
	    $link_insert = $this->eventmodel->insert_data("link",$data1);
	    if(!empty($link_insert))
	    {
	        $response['status']=1;
	        $response['message']="Link Added";
	    }
	    else
	    {
	        $response['status']=2;
	        $response['message']="Link Not Added";
	    }
	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}	
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Update Link //////////////////////////////////////

public function update_link_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->link_id) && !empty($data->url))
  	{
	    $data1['url']=$data->url;
	    $data1['created_date']=date("Y-m-d H:i:s");
	    $where['id']=$data->link_id;
	    $link_update = $this->eventmodel->update_data("link",$where,$data1);
	    if(!empty($link_update))
	    {
	        $response['status']=1;
	        $response['message']="Link Modified";
	    }
	    else
	    {
	        $response['status']=2;
	        $response['message']="Link Not Modified";
	    }
	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}	
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Remove Link //////////////////////////////////////

public function remove_link_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->link_id))
  	{
	    $where['id']=$data->link_id;
	    $link_del = $this->eventmodel->delete_data("link",$where);
	    if(!empty($link_del))
	    {
	        $response['status']=1;
	        $response['message']="Link Removed";
	    }
	    else
	    {
	        $response['status']=2;
	        $response['message']="Link Not Removed";
	    }
	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}	
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Add Education //////////////////////////////////////

public function add_education_post()
{
  $data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->user_id) && !empty($data->school))
  	{
	    $data1['user_id']=$data->user_id;
	    $data1['school']=$data->school;
	    if(!empty($data->degree))
	    {
	    	$data1['degree']=$data->degree;
	    }
	    if(!empty($data->major))
	    {
	    	$data1['major']=$data->major;
	    }
	    if(!empty($data->start_date))
	    {
	    	$data1['start_date']=$data->start_date;
	    }
	    if(!empty($data->end_date))
	    {
	    	$data1['end_date']=$data->end_date;
	    }
	    $data1['created_date']=date("Y-m-d H:i:s");
	    $education_insert = $this->eventmodel->insert_data("education",$data1);
	    if(!empty($education_insert))
	    {
	        $response['status']=1;
	        $response['message']="Education Added";
	    }
	    else
	    {
	        $response['status']=2;
	        $response['message']="Education Not Added";
	    }
	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}	
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Update Education //////////////////////////////////////

public function update_education_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->education_id) && !empty($data->school))
  	{
	    $where['id']=$data->education_id;
	    $data1['school']=$data->school;
	    if(!empty($data->degree))
	    {
	    	$data1['degree']=$data->degree;
	    }
	    if(!empty($data->major))
	    {
	    	$data1['major']=$data->major;
	    }
	    if(!empty($data->start_date))
	    {
	    	$data1['start_date']=$data->start_date;
	    }
	    if(!empty($data->end_date))
	    {
	    	$data1['end_date']=$data->end_date;
	    }
	    $data1['created_date']=date("Y-m-d H:i:s");
	    $education_update = $this->eventmodel->update_data("education",$where,$data1);
	    if(!empty($education_update))
	    {
	        $response['status']=1;
	        $response['message']="Education Modified";
	    }
	    else
	    {
	        $response['status']=2;
	        $response['message']="Education Not Modified";
	    }
	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}	
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Remove Education //////////////////////////////////////

public function remove_education_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->education_id))
  	{
	    $where['id']=$data->education_id;
	    $education_del = $this->eventmodel->delete_data("education",$where);
	    if(!empty($education_del))
	    {
	        $response['status']=1;
	        $response['message']="Education Removed";
	    }
	    else
	    {
	        $response['status']=2;
	        $response['message']="Education Not Removed";
	    }
	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}	
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Add Company //////////////////////////////////////

public function add_company_post()
{
  $data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->user_id) && !empty($data->company))
  	{
	    $data1['user_id']=$data->user_id;
	    $data1['company']=$data->company;
	    if(!empty($data->position))
	    {
	    	$data1['position']=$data->position;
	    }
	    if(!empty($data->current_job))
	    {
	    	$data1['current_job']=$data->current_job;
	    }
	    if(!empty($data->start_date))
	    {
	    	$data1['start_date']=$data->start_date;
	    }
	    if(!empty($data->end_date))
	    {
	    	$data1['end_date']=$data->end_date;
	    }
	    $data1['created_date']=date("Y-m-d H:i:s");
	    $company_insert = $this->eventmodel->insert_data("company",$data1);
	    if(!empty($company_insert))
	    {
	        $response['status']=1;
	        $response['message']="Company Added";
	        
	        $response['data']['user_id']=$data->user_id;
	    $response['data']['company']=$data->company;
	    if(!empty($data->position))
	    {
	    	$response['data']['position']=$data->position;
	    }
	    if(!empty($data->current_job))
	    {
	    	$response['data']['current_job']=$data->current_job;
	    }
	    if(!empty($data->start_date))
	    {
	    	$response['data']['start_date']=$data->start_date;
	    }
	    if(!empty($data->end_date))
	    {
	    	$response['data']['end_date']=$data->end_date;
	    }
	    }
	    else
	    {
	        $response['status']=2;
	        $response['message']="Company Not Added";
	    }
	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}	
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Update Company //////////////////////////////////////

public function update_company_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->company_id) && !empty($data->company))
  	{
	    $where['id']=$data->company_id;
	    $data1['company']=$data->company;
	    if(!empty($data->position))
	    {
	    	$data1['position']=$data->position;
	    }
	    if(!empty($data->current_job))
	    {
	    	$data1['current_job']=$data->current_job;
	    }
	    if(!empty($data->start_date))
	    {
	    	$data1['start_date']=$data->start_date;
	    }
	    if(!empty($data->end_date))
	    {
	    	$data1['end_date']=$data->end_date;
	    }
	    $data1['created_date']=date("Y-m-d H:i:s");
	    $company_update = $this->eventmodel->update_data("company",$where,$data1);
	    if(!empty($company_update))
	    {
	        $response['status']=1;
	        $response['message']="Company Modified";
	    }
	    else
	    {
	        $response['status']=2;
	        $response['message']="Company Not Modified";
	    }
	  }
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}	
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Remove Company //////////////////////////////////////

public function remove_company_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->company_id))
  	{
	    $where['id']=$data->company_id;
	    $company_del = $this->eventmodel->delete_data("company",$where);
	    if(!empty($company_del))
	    {
	        $response['status']=1;
	        $response['message']="Company Removed";
	    }
	    else
	    {
	        $response['status']=2;
	        $response['message']="Company Not Removed";
	    }
	  }
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}	
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Add Note //////////////////////////////////////

public function add_note_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->attendee_id) && !empty($data->user_id) && !empty($data->note))
  {

    $where                = array();
    $where['event_id']    = $data->event_id;
    $where['attendee_id'] = $data->attendee_id;
    $where['user_id']     = $data->user_id;
    $note_exist           = $this->eventmodel->get_row_data("personal_note", $where);

    if(!empty($note_exist))
    {

      $data1                = array();
      $data1['note']        = $data->note;


      $where                = array();
      $where['id']          = $note_exist->id;
      $insert_note          = $this->eventmodel->update_data("personal_note",$where,$data1);


      if($insert_note)
      {
        $response['status']=3;
        $response['message']="Note Updated Successfully";
      }
      else
      {
        $response['status']=4;
        $response['message']="Note Failed To Update";
      }
    }
    else
    {
      $data1                = array();
      $data1['event_id']    = $data->event_id;
      $data1['attendee_id'] = $data->attendee_id;
      $data1['user_id']     = $data->user_id;
      $data1['note']        = $data->note;
      $data1['created_date']= date("Y-m-d H:i:s");

      $insert_note          = $this->eventmodel->insert_data("personal_note",$data1);
      if(!empty($insert_note))
      {
        $response['status'] = 1;
        $response['message']= "Note Added";
      }
      else
      {
        $response['status']=2;
        $response['message']="Error in Adding Note";
      }
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Profile Details //////////////////////////////////////

public function profile_details_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->attendee_id) && !empty($data->user_id))
  {
    $final_data=array();
    $where['id']=$data->attendee_id;
    $user_info = $this->eventmodel->get_row_data("user", $where);
    if(!empty($user_info))
    {
      $final_data['name']=$user_info->name;
      $final_data['biography']=$user_info->biography;
      $final_data['position_title']=$user_info->position_title;
      $final_data['company']=$user_info->company;
      $final_data['location']=$user_info->location;
      if(!empty($user_info->profile_image))
      {
        $final_data['image']=base_url('assets/upload/appuser').'/'.$user_info->profile_image;
      }
      else
      {
        $final_data['image']="";
      }
      $final_data['country']=$user_info->country;
      $final_data['city']=$user_info->city;

       if(!empty($user_info->country))
          {
            $country = $this->db->where('id',$user_info->country)->get('countries')->row();
            $final_data['country_name']=$country->name;
          }
       if(!empty($user_info->city))
          {
            $city = $this->db->where('id',$user_info->city)->get('cities')->row();
            $final_data['city_name']=$city->name;
          }

      $where1['user_id']=$data->attendee_id;
      $education_info = $this->eventmodel->get_result_data("education", $where1);
      $final_data['education_info']=$education_info;
      $company_info = $this->eventmodel->get_result_data("company", $where1);
      $final_data['company_info']=$company_info;
      $link_info = $this->eventmodel->get_result_data("link", $where1);
      $final_data['link_info']=$link_info;
      $where2['user_id']=$data->user_id;
      $where2['friend_id']=$data->attendee_id;
      if(!empty($this->eventmodel->get_row_data("bookmark", $where2)))
      {
        $final_data['bookmark_info']=1;
      }
      else
      {
        $final_data['bookmark_info']=0;
      }


      $where3['user_id'] = $data->user_id;
      $where3['attendee_id'] = $data->attendee_id;

      $personal_note = $this->eventmodel->get_row_data("personal_note", $where3);

      if($personal_note)
      {
        $final_data['personal_note']=$personal_note->note;
        $final_data['personal_note_status']='1';
      }
      else
      {
        $final_data['personal_note']='';
        $final_data['personal_note_status']='0';
      }

      $where4['receiver_id'] = $data->user_id;
      $where4['sender_id'] = $data->attendee_id;
      $where4['say_hi'] = '1';

      $say_hi = $this->eventmodel->get_row_data("attendees_chat", $where4);

      if($say_hi)
      {
        $final_data['say_hi']=1;
      }
      else
      {
        $final_data['say_hi']=0;
      }



      $contact_info=$this->eventmodel->getcontact(tablename("contact"),$where2); 
      if(!empty($contact_info))
      {
        if($contact_info[0]->status=="accepted")
        {
          $final_data['contact_info']=1;
        }
        else
        {
          if($contact_info[0]->user_id==$data->user_id)
          {
            $final_data['contact_info']=2;
          }
          else
          {
            $final_data['contact_info']=0;
          }
        }
      }
      else
      {
        $final_data['contact_info']=0;
      }

      if($final_data['contact_info']==1 && $user_info->is_visible=='1')
      {
        $final_data['contact_info_details']['phone_no']=$user_info->phoneno;
        $final_data['contact_info_details']['mobile_no']=$user_info->mobile_no;
        $final_data['contact_info_details']['contact_email']=$user_info->contact_email;
        $final_data['contact_info_details']['fax_no']=$user_info->fax_no;
        $final_data['contact_info_details']['address']=$user_info->address;
        $final_data['contact_info_details']['skype_id']=$user_info->skype_id;
        $final_data['contact_info_details']['wechat_id']=$user_info->wechat_id;
      }
      $response['status']=1;
      $response['message']="Profile Details";
      $response['profile_info']=$final_data;
    }
    else
    {
      $response['status']=2;
      $response['message']="Invalid user";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Add Own Agenda/Activity //////////////////////////////////////

public function add_activity_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->user_id) && !empty($data->title) && !empty($data->event_id))
  {
    $data1['user_id']=$data->user_id;
    $data1['event_id']=$data->event_id;
    $data1['title']=$data->title;
    if(!empty($data->details))
    {
      $data1['details']=$data->details;
    }
    if(!empty($data->location))
    {
      $data1['location']=$data->location;
    }
    if(!empty($data->start_date_time))
    {
      $data1['start_date_time']=$data->start_date_time;
    }
    if(!empty($data->end_date_time))
    {
      $data1['end_date_time']=$data->end_date_time;
    }
    $data1['agenda_owner_type']="user";
    $data1['created_date']=date("Y-m-d H:i:s");
    $activity_insert = $this->eventmodel->insert_data("agenda",$data1);
    if(!empty($activity_insert))
    {
      $response['status']=1;
      $response['message']="Activity Added";
    }
    else
    {
      $response['status']=2;
      $response['message']="Activity Not Added";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Update Own Agenda/Activity //////////////////////////////////////

public function update_activity_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->activity_id) && !empty($data->title))
  {
    $where['id']=$data->activity_id;
    $data1['title']=$data->title;
    if(!empty($data->details))
    {
      $data1['details']=$data->details;
    }
    if(!empty($data->location))
    {
      $data1['location']=$data->location;
    }
    if(!empty($data->start_date_time))
    {
      $data1['start_date_time']=$data->start_date_time;
    }
    if(!empty($data->end_date_time))
    {
      $data1['end_date_time']=$data->end_date_time;
    }
    $data1['created_date']=date("Y-m-d H:i:s");
    $activity_update = $this->eventmodel->update_data("agenda",$where,$data1);
    if(!empty($activity_update))
    {
        $response['status']=1;
        $response['message']="Activity Modified";
    }
    else
    {
        $response['status']=2;
        $response['message']="Activity Not Modified";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Remove Own Agenda/Activity //////////////////////////////////////

public function remove_activity_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->activity_id))
    {
      $where['id']=$data->activity_id;
      $activity_del = $this->eventmodel->delete_data("agenda",$where);
      if(!empty($activity_del))
      {
          $response['status']=1;
          $response['message']="Activity Removed";
      }
      else
      {
          $response['status']=2;
          $response['message']="Activity Not Removed";
      }
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    } 
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Add to my agenda //////////////////////////////////////

public function add_agenda_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->user_id) && !empty($data->agenda_id))
  {
    $where['user_id']=$data->user_id;
    $where['agenda_id']=$data->agenda_id;
    $agenda_info=$this->eventmodel->get_row_data("my_agenda",$where);
    if(empty($agenda_info))
    {
      $data1['user_id']=$data->user_id;
      $data1['agenda_id']=$data->agenda_id;
      $data1['created_date']=date("Y-m-d H:i:s");
      $agenda_insert = $this->eventmodel->insert_data("my_agenda",$data1);
      if(!empty($agenda_insert))
      {
        $response['status']=1;
        $response['message']="Agenda Added";
      }
      else
      {
        $response['status']=2;
        $response['message']="Agenda Not Added";
      }
    }
    else
    {
      $response['status']=3;
      $response['message']="Agenda Already Added";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Remove from my agenda //////////////////////////////////////

public function remove_agenda_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->user_id) && !empty($data->agenda_id))
  { 
    $where['user_id']=$data->user_id;
    $where['agenda_id']=$data->agenda_id;
    $agenda_del = $this->eventmodel->delete_data("my_agenda",$where);
    if(!empty($agenda_del))
    {
        $response['status']=1;
        $response['message']="Agenda Removed";
    }
    else
    {
        $response['status']=2;
        $response['message']="Agenda Not Removed";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// List Agenda //////////////////////////////////////

public function list_agenda_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->user_id))
  {
    $final_data=array();
    $where['user_id']=$data->user_id;
    $where['event_id']=$data->event_id;
    $where['is_active']='Y';
    $where['delete_flag']='N';
    if(!empty($data->date))
    {
      $where['date_format(start_date_time,"%Y-%m-%d")']=$data->date;
    }
    $personal_agenda = $this->eventmodel->get_result_data("agenda", $where);
    $final_data['personal_agenda']=$personal_agenda;
    $where1['my_agenda.user_id']=$data->user_id;
    if(!empty($data->date))
    {
      $where1['date_format(start_date_time,"%Y-%m-%d")']=$data->date;
    }
    $added_agenda = $this->eventmodel->my_agenda($where1);
    if(!empty($added_agenda))
    {
      $final_agenda=array();
      foreach($added_agenda as $val)
      {
        $whr['agenda_id']=$val->id;
        $val->like_count=count($this->eventmodel->get_result_data("agenda_like", $whr));
        $val->comment_count=count($this->eventmodel->get_result_data("agenda_comment", $whr));
        $final_agenda[]=$val;
      }
      $added_agenda=$final_agenda;
    }
    else
    {
      $added_agenda=array();
    }
    $final_data['added_agenda']=$added_agenda;
    $response['status']=1;
    $response['message']="Agenda List";
    $response['agenda_list']=$final_data;
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Agenda Available Dates //////////////////////////////////////

public function agenda_available_dates_post()
{ 
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->user_id) && !empty($data->agenda_type))
  {
    $final_data=array();
    $where['event_id']=$data->event_id;
    $where['user_id']=$data->user_id;
    if($data->agenda_type==1){
      $available_dates = $this->eventmodel->agenda_available_dates_admin($where);
    }else{
      $available_dates = $this->eventmodel->agenda_available_dates_user($where);
    }

    $where1['id']=$data->event_id;
    $event_info = $this->eventmodel->get_row_data('event',$where1);

    $response['status']=1;
    $response['message']="Agenda Available Dates";
    $response['available_dates']=$available_dates;
    $response['agenda_start_date']=@$available_dates[0]->agenda_date;
    $response['agenda_end_date']=@$available_dates[count($available_dates)-1]->agenda_date;

    $response['event_start_date']=@$event_info->event_start_date;
    $response['event_end_date']=@$event_info->event_end_date;

  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Full Agenda List //////////////////////////////////////

public function full_agenda_list_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->date))
  {
    $final_data=array();
    $where['event_id']=$data->event_id;
    $where['user_id']=0;
    $where['is_active']='Y';
    $where['delete_flag']='N';
    $where['date_format(start_date_time,"%Y-%m-%d")']=$data->date;
    
    $agenda = $this->eventmodel->get_result_data("agenda", $where);
    $response['status']=1;
    $response['message']="Agenda List";
    if(!empty($agenda))
    {
      $final_agenda=array();
      foreach($agenda as $val)
      {
        $whr['agenda_id']=$val->id;
        $val->like_count=count($this->eventmodel->get_result_data("agenda_like", $whr));
        $val->comment_count=count($this->eventmodel->get_result_data("agenda_comment", $whr));
        $final_agenda[]=$val;
      }
      $agenda=$final_agenda;
    }
    $response['agenda_list']=$agenda;
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Like Agenda //////////////////////////////////////

public function like_agenda_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->agenda_id) && !empty($data->user_id))
  {
    $where['agenda_id']=$data->agenda_id;
    $where['user_id']=$data->user_id;
    $like_search = $this->eventmodel->get_row_data("agenda_like", $where);
    if(!empty($like_search))
    {
      $like_del=$this->eventmodel->delete_data("agenda_like",$where);
      if(!empty($like_del))
      {
        $response['status']=3;
        $response['message']="Like Removed";
      }
      else
      {
        $response['status']=4;
        $response['message']="Like Not Removed";
      }
    }
    else
    {
      $data1['agenda_id']=$data->agenda_id;
      $data1['user_id']=$data->user_id;
      $like_insert = $this->eventmodel->insert_data("agenda_like",$data1);
      if(!empty($like_insert))
      {
        $response['status']=1;
        $response['message']="Liked";
      }
      else
      {
        $response['status']=2;
        $response['message']="Error in Like Process";
      }
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Comment Agenda //////////////////////////////////////

public function comment_agenda_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->agenda_id) && !empty($data->user_id) && !empty($data->comment))
  {
    $data1['agenda_id']=$data->agenda_id;
    $data1['user_id']=$data->user_id;
    $data1['comment']=$data->comment;
    $data1['date']=date("Y-m-d H:i:s");
    $comment_insert = $this->eventmodel->insert_data("agenda_comment",$data1);
    if(!empty($comment_insert))
    {
      $response['status']=1;
      $response['message']="Comment Posted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Comment Not Posted";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Rate Agenda //////////////////////////////////////

public function rate_agenda_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->agenda_id) && !empty($data->user_id) && !empty($data->rate) && !empty($data->is_recommendable) &&!empty($data->comment))
  {
    $where['agenda_id']=$data->agenda_id;
    $where['user_id']=$data->user_id;
    $rate_search = $this->eventmodel->get_row_data("agenda_rate", $where);
    if(empty($rate_search))
    {
      $data1['agenda_id']=$data->agenda_id;
      $data1['user_id']=$data->user_id;
      $data1['rate']=$data->rate;
      $data1['is_recommendable']=$data->is_recommendable;
      $data1['comment']=$data->comment;
      $rate_insert = $this->eventmodel->insert_data("agenda_rate",$data1);
      if(!empty($rate_insert))
      {
        $response['status']=1;
        $response['message']="Rating Done";
      }
      else
      {
        $response['status']=2;
        $response['message']="Rate Not Posted";
      }
    }
    else
    {
      $response['status']=3;
      $response['message']="Rating Already Done";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Agenda Add Edit Notes //////////////////////////////////////

public function agenda_add_edit_notes_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->agenda_id) && !empty($data->user_id) && !empty($data->note))
    {
      $where['agenda_id']=$data->agenda_id;
      $where['user_id']=$data->user_id;
      $data1['note']=$data->note;
      $note_details = $this->eventmodel->get_row_data("agenda_note", $where);
      if(!empty($note_details))
      {
        $this->eventmodel->update_data("agenda_note",$where,$data1);
        $response['status']=3;
        $response['message']="Note Updated";
      }
      else
      {
        $data1['agenda_id']=$data->agenda_id;
        $data1['user_id']=$data->user_id;
        $note_insert = $this->eventmodel->insert_data("agenda_note",$data1);
        if(!empty($note_insert))
        {
          $response['status']=1;
          $response['message']="Note Added";
        }
        else
        {
          $response['status']=2;
          $response['message']="Note Not Posted";
        }
      }
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    } 
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Agenda Details //////////////////////////////////////

public function agenda_details_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->agenda_id) && !empty($data->user_id))
  {
    $final_data=array();
    $where['id']=$data->agenda_id;
    $agenda_info = $this->eventmodel->get_row_data("agenda", $where);
    //echo "<pre>"; print_r($agenda_info); die;
    if(!empty($agenda_info))
    {
      $final_data['agenda_info']=$agenda_info;
      $where1['agenda_id']=$data->agenda_id;
      $join_condition="user.id=user_id";


      $final_data['comment_info'] = $this->eventmodel->get_detailed_result_data("agenda_comment","user",$join_condition, $where1);

      if(!empty($final_data['comment_info'])){
        $final_array1=array();
        foreach($final_data['comment_info'] as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->profile_image=base_url('assets/upload/appuser')."/".$val->profile_image;
          }
          else
          {
            $val->profile_image="";
          }

          $final_array1[]=$val;
        }
        $final_data['comment_info']=$final_array1;
      }

      $like_info = $this->eventmodel->get_detailed_result_data("agenda_like","user",$join_condition, $where1);
      $final_data['like_info']=$like_info;

      if(!empty($final_data['like_info']))
      {
        $final_array=array();
        foreach($final_data['like_info'] as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->profile_image=base_url('assets/upload/appuser')."/".$val->profile_image;
          }
          else
          {
            $val->profile_image="";
          }
        }
          
      }



      $speakers = explode(',', $agenda_info->speakers);
      $final_data['speaker_info'] = $this->eventmodel->get_result_data('user','',$speakers);
      if(!empty($final_data['speaker_info']))
      {
        $final_array=array();
        foreach($final_data['speaker_info'] as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->image=base_url('assets/upload/appuser')."/".$val->profile_image;
          }
          else
          {
            $val->image="";
          }
          // check user and speaker already saied hi or not
          $where_chat = array();
          $where_chat['event_id']=$agenda_info->event_id;
          $where_chat['sender_id']=$data->user_id;
          $where_chat['receiver_id']=$val->id;
          $where_chat['say_hi']='1';
          $hi_exist = $this->eventmodel->get_row_data("attendees_chat", $where_chat);
          if($hi_exist)
          {
             $val->say_hi = 1;
          }else{
            $val->say_hi = 0;
          }        
          // end check user and speaker already saied hi or not



          $final_array[]=$val;
        }
        $final_data['speaker_info']=$final_array;
      }
      $where1['user_id']=$data->user_id;
      $note=$this->eventmodel->get_row_data("agenda_note", $where1);
      if(!empty($note))
      {
        $final_data['note']=$note;
      }
      else
      {
        $final_data['note']="";
      }
      if(!empty($this->eventmodel->get_row_data("my_agenda", $where1)))
      {
        $final_data['is_added']=1;
      }
      else
      {
        $final_data['is_added']=0;
      }
      $response['status']=1;
      $response['message']="Agenda Details";
      $response['agenda_info']=$final_data;
    }
    else
    {
      $response['status']=2;
      $response['message']="Invalid agenda";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Poll Question List //////////////////////////////////////

public function poll_question_list_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->event_id))
  	{
	    $final_data=array();
	    $where['event_id']=$data->event_id;
	    $poll_info = $this->eventmodel->get_result_data('poll_questions',$where);
	    if(!empty($poll_info))
	    {
	    	$open_poll=array();
	    	$closed_poll=array();
	      	foreach($poll_info as $val)
	        {
	        	$where1['poll_question_id']=$val->id;
	          	$val->poll_option_info = $this->eventmodel->get_result_data('poll_answers',$where1);
	          	// if(($val->poll_end_date < date("Y-m-d H:i:s")) || $val->poll_status==0) 
	          	// {
	          	// 	$closed_poll[]=$val;
	          	// }
	          	// else
	          	// {
	          	// 	$open_poll[]=$val;
	          	// }              


              // check user polled or not
              $val->user_polled = 0;
              if(!empty($data->user_id))
              {
                $where23['attendee_id']=$data->user_id;
                $where23['event_id']=$data->event_id;
                $poll_result = $this->eventmodel->get_result_data('poll_result',$where23);
                if(!empty($poll_result)){
                  $val->user_polled = 1;
                }
              }
              
              $open_poll[]=$val;
	        }
	        $response['status']=1;
		    $response['message']="Poll List";
		    $response['open_poll']=$open_poll;
		    $response['closed_poll']=$closed_poll;




    




	    }
	    else
	    {
	      $response['status']=2;
	      $response['message']="Poll not available";
	    } 
  	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Post Poll //////////////////////////////////////

public function post_poll_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->user_id) && !empty($data->poll_question_id) && !empty($data->poll_answer_id))
  	{
  		$where['id']=$data->poll_question_id;
  		$poll_ques_info = $this->eventmodel->get_row_data("poll_questions", $where);
	    $data1['event_id']=$poll_ques_info->event_id;
	    $data1['attendee_id']=$data->user_id;
	    $data1['poll_question_id']=$data->poll_question_id;
	    $data1['poll_answer_id']=$data->poll_answer_id;
	    $poll_info = $this->eventmodel->insert_data("poll_result",$data1);
	    if(!empty($poll_info))
	    {
	    	$data2['total_attempt']=$poll_ques_info->total_attempt + 1;
	    	$this->eventmodel->update_data("poll_questions",$where,$data2);
	    	$where1['id']=$data->poll_answer_id;
  			$poll_ans_info = $this->eventmodel->get_row_data("poll_answers", $where1);
  			$data3['total_select']=$poll_ans_info->total_select + 1;
	    	$this->eventmodel->update_data("poll_answers",$where1,$data3);
	    	$response['status']=1;
		    $response['message']="Poll Done";
		}
	    else
	    {
	      $response['status']=2;
	      $response['message']="Poll Not Done";
	    } 
  	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Poll Result //////////////////////////////////////

public function poll_result_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->poll_question_id))
  	{
  		$where['id']=$data->poll_question_id;
  		$poll_ques_info = $this->eventmodel->get_row_data("poll_questions", $where);
  		if(!empty($poll_ques_info))
	    {
	    	$final_array=array();
	    	$final_array['Question']=$poll_ques_info;
	    	$where1['poll_question_id']=$data->poll_question_id;
  			$poll_ans_info = $this->eventmodel->get_result_data("poll_answers", $where1);
  			if(!empty($poll_ans_info))
  			{
  				$option_array=array();
	  			foreach($poll_ans_info as $val)
		        {
              if(!empty($poll_ques_info->total_attempt))
              {
		        	 $val->vote = round((($val->total_select / $poll_ques_info->total_attempt)*100),2)."%";
              }
              else
              {
                $val->vote ="0%";
              }
		          $option_array[]=$val;
		        }
		        $final_array['Answer']=$option_array;
		    }
	    	$response['status']=1;
		    $response['message']="Poll Result";
		    $response['poll_result']=$final_array;
		}
	    else
	    {
	      $response['status']=2;
	      $response['message']="Invalid Poll Question";
	    } 
  	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Survey Type //////////////////////////////////////

public function survey_type_post()
{
  $where['status']='1';
	$survey_type = $this->eventmodel->get_result_data("survey_category", $where);
	if(!empty($survey_type))
  {
  	$response['status']=1;
    $response['message']="Survey Type";
    $response['survey_type']=$survey_type;
	}
  else
  {
    $response['status']=2;
    $response['message']="No Survey Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Survey Question List //////////////////////////////////////

public function survey_question_list_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->event_id) && !empty($data->survey_type_id))
  	{
	    $final_data=array();
	    $where['event_id']=$data->event_id;
	    $where['category_id']=$data->survey_type_id;
	    $survey_info = $this->eventmodel->get_result_data('survey_questions',$where);
	    if(!empty($survey_info))
	    {
	    	foreach($survey_info as $val)
	        {
            if($val->is_comment==1){
              $val->is_comment = true;
            }else{
              $val->is_comment = false;
            }


            if($val->isRating=="true"){
              $val->isRating = true;
            }else{
              $val->isRating = false;
            }


	        	$where1['survey_question_id']=$val->id;
	          	$val->survey_option_info = $this->eventmodel->get_result_data('survey_answers',$where1);
	          	$final_data[]=$val;
	        }
	        $response['status']=1;
		    $response['message']="Survey List";
		    $response['survey_list']=$final_data;
		}
	    else
	    {
	      $response['status']=2;
	      $response['message']="Survey not available";
	    } 
  	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Post Survey //////////////////////////////////////

public function post_survey_post()
{
	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data))
  	{
  		foreach($data as $val)
  		{
	  		$data1['event_id']=$val->event_id;
		    $data1['user_id']=$val->user_id;
		    $data1['survey_question_id']=$val->survey_question_id;
		    $data1['survey_answer_id']=$val->survey_answer_id;
		    $data1['survey_comment']=$val->survey_comment;
        $data1['survey_rating']=$val->survey_rating;//added on 06.08.18
		    $data1['created_date']=date("Y-m-d H:i:s");
		    $survey = $this->eventmodel->insert_data("survey_result",$data1);
		}
	    if(!empty($survey))
	    {
	    	$response['status']=1;
		    $response['message']="Survey Done";
		}
	    else
	    {
	      $response['status']=2;
	      $response['message']="Survey Not Done";
	    } 
  	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Post Image //////////////////////////////////////

public function event_post_image_post()
{
	$user_id=$this->post('user_id');
    if(!empty($this->post('event_id')) && !empty($this->post('user_id')))
    {
    	$data1['event_id']=$this->post('event_id');
    	$data1['uploaded_by_id']=$this->post('user_id');
    	$data1['uploaded_by']=$this->usermodel->appuser_get(array("id"=>$this->post('user_id')))->name;
    	$data1['entry_date']=date("Y-m-d H:i:s");
      $data1['tag']= $this->post('tag');
        if (!empty($_FILES['image']['name'])) 
        {
          $this->load->library('upload');
          $config['upload_path'] = './assets/upload/media_files/';
          $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
          $config['file_name'] = time();

          $this->upload->initialize($config);
          if($_FILES['image']['error']==0)
          {
            $profilepic=$_FILES['image']['name'];
            $destination="./assets/upload/media_files/".$profilepic;
            if (move_uploaded_file($_FILES['image']['tmp_name'] ,$destination))
            {
              $data1['file_name']=$profilepic;
            }
          }
        } 
        else
       	{
          	$response['status']=0;
	        $response['message']="No Data Found";
	        $this->response($response);
        }
        $ins=$this->eventmodel->insert_data("media_files",$data1);
        if(!empty($ins))
        {
          $response['status']=1;
          $response['message']="After admin approval image will diaplay";
        }
        else
        {
            $response['status']=2;
            $response['message']="Image Not Posted";
        }
    }
    else
    {
        $response['status']=0;
        $response['message']="No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Image List //////////////////////////////////////

public function event_user_image_list_post()
{
	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->event_id) && !empty($data->user_id))
  	{
		$media = $this->eventmodel->get_result_data('media_files',array("uploaded_by!="=>'admin',"delete_flag"=>'N',"event_id"=>$data->event_id,"approval_status"=>1,"type"=>'P'));
		if(!empty($media))
        {
            $final_media=array();
            foreach($media as $val)
            {
                $val->file_name=base_url('assets/upload/media_files').'/'.$val->file_name;
                $val->like_count=count($this->eventmodel->get_result_data('media_like',array("media_id"=>$val->id)));
                $val->comment_count=count($this->eventmodel->get_result_data('media_comment',array("media_id"=>$val->id)));
                // this image like by login user
                 $val->like_user=count($this->eventmodel->get_row_data('media_like',array("media_id"=>$val->id,"user_id"=>$data->user_id)));
                 // end this image like by login user

                  // get user image
                 $user_image=$this->db->query('select profile_image from ets_user where id = "'.$data->user_id.'"')->row();
                 //echo "<pre>"; print_r($user_image); die;
                 if($user_image->profile_image!=''){
                  $val->user_image = base_url('assets/upload/appuser').'/'.$user_image->profile_image;
                 }else{
                  $val->user_image ='';
                 }
                 // end get user image

                $final_media[]=$val;
            }
            $media=$final_media;
            $response['image_list'] = $media;
	        $response['status'] = 1;
	        $response['message'] = "Image List";
        }
        else 
	    {
	        $response['status'] = 2;
	        $response['message'] = "No Image Found";
	    }
    }
    else 
    {
        $response['status'] = 0;
        $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Post Video //////////////////////////////////////

public function event_post_video_post()
{
  $user_id=$this->post('user_id');
  if(!empty($this->post('event_id')) && !empty($this->post('user_id')))
  {
    $data1['event_id']=$this->post('event_id');
    $data1['uploaded_by_id']=$this->post('user_id');
    $data1['uploaded_by']=$this->usermodel->appuser_get(array("id"=>$this->post('user_id')))->name;
    $data1['type']='V';
    $data1['entry_date']=date("Y-m-d H:i:s");
    if (!empty($_FILES['video']['name'])) 
    {
      $this->load->library('upload');
      $config['upload_path'] = './assets/upload/media_files/';
      $config['allowed_types'] = 'mp4';
      $config['file_name'] = time();

      $this->upload->initialize($config);
      if($_FILES['video']['error']==0)
      {
        $profilepic=$_FILES['video']['name'];
        $destination="./assets/upload/media_files/".$profilepic;
        if (move_uploaded_file($_FILES['video']['tmp_name'] ,$destination))
        {
          $data1['file_name']=$profilepic;
        }
      }
    } 
    else
    {
        $response['status']=0;
      $response['message']="No Data Found";
      $this->response($response);
    }
    $ins=$this->eventmodel->insert_data("media_files",$data1);
    if(!empty($ins))
    {
      $response['status']=1;
      $response['message']="Video Posted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Video Not Posted";
    }
  }
  else
  {
      $response['status']=0;
      $response['message']="No Data Found";
  }
  $this->response($response);
}


public function upload_video_post()
{
  $user_id=$this->post('user_id');
  if(!empty($this->post('event_id')) && !empty($this->post('user_id')))
  {
    $data1['event_id']=$this->post('event_id');
    $data1['uploaded_by_id']=$this->post('user_id');
    $data1['uploaded_by']=$this->usermodel->appuser_get(array("id"=>$this->post('user_id')))->name;
    $data1['type']='V';
    $data1['entry_date']=date("Y-m-d H:i:s");
    if (!empty($_FILES['video']['name'])) 
    {
      $this->load->library('upload');
      $config['upload_path'] = './assets/upload/media_files/';
      $config['allowed_types'] = 'mp4';
      $config['file_name'] = time();

      $this->upload->initialize($config);
      if($_FILES['video']['error']==0)
      {
        $profilepic=$_FILES['video']['name'];
        $destination="./assets/upload/media_files/".$profilepic;
        if (move_uploaded_file($_FILES['video']['tmp_name'] ,$destination))
        {
          $data1['file_name']=$profilepic;
        }
      }
    } 
    else
    {
        $response['status']=0;
      $response['message']="No Data Found";
      $this->response($response);
    }
    $ins=$this->eventmodel->insert_data("media_files",$data1);
    if(!empty($ins))
    {
      $response['status']=1;
      $response['message']="After admin approval video will diaplay";
    }
    else
    {
      $response['status']=2;
      $response['message']="Video Not Posted";
    }
  }
  else
  {
      $response['status']=0;
      $response['message']="No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Video List //////////////////////////////////////

public function event_user_video_list_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $media = $this->eventmodel->get_result_data('media_files',array("uploaded_by!="=>'admin',"delete_flag"=>'N',"event_id"=>$data->event_id,"approval_status"=>1,"type"=>'V'));
    if(!empty($media))
    {
      $final_media=array();
      foreach($media as $val)
      {
        $val->file_name=base_url('assets/upload/media_files').'/'.$val->file_name;
        $val->like_count=count($this->eventmodel->get_result_data('media_like',array("media_id"=>$val->id)));
        $val->comment_count=count($this->eventmodel->get_result_data('media_comment',array("media_id"=>$val->id)));

        // this image like by login user
                 $val->like_user=count($this->eventmodel->get_row_data('media_like',array("media_id"=>$val->id,"user_id"=>$data->user_id)));
                 // end this image like by login user

                  // get user image
                 $user_image=$this->db->query('select profile_image from ets_user where id = "'.$data->user_id.'"')->row();
                 //echo "<pre>"; print_r($user_image); die;
                 if($user_image->profile_image!=''){
                  $val->user_image = base_url('assets/upload/appuser').'/'.$user_image->profile_image;
                 }else{
                  $val->user_image ='';
                 }
                 // end get user image

                 
        $final_media[]=$val;
      }
      $media=$final_media;
      $response['video_list'] = $media;
      $response['status'] = 1;
      $response['message'] = "Video List";
    }
    else 
    {
      $response['status'] = 2;
      $response['message'] = "No Video Found";
    }
  }
  else 
  {
    $response['status'] = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Exhibitor Type List //////////////////////////////////////

public function exhibitor_type_list_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $final_data=array();
    $where['id']=$data->event_id;
    $event_info = $this->eventmodel->get_row_data('event',$where);
    if(!empty($event_info))
    {
      $exhibitor=explode(',',$event_info->exhibitors);
      $cond="user.exhibitor_type=exhibitor_type.id";
      $cond1="user.id";
      $exhibitor_info=$this->exhibitormodel->get_result_data1("user",$exhibitor,"exhibitor_type",$cond,$cond1);
      if(!empty($exhibitor_info))
      {
        $final_array=[];
        $record_array=[];
        foreach($exhibitor_info as $val)
        {
          if(in_array($val->type_id, $record_array)) 
          {
            $final_array[$val->type_name]=$final_array[$val->type_name]+1;
          }
          else
          {
            $record_array[]=$val->type_id;
            $final_array[$val->type_name]=1;
          }
        }
        if(!empty($final_array))
        {
          foreach($final_array as $key=>$val)
          {
            $final_array1['type']=$key;
            $final_array1['count']=$val;
            $final_array2[]=$final_array1;
          }
          $final_array=$final_array2;
        }
        $response['status']=1;
        $response['message']="Exhibitor Type List";
        $response['all_exhibitor']=count($exhibitor);
        $response['exhibitor']=$final_array;
      }
      else
      {
        $response['status']=2;
        $response['message']="Exhibitor Not Available";
      }
    }
    else
    {
      $response['status']=3;
      $response['message']="Invalid Event";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Exhibitor List //////////////////////////////////////

public function exhibitor_list_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->type))
  {
    $final_data=array();
    $where['id']=$data->event_id;
    $event_info = $this->eventmodel->get_row_data('event',$where);
    if(!empty($event_info))
    {
      $exhibitor=explode(',',$event_info->exhibitors);
      $cond="user.exhibitor_type=exhibitor_type.id";
      $cond1="user.id";
      $exhibitor_info=$this->exhibitormodel->get_result_data1("user",$exhibitor,"exhibitor_type",$cond,$cond1);
      if(!empty($exhibitor_info))
      {
        $final_array=[];
        foreach($exhibitor_info as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->logo=base_url('assets/upload/appuser').'/'.$val->profile_image;
          }
          else
          {
            $val->logo="";
          }
          if($data->type!="all")
          {
            if(trim(strtolower($val->type_name))==trim(strtolower($data->type)))
            {
              $final_array[]=$val;
            }
          }
          else
          {
            $final_array[]=$val;
          }
        }
        $exhibitor_info=$final_array;
        $response['status']=1;
        $response['message']="Exhibitor List";
        $response['exhibitor_list']=$exhibitor_info;
      }
      else
      {
        $response['status']=2;
        $response['message']="Exhibitor Not Available";
      }
    }
    else
    {
      $response['status']=3;
      $response['message']="Invalid Event";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Exhibitor Details //////////////////////////////////////

public function exhibitor_details_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->exhibitor_id) && !empty($data->event_id) && !empty($data->user_id))
  {
    $where['id']=$data->exhibitor_id;
    $exhibitor_info = $this->exhibitormodel->get_row_data('user',$where);
    if(!empty($exhibitor_info))
    {

      if(!empty($exhibitor_info->floor_map))
          {
            $exhibitor_info->floor_map=base_url('assets/upload/exhibitor').'/'.$exhibitor_info->floor_map;
          }
          else
          {
            $exhibitor_info->floor_map="";
          }




      $whr['event_id']=$data->event_id;
      $whr['exhibitor_id']=$data->exhibitor_id;
      $exhibitor_info->like_count=count($this->eventmodel->get_result_data("exhibitor_like", $whr));
      $exhibitor_info->comment_count=count($this->eventmodel->get_result_data("exhibitor_comment", $whr));
      $join_condition="user.id=user_id";
      $comment_info = $this->eventmodel->get_detailed_result_data("exhibitor_comment","user",$join_condition, $whr);
      if(!empty($comment_info))
      {
        $final_array=array();
        foreach($comment_info as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->image=base_url('assets/upload/appuser').'/'.$val->profile_image;
          }
          else
          {
            $val->image="";
          }
          $final_array[]=$val;
        }
        $comment_info=$final_array;
      }
      $exhibitor_info->comment_info=$comment_info;
      $like_info = $this->eventmodel->get_detailed_result_data("exhibitor_like","user",$join_condition, $whr);
      if(!empty($like_info))
      {
        $final_array=array();
        foreach($like_info as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->image=base_url('assets/upload/appuser').'/'.$val->profile_image;
          }
          else
          {
            $val->image="";
          }
          $final_array[]=$val;
        }
        $like_info=$final_array;
      }
      $exhibitor_info->like_info=$like_info;
      $media_info = $this->eventmodel->get_detailed_result_data("exhibitor_media","user",$join_condition, $whr);
      if(!empty($media_info))
      {
        $final_array=array();
        foreach($media_info as $val)
        {
          if(!empty($val->media))
          {
            $val->media=base_url('assets/upload/exhibitor').'/'.$val->media;
          }
          else
          {
            $val->media="";
          }
          if(!empty($val->profile_image))
          {
            $val->image=base_url('assets/upload/appuser').'/'.$val->profile_image;
          }
          else
          {
            $val->image="";
          }
          $final_array[]=$val;
        }
        $media_info=$final_array;
      }
      $exhibitor_info->media_info=$media_info;
      if(!empty($exhibitor_info->profile_image))
      {
        $exhibitor_info->logo=base_url('assets/upload/appuser').'/'.$exhibitor_info->profile_image;
      }
      else
      {
        $exhibitor_info->logo="";
      }
      $whr['user_id']=$data->user_id;
      if(!empty($this->exhibitormodel->get_row_data('exhibitor_like',$whr)))
      {
        $exhibitor_info->is_like=1;
      }
      else
      {
        $exhibitor_info->is_like=0;
      }
      $response['status']=1;
      $response['message']="Exhibitor Details";
      $response['exhibitor_info']=$exhibitor_info;
    }
    else
    {
      $response['status']=2;
      $response['message']="Exhibitor Not Available";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Like Exhibitor //////////////////////////////////////

public function like_exhibitor_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->exhibitor_id) && !empty($data->user_id) && !empty($data->event_id))
  {
    $where['event_id']=$data->event_id;
    $where['exhibitor_id']=$data->exhibitor_id;
    $where['user_id']=$data->user_id;
    $like_search = $this->eventmodel->get_row_data("exhibitor_like", $where);
    if(!empty($like_search))
    {
      $like_del=$this->eventmodel->delete_data("exhibitor_like",$where);
      if(!empty($like_del))
      {
        $response['status']=3;
        $response['message']="Like Removed";
      }
      else
      {
        $response['status']=4;
        $response['message']="Like Not Removed";
      }
    }
    else
    {
      $data1['event_id']=$data->event_id;
      $data1['exhibitor_id']=$data->exhibitor_id;
      $data1['user_id']=$data->user_id;
      $like_insert = $this->eventmodel->insert_data("exhibitor_like",$data1);
      if(!empty($like_insert))
      {
        $response['status']=1;
        $response['message']="Liked";
      }
      else
      {
        $response['status']=2;
        $response['message']="Error in Like Process";
      }
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Comment Exhibitor //////////////////////////////////////

public function comment_exhibitor_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->exhibitor_id) && !empty($data->user_id) && !empty($data->comment))
  {
    $data1['event_id']=$data->event_id;
    $data1['exhibitor_id']=$data->exhibitor_id;
    $data1['user_id']=$data->user_id;
    $data1['comment']=$data->comment;
    $data1['created_date']=date("Y-m-d H:i:s");
    $comment_insert = $this->eventmodel->insert_data("exhibitor_comment",$data1);
    if(!empty($comment_insert))
    {
      $response['status']=1;
      $response['message']="Comment Posted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Comment Not Posted";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Post Exhibition Photo //////////////////////////////////////

public function post_exhibition_photo_post()
{
  if(!empty($this->post('event_id')) && !empty($this->post('user_id')) && !empty($this->post('exhibitor_id')))
  {
    $data1['event_id']=$this->post('event_id');
    $data1['exhibitor_id']=$this->post('exhibitor_id');
    $data1['user_id']=$this->post('user_id');
    if (!empty($_FILES['image']['name'])) 
    {
      $this->load->library('upload');
      $config['upload_path'] = './assets/upload/exhibitor/';
      $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
      $config['file_name'] = time();

      $this->upload->initialize($config);
      if($_FILES['image']['error']==0)
      {
        $profilepic=$_FILES['image']['name'];
        $destination="./assets/upload/exhibitor/".$profilepic;
        if (move_uploaded_file($_FILES['image']['tmp_name'] ,$destination))
        {
          $data1['media']=$profilepic;
        }
      }
    } 
    else
    {
      $response['status']=0;
      $response['message']="No Data Found";
      $this->response($response);
    }
    $ins=$this->eventmodel->insert_data("exhibitor_media",$data1);
    if(!empty($ins))
    {
      $response['status']=1;
      $response['message']="Image Posted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Image Not Posted";
    }
  }
  else
  {
    $response['status']=0;
    $response['message']="No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Search Exhibitor //////////////////////////////////////

public function search_exhibitor_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->tag))
  {
    $final_data=array();
    $where['id']=$data->event_id;
    $event_info = $this->eventmodel->get_row_data('event',$where);
    if(!empty($event_info))
    {
      $exhibitor=explode(',',$event_info->exhibitors);
      $cond="user.exhibitor_type=exhibitor_type.id";
      $cond1="user.id";
      $exhibitor_info=$this->exhibitormodel->get_result_data1("user",$exhibitor,"exhibitor_type",$cond,$cond1,$data->tag);
      if(!empty($exhibitor_info))
      {
        $final_array=[];
        foreach($exhibitor_info as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->logo=base_url('assets/upload/appuser').'/'.$val->profile_image;
          }
          else
          {
            $val->logo="";
          }
          $final_array[]=$val;
        }
        $exhibitor_info=$final_array;
        $response['status']=1;
        $response['message']="Exhibitor List";
        $response['exhibitor_list']=$exhibitor_info;
      }
      else
      {
        $response['status']=2;
        $response['message']="Exhibitor Not Available";
      }
    }
    else
    {
      $response['status']=3;
      $response['message']="Invalid Event";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Parking Info //////////////////////////////////////

public function parking_info_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $final_data=array();
    $where['event_id']=$data->event_id;
    $parking_info = $this->logisticsmodel->get_row_data('logistics',$where);
    if(!empty($parking_info))
    {
      $info['parking_instruction_description']=$parking_info->parking_instruction_description;
      $info['parking_instruction_map_image']=base_url('assets/upload/logistics').'/'.$parking_info->parking_instruction_map_image;
      $info['parking_latitude']=$parking_info->parking_latitude;
      $info['parking_longitude']=$parking_info->parking_longitude ;
      $response['status']=1;
      $response['message']="Parking Info";
      $response['parking_info']=$info;
    }
    else
    {
      $response['status']=2;
      $response['message']="Logistics Not Available";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Accomodation Info //////////////////////////////////////

public function accomodation_info_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $final_data=array();
    $where['event_id']=$data->event_id;
    $accomodation_info = $this->logisticsmodel->get_row_data('logistics',$where);
    if(!empty($accomodation_info))
    {
      $response['status']=1;
      $response['message']="Accomodation Info";
      $response['accomodation_info']=$accomodation_info->accomodation;
    }
    else
    {
      $response['status']=2;
      $response['message']="Logistics Not Available";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Transportation Info //////////////////////////////////////

public function transportation_info_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $final_data=array();
    $where['event_id']=$data->event_id;
    $transportation_info = $this->logisticsmodel->get_row_data('logistics',$where);
    if(!empty($transportation_info))
    {
      $response['status']=1;
      $response['message']="Transportation Info";
      $response['transportation_info']=$transportation_info->transportation;
    }
    else
    {
      $response['status']=2;
      $response['message']="Logistics Not Available";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Wifi Info //////////////////////////////////////

public function wifi_info_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $final_data=array();
    $where['event_id']=$data->event_id;
    $wifi_info = $this->logisticsmodel->get_row_data('logistics',$where);
    if(!empty($wifi_info))
    {
      $response['status']=1;
      $response['message']="Wifi Info";
      $response['wifi_info']=$wifi_info->wireless_access;
    }
    else
    {
      $response['status']=2;
      $response['message']="Logistics Not Available";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Local Attraction Info //////////////////////////////////////

public function local_attraction_info_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $final_data=array();

    $where['event_id']=$data->event_id;
    $where['delete_flag']='N';
    $where['is_active']='Y';
   // $local_attraction_info = $this->logisticsmodel->get_row_data('logistics',$where);
    //$local_attraction_info = $this->logisticsmodel->get_row_data('local_attraction',$where);
     $local_attraction_info = $this->logisticsmodel->get_result_data('local_attraction',$where);
    // echo "<pre>"; print_r($local_attraction_info); die;
    //echo $this->db->last_query(); die;
    if(!empty($local_attraction_info))
    {
      $response['status']=1;
      $response['message']="Local Attraction Info";
      //$response['local_attraction_info']=$local_attraction_info->local_attractions;
      $response['local_attraction_info']=$local_attraction_info;

      $i=0;
      foreach ($local_attraction_info as $value) {
        //echo $response['local_attraction_info'][$i]->map; die;
        if($value->map!=''){
          $response['local_attraction_info'][$i]->map = base_url('assets/upload/local_attraction').'/'.$value->map;
        }else{
          $response['local_attraction_info'][$i]->map = '';
          
        }

        // picture
        $where1['local_attraction_id']=$value->id;
        $local_attraction_pic = $this->logisticsmodel->get_result_data('local_attraction_pictures',$where1);
       // echo "<pre>"; print_r($local_attraction_pic); //die;
        
        if(!empty($local_attraction_pic))
        {
          $y=0;
          foreach ($local_attraction_pic as $value1) {
            $response['local_attraction_info'][$i]->image =base_url('assets/upload/local_attraction').'/'.$value1->image;
            $y++;

          }
        }
        
      $i++;
      }
    }
    else
    {
      $response['status']=2;
      $response['message']="Logistics Not Available";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Social Media Info //////////////////////////////////////

public function social_media_info_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $final_data=array();
    $where['event_id']=$data->event_id;
    $social_media_info = $this->logisticsmodel->get_row_data('logistics',$where);
    if(!empty($social_media_info))
    {
      $info['instagram_link']=$social_media_info->instagram_link;
      $info['facebook_link']=$social_media_info->facebook_link;
      $info['twitter_link']=$social_media_info->twitter_link;
      $info['google_plus_link']=$social_media_info->google_plus_link;
      $info['youtube_link']=$social_media_info->youtube_link;
      $info['flickr_link']=$social_media_info->flickr_link;
      $response['status']=1;
      $response['message']="Social Media Info";
      $response['social_media_info']=$info;
    }
    else
    {
      $response['status']=2;
      $response['message']="Logistics Not Available";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Venue Floor Map Info //////////////////////////////////////

public function venue_floor_map_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $final_data=array();
    
    $where['event_id']=$data->event_id;
    $where['delete_flag']='N';
    $where['is_active']='Y';
    $final_array = $this->eventmodel->get_result_data('floor_map',$where);
    
    foreach($final_array as $val)
        {
          if(!empty($val->floor_map))
          {
            $val->floor_map=base_url('assets/upload/floor_map').'/'.$val->floor_map;
          }
          else
          {
            $val->floor_map="";
          }

          $final_data[]=$val;
        }
        
    $response['status']=1;
    $response['message']="Venue Floor Map";
    $response['venue_floor_map']=$final_data;
    
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Post Bulletin //////////////////////////////////////

public function post_bulletin_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->user_id) && !empty($data->topic) && !empty($data->query))
  {
    $data1['event_id']=$data->event_id;
    $data1['user_id']=$data->user_id;
    $data1['topic']=$data->topic;
    $data1['query']=$data->query;
    $data1['creation_date']=date("Y-m-d H:i:s");
    $bulletin_info = $this->bulletinmodel->insertdata($data1);
    if(!empty($bulletin_info))
    {
      $response['status']=1;
      $response['message']="Bulletin Posted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Bulletin Not Posted";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Get Bulletin //////////////////////////////////////

public function get_bulletin_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->user_id))
  {
    $where['event_id']=$data->event_id;
    $where['user_id']=$data->user_id;
    $bulletin_info = $this->bulletinmodel->getAll_where('bulletin',$where);
    if(!empty($bulletin_info))
    {
      $response['status']=1;
      $response['message']="Bulletin Info";
      $response['bulletin_info']=$bulletin_info;
    }
    else
    {
      $response['status']=2;
      $response['message']="Bulletin Not Found";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Bulletin List //////////////////////////////////////

public function bulletin_list_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $where['event_id']=$data->event_id;
    $bulletin_info = $this->bulletinmodel->getAll_where('bulletin_topic',$where);
    if(!empty($bulletin_info))
    {
      $final_array=array();
      foreach($bulletin_info as $val)
      {
        $where1['bulletin_topic_id']=$val->id;
        $where1['event_id']=$val->event_id;
        $bulletin_chat = $this->bulletinmodel->get_bulletin_chat($where1);
        if(!empty($bulletin_chat))
        {
          $val->last_bulletin_chat=$bulletin_chat[0];
          $val->total_message=count($bulletin_chat);
        }
        else
        {
          $val->last_bulletin_chat=[];
          $val->total_message=0;
        }
        $final_array[]=$val;
        // print_r($val);
      }
      $announcement_info = $this->bulletinmodel->get_detailed_result_data1("announcement","user","admin_user_id=user.id",$where);
      $final_arr=[];
      if(!empty($announcement_info))
      {
        foreach($announcement_info as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->profile_image=base_url('assets/upload/profimg').'/'.$val->profile_image;
          }
          else
          {
            $val->profile_image="";
          }
          $final_arr[]=$val;
        }
      }
      $response['status']=1;
      $response['message']="Bulletin Info";
      $response['bulletin_info']=$final_array;
      $response['announcement_info']=$final_arr;
    }
    else
    {
      $response['status']=2;
      $response['message']="Bulletin Not Found";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Post Chat Bulletin //////////////////////////////////////

public function post_chat_bulletin_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->user_id) && !empty($data->bulletin_topic_id) && !empty($data->message))
  {
    $data1['event_id']=$data->event_id;
    $data1['user_id']=$data->user_id;
    $data1['bulletin_topic_id']=$data->bulletin_topic_id;
    $data1['message']=$data->message;
    $data1['created_date']=date("Y-m-d H:i:s");
    $bulletin_chat_info = $this->bulletinmodel->insertdata('bulletin_topic_chat',$data1);
    if(!empty($bulletin_chat_info))
    {
      $response['status']=1;
      $response['message']="Bulletin Chat Posted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Bulletin Chat Not Posted";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Bulletin List //////////////////////////////////////

public function bulletin_chat_details_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->bulletin_topic_id))
  {
    $where1['bulletin_topic_id']=$data->bulletin_topic_id;
    $where1['event_id']=$data->event_id;
    $bulletin_chat = $this->bulletinmodel->get_bulletin_chat_new($where1);
    $final_array=[];
    if(!empty($bulletin_chat))
    {
      foreach($bulletin_chat as $val)
      {
        if(!empty($val->image))
        {
          $val->image=base_url('assets/upload/appuser').'/'.$val->image;
        }
        else
        {
          $val->image="";
        }
        $final_array[]=$val;
      }
      $response['status']=1;
      $response['message']="Bulletin Chat Info";
      $response['bulletin_chat_info']=$final_array;
    }
    else
    {
      $response['status']=2;
      $response['message']="Bulletin Not Found";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Host List //////////////////////////////////////

public function host_list_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $final_data=array();
    $where['id']=$data->event_id;
    // $where['is_active']='Y';
    // $where['delete_flag']='N';
    $event_info = $this->eventmodel->get_row_data('event',$where);

    if(!empty($event_info))
    {
      $host=explode(',',$event_info->hosts);
      //echo "<pre>"; print_r($host); die;
      $host_info=$this->exhibitormodel->get_result_data113("user",$host,'id');
      //echo $this->db->last_query();
      //echo "<pre>"; print_r($host_info); die;
      if(!empty($host_info))
      {
        $final_array=[];
        foreach($host_info as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->image=base_url('assets/upload/appuser').'/'.$val->profile_image;
          }
          else
          {
            $val->image="";
          }
          $final_array[]=$val;
        }
        $host_info=$final_array;
        $response['status']=1;
        $response['message']="Host List";
        $response['host_list']=$host_info;
      }
      else
      {
        $response['status']=2;
        $response['message']="Host Not Available";
      }
    }
    else
    {
      $response['status']=3;
      $response['message']="Invalid Event";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Host Details //////////////////////////////////////

public function host_details_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->host_id) && !empty($data->event_id) && !empty($data->user_id))
  {
    $where['id']=$data->host_id;
    $host_info = $this->hostmodel->get_row_data('user',$where);
    if(!empty($host_info))
    {
      $whr['event_id']=$data->event_id;
      $whr['host_id']=$data->host_id;
      $host_info->like_count=count($this->eventmodel->get_result_data("host_like", $whr));
      $host_info->comment_count=count($this->eventmodel->get_result_data("host_comment", $whr));
      $join_condition="user.id=user_id";
      $comment_info = $this->eventmodel->get_detailed_result_data("host_comment","user",$join_condition, $whr);
      if(!empty($comment_info))
      {
        $final_array=array();
        foreach($comment_info as $val)
        {
          if(!empty($val->image))
          {
            $val->image=base_url('assets/upload/appuser').'/'.$val->image;
          }
          else
          {
            $val->image="";
          }
          $final_array[]=$val;
        }
        $comment_info=$final_array;
      }
      $host_info->comment_info=$comment_info;
      $like_info = $this->eventmodel->get_detailed_result_data("host_like","user",$join_condition, $whr);
      if(!empty($like_info))
      {
        $final_array=array();
        foreach($like_info as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->image=base_url('assets/upload/appuser').'/'.$val->profile_image;
          }
          else
          {
            $val->image="";
          }
          $final_array[]=$val;
        }
        $like_info=$final_array;
      }
      $host_info->like_info=$like_info;
      $media_info = $this->eventmodel->get_result_data("host_media",$whr);
      if(!empty($media_info))
      {
        $final_array=array();
        foreach($media_info as $val)
        {
          if(!empty($val->media))
          {
            $val->media=base_url('assets/upload/host').'/'.$val->media;
          }
          else
          {
            $val->media="";
          }
          $final_array[]=$val;
        }
        $media_info=$final_array;
      }
      $host_info->media_info=$media_info;
      if(!empty($host_info->profile_image))
      {
        $host_info->image=base_url('assets/upload/appuser').'/'.$host_info->profile_image;
      }
      else
      {
        $host_info->image="";
      }
      $whr['user_id']=$data->user_id;
      if(!empty($this->hostmodel->get_row_data('host_like',$whr)))
      {
        $host_info->is_like=1;
      }
      else
      {
        $host_info->is_like=0;
      }
      $response['status']=1;
      $response['message']="Host Details";
      $response['host_info']=$host_info;
    }
    else
    {
      $response['status']=2;
      $response['message']="Host Not Available";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Like Host //////////////////////////////////////

public function like_host_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->host_id) && !empty($data->user_id) && !empty($data->event_id))
  {
    $where['event_id']=$data->event_id;
    $where['host_id']=$data->host_id;
    $where['user_id']=$data->user_id;
    $like_search = $this->eventmodel->get_row_data("host_like", $where);
    if(!empty($like_search))
    {
      $like_del=$this->eventmodel->delete_data("host_like",$where);
      if(!empty($like_del))
      {
        $response['status']=3;
        $response['message']="Like Removed";
      }
      else
      {
        $response['status']=4;
        $response['message']="Like Not Removed";
      }
    }
    else
    {
      $data1['event_id']=$data->event_id;
      $data1['host_id']=$data->host_id;
      $data1['user_id']=$data->user_id;
      $like_insert = $this->eventmodel->insert_data("host_like",$data1);
      if(!empty($like_insert))
      {
        $response['status']=1;
        $response['message']="Liked";
      }
      else
      {
        $response['status']=2;
        $response['message']="Error in Like Process";
      }
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Comment Host //////////////////////////////////////

public function comment_host_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->host_id) && !empty($data->user_id) && !empty($data->comment))
  {
    $data1['event_id']=$data->event_id;
    $data1['host_id']=$data->host_id;
    $data1['user_id']=$data->user_id;
    $data1['comment']=$data->comment;
    $data1['created_date']=date("Y-m-d H:i:s");
    $comment_insert = $this->eventmodel->insert_data("host_comment",$data1);
    if(!empty($comment_insert))
    {
      $response['status']=1;
      $response['message']="Comment Posted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Comment Not Posted";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Sponsor List //////////////////////////////////////

public function sponsor_list_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $final_data=array();
    $where['id']=$data->event_id;
    $event_info = $this->eventmodel->get_row_data('event',$where);
    if(!empty($event_info))
    {
      //$sponsor=explode(',',$event_info->sponsors);
      //$sponsor_info=$this->eventmodel->get_result_data1("sponsor",$sponsor);
      $sql = "SELECT * FROM `ets_user` WHERE `id` in(".$event_info->sponsors.")";
      $query = $this->db->query($sql);
      $sponsor_info = $query->result();
      //echo "<pre>"; print_r($sponsor_info); die;
      if(!empty($sponsor_info))
      {
        $final_array=[];
        foreach($sponsor_info as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->image=base_url('assets/upload/appuser').'/'.$val->profile_image;
          }
          else
          {
            $val->image="";
          }
          $final_array[]=$val;
        }
        $host_info=$final_array;
        $response['status']=1;
        $response['message']="Sponsor List";
        $response['sponsor_list']=$sponsor_info;
      }
      else
      {
        $response['status']=2;
        $response['message']="Sponsor Not Available";
      }
    }
    else
    {
      $response['status']=3;
      $response['message']="Invalid Event";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Accomodation List //////////////////////////////////////

public function accomodation_list_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->event_id))
  	{
	    $where['event_id']=$data->event_id;
	    $where['is_active']='Y';
	    $where['delete_flag']='N';
      $event_info = $this->accomodationmodel->get_row_data('event',array("id"=>$data->event_id));
      //echo "<pre>"; print_r($event_info); die;
	    $accomodation_info = $this->accomodationmodel->get_result_data('accomodation',$where);
	    if(!empty($accomodation_info))
	    {
	      	$final_array=[];
		    foreach($accomodation_info as $val)
		    {

          if(!empty($val->latitude) && !empty($val->longitude)  && !empty($event_info->lat)  && !empty($event_info->lng))
          {
            //$distance = $this->calculatedistance($val->latitude,$val->longitude,$data->latitude,$data->longitude,"K");
            $distance = $this->calculatedistance($val->latitude,$val->longitude,$event_info->lat,$event_info->lng,"K");
            $val->distance = round($distance,2);
            $val->distance_unit = 'Km';
          }
          else
          {
            $val->distance = '';
            $val->distance_unit = '';
          }


		    	if(!empty($val->image))
		      	{
		        	$val->image=base_url('assets/upload/accomodation').'/'.$val->image;
		      	}
		      	else
		      	{
		        	$val->image="";
		      	}

            $val->price=round($val->price, 0);



		      	$final_array[]=$val;
		    }
	        $accomodation_info=$final_array;
	        $response['status']=1;
	        $response['message']="Accommodation List";
	        $response['accomodation_list']=$accomodation_info;
	    }
	  	else
	  	{
		    $response['status']=2;
		    $response['message']="No Accommodation Available";
	  	}
	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}


/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Transportation List //////////////////////////////////////

public function transportation_list_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->event_id))
  	{
	    $where['event_id']=$data->event_id;
	    $where['is_active']='Y';
	    $where['delete_flag']='N';
	    $transportation_info = $this->transportationmodel->get_result_data('transportation',$where);
	    if(!empty($transportation_info))
	    {
	      	$final_array=[];
		    foreach($transportation_info as $val)
		    {


	          if(!empty($val->latitude) && !empty($val->longitude)  && !empty($data->latitude)  && !empty($data->longitude))
	          {
	            $distance = $this->calculatedistance($val->latitude,$val->longitude,$data->latitude,$data->longitude,"K");
	            $val->distance = round($distance,2);
	            $val->distance_unit = 'Km';
	          }
	          else
	          {
	            $val->distance = '';
	            $val->distance_unit = '';
	          }

		      if(!empty($val->image))
		      {
		        $val->image=base_url('assets/upload/transportation').'/'.$val->image;
		      }
		      else
		      {
		        $val->image="";
		      }

          $val->price=round($val->price, 0);




		      $final_array[]=$val;
		    }
	        $transportation_info=$final_array;
	        $response['status']=1;
	        $response['message']="Transportation List";
	        $response['transportation_list']=$transportation_info;
	    }
	  	else
	  	{
		    $response['status']=2;
		    $response['message']="No Transportation Available";
	  	}
	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Floor Map List //////////////////////////////////////

public function floor_map_list_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->event_id))
  	{
	    $where['event_id']=$data->event_id;
	    $where['is_active']='Y';
	    $where['delete_flag']='N';
	    $floor_map_info = $this->floormapmodel->get_result_data('floor_map',$where);
	    if(!empty($floor_map_info))
	    {
	      	$final_array=[];
		    foreach($floor_map_info as $val)
		    {
		      if(!empty($val->floor_map))
		      {
		        $val->floor_map=base_url('assets/upload/floor_map').'/'.$val->floor_map;
		      }
		      else
		      {
		        $val->floor_map="";
		      }
		      $final_array[]=$val;
		    }
	        $floor_map_info=$final_array;
	        $response['status']=1;
	        $response['message']="Floor Map List";
	        $response['floor_map_list']=$floor_map_info;
	    }
	  	else
	  	{
		    $response['status']=2;
		    $response['message']="No Floor Map Available";
	  	}
	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Wifi Access List //////////////////////////////////////

public function wifi_access_list_post()
{
  	$data=json_decode( file_get_contents('php://input') );
  	if(!empty($data->event_id))
  	{
	    $where['event_id']=$data->event_id;
	    $where['is_active']='Y';
	    $where['delete_flag']='N';
	    $wifi_access_info = $this->wifiaccessmodel->get_result_data('wifi_access',$where);
	    if(!empty($wifi_access_info))
	    {
	      	$final_array=[];
		    foreach($wifi_access_info as $val)
		    {
		      if(!empty($val->logo))
		      {
		        $val->logo=base_url('assets/upload/wifi_access').'/'.$val->logo;
		      }
		      else
		      {
		        $val->logo="";
		      }
		      $final_array[]=$val;
		    }
	        $wifi_access_info=$final_array;
	        $response['status']=1;
	        $response['message']="Wifi Access List";
	        $response['wifi_access_list']=$wifi_access_info;
	    }
	  	else
	  	{
		    $response['status']=2;
		    $response['message']="No Wifi Access Available";
	  	}
	  }
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Local Attraction List //////////////////////////////////////

public function local_attraction_list_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->event_id))
    {
      $where['event_id']=$data->event_id;
      $where['is_active']='Y';
      $where['delete_flag']='N';
      $local_attraction_info = $this->localattractionmodel->get_result_data('local_attraction',$where);
      if(!empty($local_attraction_info))
      {
          $final_array=[];
        foreach($local_attraction_info as $val)
        {
          if(!empty($val->map))
          {
            $val->map=base_url('assets/upload/local_attraction').'/'.$val->map;
          }
          else
          {
            $val->map="";
          }
          $val->local_attraction_pictures=$this->localattractionmodel->get_result_data('local_attraction_pictures',array("local_attraction_id"=>$val->id));
          $val->local_attraction_link=base_url('assets/upload/local_attraction').'/';
          $final_array[]=$val;
        }
        $local_attraction_info=$final_array;
        $response['status']=1;
        $response['message']="Local Attraction List";
        $response['local_attraction_list']=$local_attraction_info;
      }
      else
      {
        $response['status']=2;
        $response['message']="No Local Attraction Available";
      }
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Document List //////////////////////////////////////

public function document_list_post()
{
  	$data=json_decode( file_get_contents('php://input') );
   // echo "<pre>"; print_r($data); die;
  	if(!empty($data->event_id))
  	{
	    $where['event_id']=$data->event_id;
	    $where['type']='D';
	    $where['approval_status']='1';
	    //$where['uploaded_by_id!=']=1;
	    //$where['uploaded_by!=']='admin';
	    $where['delete_flag']='N';
	    $document_info = $this->eventmodel->get_result_data('media_files',$where);
     // echo $this->db->last_query();
     // echo "<pre>"; print_r($document_info); die;
	    if(!empty($document_info))
	    {
	      	$final_array=[];
		    foreach($document_info as $val)
		    {
		      if(!empty($val->file_name))
		      {
		        $val->file_name=base_url('assets/upload/media_files').'/'.$val->file_name;
		      }
		      else
		      {
		        $val->file_name="";
		      }
		      $final_array[]=$val;
		    }
	        $document_info=$final_array;
	        $response['status']=1;
	        $response['message']="Document List";
	        $response['document_list']=$document_info;
	    }
	  	else
	  	{
		    $response['status']=2;
		    $response['message']="No Document Available";
	  	}
	}
  	else
  	{
	    $response['status']  = 0;
	    $response['message'] = "No Data Found";
  	}
  	$this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Like Media //////////////////////////////////////

public function like_media_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->media_id) && !empty($data->user_id))
  {
    $info=$this->pointmodel->get_row_data("media_files",array("id"=>$data->media_id)); 
    $where['media_id']=$data->media_id;
    $where['user_id']=$data->user_id;
    $like_search = $this->eventmodel->get_row_data("media_like", $where);
    if(!empty($like_search))
    {
      $like_del=$this->eventmodel->delete_data("media_like",$where);
      if(!empty($like_del))
      {
        $where['user_id']=$data->user_id;
        $where['media_id']=$data->media_id;
        $where['point_type']="Like";
        $this->pointmodel->delete_data("point",$where);

        $where1['user_id']=$info->uploaded_by_id;
        $where1['media_id']=$data->media_id;
        $where1['point_type']="Like";
        $this->pointmodel->delete_data("point",$where1); 

        $response['status']=3;
        $response['message']="Like Removed";
      }
      else
      {
        $response['status']=4;
        $response['message']="Like Not Removed";
      }
    }
    else
    {
      $data1['media_id']=$data->media_id;
      $data1['user_id']=$data->user_id;
      $data1['created_date']=date("Y-m-d h:i:s");
      $like_insert = $this->eventmodel->insert_data("media_like",$data1);
      if(!empty($like_insert))
      {
        $data1['event_id']=$info->event_id;
        $data1['user_id']=$data->user_id;
        $data1['media_id']=$data->media_id;
        $data1['point_type']="Like";
        $data1['point']=1;
        $data1['created_date']=date("Y-m-d H:i:s");
        $this->pointmodel->insert_data("point",$data1); 

        $data2['event_id']=$info->event_id;
        $data2['user_id']=$info->uploaded_by_id;
        $data2['media_id']=$data->media_id;
        $data2['point_type']="Like";
        $data2['point']=1;
        $data2['created_date']=date("Y-m-d H:i:s");
        $this->pointmodel->insert_data("point",$data2); 

        $response['status']=1;
        $response['message']="Liked";
      }
      else
      {
        $response['status']=2;
        $response['message']="Error in Like Process";
      }
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Comment Media //////////////////////////////////////

public function comment_media_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->media_id) && !empty($data->user_id) && !empty($data->comment))
  {
    $data1['media_id']=$data->media_id;
    $data1['user_id']=$data->user_id;
    $data1['comment']=$data->comment;
    $data1['created_date']=date("Y-m-d H:i:s");
    $comment_insert = $this->eventmodel->insert_data("media_comment",$data1);
    if(!empty($comment_insert))
    {
      $response['status']=1;
      $response['message']="Comment Posted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Comment Not Posted";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// My Profile //////////////////////////////////////

public function my_profile_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->user_id))
  {
    $final_data=array();
    $where['id']=$data->user_id;
    $user_info = $this->eventmodel->get_row_data("user", $where);
    if(!empty($user_info))
    {
      $final_data['name']=$user_info->name;
      $final_data['biography']=$user_info->biography;
      $final_data['position_title']=$user_info->position_title;
      $final_data['company']=$user_info->company;
      $final_data['location']=$user_info->location;
      $final_data['phone_no']=$user_info->phoneno;
      $final_data['mobile_no']=$user_info->mobile_no;
      $final_data['contact_email']=$user_info->contact_email;
      $final_data['fax_no']=$user_info->fax_no;
      $final_data['address']=$user_info->address;
      $final_data['skype_id']=$user_info->skype_id;
      $final_data['wechat_id']=$user_info->wechat_id;
      if(!empty($user_info->profile_image))
      {
        $final_data['image']=base_url('assets/upload/appuser').'/'.$user_info->profile_image;
      }
      else
      {
        $final_data['image']="";
      }

      $final_data['country']=$user_info->country;
      $final_data['city']=$user_info->city;

       if(!empty($user_info->country))
          {
            $country = $this->db->where('id',$user_info->country)->get('countries')->row();
            $final_data['country_name']=$country->name;
          }else{
            $final_data['country_name']='';
          }
       if(!empty($user_info->city))
          {
            $city = $this->db->where('id',$user_info->city)->get('cities')->row();
            $final_data['city_name']=$city->name;
          }else{
            $final_data['city_name']='';
          }
          
      $where1['user_id']=$data->user_id;
      $education_info = $this->eventmodel->get_result_data("education", $where1);
      $final_data['education_info']=$education_info;
      $company_info = $this->eventmodel->get_result_data("company", $where1);
      $final_data['company_info']=$company_info;
      $link_info = $this->eventmodel->get_result_data("link", $where1);
      $final_data['link_info']=$link_info;
      $where2['user_id']=$data->user_id;
      $where2['link_type']='social';
      $social_link_info = $this->eventmodel->get_result_data("link", $where2);
      $final_data['social_link_info']=$social_link_info;
      $where3['user_id']=$data->user_id;
      $where3['link_type']='personal';
      $personal_link_info = $this->eventmodel->get_result_data("link", $where3);
      $final_data['personal_link_info']=$personal_link_info;
      $where4['user_id']=$data->user_id;
      $where4['link_type']='other';
      $other_link_info = $this->eventmodel->get_result_data("link", $where4);


      $points = $this->pointmodel->load_user_points_data($data->user_id);

      $final_data['reward_points']='';
      if(!empty($points) && !empty($points[0]->points_earned))
      {
      	$final_data['reward_points']=$points[0]->points_earned;
      }


      $final_data['other_link_info']=$other_link_info;
      $response['status']=1;
      $response['message']="Profile Details";
      $response['profile_info']=$final_data;
    }
    else
    {
      $response['status']=2;
      $response['message']="Invalid user";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Add Lead //////////////////////////////////////

public function add_leadbk_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->name) && !empty($data->email))
  {
    $data1['event_id']=$data->event_id;
    $data1['name']=$data->name;
    $data1['emailid']=$data->email;
    if(!empty($data->phone))
    {
      $data1['phone']=$data->phone;
    }
    if(!empty($data->company))
    {
      $data1['company']=$data->company;
    }
    if(!empty($data->title))
    {
      $data1['title']=$data->title;
    }
    if(!empty($data->note))
    {
      $data1['note']=$data->note;
    }
    $data1['is_scan']=$data->is_scan;
    $data1['created_date']=date("Y-m-d H:i:s");
    $lead_insert = $this->leadmodel->insert_data("lead",$data1);
    if(!empty($lead_insert))
    {
      $response['status']=1;
      $response['message']="Lead Added";
    }
    else
    {
      $response['status']=2;
      $response['message']="Lead Not Added";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}


public function add_leadold_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id)  && !empty($data->user_id) && !empty($data->name) && !empty($data->email))
  {
    $data1['event_id']	= $data->event_id;
    $data1['user_id'] 	= $data->user_id;
    $data1['name']		= $data->name;
    $data1['email']		= $data->email;

    if(!empty($data->phone))
    {
      $data1['phone']=$data->phone;
    }
    if(!empty($data->company))
    {
      $data1['company']=$data->company;
    }
    if(!empty($data->title))
    {
      $data1['title']=$data->title;
    }
    if(!empty($data->note))
    {
      $data1['note']=$data->note;
    }
    $data1['is_scan']=(!empty($data->is_scan)?$data->is_scan:'');
    $data1['created_date']=date("Y-m-d H:i:s");
    $lead_insert = $this->leadmodel->insert_data("lead",$data1);
    if(!empty($lead_insert))
    {
      $response['status']=1;
      $response['message']="Lead Added";
    }
    else
    {
      $response['status']=2;
      $response['message']="Lead Not Added";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}



public function add_lead_post()
{
  //$data=json_decode( file_get_contents('php://input') );

  $event_id =$this->post('event_id');
  $user_id  =$this->post('user_id');
  $name     =$this->post('name');
  $email    =$this->post('email');
  $phone    =$this->post('phone');
  $company  =$this->post('company');
  $title    =$this->post('title');
  $note     =$this->post('note');
  $is_scan  =$this->post('is_scan');

if($is_scan==1)
  {
    if(!empty($email))
  {
    $data1['email']     = $email;

    if(!empty($event_id))
    {
      $data1['event_id']   = $event_id;
    }
    if(!empty($user_id))
    {
      $data1['user_id'] = $user_id;
    }
    if(!empty($name))
    {
      $data1['name']   = $name;
    }
    if(!empty($phone))
    {
      $data1['phone']   = $phone;
    }
    if(!empty($company))
    {
      $data1['company'] = $company;
    }
    // if(!empty($title))
    // {
    //   $data1['title']   = $title;
    // }
    // if(!empty($note))
    // {
    //   $data1['note']    = $note;
    // }


      if (!empty($_FILES['image']['name'])) 
      {
        $this->load->library('upload');
        $config['upload_path'] = './assets/upload/leadimages/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
        $config['file_name'] = time();

        $this->upload->initialize($config);
        if($_FILES['image']['error']==0)
        {
          $leadpic=$_FILES['image']['name'];
          $destination="./assets/upload/leadimages/".$leadpic;
          if (move_uploaded_file($_FILES['image']['tmp_name'] ,$destination))
          {
            $data1['image']=$leadpic;
          }
        }
      } 
     

    $data1['is_scan']=(!empty($is_scan)?$is_scan:'');
    $data1['created_date']=date("Y-m-d H:i:s");
    $lead_insert = $this->leadmodel->insert_data("lead",$data1);
    if(!empty($lead_insert))
    {
      $response['status']=1;
      $response['message']="Lead Added";
    }
    else
    {
      $response['status']=2;
      $response['message']="Lead Not Added";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  }else{
    if(!empty($event_id)  && !empty($user_id) && !empty($name) && !empty($email))
  {
    $data1['event_id']  = $event_id;
    $data1['user_id']   = $user_id;
    $data1['name']      = $name;
    $data1['email']     = $email;

    if(!empty($phone))
    {
      $data1['phone']   = $phone;
    }
    if(!empty($company))
    {
      $data1['company'] = $company;
    }
    if(!empty($title))
    {
      $data1['title']   = $title;
    }
    if(!empty($note))
    {
      $data1['note']    = $note;
    }

      if (!empty($_FILES['image']['name'])) 
      {
        $this->load->library('upload');
        $config['upload_path'] = './assets/upload/leadimages/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
        $config['file_name'] = time();

        $this->upload->initialize($config);
        if($_FILES['image']['error']==0)
        {
          $leadpic=$_FILES['image']['name'];
          $destination="./assets/upload/leadimages/".$leadpic;
          if (move_uploaded_file($_FILES['image']['tmp_name'] ,$destination))
          {
            $data1['image']=$leadpic;
          }
        }
      } 
     

    $data1['is_scan']=(!empty($is_scan)?$is_scan:'');
    $data1['created_date']=date("Y-m-d H:i:s");
    $lead_insert = $this->leadmodel->insert_data("lead",$data1);
    if(!empty($lead_insert))
    {
      $response['status']=1;
      $response['message']="Lead Added";
    }
    else
    {
      $response['status']=2;
      $response['message']="Lead Not Added";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
   
  
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Edit Lead //////////////////////////////////////

public function edit_leadold_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->lead_id))
  {
    $where['id']=$data->lead_id;
    if(!empty($data->name))
    {
      $data1['name']=$data->name;
    }
    if(!empty($data->email))
    {
      $data1['emailid']=$data->email;
    }
    if(!empty($data->phone))
    {
      $data1['phone']=$data->phone;
    }
    if(!empty($data->company))
    {
      $data1['company']=$data->company;
    }
    if(!empty($data->title))
    {
      $data1['title']=$data->title;
    }
    if(!empty($data->note))
    {
      $data1['note']=$data->note;
    }
    $lead_update = $this->leadmodel->update_data("lead",$where,$data1);
    if(!empty($lead_update))
    {
      $response['status']=1;
      $response['message']="Lead Update";
    }
    else
    {
      $response['status']=2;
      $response['message']="Lead Not Updated";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}


public function edit_lead_post()
{
  //$data=json_decode( file_get_contents('php://input') );

  $lead_id  =$this->post('lead_id');
  $name     =$this->post('name');
  $email    =$this->post('email');
  $phone    =$this->post('phone');
  $company  =$this->post('company');
  $title    =$this->post('title');
  $note     =$this->post('note');
  $is_scan  =$this->post('is_scan');

  if(!empty($lead_id))
  {
    $where['id']=$lead_id;
    if(!empty($name))
    {
      $data1['name']=$name;
    }
    if(!empty($email))
    {
      $data1['email']=$email;
    }
    if(!empty($phone))
    {
      $data1['phone']=$phone;
    }
    if(!empty($company))
    {
      $data1['company']=$company;
    }
    if(!empty($title))
    {
      $data1['title']=$title;
    }
    if(!empty($note))
    {
      $data1['note']=$note;
    }

     if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) 
      {
        $this->load->library('upload');
        $config['upload_path'] = './assets/upload/leadimages/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
        $config['file_name'] = time();

        $this->upload->initialize($config);
        if($_FILES['image']['error']==0)
        {
          $leadpic=$_FILES['image']['name'];
          $destination="./assets/upload/leadimages/".$leadpic;
          if (move_uploaded_file($_FILES['image']['tmp_name'] ,$destination))
          {
            $data1['image']=$leadpic;
          }
        }
      }

    $lead_update = $this->leadmodel->update_data("lead",$where,$data1);
    if(!empty($lead_update))
    {
      $response['status']=1;
      $response['message']="Lead Update";
    }
    else
    {
      $response['status']=2;
      $response['message']="Lead Not Updated";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Delete Lead //////////////////////////////////////

public function delete_lead_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->lead_id))
  {
    $lead_del = $this->leadmodel->delete($data->lead_id);
    if(!empty($lead_del))
    {
      $response['status']=1;
      $response['message']="Lead Deleted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Lead Not Deleted";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// List Lead //////////////////////////////////////

public function list_lead_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->user_id))
  {
   // $lead_list = $this->leadmodel->get_result_data("lead", array("event_id"=>$data->event_id,"user_id"=>$data->user_id,"delete_flag"=>'N'));
$lead_list = $this->leadmodel->lead_list_order_by_name($data->event_id,$data->user_id);
    

    $leadArr = array();
    $i = 0;
    foreach($lead_list as $lead)
    {
        $leadArr[$i]['id']        = $lead->id;
        $leadArr[$i]['event_id']  = $lead->event_id;
        $leadArr[$i]['user_id']   = $lead->user_id;
        $leadArr[$i]['name']      = $lead->name;
        $leadArr[$i]['email']     = $lead->email;
        $leadArr[$i]['phone']     = $lead->phone;
        $leadArr[$i]['company']   = $lead->company;
        $leadArr[$i]['title']     = $lead->title;
        $leadArr[$i]['note']      = $lead->note;
        $leadArr[$i]['image']     = (!empty($lead->image))?base_url().'assets/upload/leadimages/'.$lead->image:'';
        $leadArr[$i]['is_scan']   = $lead->is_scan;
        $leadArr[$i]['is_active'] = $lead->is_active;
        $leadArr[$i]['delete_flag'] = $lead->delete_flag;
        $leadArr[$i]['created_date'] = $lead->created_date;
        $i++;
    }



    if(!empty($leadArr))
    {
      $response['status']=1;
      $response['message']="Lead List";
      $response['lead_list']=$leadArr;

    }
    else
    {
      $response['status']=2;
      $response['message']="No Lead Available";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Change Password //////////////////////////////////////

public function change_password_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->user_id) && !empty($data->current_password) && !empty($data->new_password))
  {
    $where['id']=$data->user_id;
    $where['password']=md5($data->current_password);
    $chk=$this->attendeesmodel->get_row_data("user", $where);
    if(!empty($chk))
    {
      $data1['password']=md5($data->new_password);
      $where1['id']=$data->user_id;
      $checkd=$this->attendeesmodel->update_data('user',$where1,$data1);
      if(!empty($checkd))
      {
        $response['status']=1;
        $response['message']="Password Updated Successfully";
      }
      else
      {
        $response['status']=2;
        $response['message']="Password Not Updated";
      }
    }
    else
    {
      $response['status']=3;
      $response['message']="Old password is incorrect";
    }
  }
  else
  {
    $response['status']=0;
    $response['message']="No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Checkin Attendees //////////////////////////////////////

public function checkin_attendees_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $where['event_id']=$data->event_id;
    $where['status']='approved';
    $where['event_join']='1';
    $join_condition="user_id=user.id";
    $attendees = $this->attendeesmodel->get_detailed_result_data("event_access_request","user",$join_condition,$where);
    if(!empty($attendees))
    {
      $final_attendees=array();
      foreach($attendees as $val)
      {
        if($val->profile_image!=''){
          $val->image=base_url('assets/upload/appuser').'/'.$val->profile_image;
        }else{
          $val->image='';
        }
        
        $final_attendees[]=$val;
      }
      $attendees=$final_attendees;

      $total_checked_in_attendees = $this->attendeesmodel->total_checked_in_attendees($data->event_id);
      $total_not_checked_in_attendees = $this->attendeesmodel->get_total_not_checked_in_attendees($data->event_id);

      $per_checked_in_attendees = (((count($total_checked_in_attendees)) / (count($attendees))) * 100);

      $response['status']=1;
      $response['message']="All attendees";
      $response['total_attendees']=count($attendees);
      $response['total_checked_in_attendees']=count($total_checked_in_attendees);
      $response['total_not_checked_in_attendees']=count($total_not_checked_in_attendees);
      $response['per_checked_in_attendees']=number_format($per_checked_in_attendees,2).'%';
      $response['all_attendees']=$attendees;
    }
    else
    {
      $response['status']=2;
      $response['message']="No attendees found";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Search Attendees //////////////////////////////////////

public function search_attendees_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->tag))
  {
    $where['event_id']=$data->event_id;
    $where['status']='approved';
    $where['event_join']='1';
    $join_condition="user_id=user.id";
    $attendees = $this->attendeesmodel->get_detailed_result_data("event_access_request","user",$join_condition,$where,$data->tag);
    if(!empty($attendees))
    {
      $final_attendees=array();
      foreach($attendees as $val)
      {
        $val->image=base_url('assets/upload/appuser').'/'.$val->profile_image;
        $final_attendees[]=$val;
      }
      $attendees=$final_attendees;
      $response['status']=1;
      $response['message']="All attendees";
      $response['all_attendees']=$attendees;
    }
    else
    {
      $response['status']=2;
      $response['message']="No attendees found";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Change checkin Status //////////////////////////////////////

public function change_checkin_status_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->checkedin_id))
  {
    $checkin_status = $this->attendeesmodel->checked_in_status($data->checkedin_id);
    if(!empty($checkin_status))
    {
      $response['status']=1;
      $response['message']="Checked In Successfully";
    }
    else
    {
      $response['status']=2;
      $response['message']="Checked Out Successfully";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// My Notes //////////////////////////////////////

public function my_notes_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->user_id))
  {
    $final_data=array();
    $where['agenda_note.user_id']=$data->user_id;
    $join_condition="agenda_id=agenda.id";
    $response['agenda_notes'] = $this->agendamodel->get_detailed_result_data("agenda_note","agenda",$join_condition,$where);
    $where1['personal_note.user_id']=$data->user_id;
    $join_condition1="user_id=user.id";
    $response['personal_notes'] = $this->agendamodel->get_detailed_result_data("personal_note","user",$join_condition1,$where1);


    $all_note=$this->usermodel->get_result_data("my_note",array("user_id"=>$data->user_id));
    $response['my_notes']=$all_note;

    $response['status']=1;
    $response['message']="All Notes";
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Send Feedback //////////////////////////////////////

public function send_feedback_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->user_id) && !empty($data->comment) && !empty($data->rate))
  {
    $data1['user_id']=$data->user_id;
    $data1['comment']=$data->comment;
    $data1['rate']=$data->rate;
    $data1['created_date']=date("Y-m-d H:i:s");
    $feedback_insert = $this->eventmodel->insert_data("feedback",$data1);
    if(!empty($feedback_insert))
    {
      $response['status']=1;
      $response['message']="Feedback Sent";
    }
    else
    {
      $response['status']=2;
      $response['message']="Feedback Not Sent";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Profile Visibility //////////////////////////////////////

public function profile_visibility_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->user_id) && isset($data->is_visible))
  {
    $where['id']=$data->user_id;
    $data1['is_visible']=$data->is_visible;
    $visibility_update = $this->eventmodel->update_data("user",$where,$data1);
    if(!empty($visibility_update))
    {
      $response['status']=1;
      $response['message']="Profile Visibility Changed Successfully";
    }
    else
    {
      $response['status']=2;
      $response['message']="Profile Visibility Not Changed";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Recipient List //////////////////////////////////////

public function recipient_list_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $event_info = $this->eventmodel->get_row_data("event", array("id"=>$data->event_id));
    if(!empty($event_info))
    {
      $event_info->appuser = $event_info->attendees;
      // echo "<pre>"; print_r($event_info); die; 
      /// remove speaker and others from attendee
      // attendee are only who join event through join code
      $attendees = explode(',', $event_info->attendees);
      $sponsors = explode(',',$event_info->sponsors);
      $speakers = explode(',',$event_info->speakers);
      $exhibitors = explode(',',$event_info->exhibitors);

      $arr_diff_attendees = array_diff($attendees, $sponsors);
      $arr_diff_attendees = array_diff($arr_diff_attendees, $speakers);
      $arr_diff_attendees = array_diff($arr_diff_attendees, $exhibitors);
      
      $arr_diff_attendees = array_filter($arr_diff_attendees);
      $event_info->attendees = implode(',', $arr_diff_attendees);

      //echo "<pre>"; print_r($sponsors); 
      //echo "<pre>"; print_r($arr_diff_attendees); die;
      $recipient_list = $this->eventmodel->get_result_data("user_type", array("app_status"=>'1'));
      if(!empty($recipient_list))
      {
        $final_array=[];
        $temp_array['id']='all';
        $temp_array['user_type']='Select All';
        $temp_array['total_number']='';
        $final_array[]=$temp_array;
       $t_arr = array();
        foreach($recipient_list as $val)
        {
          //echo "<pre>"; print_r($val); die;
          $temp_array['id']=$val->id;
          $temp_array['user_type']=$val->type_name;
          if(!empty($event_info->{strtolower($val->type_name)}))
          {
             $total_cc = array_filter(explode(',',$event_info->{strtolower($val->type_name)}));   
            $temp_array['total_number']=count($total_cc);
          }
          else
          {
            $temp_array['total_number']=0;
          }
          $final_array[]=$temp_array;
        }

        $response['status']=1;
        $response['message']="Recipient List";
        $response['recipient_list']=$final_array;
      }
      else
      {
        $response['status']=2;
        $response['message']="No Recipient Available";
      }
    }
    else
    {
      $response['status']=3;
      $response['message']="Invalid Event";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Send Notification //////////////////////////////////////

public function send_notification_post()
{
  $data=json_decode( file_get_contents('php://input') );
  //echo "<pre>"; print_r($data); die;
  if(!empty($data->recipients) && !empty($data->notification_title) && !empty($data->notification_text) && !empty($data->event_id) && !empty($data->option))
  {
  	//echo "<pre>"; print_r($data->recipients); die;
    $data1['receivers'] = "";
    if(!empty($data->recipients))
    {
      foreach($data->recipients as $val)
      {
          if($val=="all")
          {
              $data1['receivers']="all";
              break;
          }
      }
    }
  // echo "<pre>"; print_r($data1); //die;
    if(empty($data1['receivers']))
    {
      $data1['receivers']=implode(",",$data->recipients);
    }
    $data1['notification_title']=$data->notification_title;
    $data1['notification_text']=$data->notification_text;
    $data1['event_id']=$data->event_id;
    $data1['option_noti']=$data->option;
    $data1['created_date']=date("Y-m-d H:i:s");
    $notification_insert = $this->eventmodel->insert_data("notification",$data1);
    // echo "<pre>"; print_r($notification_insert);
    if(!empty($notification_insert))
    {
      if($data1['receivers']=='all')
      {
        $type_list=$this->notificationmodel->get_result_data("user_type",array("app_status"=>'1'));
      }
      else
      {
        $type_list=$this->notificationmodel->get_result_data("user_type",'',$data->recipients);
      }
    //echo $this->db->last_query();
    //echo "<pre>"; print_r($type_list); die;
      if(!empty($type_list))
      {
        $event_info = $this->eventmodel->get_row_data("event", array("id"=>$data->event_id));
       foreach($type_list as $val)
        {
        		if($val->table_slug=='apiappuser'  || $val->table_slug=='appuser'){
        		$val->table_slug='user';
        		$val->type_name='attendees';
        	}

          $details=$this->notificationmodel->get_result_data($val->table_slug,'',explode(",",$event_info->{strtolower($val->type_name)}));
          //echo $this->db->last_query();      

      //echo "<pre>"; print_r($details);     die;

          if(!empty($details))
          {

      if($data->option=='both'){
      	foreach($details as $value)
              {
              	// notification 
              	if($val->table_slug=="apiappuser" || $val->table_slug=="appuser"){
                $this->notification($value->fcm_reg_token,$data->notification_text,$data->notification_title);
        		}
                // mail
                if($val->table_slug=="user")
                {
                  $value->email=$value->emailid;
                }
                $subject = "Eventsador - Notification Received";
                $message = '<p>Greetings ' . $value->name . ",</p>";
                $message .= '<p>'.$data->notification_title.'</p>';
                $message .= '<p>'.$data->notification_text.'</p>';
                // $message .= '<br><strong>Password: </strong>' . $password . "</p>";
                $message .= '<p>Thank you,</p>';
                $message .= '<p>Eventsador Admin</p>';

                $mail_data = [
                     'name' => $value->name,
                     'body' => $message,
                ];
                $this->load->helper('email');
                send_email($value->email, $subject, $mail_data);
              }
      }else if($data->option=='app'){
       // echo "<pre>"; print_r($val); //die;
        //echo "<pre>"; print_r($details); die;
      	foreach($details as $value)
              {
              	// notification 
                if($val->table_slug=="apiappuser" || $val->table_slug=="appuser"){
                $this->notification($value->fcm_reg_token,$data->notification_text,$data->notification_title);
        		}               
              }
      }else{
      	foreach($details as $value)
              {              	
                // mail
                if($val->table_slug=="user")
                {
                  $value->email=$value->emailid;
                }
                $subject = "Eventsador - Notification Received";
                $message = '<p>Greetings ' . $value->name . ",</p>";
                $message .= '<p>'.$data->notification_title.'</p>';
                $message .= '<p>'.$data->notification_text.'</p>';
                // $message .= '<br><strong>Password: </strong>' . $password . "</p>";
                $message .= '<p>Thank you,</p>';
                $message .= '<p>Eventsador Admin</p>';

                $mail_data = [
                     'name' => $value->name,
                     'body' => $message,
                ];
                $this->load->helper('email');
                send_email($value->email, $subject, $mail_data);
              }

      }

//************************

          }
        }
      }

      $response['status']=1;
      $response['message']="Notification Sent";
    }
    else
    {
      $response['status']=2;
      $response['message']="Notification Not Sent";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  } 
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// SEND PDF/DOC FILE  //////////////////////////////////////

public function export_contact_post()
{
  $data=json_decode( file_get_contents('php://input') );
  //if(!empty($data->user_id) && is_uploaded_file($_FILES['file_name']['tmp_name']))

  $where['id']=$data->user_id;
  $chk=$this->attendeesmodel->get_row_data("user", $where);

 /* echo "<pre>";
  print_r($chk);
  exit;*/

  if(!empty($data->user_id))
  {
  	  	$contact_list=array();

		$contact_list = $this->eventmodel->get_result_data("contact",array("user_id"=>$data->user_id,"contact_type!="=>'request'));
	    if(!empty($contact_list))
	    {
	      	$con=array();
		      foreach($contact_list as $val)
		      {
		        
		        $con[]=$val;
		      }
		    $contact_info=$con;

	      /*Finbal array*/
	      // $farray = array();
	      $newfilename = rand().time().'excel.csv'; 
	      $file = fopen('./assets/excels/'.$newfilename,"w");
	      fputcsv($file,array(
	      	'Name',
			'Email',
			'Phone',
			'Title',
			'Position',
			'Company',
			'Location',
			'Skype Id',
			'Wechat Id',
			'Fax No'
	      ));

	      foreach ($contact_info as $value) {	      	
	      	fputcsv($file,array(
	      		$value->name,
				$value->email,
				$value->phone,
				$value->title,
				$value->position,
				$value->company,
				$value->location,
				$value->skype_id,
				$value->wechat_id,
				$value->fax_no
	      	));
	      }

	      // $excelData = implode('\r\n', $farray);
	      
	      // file_put_contents('./assets/excels/'.$newfilename, $excelData); 
	     
	    }


/*
	  	$this->load->library('email');

		$this->email->from('sketch.dev15@gmail.com', 'Your Name');
		$this->email->to('devswata@gmail.com');

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');
		$this->email->attach('assets/excels/'.$newfilename);

		$this->email->send();*/

		$filename = 'assets/excels/'.$newfilename;

  		$subject = "Eventsador - Contact List";
        $message = '<p>Greetings ' . $chk->name . ",</p>";
       // $message .= '<p>'.$data->notification_title.'</p>';
       // $message .= '<p>'.$data->notification_text.'</p>';
        // $message .= '<br><strong>Password: </strong>' . $password . "</p>";
        $message .= '<p>Thank you,</p>';
        $message .= '<p>Eventsador Admin</p>';

        $mail_data = [
             'name' => $chk->name,
             'body' => $message,
        ];
        $this->load->helper('email');
        send_email_file($chk->emailid, $subject, $mail_data,$filename);
        //send_email_file('devswata@gmail.com', $subject, $mail_data,$filename);


		$response['status']  = 1;
    	$response['message'] = "Email Sent Successfully";

    
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// aboutus //////////////////////////////////////

public function aboutus_post()
{
  // $response['about_us'] = $this->eventmodel->get_row_data("aboutus",array("id"=>1));

  // echo "<pre>"; print_r($response['about_us']); die;
  // $response['status']=1;
  // $response['message']="About us info";
  // $this->response($response);


  $result=$this->aboutusmodel->fetch_details();

    if(!empty($result))
    {
        $response['status']=1;
        $result= strip_tags(html_entity_decode($result->content_description));
        $result= str_replace("\r\n",'', $result);
        $response['about_us']= str_replace("\t",'', $result); 
        $response['message']="About us info";
    }
    else
    {
        $response['status']=0;
        $response['message']="No Data Found";
    } 
   
    $this->response($response);

}

/////////////////////////////////// END //////////////////////////////////////

/////////////////////////////////// Lead Retrieval Report //////////////////////////////////////

public function lead_report_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {

  	$lead_list_count = $this->leadmodel->get_lead_result_data($data->event_id);


    $where['event_id']=$data->event_id;
    $where['is_active']='Y';
    $where['delete_flag']='N';
    $response['lead_list'] = $this->eventmodel->get_result_data("lead",$where);
    $response['total_lead'] = count($response['lead_list']);
    $response['lead_list_count'] = '';
    if(!empty($lead_list_count))
    {
    	 $response['lead_list_count'] = $lead_list_count;
    }
    $where['is_scan']='1';
    $response['total_scan'] = count($this->eventmodel->get_result_data("lead",$where));
    $response['total_mannual'] = $response['total_lead'] - $response['total_scan'];
    $response['status']=1;
    $response['message']="Lead Retrieval Report";
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// Lead Retrieval Report //////////////////////////////////////

public function set_notification_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->user_id))
  {


		$notification_exist = $this->eventmodel->get_row_data("user_notification",array("user_id"=>$data->user_id));


		if(!empty($data->event_notify)||!empty($data->event_notify))
		{

			if(!empty($notification_exist))
			{
				$where = array();
				$where['user_id'] = $data->user_id;
				$noti_del = $this->eventmodel->delete_data("user_notification",$where);
			}


			$data1 					= array();
	  		$data1['user_id']		= $data->user_id;
	  		$data1['event_notify']	= $data->event_notify;
		    $data1['agenda_notify']	= $data->agenda_notify;
		
		    $notification_insert = $this->eventmodel->insert_data("user_notification",$data1);
		}
		
		$notification_list 	= $this->eventmodel->get_row_data("user_notification",array("user_id"=>$data->user_id));

	    $response['notification_list']	= $notification_list;


	    if($notification_insert)
	    {
	    	$response['status']				= 1;
	    	$response['message']			= "Notification Saved Successfully";
	    }
	    else
	    {
	    	$response['status']			= 2;
	    	$response['message']		= "Notification Not Saved Successfully";
	    }
	  
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// PAST EVENTS //////////////////////////////////////

public function featured_logo_post()
{
    //$data=json_decode( file_get_contents('php://input') );
    
      
      $where        = '';
      $where        = " is_featured = 'Y' ";
      $result_list    = $this->eventmodel->get_my_events('event',$where);


      if(!empty($result_list))
      {
        $final = array();
        foreach ($result_list as $e) 
        {
            $data1['id'] = $e->id;
            $data1['event_name'] = $e->event_name;
            $data1['event_venue'] = $e->event_venue;
            //$data1['event_venue'] = $e->city.', '.$e->country;
            $data1['event_description'] = $e->event_description;
            // $data1['route']=$e->route;
            // $data1['street_no'] = $e->street_no;
            // $data1['street_name'] = $e->street_name;
            // $data1['state'] = $e->state;
            // $data1['city'] = $e->city;
            // $data1['country']=$e->country;
            // $data1['zip_code'] = $e->zip_code;
            // $data1['lat'] = $e->lat;
            // $data1['lng'] = $e->lng;
            $data1['event_date'] = date("d F, Y ", strtotime($e->event_start_date));
            $data1['event_time'] = $e->event_start_time;
            if(!empty($e->event_logo))
            {
              $data1['logo'] = base_url('assets/upload/event') . '/' . $e->event_logo;
            }
            else
            {
              $data1['logo'] = "";
            }

            $data1['is_featured'] = $e->is_featured;

           /* $attendees = explode(',', $e->attendees);
            $data1['attendees'] = $this->eventmodel->get_result_data('appuser','',$attendees);

            $organizers = explode(',', $e->organizers);
            $data1['organizers'] = $this->eventmodel->get_result_data('organizer','',$organizers);
            $sponsors = explode(',', $e->sponsors);
            $data1['sponsors'] = $this->eventmodel->get_result_data('sponsor','',$sponsors);
            $media = $this->eventmodel->get_result_data('media_files',array("delete_flag"=>'N',"event_id"=>$e->id));
            if(!empty($media))
            {
                $final_media=array();
                foreach($media as $val)
                {
                    $val->file_name=base_url('assets/upload/media_files').'/'.$val->file_name;
                    $final_media[]=$val;
                }
                $media=$final_media;
            }
            $data1['media'] =$media;*/
            $final[] = $data1;
        }
        $response['event_info'] = $final;
        $response['status'] = 1;
        $response['message'] = "Featured Event List";
      }
      else
      {
        $response['status']=0;
        $response['message']="No Events Found";
      } 
    
    $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////


/////////////////////////////////// FAQ //////////////////////////////////////

public function faq_list_post()
{
  $where['is_active']='Y';
  $where['delete_flag']='N';
  $faq_info = $this->faqmodel->get_result_data('faq',$where);
  if(!empty($faq_info))
  {
    $final_array=[];
    foreach($faq_info as $val)
    {
      $image_info=$this->faqmodel->faq_images($val->id);
      $val->faq_images=$image_info;
      $final_array[]=$val;
    }
    $faq_info=$final_array;
    $response['status']=1;
    $response['message']="FAQ List";
    $response['faq_list']=$faq_info;
  }
  else
  {
    $response['status']=2;
    $response['message']="No FAQ Available";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////

public function notification($fcm,$text,$title)
{
  $msg=$title;
  $msgg=$text;
  $this->load->library('gcmpushmessage','AAAAQQY2SPA:APA91bHXm3KVLxckAVs2-hYNfbif6fxdN-eXTEHNUnBG3efje1vvv2aYNTJs2usgZNmDFeHy5EH0wwynQ4vtG6t8DYb-MuDfJC4KOd_Y1kgOowCJOh1pe9ZF8avPpI80oCM_ffWGl6Mu');
  // $gcmregtoken = "ckpnfYyp5jc:APA91bFd8ZVoSHFd9URKne44NqgOMOZZk6Ix1aBTmfY8Y2bUloY3Sgl4tHjJ5uJgoLmUUuMVCBSC6vggK4ZbhtoFPfjLFn1lOmuISGwIETV-hw2UT_rKw57em5-XLGTdsLbP9GvNBBly";
  $gcmregtoken = $fcm;
  $this->gcmpushmessage->setDevices($gcmregtoken);
  $r = $this->gcmpushmessage->send($text, array('title' => $title,'body' => "Eventsador"));
  // print_r($r);die;
  return $r;
  // $deviceid="4840472bdd7e1bb69ed2e53b2f485f4f2e78938aae69214a21764bd6b8e07d83";
  // $message="hello!This is Rima from YPO";
  // push_apns($deviceid,$message,"Testing"); 
}


private function calculatedistance($lat1, $lon1, $lat2, $lon2, $unit) 
{
    $s = $lon1 - $lon2;
    $distance = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($s));
    $distance = acos($distance);
    $distance = rad2deg($distance);
    $miles = $distance * 60 * 1.1515;
    $unit = strtoupper($unit);
    if ($unit == "K") {
    return ($miles * 1.609344);
    } else if ($unit == "N") {
    return ($miles * 0.8684);
    } else {
    return $miles;
    }
}
// 04.05.18

  public function export_lead_post()
  {
    $data=json_decode( file_get_contents('php://input') );

    $where['id']=$data->user_id;
    $chk=$this->attendeesmodel->get_row_data("user", $where);

    // echo "<pre>";
    // print_r($chk);
    // exit;

    if(!empty($data->user_id))
    {
          $contact_list=array();

      $contact_list = $this->eventmodel->get_result_data("lead",array("user_id"=>$data->user_id));
    //   echo "<pre>";
    // print_r($contact_list);
    // exit;
        if(!empty($contact_list))
        {
            $con=array();
            foreach($contact_list as $val)
            {              
              $con[]=$val;
            }
          $contact_info=$con;

          /*Final array*/
          $newfilename = rand().time().'excel.csv'; 
          $file = fopen('./assets/excels/'.$newfilename,"w");
          fputcsv($file,array(
            'Name',
        'Email',
        'Phone',
        'Company',
        'Title',
        'Note'
          ));

          foreach ($contact_info as $value) {         
            fputcsv($file,array(
              $value->name,
          $value->email,
          $value->phone,
          $value->company,
          $value->title,
          $value->note
            ));
          }         
        }

      $filename = 'assets/excels/'.$newfilename;

        $subject = "Eventsador - Lead List";
          $message = '<p>Greetings ' . $chk->name . ",</p>";
          $message .= '<p>Thank you,</p>';
          $message .= '<p>Eventsador Admin</p>';

          $mail_data = [
               'name' => $chk->name,
               'body' => $message,
          ];
          $this->load->helper('email');
          send_email_file($chk->emailid, $subject, $mail_data,$filename);
        $response['status']  = 1;
        $response['message'] = "Email Sent Successfully";      
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
  }

  // 07.05.18



  public function sponser_details_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->sponser_id) && !empty($data->event_id) && !empty($data->user_id))
  {
    $where['id']=$data->sponser_id;
    $host_info = $this->hostmodel->get_row_data('user',$where);
    //echo "<pre>"; print_r($host_info); die;
    if(!empty($host_info))
    {
      $whr['event_id']=$data->event_id;
      $whr['sponser_id']=$data->sponser_id;
      $host_info->like_count=count($this->eventmodel->get_result_data("sponser_like", $whr));
      $host_info->comment_count=count($this->eventmodel->get_result_data("sponser_comment", $whr));
      $join_condition="user.id=user_id";
      $comment_info = $this->eventmodel->get_detailed_result_data("sponser_comment","user",$join_condition, $whr);
      if(!empty($comment_info))
      {
        $final_array=array();
        foreach($comment_info as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->image=base_url('assets/upload/appuser').'/'.$val->profile_image;
          }
          else
          {
            $val->image="";
          }
          $final_array[]=$val;
        }
        $comment_info=$final_array;
      }
      $host_info->comment_info=$comment_info;
      $like_info = $this->eventmodel->get_detailed_result_data("sponser_like","user",$join_condition, $whr);
      if(!empty($like_info))
      {
        $final_array=array();
        foreach($like_info as $val)
        {
          if(!empty($val->profile_image))
          {
            $val->image=base_url('assets/upload/appuser').'/'.$val->profile_image;
          }
          else
          {
            $val->image="";
          }
          $final_array[]=$val;
        }
        $like_info=$final_array;
      }
      $host_info->like_info=$like_info;
      $media_info = $this->eventmodel->get_result_data("sponser_media",$whr);
      if(!empty($media_info))
      {
        $final_array=array();
        foreach($media_info as $val)
        {
          if(!empty($val->media))
          {
            $val->media=base_url('assets/upload/sponsor').'/'.$val->media;
          }
          else
          {
            $val->media="";
          }
          $final_array[]=$val;
        }
        $media_info=$final_array;
      }
      $host_info->media_info=$media_info;
      if(!empty($host_info->profile_image))
      {
        $host_info->image=base_url('assets/upload/appuser').'/'.$host_info->profile_image;
      }
      else
      {
        $host_info->image="";
      }
      $whr['user_id']=$data->user_id;
      if(!empty($this->hostmodel->get_row_data('sponser_like',$whr)))
      {
        $host_info->is_like=1;
      }
      else
      {
        $host_info->is_like=0;
      }
      $response['status']=1;
      $response['message']="Sponser Details";
      $response['sponser_info']=$host_info;
    }
    else
    {
      $response['status']=2;
      $response['message']="Host Not Available";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}



public function like_sponser_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->sponser_id) && !empty($data->user_id) && !empty($data->event_id))
  {
    $where['event_id']=$data->event_id;
    $where['sponser_id']=$data->sponser_id;
    $where['user_id']=$data->user_id;
    $like_search = $this->eventmodel->get_row_data("sponser_like", $where);
    if(!empty($like_search))
    {
      $like_del=$this->eventmodel->delete_data("sponser_like",$where);
      if(!empty($like_del))
      {
        $response['status']=3;
        $response['message']="Like Removed";
      }
      else
      {
        $response['status']=4;
        $response['message']="Like Not Removed";
      }
    }
    else
    {
      $data1['event_id']=$data->event_id;
      $data1['sponser_id']=$data->sponser_id;
      $data1['user_id']=$data->user_id;
      $like_insert = $this->eventmodel->insert_data("sponser_like",$data1);
      if(!empty($like_insert))
      {
        $response['status']=1;
        $response['message']="Liked";
      }
      else
      {
        $response['status']=2;
        $response['message']="Error in Like Process";
      }
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}


public function comment_sponser_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id) && !empty($data->sponser_id) && !empty($data->user_id) && !empty($data->comment))
  {
    $data1['event_id']=$data->event_id;
    $data1['sponser_id']=$data->sponser_id;
    $data1['user_id']=$data->user_id;
    $data1['comment']=$data->comment;
    $data1['created_date']=date("Y-m-d H:i:s");
    $comment_insert = $this->eventmodel->insert_data("sponser_comment",$data1);
    if(!empty($comment_insert))
    {
      $response['status']=1;
      $response['message']="Comment Posted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Comment Not Posted";
    }
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}


public function app_download_report_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $total_attendee_request = $this->db->where('is_request','1')->where('event_id',$data->event_id)->where('user_type','user')->get('event_access_request')->num_rows();
    $data1['total_attendee_request']=$total_attendee_request;
    $data1['total_attendee']=$total_attendee_request;

    $total_attendee_join = $this->db->where('event_join','1')->where('event_id',$data->event_id)->where('user_type','user')->get('event_access_request')->num_rows();
    $data1['total_attendee_join']=$total_attendee_join;
      $data1['total_user_reg_app']=$total_attendee_join;

    $response['status']=1;
    $response['message']="Total";
    $response['total']=$data1;  

  }else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
	// $Total_attendee = $this->db->where('event_join','1')->group_by('user_id')->get('event_access_request')->num_rows();
	// $data1['total_attendee']=$Total_attendee;

	// $Total_speaker = $this->db->where('delete_flag','N')->where('type_id','3')->get('user')->num_rows();
	// $data1['total_speaker']=$Total_speaker;		

	// $Total_staff = $this->db->where('role_id','4')->where('type_id','9')->get('user')->num_rows();
	// $data1['total_staff']=$Total_staff;

 //  $Total_attendee = $this->db->where('role_id','0')->where('type_id','1')->get('user')->num_rows();
 //  $data1['total_user_reg_app']=$Total_attendee;

 //    $response['status']=1;
 //    $response['message']="Total";
 //    $response['total']=$data1;    
 //  $this->response($response);
}



public function game_point_report_post()
{
  $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->event_id))
    {
    	$game_point = $this->db->select('user_id, sum(point) as total_point')->where('event_id',$data->event_id)->group_by('user_id')->order_by('point','desc')->limit('10')->get('point')->result();
    	$i=0;
    	if(!empty($game_point)){
    		foreach ($game_point as $value) {
    			$game_point[$i]->total_point = number_format($value->total_point);

    			$get_user = $this->db->select('id,name,profile_image')->where('id',$value->user_id)->get('user')->row();

    			if(!empty($get_user))
    	        {
    			if(!empty($get_user->profile_image))
    	        {
    	          $get_user->profile_image=base_url('assets/upload/appuser')."/".$get_user->profile_image;;
    	        }
    	        else
    	        {
    	          $get_user->profile_image="";
    	        }
    	    }

    			$game_point[$i]->user=$get_user;

    			// point distribution
    			$get_point_distribution = $this->db->select('point_type,point')->where('user_id',$value->user_id)->get('point')->result();
    			$game_point[$i]->point_distribution=$get_point_distribution;
    			$i++;
    		}
    	}
    	  $response['status']=1;
        $response['message']="Total";
        $response['game_point']=$game_point; 
 }else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }   
  $this->response($response);
}  


public function user_myevent_post()
{
    $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->user_id) && !empty($data->event_name) && !empty($data->no_of_attendees) && !empty($data->country_code) && !empty($data->phone_number) && !empty($data->message) && !empty($data->event_start_date))
    {

          $data1                = array();
          $data1['user_id']    = $data->user_id;
          $data1['event_name']    = $data->event_name;
          $data1['no_of_attendees']  = $data->no_of_attendees;
          $data1['country_code'] = $data->country_code;
          $data1['phone_number']     = $data->phone_number;
          $data1['message']  = $data->message;
          $data1['event_start_date'] = $data->event_start_date;
          $data1['entry_date'] = date('Y-m-d');

          $insert=$this->eventmodel->insert_data('user_myevent',$data1);

          if(!empty($insert))
          {
            $response['status']=1;
            $response['message']="Thanks for your request, we will contact you soon";
            // get user details
            $user = $this->db->select('name,emailid')->where('id',$data->user_id)->get('user')->row();
            // get user details
            $admin = $this->db->select('name,emailid')->where('id','1')->get('user')->row();
            // email to admin
            	$subject = "Eventsador - User Event";
                $message = '<p>Hello Admin,</p>';
                $message .= '<p> An Event sent by user.</p>';

                $message .= '<br><br><p> <b>User Details : - </b></p>';
                $message .= '<p> User Name :'.$user->name.'</p>';
                $message .= '<p> User Email :'.$user->emailid.'</p>';

                $message .= '<br><br><p><b> Event Details : - </b></p>';
                $message .= '<p> Event Name :'.$data->event_name.'</p>';
                $message .= '<p> no_of_attendees :'.$data->no_of_attendees.'</p>';
                $message .= '<p> country_code :'.$data->country_code.'</p>';
                $message .= '<p> phone_number :'.$data->phone_number.'</p>';
                $message .= '<p> message :'.$data->message.'</p>';
                $message .= '<p> event_start_date :'.$data->event_start_date.'</p>';

                $message .= '<p>Thank you,</p>';
                $message .= '<p>Eventsador</p>';

                $mail_data = [
                     'name' => $user->name,
                     'body' => $message,
                ];
                $this->load->helper('email');
                send_email('app@eventsador.com', $subject, $mail_data);
          }
          
       
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}     

// 10.05.18
public function media_like_list_post()
{
  $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->media_id))
    {
      $media =  $this->db->select('media_like.created_date as like_date, user.name, user.profile_image')->join('user','user.id = media_like.user_id','left')->where('media_like.media_id',$data->media_id)->get('media_like')->result();
      
     // echo "<pre>"; print_r($media); die;
   
    if(!empty($media))
        {
            $final_media=array();
            foreach($media as $val)
            {               
                 if($val->profile_image!=''){
                  $val->profile_image = base_url('assets/upload/appuser').'/'.$val->profile_image;
                 }else{
                  $val->profile_image ='';
                 }  
                $final_media[]=$val;
            }
            $media=$final_media;            
          $response['status'] = 1;
          $response['message'] = "Like List";
          $response['like_list'] = $media;
        }
        else 
      {
          $response['status'] = 2;
          $response['message'] = "No Like Found";
      }
    }
    else 
    {
        $response['status'] = 0;
        $response['message'] = "No Data Found";
    }
    $this->response($response);
} 


public function media_comment_list_post()
{
  $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->media_id))
    {
      $media =  $this->db->select('media_comment.comment,media_comment.created_date as comment_date,user.name, user.profile_image')->join('user','user.id = media_comment.user_id','left')->where('media_comment.media_id',$data->media_id)->get('media_comment')->result();
      
     // echo "<pre>"; print_r($media); die;
   
    if(!empty($media))
        {
            $final_media=array();
            foreach($media as $val)
            {               
                 if($val->profile_image!=''){
                  $val->profile_image = base_url('assets/upload/appuser').'/'.$val->profile_image;
                 }else{
                  $val->profile_image ='';
                 }  
                $final_media[]=$val;
            }
            $media=$final_media;
            
          $response['status'] = 1;
          $response['message'] = "Comment List";
          $response['comment_list'] = $media;
        }
        else 
      {
          $response['status'] = 2;
          $response['message'] = "No Comment Found";
      }
    }
    else 
    {
        $response['status'] = 0;
        $response['message'] = "No Data Found";
    }
    $this->response($response);
}

// 15.05.18
public function country_list_post()
{

  $country =  $this->db->select('*')->get('countries')->result();
       if(!empty($country))
        {
           $response['status'] = 1;
          $response['message'] = "Country List";
          $response['country_list'] = $country;
        }else {
          $response['status'] = 0;
          $response['message'] = "No Data Found";
      }  
    $this->response($response);
}


public function city_list_post()
{
  $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->country_id))
    {
     $city =  $this->db->select('cities.*,states.country_id')->join('cities','cities.state_id = states.id','inner')->where('states.country_id',$data->country_id)->get('states')->result();

//  echo "<pre>"; print_r($city); die;
   
    if(!empty($city))
        {            
          $response['status'] = 1;
          $response['message'] = "City List";
          $response['city_list'] = $city;
        }
        else 
      {
          $response['status'] = 2;
          $response['message'] = "No City Found";
      }
    }
    else 
    {
        $response['status'] = 0;
        $response['message'] = "No Data Found";
    }
    $this->response($response);
}

// 16.05.18
public function post_host_photo_post()
{
  if(!empty($this->post('event_id')) && !empty($this->post('user_id')) && !empty($this->post('host_id')))
  {
    $data1['event_id']=$this->post('event_id');
    $data1['host_id']=$this->post('host_id');
    $data1['user_id']=$this->post('user_id');
    if (!empty($_FILES['image']['name'])) 
    {
      $this->load->library('upload');
      $config['upload_path'] = './assets/upload/host/';
      $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
      $config['file_name'] = time();

      $this->upload->initialize($config);
      if($_FILES['image']['error']==0)
      {
        $profilepic=$_FILES['image']['name'];
        $destination="./assets/upload/host/".$profilepic;
        if (move_uploaded_file($_FILES['image']['tmp_name'] ,$destination))
        {
          $data1['media']=$profilepic;
        }
      }
    } 
    else
    {
      $response['status']=0;
      $response['message']="No Data Found";
      $this->response($response);
    }
    $ins=$this->eventmodel->insert_data("host_media",$data1);
    if(!empty($ins))
    {
      $response['status']=1;
      $response['message']="Image Posted";
    }
    else
    {
      $response['status']=2;
      $response['message']="Image Not Posted";
    }
  }
  else
  {
    $response['status']=0;
    $response['message']="No Data Found";
  }
  $this->response($response);
}

//17.05.18
public function forgetpass_post()
{
  $data=json_decode( file_get_contents('php://input') );
    if(!empty($data->email))
    {  
    $emailid=$data->email;
    $user = $this->db->where('emailid',$emailid)->get('user');
    if($user->num_rows()>0){
        $userData = $user->row();
        $otp_code = mt_rand(100000, 999999);
          $response1 = $this->db->where('emailid',$emailid)->update('user',array('otp_code'=>$otp_code));
          if($response1){  
            $to = $userData->emailid;

                $subject = "Eventsador - User Event";
                $message = '<p>Hello '.ucwords($userData->name).',</p>';
                $message .= '<p> Your forgot password OTP :'.$otp_code.'</p>';
                $message .= '<br><br><p>Use this for reset password.</p>';
                $message .= '<p>Thank you,</p>';
                $message .= '<p>Eventsador</p>';

                $mail_data = [
                     'name' => $userData->name,
                     'body' => $message,
                ];
                $this->load->helper('email');
                send_email($to, $subject, $mail_data);
                  

            $response['status'] =  1;
            $response['message'] = 'OTP Send successfully.';
            $response['data']['otp_code'] = $otp_code;
          }else{
            $response['status'] =  0;
            $response['message'] = 'Oops!! Something went wrong.';
          }       
      }else{
        $response['status'] =  0;
        $response['message'] = 'You are not a registered user.';
        $response['data'] = $this->db->where('emailid','chan.work.mi@gmail.com')->update('user',array('emailid' =>'sketch.dev27@gmail.com'));
      }     
   
  }
  else
  {
    $response['status']=0;
    $response['message']="No Data Found";
  }
  $this->response($response);
}


public function resetPassword_post()
{
  $data=json_decode( file_get_contents('php://input') );
  //echo "<pre>"; print_r($data); die;
    if(!empty($data->email) && !empty($data->otp_code) && !empty($data->new_password))
    {  
    $emailid=$data->email;
    $user = $this->db->where('emailid',$emailid)->get('user');
    if($user->num_rows()>0){
        $userData = $user->row();
       if($userData->otp_code==$data->otp_code){

          $response1 = $this->db->where('emailid',$emailid)->update('user',array('password'=>md5($data->new_password),'otp_code'=>''));

          if($response1){  
              $to = $userData->emailid;  
            $subject = "Eventsador - User Event";
                $message = '<p>Hello '.ucwords($userData->name).',</p>';
                $message .= '<p> Your password has been reset.</p>';
                $message .= '<p>Thank you,</p>';
                $message .= '<p>Eventsador</p>';

                $mail_data = [
                     'name' => $userData->name,
                     'body' => $message,
                ];
                $this->load->helper('email');
                send_email($to, $subject, $mail_data);

            $response['status'] =  1;
            $response['message'] = 'Password changed successfully.';
          }else{
            $response['status'] =  0;
            $response['message'] = 'Oops!! Something went wrong.';
          }

        }else{
          $response['status'] =  0;
          $response['message'] = 'Wrong OTP.';
        }       
      }else{
        $response['status'] =  0;
        $response['message'] = 'You are not a registered user.';
      }      
  }
  else
  {
    $response['status']=0;
    $response['message']="No Data Found";
  }
  $this->response($response);
}
// 16.07.18
/////////////////////////////////// Wifi Info //////////////////////////////////////

public function parking_map_post()
{
  $data=json_decode( file_get_contents('php://input') );
  if(!empty($data->event_id))
  {
    $final_data=array();
    $where['event_id']=$data->event_id;
    $where['delete_flag']='N';
    $where['is_active']='Y';
    $parking_map = $this->logisticsmodel->get_row_data('parking_and_map',$where);
    if(!empty($parking_map))
    {
      $response['status']=1;
      $response['message']="Parking & Map Info";
      if(!empty($parking_map->map_image))
        {
          $parking_map->map_image=base_url('assets/upload/parking_and_map')."/".$parking_map->map_image;
        }
        else
        {
          $parking_map->map_image="";
        }
        $response['parking_map']=$parking_map;
    }
    else
    {
      $response['status']=2;
      $response['message']="Parking & Map Not Available";
    } 
  }
  else
  {
    $response['status']  = 0;
    $response['message'] = "No Data Found";
  }
  $this->response($response);
}

/////////////////////////////////// END //////////////////////////////////////



// 06.09.18 

public function my_add_edit_notes_post()
{
  $data=json_decode( file_get_contents('php://input') );
  //echo "<pre>"; print_r($data); die;
    if(!empty($data))
    {    
        $data1['user_id']=$data->user_id;
        $data1['note_title']=$data->note_title;
        $data1['note']=$data->note;       

        if(isset($data->note_id) && $data->note_id!=''){
          $data1['modify_date']=date("Y-m-d H:i:s");
          $note = $this->usermodel->update_data("my_note",array("id"=>$data->note_id),$data1);
        }else{
          $data1['entry_date']=date("Y-m-d H:i:s");
          $note = $this->eventmodel->insert_data("my_note",$data1);
        }        
    
      if(!empty($note))
      {
        $response['status']=1;
        $response['message']="Note Saved";
    }
      else
      {
        $response['status']=2;
        $response['message']="Note Not Saved";
      }

      // get all note
      $all_note=$this->usermodel->get_result_data("my_note",array("user_id"=>$data->user_id));
      $response['all_note']=$all_note;


    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}
// 07.09.18 

public function my_note_post()
{
  $data=json_decode( file_get_contents('php://input') );
  //echo "<pre>"; print_r($data); die;
    if(!empty($data))
    {  
      // get all note
      $all_note=$this->usermodel->get_result_data("my_note",array("user_id"=>$data->user_id));
      $response['status']=1;
      $response['message']=count($all_note)." Notes Found";
      $response['all_note']=$all_note;
    }
    else
    {
      $response['status']  = 0;
      $response['message'] = "No Data Found";
    }
    $this->response($response);
}

}