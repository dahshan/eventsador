<?php

/**
 * No direct access
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Admin Class. Handles all the datatypes and methodes required for Admin section of PicSnippet
 *
 * @author	pYt|-|on <sketch.dev23@gmail.com>
 * @access      public
 * @since	Version 0.0.1
 */
class Admin extends MY_Controller {

    /**
     * The constructor method of this class.
     *
     * @access	public
     * @param	none
     * @return	Loads all the method, helper, library etc. required throughout this class
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Adminsmodel');
        $this->load->model('admin/Adminauthmodel');
        $this->load->model('event/admin/eventmodel');
        $this->load->database();
    }

    /**
     * Index Page for this controller.
     *
     * @access	public
     * @param	none
     * @return	string
     */
    public function index() {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('username', 'Email', 'trim|required');
            $this->form_validation->set_rules('passwd', 'Password', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->Adminauthmodel->admin_login();
//                redirect(base_url('admin'));
            }
        }
        $this->load->view('admin/index');
    }

    /**
     * Used for login of an admin
     *
     * @access	public
     * @param	none
     * @return	string. The success or failure of login.
     */
    public function check_login() {
        $res_save = $this->Adminauthmodel->admin_login();
    }

    /**
     * Used for logout of an admin
     *
     * @access	public
     * @param	none
     * @return	string. The success or failure of logout.
     */
    public function admin_logout() {
        $res_logout = $this->Adminauthmodel->admin_logout();
    }

    /**
     * Used for the Forgot Password Functionality of Admin
     *
     * @access	public
     * @param	none
     * @return	string.
     */
    public function forgot_password() {
        $this->load->view('admin/forgotpassword');
    }

    /**
     * Used for password reset of Admin
     *
     * @access	public
     * @param	none
     * @return	string. The success or failure of password reset.
     */
    public function check_forgot_password() {
        $res_save = $this->Adminauthmodel->admin_forgot_password();
        redirect(base_url('admin'));
    }

    /**
     * Used for dashboard functionality of an admin
     *
     * @access	public
     * @param	none
     * @return	string
     */
    public function dashboard() {
        admin_authenticate();
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['total_appuser'] = count($this->eventmodel->get_result_data('user',array("register_status"=>'1',"role_id"=>'0')));
        $this->data['total_company'] = count($this->eventmodel->get_result_data('company_admin',array("delete_flag"=>'N')));
        $this->data['total_organizer'] = count($this->eventmodel->get_result_data('user',array("role_id"=>2,"delete_flag"=>'N',"is_active"=>'Y')));
        if($this->session->userdata('admin_role_type')==1)
        {
            $this->data['total_event'] = count($this->eventmodel->get_result_data('event',array("delete_flag"=>'N')));
            $this->data['upcoming_event'] = count($this->eventmodel->get_result_data('event',array("is_active"=>'Y',"delete_flag"=>'N',"event_start_date>="=>date("Y-m-d"))));
            $this->data['ongoing_event'] = count($this->eventmodel->get_result_data('event',array("delete_flag"=>'N',"event_start_date<="=>date("Y-m-d"),"event_end_date>="=>date("Y-m-d"))));
        }
        else
        {
            if($this->session->userdata('admin_role_type')==2)
            {
                $this->data['total_event'] = count($this->eventmodel->get_result_data('event',array("delete_flag"=>'N','organizers'=>$this->session->userdata('admin_uid'))));
                $this->data['upcoming_event'] = count($this->eventmodel->get_result_data('event',array("is_active"=>'Y',"delete_flag"=>'N','organizers'=>$this->session->userdata('admin_uid'),"event_start_date>="=>date("Y-m-d"))));
                $this->data['ongoing_event'] = count($this->eventmodel->get_result_data('event',array("delete_flag"=>'N','organizers'=>$this->session->userdata('admin_uid'),"event_start_date<="=>date("Y-m-d"),"event_end_date>="=>date("Y-m-d"))));
            }
            else
            {
                $this->data['total_event'] = count($this->eventmodel->get_result_data('event',array("delete_flag"=>'N','organizers'=>$this->session->userdata('admin_org_id'))));
                $this->data['upcoming_event'] = count($this->eventmodel->get_result_data('event',array("is_active"=>'Y',"delete_flag"=>'N','organizers'=>$this->session->userdata('admin_org_id'),"event_start_date>="=>date("Y-m-d"))));
                $this->data['ongoing_event'] = count($this->eventmodel->get_result_data('event',array("delete_flag"=>'N','organizers'=>$this->session->userdata('admin_org_id'),"event_start_date<="=>date("Y-m-d"),"event_end_date>="=>date("Y-m-d"))));
            }
        }
        $this->middle = 'admin/dashboard';
        $this->admin_layout();
    }

    /**
     * Used for edit functionality of admin details
     *
     * @access	public
     * @param	none
     * @return	string
     */
    public function admin_edit_profile() {
        admin_authenticate();
        $type_id = $this->session->userdata('admin_user_type');
        if ($this->session->userdata('admin_detail') == null) {
            redirect(base_url('user'));
        }

        $this->data = array();
        $this->data['data_single'] = $this->Adminauthmodel->get_row_data('user', ['id' => $this->session->userdata('admin_uid')]);
        //echo "<pre>"; print_r($this->data['data_single']); die;

        if (isset($_POST['submit'])) {
            //$this->form_validation->set_rules('f_name', 'First name', 'trim|required');
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
            if (in_array($type_id, ['2'])) {
                $this->form_validation->set_rules('phoneno', 'Phone no', 'trim|numeric');
            }
            if ($this->form_validation->run() == TRUE) {
                $data = [
                    'name' => $this->input->post('name'),
                    //'l_name' => $this->input->post('l_name'),
                    'emailid' => $this->input->post('email'),
                ];
                if (in_array($type_id, ['2'])) {
                    $data['phoneno'] = $this->input->post('phoneno');
                }
                else
                {
                  $data['phoneno'] = $this->input->post('phoneno');
                }


                // Image Upload
                $img_error = 0;
                $file = $_FILES['img_file'];
                if (!empty($file['name'])) {
                    $imagename = $file['name'];
                    $imagearr = explode('.', $imagename);
                    $ext = end($imagearr);

                    $newimg = "primg_" . time() . "." . $ext;

                    if ($ext == "jpg" or $ext == "jpeg" or $ext == "png") {
                        $config['upload_path'] = "./assets/upload/profimg/";
                        $config['allowed_types'] = 'jpg|png|jpeg';
                        $config['file_name'] = $newimg;

                        $this->load->library('upload', $config);

                        if (!$this->upload->do_upload('img_file')) {
                            $newimg = "";
                        }
                        $img_error = 0;
                    } else {
                        $img_error = 1;
                        $this->session->set_flashdata('errormessage', 'Only images are supported');
                    }
                } 

                if (!empty($newimg)) {
                    $data["profile_image"] = $newimg;
                }


                if ($img_error == 0) {
                    $flg = $this->Adminauthmodel->update_row_data('user', $data, ['id' => $this->session->userdata('admin_uid')]);

                    if (!empty($flg)) {
                        $this->session->set_flashdata('successmessage', 'Profile updated successfully');
                    } else {
                        $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                    }
                }

                redirect(base_url('admin_edit_profile'));
            }
        }

        $this->middle = 'admin/admin_edit_profile';
        $this->admin_layout();
    }

    /**
     * Used for change password functionality of admin details
     *
     * @access	public
     * @param	none
     * @return	string
     */
    public function admin_change_password() {
        admin_authenticate();

        if ($this->session->userdata('admin_detail') == null) {
            redirect(base_url('admin'));
        }

        $this->data = array();
        $this->data['data_single'] = $this->Adminauthmodel->get_row_data('user', ['id' => $this->session->userdata('admin_uid')]);
        //echo "<pre>"; print_r($this->data['data_single']); die;

        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('o_password', 'Old Password', 'trim|required|callback_old_password_check');
            $this->form_validation->set_rules('n_password', 'New Password', 'trim|required');
            $this->form_validation->set_rules('c_password', 'Confirm Password', 'trim|required|matches[n_password]');
            if ($this->form_validation->run() == TRUE) {
                $data = [
                    'password' => md5($this->input->post('n_password')),
                ];
                $flg = $this->Adminauthmodel->update_row_data('user', $data, ['id' => $this->session->userdata('admin_uid')]);

                if (!empty($flg)) {
                    $this->session->set_flashdata('successmessage', 'Password updated successfully');
                } else {
                    $this->session->set_flashdata('errormessage', 'Oops! Something went wrong');
                }

                redirect(base_url('admin_change_password'));
            }
        }
        $this->middle = 'admin/admin_change_password';
        $this->admin_layout();
    }

    /**
     * Used for old password validation check
     *
     * @access	public
     * @param	none
     * @return	boolean
     */
    public function old_password_check($password) {
        if (isset($password) && $password != '') {
            $admin_uid = $this->session->userdata('admin_uid');
            $user_data = $this->Adminauthmodel->get_row_data('user', ['id' => $admin_uid]);
            if (md5($password) != $user_data->password) {
                $this->form_validation->set_message('old_password_check', '{field} does not match!');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }


     /*-- About Us --*/

    public function aboutus() {

        $data = array();
        $where['id']='1';

        //$data['result']=$this->Adminsmodel->content_get($where);


        $result = $this->Adminsmodel->content_get($where);
        $uri = $this->uri->segment(1);
        $this->data = array();
        $this->data['uri'] = $uri;
        $this->data['result'] = $result;
        $this->middle = 'admin/about_us';
        $this->admin_layout();

       /* echo "<pre>";
        print_r($data['result']);
        exit;
        $getsitename = getsitename();
        $this->layouts->set_title($getsitename . '');
        $this->layouts->render('admin/about_us', $data,"admin");*/
    }




    public function formcontent($id=NULL)
    {
        //admin_authenticate();
        //$admin_uid=$this->session->userdata('superadmin_uid');


        $msg="Content Added Successfully";
        if(!empty($id))
        {
          $id=base64_decode(urldecode($id));
          $msg="Content Updated Successfully";
        }

        if($this->input->post())
        {
            $arr['content_description'] = htmlentities($this->input->post('content_description'),ENT_QUOTES,'utf-8');
            $arr['privacy_statement'] = htmlentities($this->input->post('privacy_statement'),ENT_QUOTES,'utf-8');
            $arr['terms_of_use'] = htmlentities($this->input->post('terms_of_use'),ENT_QUOTES,'utf-8');
            if(!empty($_FILES['content_image']['name']))
            {
                $content_image   = $_FILES['content_image'];

                $content_image = fileUpload1($content_image,'./assets/uploads/aboutus/','gif|jpg|jpeg|png|bmp');

                $arr['content_image']  = $content_image;
               
            }

            $arr['created_date ']      = date('Y-m-d H:i:s');

            $returnval = $this->Adminsmodel->changecontent($arr,$id);
            if (!empty($returnval)) 
            {
                $this->session->set_flashdata('successmessage', $msg);
            }
            else 
            {
                $this->session->set_flashdata('errormessage', "Oops! Something went wrong.Please try again later.");

            }
            $redirect = site_url('admin_aboutus');
            redirect($redirect);
        }
        else
        {   
                $data = array();
                $where['id']='1';

                //$data['result']=$this->Adminsmodel->content_get($where);


                $result = $this->Adminsmodel->content_get($where);
                $uri = $this->uri->segment(1);
                $this->data = array();
                $this->data['uri'] = $uri;
                $this->data['result'] = $result;
                $this->middle = 'admin/about_us';
                $this->admin_layout();
        }
    }

    /* public function change_status($id, $status) {
      admin_authenticate();
      $this->load->model('admin/TemplateModel');

      $this->TemplateModel->changeStatus($id, $status);
      redirect(base_url('cms_list'));
      }

      public function users() {
      admin_authenticate();
      $this->load->model('admin/TemplateModel');

      $this->data = array();
      $this->data['users'] = $this->TemplateModel->getAllUsers();
      $this->middle = 'admin/user_list';
      $this->admin_layout();
      } */
}

/* End of file front.php */
    /* Location: ./application/controllers/front.php */
    