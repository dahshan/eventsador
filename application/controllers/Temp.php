<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'libraries/REST_Controller.php');
require_once(APPPATH.'libraries/phpqrcode/qrlib.php');
require 'vendor/autoload.php';
class Temp extends CI_Controller {

    function __construct() {
      // Construct the parent class
      parent::__construct();
      $this->load->model("user/admin/usermodel");
      $this->load->model("event/admin/eventmodel");
      $this->load->model("aboutus/admin/aboutusmodel");
      $this->load->model("exhibitor/admin/exhibitormodel");
      $this->load->model("logistics/admin/logisticsmodel");
      $this->load->model("bulletin/admin/bulletinmodel");
      $this->load->model("host/admin/hostmodel");
      $this->load->model("accomodation/admin/accomodationmodel");
      $this->load->model("transportation/admin/transportationmodel");
      $this->load->model("wifi_access/admin/wifiaccessmodel");
      $this->load->model("floor_map/admin/floormapmodel");
      $this->load->model("local_attraction/admin/localattractionmodel");
      $this->load->model("lead/admin/leadmodel");
      $this->load->model("attendees/admin/attendeesmodel");
      $this->load->model("agenda/admin/agendamodel");
      $this->load->model("notification/admin/notificationmodel");
      $this->load->model("faq/admin/faqmodel");
      $this->load->model("point/admin/pointmodel");
      $this->load->helper('email');
    }

    function fix_company_issue_post(){
        $users = $this->db->select('id, company, position_title')->where('company !=', '')->where('position_title!=', '')->get('user')->result();
        foreach ($users as $user){
            $where = ['user_id'=> $user->id, 'company'=> $user->company, 'position'=> $user->position_title];
            $user->has_company = $has_company = $this->db->where($where)->get('company')->num_rows();
            if ($has_company) continue;
            
            $user->company_data = $company_data = ['user_id'=> $user->id, 'company'=> $user->company, 'position'=> $user->position_title, 'current_job'=> 1, 'start_date'=> date('Y-m-d'), 
                    'end_date'=> '0000-00-00', 'is_active'=> 1, 'delete_flag'=> 'N', 'created_date'=> date('Y-m-d H:i:s')];
            $this->db->insert('company', $user->company_data);
            $user->inserted = $this->db->insert_id();
        }
        $this->response($users);
    }
    
    public function test_ffmpeg(){
        // echo phpversion();
		$this->load->helper('url');
		$sec = 5;
		$movie = base_url().'assets/upload/media_files/media_15180778760.mp4';
		$thumbnail = 'assets/thumbnail.png';

		$ffmpeg = FFMpeg\FFMpeg::create();
		$video = $ffmpeg->open($movie);
		$frame = $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds($sec));
		$frame->save($thumbnail);
		echo '<img src="'.base_url().$thumbnail.'">';exit;
		$this->load->view('welcome_message');
    }
    
    public function generate_poster($url){
        $this->load->library('poster');
        $origin = $this->poster->get_poster(['url'=>$url]);
        $thumbnail = $this->resize_image(['origin'=> $origin, 'width'=> 200]); 
        return $thumbnail;
    }
    
    /**
     * origin: full path, width: (optinal)
     */
    public function resize_image($data){
        $origin = $data['origin'];
        $origin_arr = explode('/', $origin);
        $name_arr = explode('.', $origin_arr[count($origin_arr) - 1]);
        $new_name = $name_arr[0].'.png';
        
        $config['image_library'] = 'gd2';
        $config['source_image'] = $origin; echo $origin.file_exists($origin).'<br>';
        $config['new_image'] = 'assets/upload/media_poster/thumbnail/'.$new_name;
        // $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']         = 200;
        // $config['height']       = 50;
        
        // $this->load->library('image_lib', $config);
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        return $new_name;
    }
    
    
    public function fix_all_poster(){
        $query = $this->db->where('type', 'V')->get('media_files');
        if ($query){
            $rows = $query->result_array();
            foreach ($rows as $row){
                $poster = $this->generate_poster($row['file_name']);
                $this->db->where('id', $row['id']);
                $this->db->update('media_files', ['poster'=> $poster]);
            }
        }
    }
}